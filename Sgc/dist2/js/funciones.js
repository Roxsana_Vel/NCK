/**
 * cargar cualquier select en base a parametros
 *
 * @param  {cadena} selector_destino   el id o clase del select (#miselect ó
 *                                     .miselect)
 * @param  {array} parametros          parametros que se enviaran por post al
 *                                     controlador
 * @param  {cadena} controlador_cargar controlador que recibe los parametros
 *                                     y devuelve un json_encode para rende-
 *                                     rizarlo en el select destino
 *
 * @return {[type]}                    [description]
 */
function cargar_select(selector_destino, parametros, controlador_cargar) {
    $.ajax({
        async: false,
        cache: false,
        url: controlador_cargar,
        type: 'POST',
        dataType: 'json',
        data: parametros,
        beforeSend: function() {
            console.log('cargando select...');
        },
        success: function(respuesta) {
            $(selector_destino).html(respuesta);
        }
    });
}

/**
 * cargar una seccion enviandole parametros por post
 *
 * @param  {objeto} data tiene la ruta, los parametros y el selector destino
 * var data = {
        url              : '<?= base_url('modulo_credito_controller/cargar_clientes') ?>',
        selector_destino : '#clientes_lineas_credito',
        parametros       : {
            zona_id         : $('#zona').val(),
            empleado_usu_id : $('#empleado').val(),
        }
    };

 * @return {[type]}      [description]
 */
function cargar_seccion(data) {
    $.ajax({
        async: true,
        cache: false,
        url: data.url,
        type: 'POST',
        dataType: 'html',
        data: data.parametros,
        beforeSend: function() {
            // $(data.selector_destino).html('<div class="text-center"><img src="../img/loader_gray.gif" alt=Cargando></div>');
        },
        success: function(respuesta) {
            $(data.selector_destino).html(respuesta);
        }
    });
}