function changeFile(me, ancho_definido, alto_definido) {
    var _URL = window.URL || window.webkitURL;
    var file, img;

    if ((file = me.files[0])) {
        img = new Image();
        img.onload = function () {
            var ancho = this.width;
            var alto = this.height;

            if (ancho <= ancho_definido && alto <= alto_definido) {
                cargarImagen(me);
            } else {
                swal({
                    title: 'Confirmar',
                    text: 'La imagen no coincide con las dimensiones recomendadas. La imagen se redimensionará. ¿Continuar?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, redimensionar la imagen!',
                    cancelButtonText: 'No'
                }).then(function () {
                    cargarImagen(me);
                });
            }
        };
        img.src = _URL.createObjectURL(file);
    }
}

function cargarImagen(me) {
    var files = me.files;
    var $scope = angular.element("[ng-controller]").scope();

    for (var i = 0; i < files.length; i++) {
        var url = window.URL.createObjectURL(files[i]);

        $scope.$apply(function () {
            $scope.setItem(url);
        });
    }
}

$('.gallery-add').click(function (e) {
    $(this).prev().trigger('click');
});