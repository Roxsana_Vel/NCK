$(document).ready(function() {
	$("#time").countdown({
		date: "january 1, 2012", 
		onChange: function( event, timer ){
		},
		onComplete: function( event ){
		
			$(this).html("Completed");
		},
		leadingZero: true,
		direction: "up"
	});
	
	$("#time2").countdown({
		date: "march 10, 2013",

		offset: 1,
		onChange: function( event, timer ){
		
		},
		onComplete: function( event ){
		
			$(this).html("Modo de Prueba");
		},
		onPause: function( event, timer ){

			$(this).html("Pause");
		},
		onResume: function( event ){
		
			$(this).html("Resumed");
		},
		leadingZero: true
	});
});