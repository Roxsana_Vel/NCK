function objetoAjax() {
    var xmlhttp = false;

    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }

    return xmlhttp;
}

function cargar(pagina, division, refrescar) {
    divContenido = document.getElementById(division);

    ajax = objetoAjax();
    ajax.open("GET", pagina);
    divContenido.innerHTML = '<img src="resources/resources_sgc/img/loader.gif">';

    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            if (refrescar === true) {
                window.location.reload();
            } else {
                divContenido.innerHTML = ajax.responseText;

                $(function () {
                    $('.dataTable').dataTable({
                        "sPaginationType": "full_numbers",
                        "iDisplayLength": 25,
                        "bInfo": false,
                        "bJQueryUI": true,
                        "oLanguage": {
                            "sLengthMenu": "_MENU_",
                            "sSearch": "",
                            "sInfo": "Mostrando _START_ de _END_ de _TOTAL_ registros",
                            "sZeroRecords": "No hay ningún registro",
                            "oPaginate": {
                                "sFirst":    "«",
                                "sPrevious": "←",
                                "sNext":     "→",
                                "sLast":     "»"
                            }
                        }
                    });

                    $('.btn-danger').click(function () {
                        var eliminarId = $(this).attr("id");
                        var arreglo = eliminarId.split("/"); // segmentamos la cadena en un arreglo donde cada posicion se divide tomando como punto de division el "/" (barra)

                        nwElementoNombreCat = arreglo[1].trim(); //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo
                        nwElementoIdCat = arreglo[0].trim();     //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo

                        $.confirm({
                            'title'     : 'Confirmar Eliminación',
                            'message'   : 'Está usted seguro de eliminar la categoría "'+nwElementoNombreCat+'"<br /><b>Eliminará los productos y la imagen asociada.</b><br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                            'buttons'   : {
                                'Si'    : {
                                    'class' : 'blue',
                                    'action': function () {
                                        $.ajax({
                                            type: "POST",
                                            url: 'categorys/delete_category_id',
                                            data: 'eliminarId=' + nwElementoIdCat,
                                            beforeSend: function () {
                                                $('#DivLoader').html('<div id="loader" class="loader"><img src="resources/resources_sgc/img/loader.gif" alt="Loader"></div>');
                                            },
                                            success: function (resp) {
                                                $('#DivLoader').html(resp).show();
                                            }
                                        });
                                    }
                                },
                                'No'    : {
                                    'class' : 'gray',
                                    'action': function () {}
                                }
                            }
                        });
                    });
                });
            }
        }
    }

    ajax.send(null);
}

function cargar_producto(pagina, division) {
    divContenido = document.getElementById(division);

    ajax = objetoAjax();
    ajax.open("GET", pagina);
    divContenido.innerHTML = '<img src="resources/resources_sgc/img/loader.gif">';

    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            divContenido.innerHTML = ajax.responseText;

            $(function () {
                $('.dataTable').dataTable({
                    "sPaginationType": "full_numbers",
                    "iDisplayLength": 25,
                    "bInfo": false,
                    "bJQueryUI": true,
                    "oLanguage": {
                        "sLengthMenu": "_MENU_",
                        "sSearch": "",
                        "sInfo": "Mostrando _START_ de _END_ de _TOTAL_ registros",
                        "sZeroRecords": "No hay ningún registro",
                        "oPaginate": {
                            "sFirst":    "«",
                            "sPrevious": "←",
                            "sNext":     "→",
                            "sLast":     "»"
                        }
                    }
                });

                $('.btn-danger').click(function () {
                    var eliminarId = $(this).attr("id");

                    $.confirm({
                        'title'     : 'Confirmar Eliminación',
                        'message'   : 'Está usted seguro de eliminar este producto <br /><b>Eliminará también la imagen asociada.</b><br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                        'buttons'   : {
                            'Sí'    : {
                                'class' : 'blue',
                                'action': function () {
                                    $.ajax({
                                        type: "POST",
                                        url: 'products/delete_products_id',
                                        data: 'eliminarId=' + eliminarId,
                                        beforeSend: function () {
                                            $('#DivLoader').html('<div id="loader" class="loader"><img src="resources/resources_sgc/img/loader.gif" alt="Loader"></div>');
                                        },
                                        success: function (resp) {
                                            $('#DivLoader').html(resp).show();
                                        }
                                    });
                                }
                            },
                            'No'    : {
                                'class' : 'gray',
                                'action': function () {}
                            }
                        }
                    });
                });
            });
        }
    }

    ajax.send(null);
}

function cargar_estado(pagina, division) {
    divContenido = document.getElementById(division);

    ajax = objetoAjax();
    ajax.open("GET", pagina);
    divContenido.innerHTML = '<img src="resources/resources_sgc/img/ajax-loader-estado.gif">';

    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            swal({
                title: 'Enhorabuena!',
                text: 'Se ha actualizado el estado satisfactoriamente.',
                type: 'success',
                confirmButtonText: 'Continuar'
            });

            divContenido.innerHTML = ajax.responseText;
        }
    }

    ajax.send(null);
}

function imagenval(me) {
    var arch = me.files[0];
    var val = arch.name.toLowerCase();

    if (!val.match(/(gif|jpg|png|jpeg)$/)) {
        $.confirm({
            'title'     : 'Informacíon',
            'message'   : 'La imagen no tiene una extensión válida. <b>Los formatos permitidos son los siguientes: jpg, png, gif</b>.',
            'buttons'   : {
                'Ok'   : {
                    'class' : 'blue',
                    'action': function () {}
                }
            }
        });

        $(me).attr('value', '');
        $('#preview_container').addClass('hidden');
    } else {
        var img = document.querySelector('#preview');

        img.onload = function (e) {
            $('#preview_container').removeClass('hidden');
            window.URL.revokeObjectURL(this.src);
        };

        img.src = window.URL.createObjectURL(arch);
    }
}

function imagenval_banner(doc) {
    var arch = doc.files[0];
    var val = arch.name.toLowerCase();

    if (!val.match(/(gif|jpg|png|jpeg)$/)) {
        $.confirm({
            'title'     : 'Informacíon',
            'message'   : 'La imagen no tiene una extensión válida. <b>Los formatos permitidos son los siguientes: jpg, png, gif</b>.',
            'buttons'   : {
                'Ok'   : {
                    'class' : 'blue',
                    'action': function () {}
                }
            }
        });

        $(doc).attr('value', '');
        $('#preview_container2').addClass('hidden');
    } else {
        var img = document.querySelector('#preview2');

        img.onload = function (e) {
            $('#preview_container2').removeClass('hidden');
            window.URL.revokeObjectURL(this.src);
        };

        img.src = window.URL.createObjectURL(arch);
    }
}