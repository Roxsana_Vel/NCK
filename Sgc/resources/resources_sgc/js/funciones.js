/* 
* Author: Diego Iparraguirre Loza
* Twitter: @dilozaDev
* Website: www.diloza.com
* 
* 1.- Obtener
* 2.- Modificar
* 3.- Validar
* 4.- Condicionar
*/


/* ===========
** 1.- Obtener
** =========== */

//funcion generica para obtener contenido
function getValue(id){
	return $(id).val();
}


/* =============
** 2.- Modificar
** ============= */

//funcion generica para resetear contenido
function setReset(id){
	$(id).val('');
}

//funcion generica para deshabilitar contenedor
function setDisable(id, action){
	$(id).prop('disabled', action);
}


/* ===========
** 3.- Validar
** =========== */

//funcion generica para validar vacios
function isEmpty(id){
	if(getValue(id).length==0){ //depende de la funcion programada getValue()
		//Si esta vacio
		$(id).focus();
		return true;
	} else{
		//No esta vacio
		return false;
	}
}

//funcion generica para validar si es un tipo de email
function isEmail(id){
	var expReg = /[\w-\.]{3,}@([\w-]{3,}\.)*([\w-]{3,}\.)[\w-]{2,4}/;
	if(expReg.test($(id).val())){
		//Si es un email
		return true;
	} else{
		//No es un email
		$(id).focus();
		return false;
	}
}

//funcion generica para validar tamaño
function isLength(id, tam){
	if(getValue(id).length==tam){ //depende de la funcion programada getValue()
		//Si cumple con el tamaño
		return true;
	} else{
		//No cumple con el tamaño
		$(id).focus();
		return false;
	}
}

//funcion generica para comparar valores
function isEqual(valX, valY){
	if(valX==valY){
		//Si es igual
		return true;
	} else{
		//No es igual
		return false;
	}
}

//funcion generica para saber si esta marcado  
function isChecked(id){
	var estado = $(id).is(":checked");
	if(estado==true){
		return '1';
	} else{
		return '0';
	}
}

/* ===============
** 4.- Condicionar
** =============== */

//funcion generica para validar caracteres permitidos
function accessOnly(id, eventType, expReg){
	$(id).on(eventType, function(e){
		var key = (document.all) ? e.keyCode : e.which;
		//8 -> backspace
		//9 -> tab
		//13 -> enter
		//32 -> space
		//46 -> delete
		if (key==8) return true;
		if (key==9) return true;
		if (key==13) return true;
		if (key==32) return true;
		if (key==46) return true;
		KeyConv = String.fromCharCode(key);
		return expReg.test(KeyConv);
	});
}

//funcion generica para validar el minimo de caracteres permitidos
function minLength(id, min){
	if(getValue(id).length>=min){ //depende de la funcion programada getValue()
		//Si cumple con el tamaño
		return true;
	} else{
		//No cumple con el tamaño
		$(id).focus();
		return false;
	}
}

//funcion generica para comparar dos campos y deshabilitar(o no) un control(input, boton, etc.)
function compare2disable(valX, valY, cmp){
	if(valX!=valY){
		//Entonces se activa el control
		setDisable(cmp, false); //depende de la funcion programada setDisable()
		return false;
	} else{
		//De lo contrario seguirá desactivado
		setDisable(cmp, true);
		return true;
	}
}