$(document).ready(function(){
	
	$('.btn-danger').click(function(){
		
		/*var elem = $(this).closest('.btn-danger');*/
		var eliminarId = $(this).first().attr("id");
		$.confirm({
			'title'		: 'Confirmar Eliminación',
			'message'	: 'Está usted seguro de eliminar este producto. <br />Eliminará la imagen asociada.<br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
			'buttons'	: {
				'Sí'	: {
					'class'	: 'blue',
					'action': function(){
						
						$.ajax({
						  type: "POST",
						  url: "test.js",
						  data: 'eliminarId=' + eliminarId,
						});
											}
				},
				'No'	: {
					'class'	: 'gray',
					'action': function(){

						
					}	// Nothing to do in this case. You can as well omit the action property.
				}
			}
		});
		
	});
	
});