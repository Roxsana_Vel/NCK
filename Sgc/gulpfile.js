var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    autoprefixer  = require('gulp-autoprefixer'),
    concat        = require('gulp-concat'),
    uglify        = require('gulp-uglify'),
    plumber       = require('gulp-plumber'),
    tinypng       = require('gulp-tinypng-compress'),
    watch         = require('gulp-watch'),
    livereload    = require('gulp-livereload');

var SRCscss       = 'src/scss/*.scss',
    SRCjsLibs     = 'src/js/libs/',
    SRCjsScripts  = 'src/js/scripts/',
    SRCimg        = 'src/img/**/*.{png,jpg,jpeg}';

var   tinyLogFile = 'src/img/.tinypng-log';

var DISTcss = 'dist/css/',
    DISTjs  = 'dist/js/',
    DISTimg = 'dist/img/';

var jsLibs = [
    SRCjsLibs + 'jquery/jquery_1.12.4.js',
    SRCjsLibs + 'bootstrap/transition.js',
    SRCjsLibs + 'bootstrap/alert.js',
    SRCjsLibs + 'bootstrap/button.js',
    SRCjsLibs + 'bootstrap/carousel.js',
    SRCjsLibs + 'bootstrap/collapse.js',
    SRCjsLibs + 'bootstrap/dropdown.js',
    SRCjsLibs + 'bootstrap/modal.js',
    SRCjsLibs + 'bootstrap/tab.js',
    SRCjsLibs + 'bootstrap/affix.js',
    SRCjsLibs + 'bootstrap/scrollspy.js',
    SRCjsLibs + 'bootstrap/tooltip.js',
    SRCjsLibs + 'bootstrap/popover.js',
    SRCjsLibs + 'owl_carousel/owl.carousel.js',
    SRCjsLibs + 'wow/wow.js',
];

var jsScripts = [
    SRCjsScripts + 'global.js',
    SRCjsScripts + 'inicio.js',
];

gulp.task('scss2css', function() {
    gulp.src(SRCscss)
    .pipe(plumber())
    .pipe(sass({outputStyle: 'nested'}).on('error', sass.logError))
    .pipe(autoprefixer("> 0%"))
    .pipe(gulp.dest(DISTcss))
    .pipe(livereload());
});

gulp.task('jsLibs', function() {
    gulp.src(jsLibs)
    .pipe(plumber())
    .pipe(concat({ path: 'libsgulp.js', stat: { mode: 0666 }}))
    .pipe(uglify())
    .pipe(gulp.dest(DISTjs))
    .pipe(livereload());
});

gulp.task('jsScripts', function() {
    gulp.src(jsScripts)
    .pipe(plumber())
    .pipe(concat({ path: 'scriptsgulp.js', stat: { mode: 0666 }}))
    .pipe(uglify())
    .pipe(gulp.dest(DISTjs))
    .pipe(livereload());
});

gulp.task('tinypng', function () {
    gulp.src(SRCimg)
    .pipe(tinypng({
        key: 'K32TCxl6bIoGRqDrT7-5WMccS4wSkVjI',
        sigFile: tinyLogFile,
        log: true
        }))
    .pipe(gulp.dest(DISTimg));
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('src/scss/**/*.scss', ['scss2css']);
    gulp.watch('src/js/**/*.js', ['jsLibs']);
    gulp.watch('src/js/**/*.js', ['jsScripts']);
});

/* Tareas por defecto */
gulp.task('default', ['scss2css', 'jsLibs', 'jsScripts', 'watch']);
