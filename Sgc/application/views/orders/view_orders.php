<div class="row-fluid">
    <div class="span12">
        <ul class="breadcrumb">
            <li ><a href="<?php echo site_url("welcome");?>" >Panel de Control</a>
            <span class="divider">/</span>
            </li>          
            <li><a href="<?php echo site_url("orders");?>">Pedidos</a><span class="divider">/</span></li>
            <li><span>Detalle</span></li>
        </ul><!-- breadcrumbs -->                    
    </div>
</div>
    <!-- Breadcrumb::end() -->
    <!-- Modules::start() -->
    <div class="row-fluid">
        <div class="span12 panel">
            <!-- Accion -->
            <?php $row_orders = $orders->row(); ?>
            <div class="panel-header">                
                <h4>Visualizar Detalle de Pedido - Nro Pedido: <span class="label label-inverse"><?php echo str_pad($row_orders->PED_ID, 11, "0", STR_PAD_LEFT); ?></span></h4>
            </div>
            <!-- Formulario -->
            
            <div class="row-fluid" style="">
                <div class="span4">
                   
                    <label for="">Cliente</label>
                    <input type="text" id="nombre_cli" readonly value="<?php echo $row_orders->PER_nombres.', '.$row_orders->PER_apellidos; ?>">
                    <br>
                    <!-- Button to trigger modal -->
                    <a href="#myModal" role="button" class="btn btn-info" data-toggle="modal">Detalle del cliente</a>
                    
                    <!-- Modal -->
                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Detalle del cliente</h3>
                      </div>
                      <div class="modal-body">
                        
                        <!-- INFORMACION -->
                        <!--<div class="span1">
                          <div class="panel">
                              
                              <div class="panel-body">
                                  <div class="row-fluid">-->
                                      <div class="row-fluid">
                                          <strong><?php echo $row_orders->PER_nombres.', '.$row_orders->PER_apellidos; ?></strong><br>
                                          <table class="table table-condensed table-responsive table-user-information">
                                              <tbody>
                                              <tr>
                                                  <td>Dirección:</td>
                                                  <td><?php echo $row_orders->PER_direccion; ?></td> 
                                              </tr>
                                              <tr>
                                                  <td>Teléfono:</td>
                                                  <td><?php if (($row_orders->PER_telefono)!="") { echo $row_orders->PER_telefono; } else { ?>Sin Teléfono registrado<?php } ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Celular:</td>
                                                  <td><?php if (($row_orders->PER_celular)!="") { echo $row_orders->PER_celular; } else { ?>Sin Celular registrado<?php } ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Departamento, Provincia y Distrito:</td>
                                                  <td><?php echo $row_orders->PER_departamento." - ".$row_orders->PER_provincia." - ".$row_orders->PER_distrito; ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Fecha de Nacimiento:</td>
                                                  <td><strong><?php echo $row_orders->PER_fechaNacimiento; ?></strong></td>
                                              </tr>
                                              <tr>
                                                  <td>Documento:</td>
                                                  <td><strong><?php echo $row_orders->PER_documento; ?></strong></td>
                                              </tr>
                                              <tr>
                                                  <td>Email:</td>
                                                  <td><?php echo $row_orders->PER_email; ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Sexo:</td>
                                                  <td><?php
                                                  switch ($row_orders->PER_sexo) {
                                                    case 'M':
                                                      echo "Masculino";
                                                      break;
                                                    
                                                    case 'F':
                                                      echo "Femenino";
                                                      break;
                                                  }
                                     
                                                        ?>
                                                  </td>
                                              </tr>
                                              </tbody>
                                          </table>
                                      <!--</div>
                                  </div>
                              </div>
                              
                          </div>-->
                      </div>
                      <!-- FIN INFORMACION -->
                      </div>
                      <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                      </div>
                    </div>
                    <span class="help-block">&nbsp;</span>
                    <label for="">RUC: </label>
                    <input type="text" id="dni" readonly value="<?php echo $row_orders->PER_documento; ?>">
                    <span class="help-block">&nbsp;</span>
                    <label for="">Teléfono: </label>
                    <input type="text" id="nombre_razonsocial" readonly value="<?php echo $row_orders->PER_telefono; ?>">
                    <span class="help-block">&nbsp;</span>
                    <label for="">Método de Pago Elegido: </label>
                    <input type="text" id="metodo_pago" readonly value="<?php echo $row_orders->PED_metodoPago; ?>">
                    <span class="help-block">&nbsp;</span>
                    <?php if (isset($row_orders->DIRP_direccion)){ ?>
                    <label for="">Direcci&oacute;n de Envío</label>                 
                   <textarea type="text" id="direccion" style="height:80px" readonly><?php echo $row_orders->DIRP_direccion.", ";?><?php echo $row_orders->DPTO_nombre.", ".$row_orders->PROV_nombre.", ".$row_orders->DIST_nombre; ?></textarea>
                    <span class="help-block">&nbsp;</span>
                <?php } ?>
                </div>
                <div class="span4">
                    <label for="">Moneda</label>
                    <input type="text" id="moneda" readonly value="<?if ($row_orders->PED_moneda == 'S') { echo 'SOL'; } else { echo 'DOLAR'; } ?>">
                    <span class="help-block">&nbsp;</span>                    
                 
                    <label for="">Total</label>
                    <input type="text" id="total" readonly value="<?php if($row_orders->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?><?php echo $row_orders->PED_costoTotal; ?>">
                    <span class="help-block">&nbsp;</span>
                    
                        <div id="DivLoader" <?php if ($row_orders->PED_estado == 1) { echo 'class="DivEstadoPedido1"'; } elseif ($row_orders->PED_estado == 2) { echo 'class="DivEstadoPedido2"'; }elseif ($row_orders->PED_estado == 3) { echo 'class="DivEstadoPedido3"'; } ?>>
                       
                        <div class="input-prepend" style="width:205px">
                            <label for="">Estado</label>
                                 <span class="add-on" style="background-color:#000"><i <?php if ($row_orders->PED_estado == 1) { echo 'class="icon-warning-sign icon-white"'; } elseif ($row_orders->PED_estado == 2) { echo 'class="icon-ok icon-white"'; }elseif ($row_orders->PED_estado == 3) { echo 'class="icon-remove icon-white"'; } ?>></i></span>
                            <input name="usuario" readonly class="input-xlarge-fluid" type="text" id="estado" value="<?php if ($row_orders->PED_estado == 1) { echo 'PENDIENTE'; } elseif ($row_orders->PED_estado == 2) { echo 'PAGADO'; }elseif ($row_orders->PED_estado == 3) { echo 'ANULADO'; } ?>" />
                        </div>
                   
                    <?php if($exist_buy == FALSE && $row_orders->PED_estado!=3  && $row_orders->PED_estado!=2)  : ?>
                    <span class="help-block">&nbsp;</span> 
                     <label for="">¿Cambiar Estado?</label>
                     <input type="radio" name="radio_btn" id="pagado" value="2" <?php if($row_orders->PED_estado == 2) { echo "checked"; } ?>>  Pagado<br>
                      <input type="radio" name="radio_btn" id="anulado" value="3" <?php if($row_orders->PED_estado == 3) { echo "checked"; } ?>>  Anulado<br>                
                    <span class="help-block">&nbsp;</span>      
                    <?php endif;?>
                     </div>
                     <span class="help-block">&nbsp;</span>
                    <div class="control-group">
              <label class="control-label">Tipo de Facturación</label> 
              <div class="controls">
              
              <input readonly="readonly" disabled type="radio" name="TipoFac" value="personal"<?php if($row_orders->PER_tipFac=="personal"){ echo "checked='checked'"; } ?> onchange="showHideInvoiceDetails(this.value)" class="radio"><span>&nbsp;&nbsp;Persona&nbsp;&nbsp;</span> 
              <input readonly="readonly"  disabled type="radio" name="TipoFac" value="empresa" <?php if($row_orders->PER_tipFac=="empresa"){ echo "checked='checked'"; } ?> onchange="showHideInvoiceDetails(this.value)" class="radio"><span>&nbsp;Empresa</span>  
            </div>
          </div>
     
          <?php if($empresa!=false) : ?>
          <?php $row_empresa = $empresa->row(); ?>
          <div  class="control-group enterprise_invoice_tr" <?php if($row_orders->PER_tipFac=="personal"){ echo 'style="display: none;"'; } ?>>
          <label class="control-label">Nombre de la Empresa:</label> 
            <div class="controls">
                <input readonly="readonly" type="text" name="NombreEmpresa" id="NombreEmpresa" value="<?php if($row_empresa->EMP_ID!=NULL){ echo utf8_encode($row_empresa->EMP_nombre); }?>" placeholder="Ingrese nombre de la empresa" class="input-xlarge"> 
            </div>
          </div>
   
          <div  class="control-group enterprise_invoice_tr" <?php if($row_orders->PER_tipFac=="personal"){ echo 'style="display: none;"'; } ?>>
          <label class="control-label">RUC de la Empresa:</label> 
            <div class="controls">
              <input readonly="readonly" type="text" name="rutEmpresa" name="rutEmpresa" value="<?php if($row_empresa->EMP_ID!=NULL){ echo $row_empresa->EMP_ruc; }?>" maxlength="12" onkeyup="this.value = formatRut(this.value)" placeholder="Rol Único Tributario" class="input-xlarge">
            </div>
          </div>
      <div  class="control-group enterprise_invoice_tr" <?php if($row_orders->PER_tipFac=="personal"){ echo 'style="display: none;"'; } ?>>
      
    
        
        <div class="control-group">
          <label for="Giro">Giro</label>
          <div class="controls">           
            <textarea readonly class="span9"><?php echo $row_empresa->GIR_nombre; ?></textarea>
          </div>
        </div>
       <?/*<div class="control-group">
          <label for="Giro">Giro</label>
          <div class="controls">
              <select readonly="readonly" disabled name="giro_emp" id="giro_emp" class="span8" required='required'>
                  <option value=''>--Seleccione--</option>
                  <?php foreach ($giro->result() as $row_giro) : ?>
                      <option value="<?php echo $row_giro->GIR_ID;?>" <?php if($row_giro->GIR_ID==$row_empresa->GIR_ID): echo "selected='selected'"; endif;?>> <?php echo $row_giro->GIR_nombre;?></option> 
                  <?php endforeach; ?>
              </select>       
          </div>   
      </div> */ ?>
       
     </div>
                <?php endif;?>
                </div>
                <div class="span4">
                    <label for="">Fecha de Pedido: </label>
                      <?php
                      $fecha = $row_orders->PED_fecha;
                      $tDate = strtotime($fecha);
                      $var_fecha = date('d-m-Y H:i:s',$tDate);
                      ?>
                    <input type="text" id="fecha" readonly value="<?php echo $var_fecha; ?>">
                    <span class="help-block">&nbsp;</span>
                    
                    <label for="numoper">Número de Operación: </label>
                    <input type="text" id="numoper" readonly value="<?php echo $row_orders->PED_numOperacion; ?>"/>
                    <span class="help-block">&nbsp;</span>
                    
                    <label for="fecha_deposito"> Fecha de Depósito: </label>
                    <input type="text" id="fecha_deposito" readonly value="<?php echo $row_orders->PED_fechaDeposito; ?>"/>
                    <span class="help-block">&nbsp;</span>

                    
                    <label for="voucher">Voucher de Depósito: </label>
                    <?php if ($row_orders->PED_voucher!="") { ?>
                    <a href="#modalVoucher" data-toggle="modal"><img src="../../../vouchers/<?php echo $row_orders->PED_voucher; ?>" alt="" width="25%"></a>
                      <div id="modalVoucher" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalVoucherLabel" aria-hidden="true">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          <h3 id="modalVoucherLabel">Voucher de Depósito</h3>
                        </div>
                        <div class="modal-body">
                          <img src="../../../vouchers/<?php echo $row_orders->PED_voucher; ?>">
                        </div>
                      </div>
                    <?php } else { ?><input type="text" id="voucher" readonly value="No subido."/><?php } ?>
                    <span class="help-block">&nbsp;</span>

                    <label for="">Usuario</label>
                    <input type="text" id="usuario" readonly value="<?php echo $row_orders->USU_login; ?>">
                    <span class="help-block">&nbsp;</span>
                    <?php if(isset($row_orders->PED_observacion) && $row_orders->PED_observacion!=''): ?>
                    
                    <label for="">Nota: </label>
                    <textarea name="observacion" readonly cols="20" rows="6" id="observacion" readonyly="readonly"><?php echo $row_orders->PED_observacion; ?></textarea>
                    <span class="help-block">&nbsp;</span>
                    <?php endif;?>
                    
                    <label for="delivery"><?php if($row_orders->PED_delivery==1){echo 'Sí solicitó entrega a domicilio';}elseif($row_orders->PED_delivery==0){echo 'No solicitó entrega a domicilio';} ?></label>
                    <span class="help-block"></span>
                    <img src="<?=base_url()?>resources/resources_sgc/img/<?php if($row_orders->PED_delivery==1){echo 'delivery_true.png';}elseif($row_orders->PED_delivery==0){echo 'delivery_false.png';} ?>" border="0" />
                </div>
            </div>
        </div>
            <div class="row-fluid">
                <div class="span12">
                    <h4>Descripci&oacute;n de pedido</h4>
                    <table width="800" class="table table-striped table-bordered table-condensed table-hover">
                        <thead>
                            <tr class="titulos">
                                <th>Cantidad</th>
                                <th>Descripci&oacute;n</th>
                                <th>Precio Unit.</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                       <?php if ($details != FALSE) : ?>                  
                        <?php foreach ($details->result() as $row) : ?>
                            <tr class="titulos">
                              <td><?php echo $row->DPED_cantidad; ?></td>
                              <td><?php echo $row->PRO_nombre; ?></td>
                              <td><?php if($row_orders->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?><?php echo $row->DPED_importe; ?></td>
                              <td><?php if($row_orders->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?><?php echo number_format(($row->DPED_importe*$row->DPED_cantidad), 2, ".", " "); ?></td>
                            </tr> 
                        <?php endforeach; ?>
                     <?php else : ?>
                         <tr>
                             <td colspan="3"><?php echo 'No hay descripción'; ?></td>              
                        </tr>
                     <?php endif; ?>
                    <?php if ($row_orders->PED_delivery==1) : ?>
                        <tr class="titulos">
                          <td>1</td>
                          <td>Envio a <?php echo $row_orders->DPTO_nombre; ?></td>
                          <td><?php if($row_orders->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?><?php echo $row_orders->DPTO_costo; ?></td>
                          <td><?php if($row_orders->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?><?php echo $row_orders->DPTO_costo; ?></td>
                        </tr> 
                    <?php endif; ?>
                    </table>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">               
                    <label class="control-label" for="Total">Es un total de: </label>
                </div>   
                <div class="span4">     
                    <label class="control-label span10 text-total">
                            <b><?php echo $number;?> <?php if ($row_orders->PED_moneda == 'S') { echo 'SOLES'; } else { echo 'DOLARES'; } ?></b>
                     </label>
                </div> 
                      
                <div class="span4"> 
                    <div class="form-horizontal" >
                    <div class="control-group">
                               
                     <label  class="control-label" for="Total">TOTAL: </label> 
                    <input type="text" id="total" readonly value="<?php echo $row_orders->PED_costoTotal; ?>">
                
                    </div>
                </div>
                </div>
            
        </div>
    <script type="text/javascript">
       function showHideInvoiceDetails(value) {
  if (value == 'personal') {
    $('.enterprise_invoice_tr').hide();
     $('#NombreEmpresa').removeAttr("required");
    $('#rutEmpresa').removeAttr("required");
    $('#giroEmpresa').removeAttr("required");
  } else {
    $('.enterprise_invoice_tr').show();
    $('#NombreEmpresa').attr('required', 'required');
    $('#rutEmpresa').attr('required', 'required');
    $('#giroEmpresa').attr('required', 'required');
  }
}
$(document).ready(function(){
  
    var nombre="<?php echo $row_orders->PER_nombres; ?>";
    var email="<?php echo $row_orders->PER_email; ?>";
    var pedido="<?php echo str_pad($row_orders->PED_ID, 11, "0", STR_PAD_LEFT); ?>";
     $('#pagado').click(function(){
        
        var estado = $(this).val();
        var id_ped = <?php echo $this->uri->segment(3); ?>;
        $.confirm({
            'title'     : 'Confirmar cambio del estado',
            'message'   : 'Está usted seguro de cambiar el estado del pedido a "Pagado" <br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
            'buttons'   : {
                'Sí'   : {
                    'class' : 'blue',
                    'action': function(){
                        
                        $.ajax({
                          type: "POST",
                          url: "<?=site_url("buys/add_buys")?>",
                          data: 'estado=' + estado + '&id_ped=' + id_ped +'&nombre='+nombre+'&email='+email+'&pedido='+pedido,
                           beforeSend: function() {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');
                },
                           success: function(resp) {
                        $('#DivLoader').html(resp).show();
                         if(estado){
                            $("#DivLoader").removeClass('DivEstadoPedido1').addClass('DivEstadoPedido2');;
                         }
                  
                }
                        });
                                            }
                },
                'No'    : {
                    'class' : 'gray',
                    'action': function(){
                        
                    }   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        
    });
     $('#anulado').click(function(){
        
        var estado = $(this).val();
        var id_ped = <?php echo $this->uri->segment(3); ?>;
        $.confirm({
            'title'     : 'Confirmar cambio del estado',
            'message'   : 'Está usted seguro de cambiar el estado del pedido a "Anulado" <br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
            'buttons'   : {
                'Sí'   : {
                    'class' : 'blue',
                    'action': function(){
                        
                        $.ajax({
                          type: "POST",
                          url: "<?=site_url("orders/orders_canceled")?>",
                          data: 'estado=' + estado + '&id_ped=' + id_ped,
                           beforeSend: function() {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');
                },
                           success: function(resp) {
                        $('#DivLoader').html(resp).show();
                        if(estado){
                            $("#DivLoader").removeClass('DivEstadoPedido1').addClass('DivEstadoPedido3');;
                         }
                }
                        });
                                            }
                },
                'No'    : {
                    'class' : 'gray',
                    'action': function(){
                        
                    }   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        
    });
    
});</script>
    <!-- Modules::end() -->
<!-- Main::end() -->