<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

				<thead>

					<tr>

						<th class="th_padding">ID</th>

						<th class="th_padding">CLIENTE</th>

						<th class="th_padding">DOC. DE IDENTIDAD</th>

						<th class="th_padding">FECHA DE PEDIDO</th>							

						<th class="th_padding">ESTADO</th>

						<th class="th_padding">PRECIO</th>

						<th class="th_padding">NUM OPER.</th>

						<th class="th_padding">ACCIONES</th>

					</tr>

				</thead> 

					<?php if ($orders != FALSE) : ?>

					<tbody>

						<?php foreach ($orders->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo $row->PED_ID; ?></td>

							<td class="td_padding"><?php echo $row->PER_nombres.", ".$row->PER_apellidos; ?> </td>

							<td class="td_padding"  width="15%"><?php echo $row->PER_documento; ?></td>						

							

							  <?php

                                $fecha = $row->PED_fecha;

                                $tDate = strtotime($fecha);

                                $var_fecha = date('d-m-Y H:i:s',$tDate);

                                ?>

							<td class="td_padding" width="15%" ><?php echo $var_fecha; ?></td>

							<td width="8%" class="td_padding">

								<?php if($row->PED_estado==1): ?>

								<?php echo "PENDIENTE"; ?>

								<?php elseif ($row->PED_estado==2): ?> 

								<?php echo "PAGADO"; ?>

								<?php elseif ($row->PED_estado==3): ?> 

								<?php echo "ANULADO"; ?>

								<?php endif;?>	

	

							</td>

							<td class="td_padding"><?php echo $row->PED_costoTotal; ?></td>

							<td class="td_padding" width="10%"><?php echo $row->PED_numOperacion; ?></td>

							<td width="18%">

								<div class="item">

								<a href="<?php echo site_url("orders/frm_view_orders_details/".$row->PED_ID); ?>" title="Detalle de Venta" class="btn btn-info"><i class="icon-search icon-white"></i> Detalle</a>

								</div>

								<div class="item">

								<a  title="Eliminar" class="btn btn-danger" id="<?php echo $row->PED_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>				

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

					<?php else : ?>

                    	<h4>No hay registros de Pedidos.</h4>

                    <?php endif; ?>

				</table>

				

<script src="<?php echo base_url().'resources';?>/resources_sgc/js/script.js"></script>

<script type="text/javascript">

$('#datatable').dataTable( {

	  "bJQueryUI":true,

      "bSort":false,

      "bPaginate":true,

      "sPaginationType":"full_numbers",

 } );

$(document).ready(function(){

	

	$('.btn-danger').click(function(){		



		var eliminarId = $(this).attr("id");

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar este Pedido.<br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',

			'buttons'	: {

				'Si'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("orders/delete_orders_id")?>",

						  data: 'eliminarId=' + eliminarId,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            $('#DivLoader').html(resp).show();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});

	

});</script>

