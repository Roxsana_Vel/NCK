<div class="row-fluid">
    <div class="span12">
        <ul class="breadcrumb">
        	<li><a href="<?php echo site_url("welcome");?>">Panel de Control</a>
        		<span class="divider">/</span>
        	</li>
        	<li><span>Pedidos</span></li>
        </ul><!-- breadcrumbs -->                    
    </div>
</div>
<div class="row-fluid">
	<div class="span12 widget">
		<div class="widget-header">
			<span class="title">
				<i class="icon-tag icon-white"></i>
				Gestión de Pedidos
			</span>
		</div>
		
			<div class="widget-content">
		<!-- loader -->
		<div id="DivLoader">
				<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">
				<thead class="fondo">
					<tr>
 						<th class="th_padding flechita" width="4%">Id</th>
                        <th class="th_padding flechita" width="25%">Cliente</th>
						<th class="th_padding flechita" width="10%">Núm. Docu.</th>
						<th class="th_padding flechita" width="13%">Fecha Pedido</th>							
						<th class="th_padding" width="9%">Estado</th>
						<th class="th_padding flechita" width="12%">Monto Compra</th>
						<th class="th_padding flechita" width="13%">Núm Operación</th>
						<th class="th_padding" width="14%">Acciones</th>
					</tr>
				</thead> 
					<?php $contador = 0; ?>
					<?php if ($orders != FALSE) : ?>
					<tbody>
						<?php foreach ($orders->result() as $row) : ?>
						<tr>
 							<td class="td_padding"><?php echo $row->PED_ID; ?></td>
                            <td class="td_padding"><?php echo $row->PER_nombres." ".$row->PER_apellidos; ?> </td>
							<td class="td_padding"><?php echo $row->PER_documento; ?></td>						
							
							  <?php
                                $fecha = $row->PED_fecha;
                                $tDate = strtotime($fecha);
                                $var_fecha = date('d-m-Y H:i:s',$tDate);
                                ?>
							<td class="td_padding"><?php echo $var_fecha; ?></td>
							<td class="td_padding">
								<?php if($row->PED_estado==1): ?>
								<?php echo "PENDIENTE"; ?>
								<?php elseif ($row->PED_estado==2): ?> 
								<?php echo "PAGADO"; ?>
								<?php elseif ($row->PED_estado==3): ?> 
								<?php echo "ANULADO"; ?>
								<?php endif;?>	
	
							</td>
							<td class="td_padding"><?php if($row->PED_moneda=='S') { echo "S/ "; } else { "$ "; } ?>
							<?php echo $row->PED_costoTotal; ?></td>
							<td class="td_padding"><?php echo $row->PED_numOperacion; ?></td>
							<td>
								<div class="item">
								<a href="<?php echo site_url("orders/frm_view_orders_details/".$row->PED_ID); ?>" title="Detalle de Venta" class="btn btn-info"><i class="icon-search icon-white"></i> Det.</a>
								</div>
								<div class="item">
								<a  title="Eliminar" class="btn btn-danger" id="<?php echo $row->PED_ID?>"><i class="icon-trash icon-white"></i> Elim.</a>				
								</div>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
					<?php else : ?>
                    	<h4>No hay registros de Pedidos.</h4>
                    <?php endif; ?>
				</table>
				</div>
		<!-- End loader -->
			</div>
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	
	$('.btn-danger').click(function(){		
		var eliminarId = $(this).attr("id");
		$.confirm({
			'title'		: 'Confirmar Eliminación',
			'message'	: 'Está usted seguro de eliminar este Pedido.<br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
			'buttons'	: {
				'Si'	: {
					'class'	: 'blue',
					'action': function(){
						
						$.ajax({
						  type: "POST",
						  url: "<?=site_url("orders/delete_orders_id")?>",
						  data: 'eliminarId=' + eliminarId,
						   beforeSend: function() {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');
                },
			               success: function(resp) {
			            $('#DivLoader').html(resp).show();
                  
                }
						});
											}
				},
				'No'	: {
					'class'	: 'gray',
					'action': function(){
						
					}	// Nothing to do in this case. You can as well omit the action property.
				}
			}
		});
		
	});
	
});</script>
