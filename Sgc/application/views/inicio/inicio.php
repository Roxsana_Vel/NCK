<link href="<?= base_url('../dist/css/style.css') ?>" rel="stylesheet">

<link href="<?= base_url('../dist/css/responsive.css') ?>" rel="stylesheet">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inicio
            <small>Panel de Control</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li class="active">Inicio</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
            <div class="row">
                <div class="  col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $numAsociados ?></h3>

                            <p>Productos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cube"></i>
                        </div>
                        
                            <a href="<?= base_url('asociados') ?>" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                        
                            
                        
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?= $numArticulos ?></h3>

                            <p>Artículos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-list "></i>
                        </div>
                        <a href="<?= base_url('articulos') ?>" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?= $numAcuerdos ?></h3>

                            <p>Proyecto</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-clipboard"></i>
                        </div>
                        <a href="<?= base_url('acuerdos') ?>" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?= $numBanners ?></h3>

                            <p>Banners</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-image"></i>
                        </div>
                        <a href="<?= base_url('banners') ?>" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                
                <!-- ./col -->
            </div>
       

        <!-- Main row -->
        <div class="info-container-1">
       <div class="container">
           <div class="row">
               <h3>Catalogo de Productos</h3>
               <div class=" col-md-3 col-xs-6">
                   <img  class="img-responsive" src=" <?= base_url('../dist/img/img/image-1.jpg') ?>" alt="">
                   <h4>descripcion</h4>
                   <span>dhewif</span>
                   
               </div>
               <div class="col-md-3 col-xs-6">
                   <img  class="img-responsive" src=" <?= base_url('../dist/img/img/image-1.jpg') ?>" alt="">
                   <span>descripcion</span>
                   
               </div>
               <div class="col-md-3 col-xs-6">
                   <img  class="img-responsive" src=" <?= base_url('../dist/img/img/image-1.jpg') ?>" alt="">
                   <span>descripcion</span>
                   
               </div>
               <div class="col-md-3 col-xs-6">
                   <img  class="img-responsive" src=" <?= base_url('../dist/img/img/image-1.jpg') ?>" alt="">
                   <span>descripcion</span>
                   
               </div>
           </div>
        
       </div>
       </div>
     
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>