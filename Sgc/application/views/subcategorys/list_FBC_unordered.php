<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead>

						<th class="th_padding flechita" width="10%">NRO</th>

						<th class="th_padding flechita" width="20%">CATEGORIA</th>						

						<th class="th_padding flechita" width="30%">NOMBRE</th>						

						<th class="th_padding" width="15%">ESTADO</th>						

						<th class="th_padding" width="25%">ACCIONES</th>

					</thead> 

					<?php if ($subcategorys != FALSE): ?>

					<tbody>

						<?php $count = 0; foreach ($subcategorys->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo ++$count; ?></td>

							<td class="td_padding"><?php echo $row->CAT_nombre; ?></td>

							<td class="td_padding"><?php echo $row->SCAT_nombre; ?></td>

							<td class="td_padding">

								<?php if($row->SCAT_estado): ?>

									<div id="estado<?php echo $row->SCAT_ID;?>" style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-check_green.png' ?>" />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>" onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=0','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php else: ?> 

									<div id="estado<?php echo $row->SCAT_ID;?>"  style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-cross_red.png' ?>"  />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=1','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php endif;?>		

	

							</td>

							<td>

								<div class="item">

								<a href="<?php echo site_url("subcategorys/frm_edit_subcategorys/".$row->SCAT_ID); ?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a  title="Eliminar" class="btn btn-danger" data-id="<?php echo $row->SCAT_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

                    <?php endif; ?>

				</table>

<script src="<?php echo base_url().'resources';?>/resources_sgc/js/script.js"></script>

<script type="text/javascript">



$(document).ready(function(){

	

	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).data("id");

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar esta subcategoría. <br />Eliminará la imagen y los registros asociados a la subcategoría.<br />¡No se podrá deshacer la acción! ¿Continuar?',

			'buttons'	: {

				'Sí'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("subcategorys/delete_subcategorys_id")?>",

						  data: 'eliminarId=' + eliminarId,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            //$('#DivLoader').html(resp).show();

			            window.location.reload();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});

	

});</script>