<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/print.css" media="print" />



<div class="content-wrapper">
<section class="content-header">
        <h1>
            Categoria
            <small>Nueva Categorias</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Categoria</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>


<div class="content">
<div class="row">

  <div class="span12 widget">
    <div class="col-xs-12">

    <div class="widget-header">

      <span class="title">

        <i class="icon-tag icon-white"></i>

      Información de la Categorias

      </span>

    </div>

      <div class="widget-content form-container">

        <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoArticulo">
                            <li class="active"><a href="#tabInformacion" data-toggle="tab" aria-expanded="true">Información</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                    </ul>


          	<form method="POST" class="form-horizontal" action="<?php echo site_url("subcategorys/add_subcategory/"); ?>">

			<span class="help-block">&nbsp;</span>

			<?php if ($categorys !== FALSE): ?>

			<div class="row">

				<label for="categoria_sub " class="col-xs-2 control-label">Categoría *</label>                      
				<div class=" col-xs-5 controls">

					<select name="categoria_sub" id="categoria_sub" class="span3 form-control select2" required autofocus>

						<option value='' selected>--Seleccione--</option>

						<?php foreach ($categorys->result() as $row) : ?>

							<option value="<?php echo $row->CAT_ID;?>"> <?php echo $row->CAT_nombre;?></option> 

						<?php endforeach; ?>

					</select>

				</div>                                       

			</div>

			<?php endif; ?>

			

			<div class="row row-padding-top">

				<label for="nombre" class="col-xs-2 control-label">Nombre *</label>                      

				<div class="controls col-xs-5">

				   <input class="span8 form-control" type="text" name="nombre" id="nombre" value="" maxlength="100" required />

				</div>                                       

			</div>

			

			<div class="row">

		        <span class="help-block">&nbsp;</span>



		        <label for="estado" class="col-xs-2 control-label">Estado</label>

		        <div class="controls col-xs-5">

		          <select name="estado" id="estado" class="span3 form-control">

		            <option value="1" selected>Activo</option>

		            <option value="0">Inactivo</option>

		          </select> 

		        </div>

		    </div>



		    <span class="help-block">&nbsp;</span>

    

	<div class="row-fluid">

	  <div class="form-actions" align="center">

		<div class="item">

		  <input type="submit" class="btn btn-primary" id="btnAgregarImagen" value="Guardar">

		</div>

		 <div class="item">

		  <a href="<?php echo site_url("subcategorys"); ?>" class="btn btn-info">Cancelar</a>

		</div>

	  </div> 

    </div>







      		</form>

             

        </div>

      </div>

  </div>

</div>
</div>
</div>
</div>

