<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Categoria
            <small>Listado de Categorias</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Categoria</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>


<div class="content">
<div class="row">

	<div class="col-xs-12">
         <div class="box box-primary">
        <div class="box-header with-border">
		<div class="widget-header">
            <i class="ion ion-clipboard"></i>
			<h3 class="box-title">Listado de Categorias</h3>
			<div class="box-tools pull-right" >
			     <a class="btn btn-success btn-sm" href="<?php= site_url('subcategorys/frm_add_subcategorys');?>"><i class="glyphicon glyphicon-plus"></i> Nuevo</a>
			 </div>
    		</div>
             </div>

		

			<div class="widget-content">

				<?php if ($this->session->flashdata('success_message')): ?>

				<div class="alert alert-success">

					<button type="button" class="close" data-dismiss="alert">&times;</button>

	                <?php echo $this->session->flashdata('success_message'); ?>

	            </div>

	        	<?php endif; ?>



				<?php if ($this->session->flashdata('error_message')): ?>

				<div class="alert alert-error">

					<button type="button" class="close" data-dismiss="alert">&times;</button>

	                <?php echo $this->session->flashdata('error_message'); ?>

	            </div>

	        	<?php endif; ?>



				 <?php if ($categorys != FALSE) : ?> 

				 <form class="form-horizontal">

                    <div class="control-group">

                        <label for="categoria_sub" style="width: 10%; text-align: left" class="col-xs-2 control-label">Marca</label>

                        <div class="col-xs-3" style="margin-left: 0">

                            <select name="categoria_sub" id="categoria_sub" class="form-control">

                                <option value='-1' selected>--Todas--</option>

                                <?php foreach ($categorys->result() as $row) : ?>

                                    <option value="<?php echo $row->CAT_ID;?>"> <?php echo $row->CAT_nombre;?></option> 

                                <?php endforeach; ?>

                            </select>       

                        </div>

                    </div> <br> <br>

                 </form> 

                 <?php endif; ?>

		<!-- loader -->

		<div id="DivLoader" class="box-body">

				<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead class="fondo">

						<th class="th_padding flechita" width="10%">NRO</th>

						<th class="th_padding flechita" width="20%">CATEGORIA</th>						

						<th class="th_padding flechita" width="30%">NOMBRE</th>						

						<th class="th_padding" width="15%">ESTADO</th>						

						<th class="th_padding" width="25%">ACCIONES</th>

					</thead> 

					<?php if ($subcategorys != FALSE): ?>

					<tbody>

						<?php $count = 0; foreach ($subcategorys->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo ++$count; ?></td>

							<td class="td_padding"><?php echo $row->CAT_nombre; ?></td>

							<td class="td_padding"><?php echo $row->SCAT_nombre; ?></td>

							<td class="td_padding">

								<?php if($row->SCAT_estado): ?>

									<div id="estado<?php echo $row->SCAT_ID;?>" style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/images/button-check_green.png' ?>" />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/images/change_2.png' ?>" onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=0','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php else: ?> 

									<div id="estado<?php echo $row->SCAT_ID;?>"  style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/images/button-cross_red.png' ?>"  />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/images/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=1','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php endif;?>		
	        				</td>							

							<td>
								
								<a href="<?php echo site_url("subcategorys/frm_edit_subcategorys/".$row->SCAT_ID); ?>" title="Editar" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> </a>
								
								<a  title="Eliminar" class="btn btn-sm   btn-danger" data-id="<?php echo $row->SCAT_ID?>"><span class="glyphicon glyphicon-trash"></span> </a>
							
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
                    <?php endif; ?>
				</table>
				</div>
		<!-- End loader -->

			</div>
	</div>

</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#categoria_sub').change(function() {
    		var id_cat = $(this).val();

  		$('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/images/loader.gif" alt="Loader"></div>');
        $.ajax({
			url: "<?=site_url('subcategorys')?>/filter_by_category/" + id_cat,
			success: function(resp) {
				$('#DivLoader').html(resp).show();
			}
		});
	});
	
	
    
    
    $('#DivLoader').on('click', '.btn-danger', function () {
            var eliminarId = $(this).attr("id");

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está usted seguro de eliminar este producto. Eliminará la imagen, los Pedidos y Compras asociados al Producto. ¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este producto!',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url("subcategorys/delete_subcategorys_id") ?>",
                    data: 'eliminarId=' + eliminarId,
                    beforeSend: function () {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?= base_url('resources/resources_sgc/images/loader.gif') ?>" alt="Loader"></div>');
                    },
                    success: function (resp) {
                        swal({
                            title: 'Eliminado!',
                            text: 'La categoria seleccionado ha sido eliminado.',
                            type: 'success',
                            confirmButtonText: 'Continuar'
                        });

                        $('#DivLoader').html(resp).show();
                    }
                });
            });
        });
	if ($('#categoria_sub').val() !== '-1') {
		$('#categoria_sub').trigger('change');
	}
});</script>

