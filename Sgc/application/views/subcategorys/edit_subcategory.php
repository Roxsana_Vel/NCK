<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />



<div class="content-wrapper">
<section class="content-header">
        <h1>
            Categoria
            <small>Editar Categoria</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Categoria</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>
<div class="container">
<div class="row">
  <div class="col-xs-12">
    
      <div class="">
        
			<?php if ($subcategorys !== FALSE): ?>
			<?php $row_subcat = $subcategorys->row_object(); ?>
          	<form method="POST" class="form-horizontal" action="<?php echo site_url("subcategorys/update_subcategory/"); ?>">
			<input type="hidden" name="id_subcategory" value="<?php echo $row_subcat->SCAT_ID; ?>" />
			<input type="hidden" name="id_category_ant" value="<?php echo $row_subcat->CAT_ID; ?>" />
			<input type="hidden" name="orden_subcategory" value="<?php echo $row_subcat->SCAT_orden; ?>" />
			<span class="help-block">&nbsp;</span>
			<div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoArticulo">
                            <li class="active"><a href="#tabInformacion" data-toggle="tab" aria-expanded="true">Información</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
			<?php if ($categorys !== FALSE): ?>
			<div class="row">
				<label for="category" class="control-label col-xs-2">Categoría</label>                      
				<div class="col-xs-5">
					<select name="categoria_sub" id="categoria_sub" class="form-control" required autofocus>
						<option value='' selected>--Seleccione--</option>
						<?php foreach ($categorys->result() as $row) : ?>
							<option value="<?php echo $row->CAT_ID;?>" <?php echo ($row->CAT_ID == $row_subcat->CAT_ID ? 'selected' : ''); ?>> <?php echo $row->CAT_nombre;?></option> 
						<?php endforeach; ?>
					</select>
				</div>                                       
			</div>
			<?php endif; ?>
			<div class="row row-padding-top">
				<label for="Nombre" class=" control-label col-xs-2">Nombre</label>                      
				<div class="controls col-xs-5">
				   <input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $row_subcat->SCAT_nombre; ?>" maxlength="100" required />
				</div>                                       
			</div>
	        <div class="row">
		        <span class="help-block">&nbsp;</span>
		        <label for="estado" class=" control-label col-xs-2">Estado</label>
		        <div class="controls col-xs-5">
		          <select name="estado" id="estado" class="form-control">
		            <option value="1" <?php echo ($row_subcat->SCAT_estado == '1' ? 'selected' : ''); ?>>Activo</option>
		            <option value="0" <?php echo ($row_subcat->SCAT_estado == '0' ? 'selected' : ''); ?>>Inactivo</option>
		          </select> 
		        </div>
		    </div>
		    <span class="help-block">&nbsp;</span>
  <div class="row">
	  <div class="form-actions" align="center">
		
		  <input type="submit" class="btn btn-primary" id="btnAgregarImagen" value="Guardar" />
		
		  <a href="<?php echo site_url("subcategorys"); ?>" class="btn btn-info">Cancelar</a>
		
	  </div> 
    </div>
</div>
</form>
<?php endif; ?>

</div>
</div>
</div>
</div>
</div>

