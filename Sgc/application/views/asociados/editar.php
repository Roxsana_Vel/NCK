<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $datosAsociado->ASOC_empresa ?>
            <small>Editar asociado</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Asociados</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoAsociado">
                            <li class="active"><a href="#tabDatosGenerales" data-toggle="tab" aria-expanded="true">Datos generales</a></li>
                            <li class=""><a href="#tabDatosUsuarioSistema" data-toggle="tab" aria-expanded="false">Usuario del sistema</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabDatosGenerales">
                                <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('asociados/guardar_edicion_asociado/'.$datosAsociado->ASOC_id) ?>">
                                    <div class="form-group">
                                        <label for="inputEmpresa" class="col-sm-2 control-label">Empresa</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputEmpresa" name="inputEmpresa" placeholder="Razon Social de la empresa" value="<?= $datosAsociado->ASOC_empresa ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputProductos" class="col-sm-2 control-label">Productos</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputProductos" name="inputProductos" placeholder="Productos" value="<?= $datosAsociado->ASOC_productos ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputDireccion" class="col-sm-2 control-label">Dirección</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputDireccion" name="inputDireccion" placeholder="Dirección" value="<?= $datosAsociado->ASOC_direccion ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputTelefono" class="col-sm-2 control-label">Teléfono</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputTelefono" name="inputTelefono" placeholder="Teléfono" value="<?= $datosAsociado->ASOC_telefono ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPaginaWeb" class="col-sm-2 control-label">Página Web</label>

                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">http://</span>
                                                <input type="text" class="form-control" id="inputPaginaWeb" name="inputPaginaWeb" placeholder="www.ejemplo.com" value="<?= $datosAsociado->ASOC_pagina_web ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPersonaContacto" class="col-sm-2 control-label">Persona de contacto</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputPersonaContacto" name="inputPersonaContacto" placeholder="Persona de contacto" value="<?= $datosAsociado->ASOC_persona_contacto ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Correo electrónico</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="email@empresa.com" value="<?= $datosAsociado->ASOC_email ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="selectMarca" class="col-sm-2 control-label">Marca</label>
                                        <div class="col-sm-10">
                                            <select name="selectMarca[]" id="selectMarca" multiple="multiple" class="form-control">
                                                <option value="">Seleccione</option>
                                                <?php foreach ($arrayMarcas as $key => $marca): ?>
                                                    <option value="<?= $marca->MAR_nombre ?>" <?= (strpos($datosAsociado->ASOC_marca, $marca->MAR_nombre) !== false) ? 'selected' : '' ?> ><?= $marca->MAR_nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <span class="help-block">Si no encuentra la marca, puede crear una marca con el siguiente botón</span>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalMarcaNueva">Agregar Nueva Marca</button>
                                        <button type="submit" class="btn btn-info" disabled>Guardar</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane fade -->

                            <div class="tab-pane fade" id="tabDatosUsuarioSistema">
                                <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('user/guardar_usuario_editado/'.$datosUsuario->USU_ID) ?>">
                                    <div class="form-group">
                                        <label for="inputLoginUsuario" class="col-sm-2 control-label">Nombre de Usuario</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputLoginUsuario" name="inputLoginUsuario" placeholder="Nombre de Usuario para ingresar al sistema" value="<?= $datosUsuario->USU_login ?>">
                                        </div>
                                    </div>

                                    <p class="text-muted"><span class="text-light-blue" style="font-weight: bold">Asignar nueva contraseña : </span>(si deja los campos en blanco, no se cambiará la contraseña)</p>

                                    <div class="form-group">
                                        <label for="inputLoginPassword" class="col-sm-2 control-label">Asignar contraseña</label>

                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputLoginPassword" name="inputLoginPassword" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputLoginPasswordRepeat" class="col-sm-2 control-label">Repita la contraseña</label>

                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputLoginPasswordRepeat" name="inputLoginPasswordRepeat" placeholder="">
                                        </div>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" disabled>Guardar</button>
                                    </div>
                                </form>
                            </div>
                                <!-- /.tab-pane fade -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
            </div>
        </div>

    </section>
</div>

<!-- ******* -->
<!-- modales -->
<!-- ******* -->

<!-- Modal de marca nueva -->
<div class="modal fade" id="modalMarcaNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-teal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar nueva Marca</h4>
            </div>

            <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('banners/guardar_nuevo/') ?>" enctype="multipart/form-data">

                <div class="modal-body row">
                    <div class="col-xs-12">

                            <div class="form-group">
                                <label for="inputMarcaNombre" class="col-sm-2 control-label">Marca</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputMarcaNombre" name="inputMarcaNombre" placeholder="Nombre de la marca">
                                </div>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success" id="btnMarcaGuardar">Guardar</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->
<script>
    $(document).ready(function() {

        /** plugins */
        $("#selectMarca").select2({
            placeholder: 'Seleccione una opción',
        });

        /** validaciones del primer tab */
        $('input[name="inputEmpresa"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputProductos"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputDireccion"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputTelefono"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputPaginaWeb"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputPersonaContacto"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputEmail"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $("#selectMarca").on('change', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });

        /** validar campos del segundo tab de datos del uusario del sistema */
        $('input[name="inputLoginUsuario"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabSegundo();
        });
        $('input[name="inputLoginPassword"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsPasswords();
        });
        $('input[name="inputLoginPasswordRepeat"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsPasswords();
        });

        /** boton guardar nueva marca */
        $('#modalMarcaNueva button#btnMarcaGuardar').on('click', function(event) {
            event.preventDefault();

            cargar_select('#selectMarca', {nombreMarca:$('input[name="inputMarcaNombre"]').val()}, '<?= base_url('marca/guardar_y_actualizar') ?>');

            $('#modalMarcaNueva').modal('hide');
        });

        /** eventos del modal nueva marca */
        $('#modalMarcaNueva').on('show.bs.modal', function (e) {
            $('#inputMarcaNombre').focus();
        })
        $('#modalMarcaNueva').on('hidden.bs.modal', function (e) {
            $('input[name="inputMarcaNombre"]').val('');
        })


        /** ********* */
        /** funciones */
        /** ********* */

        /**
         * validar los inputs del tab de datos generales
         *
         * @return boolean retorna true cuando se han cumplido todas las validaciones
         *                         false cuando no se han cumplido todas.
         */
        function validarInputsTabPrimero() {
            var contadorPermitir = 0;

            /** campos que tienen validaciones */
            if ($('input[name="inputEmpresa"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputEmpresa"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputEmpresa"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputEmpresa"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputEmpresa"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputEmpresa"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputProductos"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputProductos"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputProductos"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputProductos"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputProductos"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputProductos"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputDireccion"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputDireccion"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputDireccion"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputDireccion"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputDireccion"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputDireccion"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputTelefono"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputTelefono"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputTelefono"]').siblings('.help-block').remove();

            } else {
                $('input[name="inputTelefono"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputTelefono"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputTelefono"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if (validarEmail($('input[name="inputEmail"]').val()) != false) {
                contadorPermitir++;
                $('input[name="inputEmail"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputEmail"]').siblings('.help-block').remove();

            } else {
                $('input[name="inputEmail"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputEmail"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputEmail"]').after('<span class="help-block">Ingrese una dirección de correo válida</span>');
                }
            }

            /** campos no obligatorios */
            if ($('input[name="inputPaginaWeb"]').val() != '') {
                $('input[name="inputPaginaWeb"]').parents('.form-group').addClass('has-success');
            }

            if ($('input[name="inputPersonaContacto"]').val() != '') {
                $('input[name="inputPersonaContacto"]').parents('.form-group').addClass('has-success');
            }

            /** return */
            if (contadorPermitir == 5) {
                $('#tabDatosGenerales button').removeAttr('disabled');
                return true;
            } else {
                $('#tabDatosGenerales button').attr('disabled', 'true');
                return false;
            }

        }

        /**
         * validar los inputs del tab de datos generales
         *
         * @return boolean retorna true cuando se han cumplido todas las validaciones
         *                         false cuando no se han cumplido todas.
         */
        function validarInputsTabSegundo() {
            var contadorPermitir = 0;

            /** campos que tienen validaciones */

            /** input nombre de usuario */
            if ($('input[name="inputLoginUsuario"]').val() != '') {
                $.ajax({
                    async: true,
                    cache: false,
                    url: '<?= base_url('user/verificar_usuario_existente') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        usuarioNombre : $('input[name="inputLoginUsuario"]').val()
                    },
                    success: function(respuesta) {
                        if (respuesta == true) {
                            $('input[name="inputLoginUsuario"]').parents('.form-group').addClass('has-error');
                            if ($('input[name="inputLoginUsuario"]').siblings('span').hasClass('help-block') === false) {
                                $('input[name="inputLoginUsuario"]').after('<span class="help-block">Este nombre de usuario ya existe</span>');
                                $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                            }
                        } else {
                            contadorPermitir++;
                            $('input[name="inputLoginUsuario"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                            $('input[name="inputLoginUsuario"]').siblings('.help-block').remove();
                            $('#tabDatosUsuarioSistema button').removeAttr('disabled');

                        }
                    }
                });
            } else {
                console.log('vacio');
                $('input[name="inputLoginUsuario"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginUsuario"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginUsuario"]').after('<span class="help-block">Este campo no puede estar vacío</span>');
                }
            }

            /** return */
            if (contadorPermitir == 1) {
                $('#tabDatosUsuarioSistema button').removeAttr('disabled');
                return true;
            } else {
                $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                return false;
            }

        }

        function validarInputsPasswords() {
            /** input contraseña */
            if ($('input[name="inputLoginPassword"]').val() != '') {
                $('input[name="inputLoginPassword"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputLoginPassword"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputLoginPassword"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginPassword"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginPassword"]').after('<span class="help-block">Este campo es obligatorio</span>');
                    $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                }
            }

            /** input repetir la contraseña */
            if ($('input[name="inputLoginPasswordRepeat"]').val() != '') {
                if ($('input[name="inputLoginPasswordRepeat"]').val() == $('input[name="inputLoginPassword"]').val()) {
                    $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                    $('input[name="inputLoginPasswordRepeat"]').siblings('.help-block').remove();
                    $('#tabDatosUsuarioSistema button').removeAttr('disabled');
                } else {
                    $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').addClass('has-error');
                    if ($('input[name="inputLoginPasswordRepeat"]').siblings('span').hasClass('help-block') === false) {
                        $('input[name="inputLoginPasswordRepeat"]').after('<span class="help-block">Las contraseñas no coinciden</span>');
                        $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                    }
                }
            } else {
                $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginPasswordRepeat"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginPasswordRepeat"]').after('<span class="help-block">Este campo es obligatorio</span>');
                    $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                }
            }
        }

        /**
         * validar un email
         * @param  string stringEmail cadena de texto del email
         * @return bool               retorna false cuando el email es valido
         *                                    true cuando es invalido
         */
        function validarEmail(stringEmail) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(stringEmail);
        }

    });
</script>