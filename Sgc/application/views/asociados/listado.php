<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Asociados
            <small>Listado de Asociados</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Asociados</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Left col -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Listado de Asociados</h3>

                        <div class="box-tools pull-right">
                            <a href="<?= base_url('asociados/nuevo') ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- listado de asociados -->
                        <table id="tablaAsociadosListado" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Empresa</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Correo electrónico</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($arrayAsociados as $key => $socio): ?>
                                <tr data-asoc-id="<?= $socio->ASOC_id ?>">
                                    <td><?= $socio->ASOC_empresa ?></td>
                                    <td><?= $socio->ASOC_direccion ?></td>
                                    <td><?= $socio->ASOC_telefono ?></td>
                                    <td><?= $socio->ASOC_email ?></td>

                                    <td class="text-center" width="120px">

                                        <?php if ($socio->ASOC_estado == 'habilitado'): ?>
                                            <span class="label label-success">Habilitado</span>
                                            <a style="margin:3px" href="<?= base_url('asociados/habilitar_deshabilitar/'.$socio->ASOC_id) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Desactivar">
                                                <span class="glyphicon glyphicon-refresh"></span>
                                            </a>
                                        <?php else: ?>
                                            <span class="label label-danger">Deshabilitado</span>
                                            <a style="margin:3px" href="<?= base_url('asociados/habilitar_deshabilitar/'.$socio->ASOC_id) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Activar">
                                                <span class="glyphicon glyphicon-refresh"></span>
                                            </a>
                                        <?php endif ?>

                                    </td>
                                    <td class="text-center">
                                        <!-- editar -->
                                        <a href="<?= base_url('asociados/editar/'.$socio->ASOC_id) ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Editar">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </a>
                                        <!-- eliminar -->
                                        <a href="<?= base_url('asociados/eliminar/'.$socio->ASOC_id) ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->

<script>
    $(document).ready(function() {

        var tablaAsociadosListado = $('#tablaAsociadosListado').DataTable();

        $('#tablaAsociadosListado').on('click', '.btn-danger', function (e) {
            e.preventDefault();

            var $this = $(this);

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está seguro de eliminar a este asociado?. ¡No se podrá deshacer la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este asociado!',
                cancelButtonText: 'No'
            }).then(function () {
                window.location.href = $this.attr('href');
            });
        });
    });
</script>