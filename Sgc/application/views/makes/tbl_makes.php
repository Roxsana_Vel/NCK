				<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead class="fondo">

						<th class="th_padding flechita" width="8%">NRO</th>

						<th class="th_padding flechita">NOMBRE</th>

						<th>LOGO</th>

						<th>BANNER</th>

						<th>CAMBIAR ORDEN</th>

						<th class="th_padding" width="20%">ACCIONES</th>

					</thead> 

					<?php if ($makes != FALSE) : ?>

					<tbody>

						<?php $i = 0; foreach ($makes->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo ++$i; ?></td>

							<td class="td_padding"><?php echo $row->MAR_nombre; ?></td>

							<td class="td_padding"><center><img src="<?php echo base_url(); ?>uploads/logos/thumbs/<?php echo $row->MAR_logo; ?>" alt="Marca" width="50" height="50"/></center></td>

							<td class="td_padding">
							<?php if ($row->MAR_banner!='') : ?>
							<center><img src="<?php echo base_url(); ?>uploads/banners/thumbs/<?php echo $row->MAR_banner; ?>" alt="Marca" width="100" height="30"/></center>
							<?php endif;?>
							</td>
							
							<td class="td_padding"> 

								<?php if ($i < $makes->num_rows()) : ?>

								<a title="Bajar" href="javascript:cargar('<?php echo site_url("makes")?>/bajar/<?php echo $row->MAR_ID; ?>/<?php echo $row->MAR_orden; ?>','DivLoader', false)"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/img/navigation-down-button_green.png" /></a>

								<?php endif;?>

								<?php if($i!=1) : ?>

								<a title="Subir" href="javascript:cargar('<?php echo site_url("makes")?>/subir/<?php echo $row->MAR_ID; ?>/<?php echo $row->MAR_orden; ?>','DivLoader', false)"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/img/navigation-up-button_green.png" /></a>

								<?php endif; ?>


							</td>

							<td width="30%">

								<div class="item">

								<a href="<?php echo site_url("makes/frm_edit_make/".$row->MAR_ID);?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a  title="Eliminar" class="btn btn-danger" data-id="<?php $cadena = $row->MAR_ID.'/'.$row->MAR_nombre;  echo $cadena; ?>"><i class="icon-trash icon-white"></i> Eliminar</a>

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

                    <?php endif; ?>

				</table>