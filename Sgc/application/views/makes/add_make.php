<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/sceditor/default.min.css" media="screen" />


<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/jquery.sceditor.xhtml.min.js"></script>

<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/jquery.sceditor.es.min.js"></script>
<div class="content-wrapper">
    <section class="content-header">
            <h1>
                Marca
                <small>Nuevo</small>
        </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
                <li><a href="#">Marca</a></li>
                <li class="active">Nuevo</li>
            </ol>
    </section>

<div class="content">
  <div class="row">
   <div class="col-xs-12">
      <?php echo form_open_multipart('makes/add_make','class="form-horizontal"');?>
      <div class="widget-content form-container">
        <div class="nav-tabs-custom">
             <ul class="nav nav-tabs bg-gray">
                <li class="active">
                    <a href="#producto" data-toggle="tab">Información</a>
                </li>
            </ul>
          <div class="tab-content">
            <div class="tab-pane fade in active" id="tabInformacion">
                        <div class="row" class="form-group" id="DivHide">
                                <label for="Nombre" class="col-sm-2 control-label">Nombre</label>                       
                                <div class="col-sm-10">
                                  <input class="span8" type="text" name="nombre" id="nombre" maxlength="50" required autofocus />
                                
                                </div>
                                                                              

                        </div>

                            <input type="hidden" name="orden" maxlength="10" value="" />

                            <div class="row" style="padding-top:2em;" id="DivHide">

                                <label for="Nombre" class="col-sm-2 control-label">Descripción</label>                       
                                            
                                <div class="controls">
                                  
                                  <textarea name="descripcion" class="span8" type="text" id="descripcion" /></textarea>
                                
                                </div>
                                                                              

                            </div>
 
                                
                            <!-- SUBIR LOGOTIPO -->
                              <div class="row row-padding-top">

                                      <label for="archivo1" class="col-sm-2 control-label">Logotipo*</label>

                                      <div class="controls">

                                        <input type="file" id="archivo1" name ="archivo1" accept="image/*" required onchange="imagenval(this)" />

                                      </div>

                              </div>

                              <div class="row row-padding-top hidden" id="preview_container">

                                <div class="controls">

                                  <img src="#" alt="Vista Previa" id="preview" />

                                </div>

                              </div>

                              <div class="row row-padding-top">

                                <div class="controls">

                                <div class="alert alert-info" style="width:60%; margin-left:0">

                                  <i class=" icon-info-sign icon-blue"></i>  

                                  Sólo se aceptan imágenes en formato jpg/png/gif.<br />

                                  <i class=" icon-info-sign icon-blue" style="opacity:0"></i>  

                                  Se recomiendan dimensiones de 400 de ancho x 400 de alto.

                                </div>

                                </div>

                              </div>
                              
                              <!-- Fin Logotipo -->
                              <!-- Subir Banner -->

                              <div class="row row-padding-top">

                                      <label for="archivo1" class="col-sm-2 control-label">Banner</label>

                                      <div class="controls">

                                        <input type="file" id="archivo2" name ="archivo2" accept="image/*" onchange="imagenval_banner(this)" />

                                      </div>

                              </div>

                              <div class="row row-padding-top hidden" id="preview_container2">

                                <div class="controls">

                                  <img src="#" alt="Vista Previa" id="preview2" />

                                </div>

                              </div>

                              <div class="row row-padding-top">

                                <div class="controls">

                                <div class="alert alert-info" style="width:60%; margin-left:0">

                                  <i class=" icon-info-sign icon-blue"></i>  

                                  Sólo se aceptan imágenes en formato jpg/png/gif.<br />

                                  <i class=" icon-info-sign icon-blue" style="opacity:0"></i>  

                                  Se recomiendan dimensiones de 1180 de ancho x 275 de alto.

                                </div>

                                </div>
</div>
                              </div>
                      </div>
                              <!-- -->
                    <div class="row-fluid">


                        <div class="form-actions" align="center">

                          <div class="item">

                            <input type="submit" class="btn btn-primary" value="Guardar">

                          </div>

                           <div class="item">

                            <a class="btn btn-info" href="<?php echo site_url("makes"); ?>">Cancelar</a>

                          </div>

                        </div> 

                       

                        

                    </div>

            



          

                    <?php echo form_close(); ?>

             

        </div>

      </div>

  </div>

</div>
</div>
</div>


<script type="text/javascript">
  $('#descripcion').sceditor({

        plugins: "xhtml",

        toolbar: "bold,italic,underline|left,center,right,justify|quote,font,size,color|youtube,link",

        emoticons: false,

        height: 200,

        style: "<?php echo base_url(); ?>resources/resources_sgc/css/jquery.sceditor.default.min.css"

    });

</script>