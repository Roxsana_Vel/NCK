<?php $onlyTable = $this->uri->segment(3); ?>


<?php if (empty($onlyTable)): ?>

<div class="content-wrapper">
<section class="content-header">

    <div class="span12">
        <ul class="breadcrumb">
        	<li><a href="<?php echo site_url("welcome");?>">Panel de Control</a>
        		<span class="divider">/</span>
        	</li>
        	<li><span>Marcas</span></li>
        </ul><!-- breadcrumbs -->                    
    </div>
</section>


 <!-- Main content -->
<section class="content">
<div class="row">
	<div class="col-xs-12">
	 <div class="box box-primary">
         <div class="box-header with-border">
	         <h3 class="box-title">Listado de <?= isset($paginaDatos->tipoArticulo) ? $paginaDatos : 'makes' ?></h3>
		
            <div class="box-tools pull-right">
				<a class="btn btn-success" href="<?php echo site_url('makes');?>/frm_add_make/<?php if ($makes != FALSE) : echo  $makes->num_rows();  endif;?>"><i class="icon-plus icon-white"></i> Agregar</a>
            </div>
         </div>
     

        <div class="widget-content">
				<?php if ($this->session->flashdata('success_message')): ?>
				
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
	                <?php echo $this->session->flashdata('success_message'); ?>
	            </div>
	        	<?php endif; ?>
	        	
				<?php if ($this->session->flashdata('error_message')): ?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
	                <?php echo $this->session->flashdata('error_message'); ?>
	            </div>
	        	<?php endif; ?>
		<!-- loader -->
		<? endif; ?>



			<?php if ($makes != FALSE) : ?> 

				 <form class="form-horizontal">

                    <div class="form-group">

                        <label for="categoria_pro"  class="col-xs-2 control-label ">Categoría</label>

                        <div class="controls col-xs-5" style="margin-left: 0">

                            <select name="categoria_pro" id="categoria_pro" class="span3 form-control select2">

                                <option value='-1' selected>--Todas--</option>
                
                                         <!--opciones de categoria-->

                            </select>       

                        </div>

                    </div>

                   
                 </form>

                 <?php endif; ?>
		
		<div id="DivLoader" class="box-body">
				<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead class="fondo">

						<th class="th_padding flechita" width="8%">NRO</th>

						<th class="th_padding flechita">NOMBRE</th>

						<th>LOGO</th>

						<th>BANNER</th>

						<th>CAMBIAR ORDEN</th>

						<th class="th_padding" width="20%">ACCIONES</th>

					</thead> 

					<?php if ($makes != FALSE) : ?>

					<tbody>

						<?php $i = 0; foreach ($makes->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo ++$i; ?></td>

							<td class="td_padding"><?php echo $row->MAR_nombre; ?></td>

							<td class="td_padding"><center><img src="<?php echo base_url(); ?>uploads/logos/thumbs/<?php echo $row->MAR_logo; ?>" alt="Marca" width="50" height="50"/></center></td>

							<td class="td_padding">
							<?php if ($row->MAR_banner!='') : ?>
							<center><img src="<?php echo base_url(); ?>uploads/banners/thumbs/<?php echo $row->MAR_banner; ?>" alt="Marca" width="100" height="30"/></center>
							<?php endif;?>
							</td>

							<td class="td_padding"> 

								<?php if ($i < $makes->num_rows()) : ?>

								<a title="Bajar" href="javascript:cargar('<?php echo site_url("makes")?>/bajar/<?php echo $row->MAR_ID; ?>/<?php echo $row->MAR_orden; ?>','DivLoader', false)"><img width="32" src="<?php echo base_url();?>dist2/img/navigation-down-button_green.png" /></a>

								<?php endif;?>

								<?php if($i!=1) : ?>

								<a title="Subir" href="javascript:cargar('<?php echo site_url("makes")?>/subir/<?php echo $row->MAR_ID; ?>/<?php echo $row->MAR_orden; ?>','DivLoader', false)"><img width="32" src="<?php echo base_url();?>dist2/img/navigation-up-button_green.png" /></a>

								<?php endif; ?>


							</td>

							<td width="20%" class="text-cente">

								<div class="">

								<a href="<?php echo site_url("makes/frm_edit_make/".$row->MAR_ID);?>" title="Editar"
								toggle="tooltip" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-pencil"></span> Editar</a>

								</div>

								<div class="">

								<a  title="Eliminar" 
								data-toggle="tooltip" class="btn btn-danger btn-xs" data-id="<?php $cadena = $row->MAR_ID.'/'.$row->MAR_nombre.'/'.$row->MAR_orden;  echo $cadena; ?>"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

                    <?php endif; ?>

				</table>

			<?php if (empty($onlyTable)): ?>

			</div>

		<!-- End loader -->



			</div>

	</div>
    </div>

    </div> </div> </section>
</div>
			<?php endif; ?>
			<?php endif; ?>



<script>
$(document).ready(function(){

	
	//**************************

    var val=$("#categoria_pro").val();

   	 	if (val=="-1") {

    	$("#subcategoria_pro").html("<option value='0' selected>--Ninguna--</option>");

        }else
        {

	        var url = "<?php echo site_url("subcategorys"); ?>/ajax_by_category/" + val;


	        $('#subcategoria_pro').empty().append("<option value='0'>--Ninguna--</option>").attr('disabled', true);


	        if (val.trim().length > 0) {

	            $.ajax({

	                'url': url,

	                'beforeSend': function () {

	                    $('#subcat_loader').show();

	                },

	                'success': function (data) {

	                    var json = JSON.parse(data);



	                    if (json.length > 0) {

		        			$('#subcategoria_pro').html("<option value='-1' selected>--Todas--</option>").attr('disabled', false);



		                    for (var i = 0; i < json.length; i++) {

		                        $('#subcategoria_pro').append("<option value='" + json[i].SCAT_ID + "'>" + json[i].SCAT_nombre + "</option>");

		                    }

		                }



	                    $('#subcat_loader').hide();

	                }

	            });

	        	}

	        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

	        $.ajax({

				url: "<?=site_url("makes")?>/filterByCategoria_Subcat/",

				type:'POST',

				data:{categoria:val,subcat:'-1'},

				success: function(resp) {

					$('#DivLoader').html(resp).show();

				}

			});

    }



	//**************************
	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).data("id");

		var arreglo = eliminarId.split("/"); // segmentamos la cadena en un arreglo donde cada posicion se divide tomando como punto de division el "/" (barra)


		nwElementoNombreCat = arreglo[1].trim(); //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo

		nwElementoIdCat = arreglo[0].trim();	 //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo

		order_make=arreglo[2].trim(); 

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar la marca "'+nwElementoNombreCat+'"<br /><b>Eliminará la marca y los productos asociados.</b><br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',

			'buttons'	: {

				'Si'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("makes/delete_make_id")?>",

						  data: 'eliminarId=' + nwElementoIdCat+"&orden="+order_make,

						    beforeSend: function() {

			                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

			                },

			               success: function(resp) {

						        //$('#DivLoader').html(resp).show();

						        window.location.reload();

			                  

			                }

						});

					}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		
 
	});


		$('#categoria_pro').change(function (e) {

        var val = $(this).val();


        if (val=="-1") {

        	$('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

	        $.ajax({

				url: "<?=site_url("makes")?>/filterByCategoria_Subcat/",

				type:'POST',

				data:{categoria:'-1',subcat:'-1'},

				success: function(resp) {

					$('#DivLoader').html(resp).show();

				}

			});

        }else
        {

	        var url = "<?php echo site_url("subcategorys"); ?>/ajax_by_category/" + val;


	        $('#subcategoria_pro').empty().append("<option value='0'>--Ninguna--</option>").attr('disabled', true);


	        if (val.trim().length > 0) {

	            $.ajax({

	                'url': url,

	                'beforeSend': function () {

	                    $('#subcat_loader').show();

	                },

	                'success': function (data) {

	                    var json = JSON.parse(data);



	                    if (json.length > 0) {

		        			$('#subcategoria_pro').html("<option value='-1' selected>--Todas--</option>").attr('disabled', false);



		                    for (var i = 0; i < json.length; i++) {

		                        $('#subcategoria_pro').append("<option value='" + json[i].SCAT_ID + "'>" + json[i].SCAT_nombre + "</option>");

		                    }

		                }



	                    $('#subcat_loader').hide();

	                }

	            });

	        	}

	        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

	        $.ajax({

				url: "<?=site_url("makes")?>/filterByCategoria_Subcat/",

				type:'POST',

				data:{categoria:$(this).val(),subcat:'-1'},

				success: function(resp) {

					$('#DivLoader').html(resp).show();

				}

			});

    }
    });


				$('#subcategoria_pro').change(function (e) {

		        var cat_id = $('#categoria_pro').val();

		        var subcat_id = $(this).val();


		        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');


		        $.ajax({

					url: "<?=site_url("makes")?>/filterByCategoria_Subcat/",

					type:"POST",

					data:{categoria:cat_id,subcat:subcat_id},

					success: function(resp) {

						$('#DivLoader').html(resp).show();

					}

				});

		    });


});
</script>
