<?php if ($makes !== FALSE): ?>

  <?php $make = $makes->row_object(); ?>

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/sceditor/default.min.css" media="screen" />


<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/jquery.sceditor.xhtml.min.js"></script>

<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/jquery.sceditor.es.min.js"></script>


<div>

    <ul class="breadcrumb">

        <li><a href="<?php echo site_url("welcome");?>">Panel de Control</a><span class="divider">/</span></li>

        <li><a href="<?php echo site_url("makes");?>">Marcas</a><span class="divider">/</span></li>

        <li class="active">Editar</li>

    </ul>

</div>



<div class="row-fluid">

  <div class="span12 widget">

    <div class="widget-header">

      <span class="title">

        <i class="icon-tag icon-white"></i>

      Información de la Marca

      </span>

    </div>

      <div class="widget-content form-container">

        <div>



          <form method="POST" class="form-horizontal" action="<?php echo site_url("makes/update_make/" . $this->uri->segment(3)); ?>" enctype="multipart/form-data">



                    <div class="row row-padding-top" id="DivHide">

                                

                      <label for="Nombre">Nombre*</label>                      

                      <div class="controls">

                           <input class="span8" type="text" name="nombre" value="<?php echo $make->MAR_nombre; ?>" required autofocus>

                      </div>

                      <input type="hidden" name="image_name" value="<?php echo $make->MAR_logo; ?>">

                    </div>



                    <div class="row row-padding-top" id="DivHide">

                                

                      <label for="Nombre">Descripción</label>                      

                      <div class="controls">

                           <textarea class="span8" type="text" name="descripcion" id="descripcion" required autofocus><?php echo $make->MAR_descripcion; ?></textarea>

                      </div>

						          <input type="hidden" name="image_name" value="<?php echo $make->MAR_logo; ?>">

                    </div>    

                    <!-- Editar Logotipo -->
                              <div class="row row-padding-top">

                                      <label for="logotipo">Logotipo *</label>

                                      <div class="controls">

                                        <input type="file" id="archivo1" name ="archivo1" accept="image/*" onchange="imagenval(this)" />

                                      </div>

                              </div>

                              <div class="row row-padding-top" id="preview_container">

                                <div class="controls">

                                  <img src="<?php echo base_url(); ?>uploads/logos/<?php echo $make->MAR_logo; ?>" alt="Vista Previa" id="preview"/>

                                </div>

                              </div>

                              <div class="row row-padding-top">

                                <div class="controls">

                                <div class="alert alert-info" style="width:60%; margin-left:0">

                                  <i class=" icon-info-sign icon-blue"></i>  

                                  Sólo se aceptan imágenes en formato jpg/png/gif.<br />

                                  <i class=" icon-info-sign icon-blue" style="opacity:0"></i>  

                                  Se recomiendan dimensiones de 400 de ancho x 400 de alto.

                                </div>

                                </div>

                              </div>

                    <!-- -->
                              
                              
                              <div class="row row-padding-top">
 
                                      <label for="archivo1">Banner</label>

                                      <div class="controls">

                                        <input type="file" id="archivo2" name ="archivo2" accept="image/*" onchange="imagenval_banner(this)" />

                                      </div>

                              </div>
                             
                              
                              <?php if ($make->MAR_banner!=''): ?>

                              <div class="row row-padding-top" id="preview_container2">

                                <div class="controls">

                                  <img src="<?php echo base_url(); ?>uploads/banners/<?php echo $make->MAR_banner; ?>" alt="Vista Previa" id="preview2" />

                              <?php else: ?>

                                  <div class="row row-padding-top hidden" id="preview_container2">

                                  <div class="controls">

                                  <img src="#" alt="Vista Previa" id="preview2" />

                              <?php endif; ?>

                                </div>

                              </div>



                              <div class="row row-padding-top">

                                <div class="controls">

                                <div class="alert alert-info" style="width:60%; margin-left:0">

                                  <i class=" icon-info-sign icon-blue"></i>  

                                  Sólo se aceptan imágenes en formato jpg/png/gif.<br />

                                  <i class=" icon-info-sign icon-blue" style="opacity:0"></i>  

                                  Se recomiendan dimensiones de 1180 de ancho x 275 de alto.

                                </div>

                                </div>

                              </div>




                    <div class="row-fluid">

                        

                    

                        <div class="form-actions" align="center">

                          <div class="item">

                            <input type="submit" class="btn btn-primary" value="Guardar">

                          </div>

                           <div class="item">

                            <a class="btn btn-info" href="<?php echo site_url("makes"); ?>">Cancelar</a>

                          </div>

                        </div> 

                       

                        

                    </div>

            



          

                    <?php echo form_close(); ?>

             

        </div>

      </div>

  </div>

</div>


<script type="text/javascript">
  $('#descripcion').sceditor({

        plugins: "xhtml",

        toolbar: "bold,italic,underline|left,center,right,justify|quote,font,size,color|youtube,link",

        emoticons: false,

        height: 200,

        style: "<?php echo base_url(); ?>resources/resources_sgc/css/jquery.sceditor.default.min.css"

    });

</script>


<?php endif; ?>