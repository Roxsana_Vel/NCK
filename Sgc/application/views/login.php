<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCK-Ingenieros</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('plugins/sweetalert2/sweetalert2.min.css') ?>">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('dist2/css/AdminLTE.css') ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('plugins/iCheck/square/blue.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- iCheck -->
    <script src="<?= base_url('plugins/iCheck/icheck.min.js') ?>"></script>
    <!-- SweetAlert2 -->
    <script src="<?= base_url('plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
</head>

<body class="hold-transition login-page" style="background: url(<?= base_url('dist2/img/slider-1.jpg') ?>);background-size: cover;background-attachment: fixed;background-position: center center; height: auto">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?= base_url() ?>" style="color:white"><b>NCK- Ingenieros</b> <span></span>SGC</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Inicie sesión con su nombre de Usuario</p>

            <?php if (isset($nextUrl)) : ?>
                <form action="<?= base_url('welcome/user_validation/?next=' . $nextUrl) ?>" method="post" accept-charset="utf-8">
            <?php else : ?>
                <form action="<?= base_url('welcome/user_validation') ?>" method="post" accept-charset="utf-8">
            <?php endif ?>
                <div class="form-group has-feedback">
                    <input type="text" name="usuario" id="username" required autofocus class="form-control" placeholder="Usuario" >
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="contrasena" id="password" required class="form-control" placeholder="Contraseña">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="control-group">
                    <?php if (isset($error_1)) : ?>
                        <div class="alert alert-error">
                            <h4>
                                <i class="icon-warning-sign icon-orange"></i> ADVERTENCIA
                            </h4>
                            <?= $error_1 ?>
                        </div>
                    <?php elseif (isset($error_2)) : ?>
                        <div class="alert">
                            <h4>
                                <i class="icon-exclamation-sign icon-orange"></i> CUIDADO
                            </h4>
                            <?= $error_2 ?>
                        </div>
                    <?php elseif (isset($error_3)) : ?>
                        <div class="alert alert-info">
                            <i class=" icon-info-sign icon-blue"></i> <?= $error_3 ?>
                        </div>
                    <?php elseif (isset($error_4)) : ?>
                        <div class="alert alert-info">
                            <i class=" icon-info-sign icon-blue"></i> <?= $error_4 ?>
                        </div>
                    <?php endif ?>
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <br>
            <div class="text-center">
                <a href="#" id="passwordRecoveryModal">He olvidado mi contraseña</a>
            </div>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <script>
        $(document).ready(function() {
            $('#passwordRecoveryModal').click(function (e) {
                e.preventDefault();

                swal({
                    title: 'Recuperar contraseña',
                    html: 'Ingrese el correo electrónico de su cuenta',
                    input: 'email',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Enviar',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve, reject) {
                            $.ajax({
                                url: '<?= base_url('password_recovery') ?>',
                                type: 'POST',
                                dataType: 'JSON',
                                data: { inputEmailRecovery : email },
                                success: function (response) {
                                    if (response.status == 'ok') {
                                        resolve();
                                    } else {
                                        reject(response.message);
                                    }
                                }
                            });
                        });
                    },
                    allowOutsideClick: false
                }).then(function (email) {
                    swal({
                        title: 'Envio realizado!',
                        html: 'Se le ha enviado un correo con el enlace para reestablecer su contraseña. Revise en su bandeja de entrada o en la carpeta de spam.',
                        type: 'success'
                    });
                });
            });
        });
    </script>

    <?php $this->load->view('template/alerts') ?>
</body>
</html>
