<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $datosUsuario->USU_login ?>
            <small>Editar organizacion</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Organización</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoAsociado">
                            <li class="active"><a href="#tabDatosGenerales" data-toggle="tab" aria-expanded="true">Datos generales</a></li>
                            <li class=""><a href="#tabDatosUsuarioSistema" data-toggle="tab" aria-expanded="false">Usuario del sistema</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabDatosGenerales">

                                <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('perfil/guardar_edicion_organizacion/') ?>" enctype="multipart/form-data">

                                    <input type="hidden" name="inputOrganizacionId" value="<?= $datosOrganizacion->ORG_ID ?>"">

                                    <div class="form-group">
                                        <label for="inputEmpresa" class="col-sm-2 control-label">Empresa</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputEmpresa" name="inputEmpresa" placeholder="Razon Social de la empresa" value="<?= $datosOrganizacion->ORG_empresa ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAdministrador" class="col-sm-2 control-label">Administrador</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputAdministrador" name="inputAdministrador" placeholder="Productos" value="<?= $datosOrganizacion->ORG_contacto ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputDireccion" class="col-sm-2 control-label">Dirección</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputDireccion" name="inputDireccion" placeholder="Dirección" value="<?= $datosOrganizacion->ORG_direccion1 ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputTelefono" class="col-sm-2 control-label">Teléfono</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputTelefono" name="inputTelefono" placeholder="Teléfono" value="<?= $datosOrganizacion->ORG_telefono ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Correo electrónico</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="email@empresa.com" value="<?= $datosOrganizacion->ORG_email ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputLicencia" class="col-sm-2 control-label">Licencia</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputLicencia" name="inputLicencia" placeholder="Teléfono" value="<?= $datosOrganizacion->ORG_licencia ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputOrganizacionLogo" class="col-sm-2 control-label">Seleccione su logo</label>

                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="inputOrganizacionLogo" name="inputOrganizacionLogo" accept=".png, .jpg, .jpeg, .gif, .svg">
                                            <p class="help-block">De preferencia, que tenga fondo transparente</p>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-info" disabled>Guardar</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane fade -->

                            <div class="tab-pane fade" id="tabDatosUsuarioSistema">
                                <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('user/guardar_usuario_editado/'.$datosUsuario->USU_ID) ?>">
                                    <div class="form-group">
                                        <label for="inputLoginUsuario" class="col-sm-2 control-label">Nombre de Usuario</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputLoginUsuario" name="inputLoginUsuario" placeholder="Nombre de Usuario para ingresar al sistema" value="<?= $datosUsuario->USU_login ?>">
                                        </div>
                                    </div>

                                    <p class="text-muted"><span class="text-light-blue" style="font-weight: bold">Asignar nueva contraseña : </span>(si deja los campos en blanco, no se cambiará la contraseña)</p>

                                    <div class="form-group">
                                        <label for="inputLoginPassword" class="col-sm-2 control-label">Asignar contraseña</label>

                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputLoginPassword" name="inputLoginPassword" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputLoginPasswordRepeat" class="col-sm-2 control-label">Repita la contraseña</label>

                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputLoginPasswordRepeat" name="inputLoginPasswordRepeat" placeholder="">
                                        </div>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" disabled>Guardar</button>
                                    </div>
                                </form>
                            </div>
                                <!-- /.tab-pane fade -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
            </div>
        </div>

    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->
<script>
    $(document).ready(function() {

        /** plugins */
        $("#selectMarca").select2({
            placeholder: 'Seleccione una opción',
        });

        /** validaciones del primer tab */
        $('input[name="inputEmpresa"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputAdministrador"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputDireccion"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputTelefono"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputEmail"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputLicencia"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });
        $('input[name="inputOrganizacionLogo"]').on('change', function(event) {
            event.preventDefault();
            validarInputsTabPrimero();
        });

        /** validar campos del segundo tab de datos del uusario del sistema */
        $('input[name="inputLoginUsuario"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabSegundo();
        });
        $('input[name="inputLoginPassword"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsPasswords();
        });
        $('input[name="inputLoginPasswordRepeat"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsPasswords();
        });

        /** ********* */
        /** funciones */
        /** ********* */

        /**
         * validar los inputs del tab de datos generales
         *
         * @return boolean retorna true cuando se han cumplido todas las validaciones
         *                         false cuando no se han cumplido todas.
         */
        function validarInputsTabPrimero() {
            var contadorPermitir = 0;

            /** campos que tienen validaciones */
            if ($('input[name="inputEmpresa"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputEmpresa"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputEmpresa"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputEmpresa"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputEmpresa"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputEmpresa"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputAdministrador"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputAdministrador"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputAdministrador"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputAdministrador"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputAdministrador"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputAdministrador"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputDireccion"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputDireccion"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputDireccion"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputDireccion"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputDireccion"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputDireccion"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputTelefono"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputTelefono"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputTelefono"]').siblings('.help-block').remove();

            } else {
                $('input[name="inputTelefono"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputTelefono"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputTelefono"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if (validarEmail($('input[name="inputEmail"]').val()) != false) {
                contadorPermitir++;
                $('input[name="inputEmail"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputEmail"]').siblings('.help-block').remove();

            } else {
                $('input[name="inputEmail"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputEmail"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputEmail"]').after('<span class="help-block">Ingrese una dirección de correo válida</span>');
                }
            }

            /** return */
            if (contadorPermitir == 5) {
                $('#tabDatosGenerales button').removeAttr('disabled');
                return true;
            } else {
                $('#tabDatosGenerales button').attr('disabled', 'true');
                return false;
            }

        }

        /**
         * validar los inputs del tab de datos generales
         *
         * @return boolean retorna true cuando se han cumplido todas las validaciones
         *                         false cuando no se han cumplido todas.
         */
        function validarInputsTabSegundo() {
            var contadorPermitir = 0;

            /** campos que tienen validaciones */

            /** input nombre de usuario */
            if ($('input[name="inputLoginUsuario"]').val() != '') {
                $.ajax({
                    async: true,
                    cache: false,
                    url: '<?= base_url('user/verificar_usuario_existente') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        usuarioNombre : $('input[name="inputLoginUsuario"]').val()
                    },
                    success: function(respuesta) {
                        if (respuesta == true) {
                            $('input[name="inputLoginUsuario"]').parents('.form-group').addClass('has-error');
                            if ($('input[name="inputLoginUsuario"]').siblings('span').hasClass('help-block') === false) {
                                $('input[name="inputLoginUsuario"]').after('<span class="help-block">Este nombre de usuario ya existe</span>');
                                $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                            }
                        } else {
                            contadorPermitir++;
                            $('input[name="inputLoginUsuario"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                            $('input[name="inputLoginUsuario"]').siblings('.help-block').remove();
                            $('#tabDatosUsuarioSistema button').removeAttr('disabled');

                        }
                    }
                });
            } else {
                console.log('vacio');
                $('input[name="inputLoginUsuario"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginUsuario"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginUsuario"]').after('<span class="help-block">Este campo no puede estar vacío</span>');
                }
            }

            /** return */
            if (contadorPermitir == 1) {
                $('#tabDatosUsuarioSistema button').removeAttr('disabled');
                return true;
            } else {
                $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                return false;
            }

        }

        function validarInputsPasswords() {
            /** input contraseña */
            if ($('input[name="inputLoginPassword"]').val() != '') {
                $('input[name="inputLoginPassword"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputLoginPassword"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputLoginPassword"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginPassword"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginPassword"]').after('<span class="help-block">Este campo es obligatorio</span>');
                    $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                }
            }

            /** input repetir la contraseña */
            if ($('input[name="inputLoginPasswordRepeat"]').val() != '') {
                if ($('input[name="inputLoginPasswordRepeat"]').val() == $('input[name="inputLoginPassword"]').val()) {
                    $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                    $('input[name="inputLoginPasswordRepeat"]').siblings('.help-block').remove();
                    $('#tabDatosUsuarioSistema button').removeAttr('disabled');
                } else {
                    $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').addClass('has-error');
                    if ($('input[name="inputLoginPasswordRepeat"]').siblings('span').hasClass('help-block') === false) {
                        $('input[name="inputLoginPasswordRepeat"]').after('<span class="help-block">Las contraseñas no coinciden</span>');
                        $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                    }
                }
            } else {
                $('input[name="inputLoginPasswordRepeat"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputLoginPasswordRepeat"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputLoginPasswordRepeat"]').after('<span class="help-block">Este campo es obligatorio</span>');
                    $('#tabDatosUsuarioSistema button').attr('disabled', 'true');
                }
            }
        }

        /**
         * validar un email
         * @param  string stringEmail cadena de texto del email
         * @return bool               retorna false cuando el email es valido
         *                                    true cuando es invalido
         */
        function validarEmail(stringEmail) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(stringEmail);
        }

    });
</script>