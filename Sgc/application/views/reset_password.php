<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pro Olivo - Reestableciendo Contraseña</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('dist2/css/AdminLTE.css') ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('plugins/iCheck/square/blue.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- iCheck -->
    <script src="<?= base_url('plugins/iCheck/icheck.min.js') ?>"></script>
</head>

<body class="hold-transition login-page" style="background: url(<?= base_url('dist2/img/fondo_login.jpg') ?>);background-size: cover;background-attachment: fixed;background-position: center center; height: auto">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?= base_url() ?>" style="color:white"><b>ProOlivo</b>SGC</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Ingrese una nueva contraseña para el usuario <b><em><?= $cuenta->USU_login ?></em></b></p>

            <form method="post" accept-charset="utf-8">
                <div class="form-group has-feedback">
                    <input type="password" name="password" id="password" required class="form-control" placeholder="Nueva contraseña" value="<?= set_value('password') ?>" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <p class="help-block"><?= form_error('password') ?></p>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="re-password" id="re-password" required class="form-control" placeholder="Repita la contraseña" value="<?= set_value('re-password') ?>" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <p class="help-block"><?= form_error('re-password') ?></p>
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Reestablecer contraseña</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
</body>
</html>
