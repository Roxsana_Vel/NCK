<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Banners
            <small>Listado de Banners</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Left col -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Listado de Banners</h3>

                        <div class="box-tools pull-right">
                            <a href="<?= base_url('banners/nuevo') ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- listado de asociados -->
                        <table id="tablaBanners" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="25%">Título</th>
                                    <th>Url</th>
                                    <th width="20%">Imagen</th>
                                    <th width="10%">Secciones</th>
                                    <th>Estado</th>
                                    <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                        <th width="5%">Acciones</th>
                                    <?php endif ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($arrayBanners as $key => $banner): ?>
                                <tr data-ban-id="<?= $banner->BAN_ID ?>" data-ban-estado="<?= $banner->BAN_estado ?>">

                                    <td style="vertical-align: middle;"><?= $banner->BAN_titulo ?></td>
                                    <td style="vertical-align: middle;"><?= $banner->BAN_url ?></td>
                                    <td class="text-center" style="vertical-align: middle;">
                                        <img class="img-responsive" style="max-height: 100px; min-width:150px; margin: 0 auto" src="<?= base_url('uploads/banners/'.$banner->BAN_archivo) ?>" alt="">
                                    </td>
                                    <td style="vertical-align: middle;" class="text-center">
                                        <?php if ($banner->BAN_mostrar_portada): ?>
                                            <span class="label label-info">Portada</span>
                                        <?php endif ?>

                                        <?php
                                            $this->db->where('VIP_img_slogan', $banner->BAN_archivo);
                                            $vistas = $this->db->get('vistas_publicas')->result();
                                        ?>
                                        <?php foreach ($vistas as $vista): ?>
                                            <span class="label label-info"><?= $vista->VIP_nombre ?></span>
                                        <?php endforeach ?>
                                    </td>
                                    <td class="text-center" width="100px" style="vertical-align: middle;">

                                        <?php if ($banner->BAN_estado == 1): ?>
                                            <span class="label label-success">Habilitado</span>

                                            <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                <a style="margin:3px" href="<?= base_url('banners/habilitar_deshabilitar/'.$banner->BAN_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Desactivar">
                                                    <span class="glyphicon glyphicon-refresh"></span>
                                                </a>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <span class="label label-danger">Deshabilitado</span>

                                            <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                <a style="margin:3px" href="<?= base_url('banners/habilitar_deshabilitar/'.$banner->BAN_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Activar">
                                                    <span class="glyphicon glyphicon-refresh"></span>
                                                </a>
                                            <?php endif ?>
                                        <?php endif ?>

                                    </td>
                                    <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                        <td class="text-center" style="vertical-align: middle;">
                                            <!-- editar -->
                                            <a href="<?= base_url('banners/editar/'.$banner->BAN_ID) ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Editar">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </a>
                                            <!-- eliminar -->
                                            <a href="<?= base_url('banners/eliminar/'.$banner->BAN_ID) ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    <?php endif ?>

                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->

<script>
    $(document).ready(function() {
        var tablaBanners = $('#tablaBanners').DataTable({
            order: [[ 2, "desc" ]],
        });

        $('#tablaBanners').on('click', '.btn-danger', function (e) {
            e.preventDefault();

            var $this = $(this);

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está seguro de eliminar este banner?. ¡No se podrá deshacer la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este banner!',
                cancelButtonText: 'No'
            }).then(function () {
                window.location.href = $this.attr('href');
            });
        });
    });
</script>