<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $datosBanner->BAN_titulo ?>
            <small>Editar banner</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Banners</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">

                <form id="formAsociadoEditar" class="form-horizontal" method="post" action="<?= base_url('banners/guardar_edicion/'.$datosBanner->BAN_ID) ?>" enctype="multipart/form-data">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="ion ion-clipboard"></i>

                            <h3 class="box-title">Editar banner</h3>

                            <div class="box-tools pull-right">
                                <!--  -->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="form-group">
                                <label for="inputBannerTitulo" class="col-sm-2 control-label">Titulo</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputBannerTitulo" name="inputBannerTitulo" placeholder="Ejm: Producimos el mejor olivo del Perú" value="<?= $datosBanner->BAN_titulo ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputBannerDescripcion" class="col-sm-2 control-label">Descripción</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" name="inputBannerDescripcion" id="inputBannerDescripcion" rows="4" placeholder="Ejm: Pro Olivo es una organización representativa del sector olivícola peruano conformada por empresas procesadoras exportadoras de aceituna, aceite de oliva y derivados del Perú."><?= $datosBanner->BAN_descripcion ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputBannerNombreUrl" class="col-sm-2 control-label">Texto del botón</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputBannerNombreUrl" name="inputBannerNombreUrl" placeholder="Ejm: Información del Sector" value="<?= $datosBanner->BAN_nombre_url ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputBannerUrl" class="col-sm-2 control-label">Url</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputBannerUrl" name="inputBannerUrl" placeholder="Ejm: proolivo.com/informacion-del-sector" value="<?= $datosBanner->BAN_url ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputBannerArchivo" class="col-sm-2 control-label">Seleccione la Nueva imagen</label>

                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="inputBannerArchivo" name="inputBannerArchivo" placeholder="Seleccione">
                                    <p class="help-block">Elija una imagen de dimensiones 1700px por 600px</p>
                                </div>
                            </div>

                            <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                <div class="form-group">
                                    <label for="selectVistaPublica" class="col-sm-2 control-label">Sección pública</label>

                                    <div class="col-sm-10">
                                        <select name="selectVistaPublica[]" id="selectVistaPublica" multiple="multiple" class="form-control">
                                            <option value="">Seleccione</option>
                                            <option value="portada" <?= $datosBanner->BAN_mostrar_portada ? 'selected' : '' ?>>Portada</option>
                                            <?php foreach ($arrayVistasPublicas as $key => $vip): ?>
                                                <option value="<?= $vip->VIP_ID ?>" <?= (in_array($vip->VIP_ID, $vistasPublicasId)) ? 'selected' : '' ?> ><?= $vip->VIP_nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <span class="help-block">Elija las secciones que mostrarán el banner como su fondo de portada</span>

                                    </div>
                                </div>
                            <?php endif ?>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Estado</label>

                                <div class="col-sm-10">
                                    <label style="margin: 6px">
                                      <input type="radio" name="inputBannerEstado" class="flat-red" value="1" <?= ($datosBanner->BAN_estado == 1) ? 'checked' : '' ?> >
                                      Activado
                                    </label>
                                    <label style="margin: 6px">
                                      <input type="radio" name="inputBannerEstado" class="flat-red" value="0" <?= ($datosBanner->BAN_estado == 0) ? 'checked' : '' ?> >
                                      Desactivado
                                    </label>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <a href="<?= base_url('banners') ?>" class="btn btn-default">Cancelar</a>
                            <button type="submit" class="btn btn-info pull-right">Guardar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->
<script>
    $(document).ready(function() {

        /** plugins */
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $("#selectVistaPublica").select2({
            placeholder: 'Seleccione una opción',
        });

        /** validaciones del primer tab */
        $('input[name="inputEmpresa"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputProductos"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputDireccion"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputTelefono"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputPaginaWeb"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputPersonaContacto"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });
        $('input[name="inputEmail"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputs();
        });

        /** mostrar el segundo tab de usuario del sistema */
        $('#tabDatosGenerales button').on('click', function(event) {
            event.preventDefault();
            $('#tabsFormNuevoAsociado a[href="#tabDatosUsuarioSistema"]').tab('show');
        });

        /** validar campos del segundo tab de datos del uusario del sistema */
        $('input[name="inputLoginUsuario"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabSegundo();
        });
        $('input[name="inputLoginPassword"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabSegundo();
        });
        $('input[name="inputLoginPasswordRepeat"]').on('keyup', function(event) {
            event.preventDefault();
            validarInputsTabSegundo();
        });


        /** ********* */
        /** funciones */
        /** ********* */

        /**
         * validar los inputs del formulario
         *
         * @return boolean retorna true cuando se han cumplido todas las validaciones
         *                         false cuando no se han cumplido todas.
         */
        function validarInputs() {
            var contadorPermitir = 0;

            /** campos que tienen validaciones */
            if ($('input[name="inputEmpresa"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputEmpresa"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputEmpresa"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputEmpresa"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputEmpresa"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputEmpresa"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputProductos"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputProductos"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputProductos"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputProductos"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputProductos"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputProductos"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputDireccion"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputDireccion"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputDireccion"]').siblings('.help-block').remove();
            } else {
                $('input[name="inputDireccion"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputDireccion"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputDireccion"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            if ($('input[name="inputTelefono"]').val() != '') {
                contadorPermitir++;
                $('input[name="inputTelefono"]').parents('.form-group').removeClass('has-error').addClass('has-success');
                $('input[name="inputTelefono"]').siblings('.help-block').remove();

            } else {
                $('input[name="inputTelefono"]').parents('.form-group').addClass('has-error');
                if ($('input[name="inputTelefono"]').siblings('span').hasClass('help-block') === false) {
                    $('input[name="inputTelefono"]').after('<span class="help-block">Este campo es obligatorio</span>');
                }
            }

            /** campos no obligatorios */
            if ($('input[name="inputPaginaWeb"]').val() != '') {
                $('input[name="inputPaginaWeb"]').parents('.form-group').addClass('has-success');
            }

            if ($('input[name="inputPersonaContacto"]').val() != '') {
                $('input[name="inputPersonaContacto"]').parents('.form-group').addClass('has-success');
            }

            /** return */
            if (contadorPermitir == 5) {
                $('#tabDatosGenerales button').removeAttr('disabled');
                return true;
            } else {
                $('#tabDatosGenerales button').attr('disabled', 'true');
                return false;
            }

        }

        $('input[name="inputBannerArchivo"]').change(function () {
            changeFile(this, 1700, 600);
        });

        function changeFile(me, ancho_definido, alto_definido) {
            var _URL = window.URL || window.webkitURL;
            var file, img;

            if ((file = me.files[0])) {
                img = new Image();
                img.onload = function () {
                    var ancho = this.width;
                    var alto = this.height;

                    if (ancho <= ancho_definido && alto <= alto_definido) {
                        swal({
                            title: 'Dimensiones del banner',
                            html: 'La imagen tiene ' + ancho + 'px de ancho y ' + alto + 'px de alto.',
                            type: 'info'
                        });
                    } else {
                        swal({
                            title: 'Confirmar',
                            html: 'La imagen no coincide con las dimensiones recomendadas (' + ancho_definido + 'px de ancho y ' + alto_definido + 'px de alto). La imagen tiene ' + ancho + 'px de ancho y ' + alto + 'px de alto.<br>La imagen se redimensionará. ¿Continuar?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sí, continuar!',
                            cancelButtonText: 'No'
                        }).then(function () {
                        }).catch(function () {
                            $('input[name="inputBannerArchivo"]').val('');
                        });
                    }
                };
                img.src = _URL.createObjectURL(file);
            }
        }
    });
</script>