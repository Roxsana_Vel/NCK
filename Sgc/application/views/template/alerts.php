<script>
    $(document).ready(function () {
        <?php if ($this->session->flashdata('alert_type') == 'success'): ?>
            swal({
                title: 'Buen trabajo!',
                html: '<?= $this->session->flashdata('alert_message') ?>',
                type: 'success'
            });
        <?php endif ?>

        <?php if ($this->session->flashdata('alert_type') == 'error'): ?>
            swal({
                title: 'Hubo un error!',
                html: '<?= $this->session->flashdata('alert_message') ?>',
                type: 'error'
            });
        <?php endif ?>
    });
</script>