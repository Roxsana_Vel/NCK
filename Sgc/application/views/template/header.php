<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCK Ingenieros - Ingeniería Industrial</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url('bootstrap/css/bootstrap.css') ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('plugins/datatables/dataTables.bootstrap.css') ?>">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?= base_url('plugins/iCheck/all.css') ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('plugins/iCheck/flat/blue.css') ?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?= base_url('plugins/morris/morris.css') ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?= base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= base_url('plugins/datepicker/datepicker3.css') ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('plugins/daterangepicker/daterangepicker.css') ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?= base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url('plugins/select2/select2.min.css') ?>">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('plugins/sweetalert2/sweetalert2.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist2/css/styles.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('dist2/skin/css/estilos.css') ?>" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('dist2/css/AdminLTE.css') ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url('dist2/css/skins/skin-proolivo.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Font Awesome -->
    <script type="text/javascript" src="<?= base_url('resources/resources_sgc/js/gallery.js')?>"></script>
    <script src="https://use.fontawesome.com/5a549bfb71.js"></script>
    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- DataTables -->
    <script src="<?= base_url('plugins/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?= base_url('plugins/morris/morris.min.js') ?>"></script>
    <!-- Sparkline -->
    <script src="<?= base_url('plugins/sparkline/jquery.sparkline.min.js') ?>"></script>
    <!-- jvectormap -->
    <script src="<?= base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
    <script src="<?= base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= base_url('plugins/knob/jquery.knob.js') ?>"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?= base_url('plugins/daterangepicker/daterangepicker.js') ?>"></script>
    <!-- datepicker -->
    <script src="<?= base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
    <!-- InputMask -->
    <script src="<?= base_url('plugins/input-mask/jquery.inputmask.js') ?>"></script>
    <script src="<?= base_url('plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
    <script src="<?= base_url('plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?= base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
    <!-- Select2 -->
    <script src="<?= base_url('plugins/select2/select2.full.js') ?>"></script>
    <!-- Libreria español para select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>
    <!-- Slimscroll -->
    <script src="<?= base_url('plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?= base_url('plugins/iCheck/icheck.min.js') ?>"></script>
    <!-- FastClick -->
    <script src="<?= base_url('plugins/fastclick/fastclick.js') ?>"></script>
    <!-- SweetAlert2 -->
    <script src="<?= base_url('plugins/sweetalert2/sweetalert2.min.js') ?>"></script>


    <!-- AdminLTE App -->
    <script src="<?= base_url('dist2/js/app.min.js') ?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--script src="<?= base_url('dist2/js/pages/dashboard.js') ?>"></script-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url('dist2/js/demo.js') ?>"></script>
    <script src="<?= base_url('dist2/js/funciones.js') ?>"></script>

</head>

<!-- <body class="hold-transition skin-blue sidebar-mini"> -->
<body class="skin-proolivo fixed sidebar-mini" style="height: auto;">
    <?php if (($this->router->class == 'articulos' || $this->router->class == 'acuerdos') && ($this->router->method == 'detalle')): ?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.9";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php endif ?>

    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?= base_url() ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>NCK</b> I</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="<?= base_url('dist2/img/logo-1.png')?>" alt="gggg" width="200px;" > </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <?php $pathUserLogo = $this->session->userdata('usu_logo') ? base_url('uploads/asociados_logo/' . $this->session->userdata('usu_logo')) : base_url('dist2/img/avatar_default.png') ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= $pathUserLogo ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $this->session->userdata('sgc_user') ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= $pathUserLogo ?>" class="img-circle" alt="User Image">

                                    <p>
                                        <?= $this->session->userdata('sgc_user') ?>
                                        <small><?= $this->session->userdata('sgc_user_nivel') ?></small>
                                    </p>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= base_url('perfil') ?>" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= base_url('welcome/remove_session') ?>" class="btn btn-default btn-flat">Salir</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= $pathUserLogo ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?= $this->session->userdata('sgc_user') ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header"> <b>MENU PRINCIPAL </b></li>
                    


                    <li class="treeview <?= ($paginaDatos->menu == 'articulos') ? 'active' : '' ?>">
                        <a href="#">
                            <i class="fa fa-file-text"></i> <span>Artículos</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?= ($paginaDatos->subMenu == 'articulos-listado-novedades') ? 'active' : '' ?>"><a href="<?= base_url('articulos/novedades') ?>"><i class="fa fa-circle-o"></i> Noticias</a></li>
                            <li class="<?= ($paginaDatos->subMenu == 'articulos-listado-proyectos') ? 'active' : '' ?>"><a href="<?= base_url('articulos/proyectos') ?>"><i class="fa fa-circle-o"></i> Proyectos</a></li>
                            <li class="<?= ($paginaDatos->subMenu == 'articulos-nuevo') ? 'active' : '' ?>"><a href="<?= base_url('articulos/nuevo') ?>"><i class="fa fa-circle-o"></i> Nuevo</a></li>
                        </ul>
                    </li>
                    
                     <li class="treeview <?= ($paginaDatos->menu == 'products') ? 'active' : '' ?>">
                        <a href="#">
                            <i class="fa fa-archive" ></i> <span>productos</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                           
                             <li class="<?= ($paginaDatos->subMenu == 'products-listado') ? 'active' : '' ?>"><a href="<?= base_url('products') ?>"><i class="fa fa-circle-o"></i> Lista de Productos</a></li>
                           
                            <li class="<?= ($paginaDatos->subMenu == 'products-nuevo') ? 'active' : '' ?>"><a href="<?= base_url('products/frm_add_products') ?>"><i class="fa fa-circle-o"></i> Nuevo</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?= ($paginaDatos->menu == 'categorys') ? 'active' : '' ?>">
                        <a href="#">
                            <i class="fa fa-bookmark-o"></i> <span>Marcas</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                           <li class="<?= ($paginaDatos->subMenu == 'categoria-listado') ? 'active' : '' ?>">
                           <a href="<?= base_url('categorys')?>"><i class="fa fa-circle-o"></i> Lista de Marcas</a></li>
                          

                           
                            <li class="<?= ($paginaDatos->subMenu == 'categoria-nuevo') ? 'active' : '' ?>"><a href="<?= base_url('categorys/frm_add_category') ?>"><i class="fa fa-circle-o"></i> Nuevo</a></li>
                        </ul>
                    </li>
                    
                    <li class="treeview <?= ($paginaDatos->menu == 'subcategoria') ? 'active' : '' ?>">
                        <a href="#">
                            <i class="fa fa-bandcamp" ></i> <span>Categoria</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                           
                             <li class="<?= ($paginaDatos->subMenu == 'subcategoria-listado') ? 'active' : '' ?>"><a href="<?= base_url('subcategorys') ?>"><i class="fa fa-circle-o"></i> Lista de Categoria</a></li>
                           
                            <li class="<?= ($paginaDatos->subMenu == 'subcategoria-nuevo') ? 'active' : '' ?>"><a href="<?= base_url('subcategorys/frm_add_subcategorys') ?>"><i class="fa fa-circle-o"></i> Nuevo</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?= ($paginaDatos->menu == 'banners') ? 'active' : '' ?>">
                            <a href="#">
                                <i class="fa fa-file-image-o"></i>
                                <span>Banners</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?= ($paginaDatos->subMenu == 'banners-listado') ? 'active' : '' ?>"><a href="<?= base_url('banners') ?>"><i class="fa fa-circle-o"></i> Ver Banners</a></li>
                                <li class="<?= ($paginaDatos->subMenu == 'banners-nuevo') ? 'active' : '' ?>"><a href="<?= base_url('banners/nuevo') ?>"><i class="fa fa-circle-o"></i> Nuevo Banner</a></li>
                            </ul>
                        </li>
                    <!--end marca-->
                    <li class="header">PERFIL</li>
                    <li class="<?= ($paginaDatos->menu == 'perfil') ? 'active' : '' ?>"><a href="<?= base_url('perfil') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Editar mi perfil</span></a></li>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- scripts -->
        <script>
            $(document).ready(function() {

                /** propiedades por defecto para todos los datatables */
                $.extend( true, $.fn.dataTable.defaults, {
                    paging:   true,
                    ordering: true,
                    info:     true,
                    order: [[ 0, "asc" ]],
                    stateSave: true,
                    pagingType :'full_numbers',

                    language: {
                        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                    },

                    lengthChange: true,
                    searching: true,
                    autoWidth: false,
                } );

                /** idioma español para todos los select2 */
                $.fn.select2.defaults.set('language', 'es');

            });
        </script>


