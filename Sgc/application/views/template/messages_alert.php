<script>
    $(document).ready(function () {
        <?php if ($this->session->flashdata('success_message')): ?>
            swal({
                title: 'Enhorabuena!',
                text: '<?= $this->session->flashdata('success_message') ?>',
                type: 'success',
                confirmButtonText: 'Continuar'
            });
        <?php endif ?>

        <?php if ($this->session->flashdata('error_message')): ?>
            swal({
                title: 'Hubo un error!',
                text: '<?= $this->session->flashdata('error_message') ?>',
                type: 'error',
                confirmButtonText: 'Continuar'
            });
        <?php endif ?>
    })
</script>