<?php

$user = $this->session->userdata('sgc_user');

if ($user == "") {
    $respuesta = "Usted no tiene permiso para estar aquí <br /><a href='" . base_url('welcome') . "'>Ir atrás</a>";
    echo utf8_decode($respuesta);
    die();
}