<meta charset="utf-8" />
<style>
    .ExternalClass a {
        color:#0095dd;
        text-decoration:none !important;
    }

    @media only screen and (max-width: 599px) {
        .ExternalClass body, .ExternalClass table, .ExternalClass td, .ExternalClass p, .ExternalClass a, .ExternalClass li, .ExternalClass blockquote {
        }
        .ExternalClass {
            width:100% !important;
            min-width:100% !important;
        }
        .ExternalClass #ecxbodyCell {
            padding:10px !important;
        }
        .ExternalClass .ecxflexibleContainer {
            width:85% !important;
        }
        .ExternalClass #ecxtemplateContainer {
            max-width:600px !important;
            width:100% !important;
        }
        .ExternalClass h1 {
            font-size:24px !important;
            line-height:100% !important;
        }
    }
</style>

<center>
    <table id="ecxbodyTable" style="background-color:lightgray;width:100% !important;padding:0;height:100% !important;border-collapse:collapse !important;" border="0" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%">
        <tbody>
            <tr>
                <td id="ecxbodyCell" style="width:100% !important;padding:50px;height:100% !important;" align="center" valign="top">
                    <table id="ecxtemplateContainer" style="border:1px solid #DDDDDD;border-bottom-color:#CCCCCC;max-width:500px !important;border-collapse:collapse !important; margin-top: 15px;" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td style="" align="center" valign="top">

                                    <table id="ecxtemplateBody" style="border-bottom:1px solid #CCCCCC;border-top:1px solid #FFFFFF;background-color:#FFFFFF;border-collapse:collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="ecxbodyContent" style="text-align:center;padding-left:20px;padding-bottom:30px;padding-right:20px;padding-top:30px;line-height:150%;font-size:15px;font-family:Helvetica;color:#424F59;" valign="top">
                                                    <h1 style="text-align:center;letter-spacing:normal;line-height:100%;font-weight:normal;font-style:normal;font-size:26px;font-family:Helvetica;display:block;color:#424F59 !important;"><?= $titulo ?></h1>
                                                    <br>
                                                    <p><?= $parrafo_inicial ?></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="" align="center" valign="top">

                                                    <table style="border-collapse:collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr height="47">
                                                                <td style="" align="center" valign="top">

                                                                    <table class="ecxflexibleContainer" style="border-collapse:collapse !important;" border="0" cellpadding="0" cellspacing="0" width="65%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="ecxflexibleContainerCell ecxbottomShim" style="" align="center" valign="top">
                                                                                    <table class="ecxemailButton" style="border-radius:4px;background-color:#0095DD;width:100% !important;border-collapse:separate;" border="0" cellpadding="0" cellspacing="0" width="280">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="ecxbuttonContent" style="text-align:center;line-height:100%;font-weight:normal;font-size:20px;font-family:Helvetica;color:#FFFFFF;" align="center" valign="middle">
                                                                                                    <a href="<?= $enlace ?>" style="text-decoration:none;font-size:20px;font-family:Helvetica;display:block;color:#FFFFFF;padding:15px !important;" target="_blank"><?= $enlace_nombre ?></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="" align="center" valign="top">
                                                    <table style="border-collapse:collapse !important;" border="0" cellpadding="0" cellspacing="0" width="70%">
                                                        <tbody>
                                                            <tr>
                                                                <td class="ecxbodyContent" style="text-align:center;padding-left:20px;padding-bottom:30px;padding-right:20px;padding-top:30px;font-family:Helvetica;color:#8A9BA8;line-height:13px;font-size:11px;" valign="top">
                                                                    Es un correo automatizado, no responda este correo. Si recibiste este correo por error, no es necesario que hagas nada. Si quieres informarte, visita <a href="<?= base_url() ?>" target="_blank">Pro Olivo</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</center>