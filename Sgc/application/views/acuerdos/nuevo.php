<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Informes
            <small>Nuevo informe</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Informe</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <form id="formNuevoArticulo" class="form-horizontal" method="post" action="<?= base_url('acuerdos/guardar_nuevo') ?>" enctype="multipart/form-data">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoArticulo">
                            <li class="active"><a href="#tabInformacion" data-toggle="tab" aria-expanded="true">Información</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabInformacion">
                                <div class="form-group" id="divTipoInforme">
                                    <label for="inputTipoInforme" class="col-sm-2 control-label">Tipo de informe</label>

                                    <div class="col-sm-5">
                                        <select class="form-control select2" style="width: 100%;" id="inputTipoInforme" name="inputTipoInforme">
                                            <option value="consejo directivo">Consejo Directivo</option>
                                            <option value="asamblea de socios">Asamblea de Socios</option>
                                            <option value="reuniones de trabajo">Reuniones de Trabajo</option>
                                            <option value="proyectos">Proyectos</option>
                                            <option value="boletin">Boletín</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputFecha" class="col-sm-2 control-label">Fecha</label>

                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="inputFecha" name="inputFecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputNombre" class="col-sm-2 control-label">Nombre</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputNombre" name="inputNombre" placeholder="Nombre del documento">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputDescripcion" class="col-sm-2 control-label">Descripción</label>

                                    <div class="col-sm-10">
                                        <textarea id="inputDescripcion" name="inputDescripcion" class="textarea" placeholder="Descripción" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAdjunto" class="col-sm-2 control-label">Archivo adjunto</label>

                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="inputAdjunto" name="inputAdjunto" accept=".pdf">
                                        <p class="help-block">Elija un archivo PDF</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Estado</label>

                                    <div class="col-sm-10">
                                        <label style="margin:6px">
                                            <input type="checkbox" id="inputEstado" name="inputEstado" class="minimal" checked> Activo
                                        </label>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-info">Guardar</button>
                                </div>
                            </div>
                            <!-- /.tab-pane fade -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->
<script>
    $(document).ready(function() {
        $('input[name="inputFecha"]').datepicker();

        $('textarea[name="inputDescripcion"]').wysihtml5();

        $('select[name="inputTipo"], select[name="inputTipoInforme"]').select2();

        $('input[name="inputEstado"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });
    });
</script>