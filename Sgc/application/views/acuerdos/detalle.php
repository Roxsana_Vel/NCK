<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detalle del acuerdo
            <!-- <small>Detalle del acuerdo</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Acuerdos</a></li>
            <li class="active">Detalle</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header bg-light-blue color-palette">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title"><strong><?= $datosAcuerdo->ACU_nombre ?></strong></h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table class="table table-bordered">

                            <tbody>
                                <tr>
                                    <th width="20%">Fecha</th>
                                    <td><?= DateTime::createFromFormat('Y-m-d', $datosAcuerdo->ACU_fecha)->format('d/m/Y') ?></td>
                                </tr>
                                <tr>
                                    <th>Descripción</th>
                                    <td class="td-descripcion">
                                        <div style="word-break: break-all !important;">
                                            <?= $datosAcuerdo->ACU_descripcion ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Adjunto</th>
                                    <td>
                                        <?php if ($datosAcuerdo->ACU_adjunto): ?>
                                            <a href="<?= base_url('uploads/acuerdos/' . $datosAcuerdo->ACU_adjunto) ?>" target="_blank"><?= $datosAcuerdo->ACU_adjunto ?></a>
                                        <?php else: ?>
                                            No se registró algún archivo adjunto
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 0">
                                        <?php if ($datosAcuerdo->ACU_adjunto): ?>
                                            <embed src="<?= base_url('uploads/acuerdos/' . $datosAcuerdo->ACU_adjunto) ?>" width="100%" height="400">
                                        <?php endif ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>
    </section>
</div>