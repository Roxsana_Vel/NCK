<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Informes
            <small>Listado de <?= isset($paginaDatos->tipoInforme) ? 'Informes de ' . $paginaDatos->tipoInforme : 'Informes' ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Informes</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Left col -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Listado de <?= isset($paginaDatos->tipoInforme) ? 'Informes de ' . $paginaDatos->tipoInforme : 'Informes' ?></h3>

                        <div class="box-tools pull-right">
                            <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                <a href="<?= base_url('acuerdos/nuevo') ?>" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                            <?php endif ?>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- listado de asociados -->
                        <table id="tablaAsociadosListado" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <?php if ( ! isset($paginaDatos->tipoInforme)): ?>
                                        <th>Tipo</th>
                                    <?php endif ?>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                        <th>Acciones</th>
                                    <?php endif ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($arrayAcuerdos as $key => $acuerdo): ?>
                                    <tr data-acu-id="<?= $acuerdo->ACU_ID ?>">
                                        <td>
                                            <a href="<?= base_url('acuerdos/detalle/'.$acuerdo->ACU_ID) ?>" class="btn-block" data-toggle="tooltip" title="Ver acuerdo" data-placement="left"><?= $acuerdo->ACU_nombre ?></a>
                                        </td>
                                        <?php if ( ! isset($paginaDatos->tipoInforme)): ?>
                                            <td class="text-center"><?= ucfirst($acuerdo->ACU_tipo_informe) ?></td>
                                        <?php endif ?>
                                        <td class="text-center"><?= $acuerdo->ACU_fecha ?></td>
                                        <td class="text-center">

                                            <?php if ($acuerdo->ACU_estado == 1): ?>
                                                <span class="label label-success">Activo</span>

                                                <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                    <a style="margin: 3px" href="<?= base_url('acuerdos/activar_desactivar/'.$acuerdo->ACU_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Desactivar">
                                                        <span class="glyphicon glyphicon-refresh"></span>
                                                    </a>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <span class="label label-danger">Inactivo</span>

                                                <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                    <a style="margin: 3px" href="<?= base_url('acuerdos/activar_desactivar/'.$acuerdo->ACU_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Activar">
                                                        <span class="glyphicon glyphicon-refresh"></span>
                                                    </a>
                                                <?php endif ?>

                                            <?php endif ?>

                                        </td>
                                        <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                            <td class="text-center">

                                                <!-- editar -->
                                                <a href="<?= base_url('acuerdos/editar/'.$acuerdo->ACU_ID) ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Editar">
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                </a>
                                                <!-- eliminar -->
                                                <a href="<?= base_url('acuerdos/eliminar/'.$acuerdo->ACU_ID) ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        <?php endif ?>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->

<script>
    $(document).ready(function() {

        var tablaAsociadosListado = $('#tablaAsociadosListado').DataTable();

        $('#tablaAsociadosListado').on('click', '.btn-danger', function (e) {
            e.preventDefault();

            var $this = $(this);

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está seguro de eliminar este acuerdo?. ¡No se podrá deshacer la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este acuerdo!',
                cancelButtonText: 'No'
            }).then(function () {
                window.location.href = $this.attr('href');
            });
        });
    });
</script>