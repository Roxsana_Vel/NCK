<table id="datatable" class="table table-striped table-bordered bootstrap-datatable dataTable">

					<thead>

						<th>ID</th>

						<th>Titulo</th>				

						<th>Acciones</th>

					</thead> 

					<?php if ($gallery != FALSE) : ?>

					<tbody>

						<?php foreach ($gallery->result() as $row) : ?>

						<tr>

							<td><?php echo $row->GAL_ID; ?></td>

							<td><?php echo $row->GAL_titulo; ?></td>



							<td width="30%">

								<div class="item">

								<a href="<?php echo site_url("gallery/frm_edit_gallery/".$row->GAL_ID."/".$row->PRO_ID); ?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a <?php /* onclick="return confirm('¿Está seguro que desea eliminar?')" href="<?php echo site_url("gallery/delete_gallery_id/".$row->GAL_ID); ?> */?> title="Eliminar" class="btn btn-danger"  id="<?php echo $row->GAL_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>				

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

					<?php else : ?>

                    	<h3>No hay registros en Galería.</h3>

                    <?php endif; ?>

</table>