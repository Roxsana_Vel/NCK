<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />

<link rel="stylesheet" href="<?=base_url().'resources';?>/resources_sgc/css/jquery.wysiwyg.css" type="text/css"/>

<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/jquery.wysiwyg.js"></script>

<script type="text/javascript" src=".<?=base_url().'resources';?>/resources_sgc/js/wysiwyg.image.js"></script>

<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/wysiwyg.link.js"></script>

<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js/wysiwyg.table.js"></script>

<script type="text/javascript">

(function($) {

    $(document).ready(function() {

        $('#descripcion_ima1').wysiwyg();

        $('#descripcion_ima2').wysiwyg();

        $('#descripcion_ima3').wysiwyg();

        $('#descripcion_ima4').wysiwyg();

        $('#descripcion_ima5').wysiwyg();

        $('#descripcion_ima6').wysiwyg();

        $('#descripcion_ima7').wysiwyg();

        $('#descripcion_ima8').wysiwyg();

        $('#descripcion_ima9').wysiwyg();

        $('#descripcion_ima10').wysiwyg();

    });

})(jQuery);

</script>

<div>

    <ul class="breadcrumb">

        <li><a href="<?php echo site_url("welcome");?>">Panel de Control</a><span class="divider">/</span></li>

        <li><a href="<?php echo site_url("gallery");?>">Galería</a><span class="divider">/</span></li>

        <li class="active">Editar</li>

    </ul>

</div>



<div class="row-fluid">

    <div class="span12 widget">

        <div class="widget-header">

            <span class="title">

                <i class="icon-tag icon-white"></i>

                Información del Archivo

            </span>

        </div>

            <div class="widget-content form-container">

                



                <div>



                    <?php if ($gallery!= FALSE): ?>

                    <?php foreach ($gallery->result() as $row): ?>

                  

                     <?php echo form_open_multipart('files/doUploadEdit','class="form-horizontal"');?>



                        <div class="control-group">

                               <label for="Visualizacion" class="control-label">Visualización</label>

                               <span>&nbsp;</span>

                                <div class="controls">                            

                               <img src="<?=base_url()?>resources/resources_web/images/<?php echo $row->GAL_archivo;?>" id="visualizar" alt="Visualizar">

                               

                               </div>

                        </div>



                        <div class="control-group">

                            <label for="Space" class="control-label">&nbsp;</label>

                            <span class="span2"></span>

                            <div class="control">

                                  <?php if($this->session->flashdata('message_upload_error')) {?>

                        <div class="alert <?php echo $this->session->flashdata('message_type_error');?>" style="float:left;width:300px">

                            <?php echo $this->session->flashdata('message_upload_error');?>

                        </div>

                        <?php }?>

                        <?php if($this->session->flashdata('message_upload_success')) {?>

                         <div class="alert <?php echo $this->session->flashdata('message_type_success');?>" style="float:left;width:300px">

                            <?php echo $this->session->flashdata('message_upload_success');?>

                        </div>

                        <?php }?>

                        <?php if($this->session->flashdata('file_data')) {?>

                         <div class="alert <?php echo $this->session->flashdata('file_data_type_info');?>" style="float:left;width:300px">

                            <?php 

                            echo "<ul>";

                            $data_info =$this->session->flashdata('file_data');

                           

                            echo "<li>" .$data_info['file_name']. "</li>";



                            echo "</ul>";



                            ?>

                        </div>

                        <?php }?>

                            </div>

                        </div>



                        <div class="control-group">

                            <div id="DivLoaderImages">



                                            <label for="file">¿Desea Actualizar la Imagen?</label>

                                            <span>&nbsp;</span>

                                            <span>&nbsp;</span>

                                            <span>&nbsp;</span>

                                            <input type="file" id="archivo1" name ="archivo1"/>

                            </div>

                        </div>





                        <div class="control-group">

                            <label for="Titulo">Título</label>

                            <div class="controls">

                                <input name="titulo_ima1" maxlength="100" required="required" class="span6" type="text" id="titulo_ima1" value="<?php echo $row->GAL_titulo;?>"/>

                                <input type="hidden" name="id_archivo" id="id_archivo" value="<?php echo $row->GAL_ID;?>">

                                <input type="hidden" name="id_tip" id="id_tip" value="<?php echo $this->uri->segment(4);?>">

                                

                                

                            </div>

                        </div>



                        <div class="control-group">

                            <label for="Descripcion">Descripción</label>

                            <div class="controls">

                                <textarea name="descripcion_ima1"  required="required" class="span6" type="text" id="descripcion_ima1"><?php echo $row->GAL_descripcion;?></textarea>

                            </div>

                        </div>



                        <div class="control-group">

                            <label for="ArchivoNombre">Nombre del Archivo</label>

                            <div class="controls">

                                <input name="archivo_nom1" required="required" class="span6" type="text" id="archivo_nom1" value="<?php echo $row->GAL_archivo;?>" disabled="disabled"/>

                            </div>

                        </div>





                      <?php /*  <div class="control-group" <?php if($logo==1){ echo ' style="display:none;"'; }?>>

                                        <label class="checkbox">Portada</label>

                                        <span>&nbsp;</span>



                                        <?php$portada = $this->uri->segment(5); ?>

                                        <?php if ($portada) { ?>

                                       

                                        <?php if($portada == 0): ?>

                                       

                                        <input type="checkbox" id="portada_ima1" name="portada_ima1" value="0">

                                        

                                        <?php else:?>

                                         

                                        <input type="checkbox" id="portada_ima1" name="portada_ima1" value="1" checked="">

                                       

                                        <?php endif;?>



                                        <?php } else { ?>

                                         <input type="checkbox" id="portada_ima1" name="portada_ima1" value="1">

                                        <?php } ?>                                      

                         </div>    */ ?>



                        <div class="form-actions">

                            <div class="item">

                            <input type="submit" class="btn btn-primary" id="btnActualizarImagen" value="Guardar">

                            </div>

                            <div class="item">

                                <a href="<?php echo site_url("gallery"); ?>" class="btn btn-info">Cancelar</a>

                            </div>

                        </div>

                   <?php echo form_close(); ?>

                    <?php endforeach;?>             

                    <?php endif;?>      





                </div>

            </div>

    </div>

</div>

<script type="text/javascript">



 $("#btnActualizarImagen").on('click', function(e) {

         if(                       

            isEmpty('#titulo_ima1')==true ||                         

            isEmpty('#descripcion_ima1')==true ||

            isEmpty('#contenido_ima')==true           

           

            ){

                e.preventDefault();

            } 

                  

        }); 



</script>