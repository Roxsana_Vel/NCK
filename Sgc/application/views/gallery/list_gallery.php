<div class="row-fluid">

    <div class="span12">

        <ul class="breadcrumb">

        	<li><a href="<?php echo site_url("welcome");?>">Panel de Control</a>

        		<span class="divider">/</span>

        	</li>

        	<li><span>Galería</span></li>

        </ul><!-- breadcrumbs -->                    

    </div>

</div>



<div class="row-fluid">

	<div class="span12 widget">

		<div class="widget-header">

			<span class="title">

				<i class="icon-tag icon-white"></i>

				Listado de Galería

			</span>

			<span class="label label-success"> <a href="<?php echo site_url('gallery/frm_add_gallery');?>">Agregar</a></span>

		</div>

			<div class="widget-content">

			<!-- Loader-->

			<div id="DivLoader">

				<table id="datatable" class="table table-striped table-bordered bootstrap-datatable dataTable">

					<thead>

						<th>ID</th>

						<th>Titulo</th>				

						<th>Acciones</th>

					</thead> 

					<?php if ($gallery != FALSE) : ?>

					<tbody>

						<?php foreach ($gallery->result() as $row) : ?>

						<tr>

							<td><?php echo $row->GAL_ID; ?></td>

							<td><?php echo $row->GAL_titulo; ?></td>



							<td width="30%">

								<div class="item">

								<a href="<?php echo site_url("gallery/frm_edit_gallery/".$row->GAL_ID."/".$row->PRO_ID); ?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a title="Eliminar" class="btn btn-danger"  id="<?php echo $row->GAL_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>				

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

					<?php else : ?>

                    	<h3>No hay registros en Galería.</h3>

                    <?php endif; ?>

				</table>

			</div>

			<!-- End Loader -->



			</div>

	</div>

</div>

<script type="text/javascript">



$(document).ready(function(){

	

	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).attr("id");

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar esta imagen. <br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',

			'buttons'	: {

				'Sí'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?php echo site_url("gallery/delete_gallery_id/");?>",

						  data: 'eliminarId=' + eliminarId,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            $('#DivLoader').html(resp).show();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});

	

});</script>