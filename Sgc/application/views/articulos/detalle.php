<div id="fb-root"></div>
<script async="async">
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<style>
    .fb-comments {
        width: 100%;
    }
    .fb-comments span, .fb-comments span iframe {
        width: 100% !important;
    }

    .articulo_descripcion {
        max-width: 100% !important;
    }
    .articulo_descripcion * {
        max-width: 100% !important;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Artículo
            <small>Detalle Artículo</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Artículos</a></li>
            <li class="active">Detalle</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header bg-light-blue color-palette">
                        <i class="ion ion-document-text"></i>

                        <h3 class="box-title"><strong><?= $datosArticulo->EVE_titulo ?></strong></h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table class="table table-bordered">
                            <tbody>

                                <tr>
                                    <th width="20%">Fecha</th>
                                    <td><?= DateTime::createFromFormat('Y-m-d', $datosArticulo->EVE_fecha)->format('d/m/Y') ?></td>
                                </tr>
                                <tr>
                                    <th>Visible por</th>
                                    <td><?= $datosArticulo->EVE_visibilidad ?></td>
                                </tr>
                                <tr>
                                    <th>Estado</th>
                                    <td><?= ($datosArticulo->EVE_estado == 1) ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>' ?></td>
                                </tr>
                                <tr>
                                    <th>Resumen</th>
                                    <td><?= $datosArticulo->EVE_resumen ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php
                                            if (is_null($datosArticulo->EVE_imagen)) {
                                                $imagen = 'sin_imagen.png';
                                            } else {
                                                $imagen = $datosArticulo->EVE_imagen;
                                            }
                                        ?>
                                        <img class="img-responsive" src="<?= base_url('uploads/articulos/'.$imagen) ?>" alt="<?= $datosArticulo->EVE_titulo ?>">
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p><strong>Descripción:</strong></p>
                                        <div class="articulo_descripcion">
                                            <?= $datosArticulo->EVE_descripcion ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Adjunto</th>
                                    <td>
                                        <?php if ($datosArticulo->EVE_pdf_adjunto): ?>
                                            <a href="<?= base_url('uploads/articulos/' . $datosArticulo->EVE_pdf_adjunto) ?>" target="_blank"><?= $datosArticulo->EVE_pdf_adjunto ?></a>
                                        <?php else: ?>
                                            No se registró algún archivo adjunto
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 0">
                                        <?php if ($datosArticulo->EVE_pdf_adjunto): ?>
                                            <embed src="<?= base_url('uploads/articulos/' . $datosArticulo->EVE_pdf_adjunto) ?>" width="100%" height="400">
                                        <?php endif ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </section>
</div>
