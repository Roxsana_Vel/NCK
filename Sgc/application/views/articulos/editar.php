<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Artículos
            <small>Editar artículo</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Artículos</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <form id="formNuevoArticulo" class="form-horizontal" method="post" action="<?= base_url('articulos/guardar_edicion/' . $datosArticulo->EVE_ID) ?>" enctype="multipart/form-data">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoArticulo">
                            <li class="active"><a href="#tabInformacion" data-toggle="tab" aria-expanded="true">Información</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabInformacion">

                                <div class="form-group">
                                    <label for="inputTipo" class="col-sm-2 control-label">Tipo</label>

                                    <div class="col-sm-10">
                                        <label style="margin: 6px">
                                            <input type="radio" name="inputTipo" class="flat-red" value="novedad" <?= ($datosArticulo->EVE_tipo == 'novedad') ? 'checked' : '' ?> required /> Novedad
                                        </label>
                                        <label style="margin: 6px">
                                            <input type="radio" name="inputTipo" class="flat-red" value="proyecto" <?= ($datosArticulo->EVE_tipo == 'proyecto') ? 'checked' : '' ?> /> Proyecto
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputTitulo" class="col-sm-2 control-label">Titulo</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputTitulo" name="inputTitulo" placeholder="Título del artículo" value="<?= $datosArticulo->EVE_titulo ?>" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputFecha" class="col-sm-2 control-label">Fecha</label>

                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="inputFecha" name="inputFecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" value="<?= DateTime::createFromFormat('Y-m-d', $datosArticulo->EVE_fecha)->format('d/m/Y') ?>" required />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputResumen" class="col-sm-2 control-label">Resumen</label>

                                    <div class="col-sm-10">
                                        <textarea id="inputResumen" name="inputResumen" class="textarea" placeholder="Resumen" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $datosArticulo->EVE_resumen ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputDescripcion" class="col-sm-2 control-label">Descripción</label>

                                    <div class="col-sm-10">
                                        <textarea id="inputDescripcion" name="inputDescripcion" class="textarea" placeholder="Descripción" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $datosArticulo->EVE_descripcion ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputImagen" class="col-sm-2 control-label">Imagen</label>

                                    <div class="col-sm-10">
                                        <input type="file" id="inputImagen" name="inputImagen" accept=".png, .jpg, .jpeg, .gif" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAdjunto" class="col-sm-2 control-label">Adjuntar Pdf</label>

                                    <div class="col-sm-10">
                                        <input type="file" id="inputAdjunto" name="inputAdjunto" accept=".pdf" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputVisibilidad" class="col-sm-2 control-label">Visibilidad</label>

                                    <div class="col-sm-5">
                                        <select class="form-control select2" style="width: 100%;" id="inputVisibilidad" name="inputVisibilidad" required>
                                            <option value="0" <?= ($datosArticulo->EVE_visibilidad == 'publico') ? 'selected' : '' ?>>Visitantes y Socios</option>
                                            <option value="1" <?= ($datosArticulo->EVE_visibilidad == 'socios') ? 'selected' : '' ?>>Sólo Socios</option>
                                        </select>
                                    </div>
                                </div>

                                <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                    <div class="form-group">
                                        <label for="inputAdjunto" class="col-sm-2 control-label">Estado</label>

                                        <div class="col-sm-10">
                                            <label>
                                                <input type="checkbox" id="inputEstado" name="inputEstado" class="minimal" <?= $datosArticulo->EVE_estado ? 'checked' : '' ?> /> Activo
                                            </label>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-info">Guardar</button>
                                </div>
                            </div>
                            <!-- /.tab-pane fade -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->
<script>
    $(document).ready(function() {

        /** plugins */
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $('input[name="inputFecha"]').datepicker({
            autoclose: true
        });

        $('textarea[name="inputDescripcion"]').wysihtml5();

        $('select[name="inputVisibilidad"]').select2();

    });
</script>