<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Artículos
            <small>Listado de <?= isset($paginaDatos->tipoArticulo) ? $paginaDatos->tipoArticulo : 'Artículos' ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Artículos</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">        
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <!-- Left col -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Listado de <?= isset($paginaDatos->tipoArticulo) ? $paginaDatos->tipoArticulo : 'Artículos' ?></h3>

                        <div class="box-tools pull-right">
                            <a href="<?= base_url('articulos/nuevo') ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- listado de asociados -->
                        <table id="tablaAsociadosListado" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <?php if ( ! isset($paginaDatos->tipoArticulo)): ?>
                                        <th>Tipo</th>
                                    <?php endif ?>
                                    <th>Resumen</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                        <th width="10%">Acciones</th>
                                    <?php endif ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($arrayArticulos as $key => $articulo): ?>
                                    <tr data-art-id="<?= $articulo->EVE_ID ?>">
                                        <td>
                                            <a href="<?= base_url('articulos/detalle/'.$articulo->EVE_ID) ?>" class="btn-block" data-toggle="tooltip" title="Ver artículo" data-placement="left">
                                                <?= $articulo->EVE_titulo ?>
                                            </a>
                                        </td>
                                        <?php if ( ! isset($paginaDatos->tipoArticulo)): ?>
                                            <td><?= ucfirst($articulo->EVE_tipo) ?></td>
                                        <?php endif ?>
                                        <td><?= $articulo->EVE_resumen ?></td>
                                        <td width="70px"><?= $articulo->EVE_fecha ?></td>

                                        <td class="text-center" width="90px">

                                            <?php if ($articulo->EVE_estado == 1): ?>
                                                <span class="label label-success">Activo</span>

                                                <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                    <a style="margin: 3px" href="<?= base_url('articulos/activar_desactivar/'.$articulo->EVE_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Desactivar">
                                                        <span class="glyphicon glyphicon-refresh"></span>
                                                    </a>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <span class="label label-danger">Inactivo</span>

                                                <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                                    <a style="margin: 3px" href="<?= base_url('articulos/activar_desactivar/'.$articulo->EVE_ID) ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Activar">
                                                        <span class="glyphicon glyphicon-refresh"></span>
                                                    </a>
                                                <?php endif ?>

                                            <?php endif ?>

                                        </td>

                                        <?php if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO'): ?>
                                            <td class="text-center">
                                                <a href="<?= base_url('articulos/editar/'.$articulo->EVE_ID) ?>" class="btn btn-info btn-xs" data-toggle="tooltip" title="Editar">
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                </a>
                                                <a href="<?= base_url('articulos/eliminar/'.$articulo->EVE_ID) ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        <?php endif ?>

                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>

<!-- ******* -->
<!-- scripts -->
<!-- ******* -->

<script>
    $(document).ready(function() {

        var tablaAsociadosListado = $('#tablaAsociadosListado').DataTable();

        $('#tablaAsociadosListado').on('click', '.btn-danger', function (e) {
            e.preventDefault();

            var $this = $(this);

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está seguro de eliminar a este artículo?. ¡No se podrá deshacer la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este artículo!',
                cancelButtonText: 'No'
            }).then(function () {
                window.location.href = $this.attr('href');
            });
        });
    });
</script>