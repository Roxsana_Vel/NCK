<!-- slogan -->
<div class="slogan">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">ProOlivo - Noticias</h3>
    </div>
</div>

<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Últimas Noticias</h2>
            </div>
        </div>

        <div class="row">
            <div class="tarjeta--noticia wow fadeInUp">
                <div class="col-xs-3 noticia_img">
                    <img src="<?= base_url('dist/img/uploads/noticias/noticia1.JPG') ?>" alt="Lanzamiento Proyecto Agenda de Innovación Tecnológica">
                </div>
                <div class="col-xs-9 noticia_resumen">
                    <h4>Lanzamiento Proyecto Agenda de Innovación Tecnológica</h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>Viernes 27 de Noviembre del 2015</span>
                    <p>El 27 de noviembre en las instalaciones de la Cámara de Comercio de Tacna se realizó el lanzamiento del Proyecto "Agenda de Innovación Tecnológica para el Desarrollo Técnico y Comercial del Olivo", proyecto ejecutado por nuestra institución con financiamiento de INNOVATE PERU - Programa Nacio...</p>
                    <div class="text-right">
                        <a class="btn btn-success" href="#!">LEER MÁS</a>
                    </div>
                </div>
            </div>

            <div class="tarjeta--noticia wow fadeInUp">
                <div class="col-xs-3 noticia_img">
                    <img src="<?= base_url('dist/img/uploads/noticias/noticia1.JPG') ?>" alt="Lanzamiento Proyecto Agenda de Innovación Tecnológica">
                </div>
                <div class="col-xs-9 noticia_resumen">
                    <h4>Lanzamiento Proyecto Agenda de Innovación Tecnológica</h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>Viernes 27 de Noviembre del 2015</span>
                    <p>El 27 de noviembre en las instalaciones de la Cámara de Comercio de Tacna se realizó el lanzamiento del Proyecto "Agenda de Innovación Tecnológica para el Desarrollo Técnico y Comercial del Olivo", proyecto ejecutado por nuestra institución con financiamiento de INNOVATE PERU - Programa Nacio...</p>
                    <div class="text-right">
                        <a class="btn btn-success" href="#!">LEER MÁS</a>
                    </div>
                </div>
            </div>

            <div class="tarjeta--noticia wow fadeInUp">
                <div class="col-xs-3 noticia_img">
                    <img src="<?= base_url('dist/img/uploads/noticias/noticia1.JPG') ?>" alt="Lanzamiento Proyecto Agenda de Innovación Tecnológica">
                </div>
                <div class="col-xs-9 noticia_resumen">
                    <h4>Lanzamiento Proyecto Agenda de Innovación Tecnológica</h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>Viernes 27 de Noviembre del 2015</span>
                    <p>El 27 de noviembre en las instalaciones de la Cámara de Comercio de Tacna se realizó el lanzamiento del Proyecto "Agenda de Innovación Tecnológica para el Desarrollo Técnico y Comercial del Olivo", proyecto ejecutado por nuestra institución con financiamiento de INNOVATE PERU - Programa Nacio...</p>
                    <div class="text-right">
                        <a class="btn btn-success" href="#!">LEER MÁS</a>
                    </div>
                </div>
            </div>

            <div class="tarjeta--noticia wow fadeInUp">
                <div class="col-xs-3 noticia_img">
                    <img src="<?= base_url('dist/img/uploads/noticias/noticia1.JPG') ?>" alt="Lanzamiento Proyecto Agenda de Innovación Tecnológica">
                </div>
                <div class="col-xs-9 noticia_resumen">
                    <h4>Lanzamiento Proyecto Agenda de Innovación Tecnológica</h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>Viernes 27 de Noviembre del 2015</span>
                    <p>El 27 de noviembre en las instalaciones de la Cámara de Comercio de Tacna se realizó el lanzamiento del Proyecto "Agenda de Innovación Tecnológica para el Desarrollo Técnico y Comercial del Olivo", proyecto ejecutado por nuestra institución con financiamiento de INNOVATE PERU - Programa Nacio...</p>
                    <div class="text-right">
                        <a class="btn btn-success" href="#!">LEER MÁS</a>
                    </div>
                </div>
            </div>


            <div class="text-center">
                <nav aria-label="Page navigation" class="wow fadeInUp">
                    <ul class="pagination">
                        <li>
                        <a href="#!" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="active"><a href="#!">1</a></li>
                        <li><a href="#!">2</a></li>
                        <li><a href="#!">3</a></li>
                        <li><a href="#!">4</a></li>
                        <li><a href="#!">5</a></li>
                        <li>
                        <a href="#!" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>

    </div>
</div>
