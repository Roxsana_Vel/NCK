<!-- slogan -->
<div class="slogan">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">ProOlivo - La empresa</h3>
    </div>
</div>

<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Acerca de Nosotros</h2>
                <p class="wow fadeInUp" data-wow-delay="0.5s">Somos una organización privada sin fines de lucro representativa del sector olivícola peruano, conformada por empresas peruanas procesadoras y/o exportadoras de aceituna de mesa, aceite de oliva y derivados, comprometidos con el desarrollo del sector, la agroindustria y el país.
                Pro olivo es una organización que facilita y promueve el desarrollo gremial y aumento de la competitividad del sector olivícola peruano, fortaleciendo la cultura empresarial de sus asociados y fortaleciendo la cadena productiva del olivo. Para ello trabajamos en tres líneas estratégicas: gestión e incidencia ante entidades públicas y privadas, asesoría técnica en i+I+D (producción y transformación) y promoción comercial.</p>
            </div>
        </div>
    </div>
</div>

<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <p class="text-center">Procesos de producción de olivo en Tacna</p>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/eKsRXmZ8TYU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>