<!-- slogan -->
<div class="slogan">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">ProOlivo - Información del Sector</h3>
    </div>
</div>

<!-- seccion de mision y vision -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <h2 class="text-center wow fadeInUp">Información del Sector</h2>
            <div class="col-xs-12">
                <h3 class="text-center wow fadeInUp">Estacionalidad de la Cosecha</h3>
                <div class="table-responsive wow flipInX">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Estación / Season</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Aug</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dec</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-center">Aceituna Negra / Black Olives</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><img src="<?= base_url('dist/img/aceituna_negra.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_negra.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_negra.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_negra.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_negra.png') ?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th class="text-center">Aceituna Verde / Green Olives</th>
                                <td></td>
                                <td></td>
                                <td><img src="<?= base_url('dist/img/aceituna_verde.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_verde.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceituna_verde.png') ?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th class="text-center">Aceite de Oliva / Olive Oil</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><img src="<?= base_url('dist/img/aceite_oliva.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceite_oliva.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceite_oliva.png') ?>"></td>
                                <td><img src="<?= base_url('dist/img/aceite_oliva.png') ?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="wow fadeInUp">En la actualidad se cultivan aprox. 20,000 hectáreas de olivos a nivel nacional, con un rendimiento promedio de 8,000 kg. de aceituna por hectárea.  El 20% de la producción peruana es destinado a la elaboración de aceite de oliva y el 80% al proceso de la aceituna de mesa.  Hoy en día a raíz del crecimiento y desarrollo productivo del sector, se está logrando la incorporación nuevas variedades en el cultivo.</p>
            </div>
        </div>
    </div>
</div>




<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <p class="text-center">Procesos de producción de olivo en Tacna</p>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/eKsRXmZ8TYU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>