<!-- slogan -->
<div class="slogan">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">ProOlivo - Nuestros Valores</h3>
    </div>
</div>

<!-- seccion de mision y vision -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center wow fadeInUp">Valores</h2>
                <ul>
                    <li>Responsabilidad: Comprometidos con el desarrollo de nuestros asociados, de la institución y del país. </li>
                    <li>Credibilidad y Confianza: Garantizamos la transparencia de la gestión y planificación participativa en nuestra organización.</li>
                    <li>Sostenibilidad: Fomentando la sostenibilidad económica, ambiental y social de nuestra asociación.</li>
                    <li>Trabajo en equipo y en red: Comprometidos con la cooperación entre los asociados y otras instituciones.</li>
                    <li>Eficiencia y eficacia: Logrando los objetivos de forma rápida y económica.</li>
                    <li>Cambio y dinamismo: con capacidad de adaptarse a las dinámicas de los mercados. </li>
                </ul>

            </div>
        </div>
    </div>
</div>




<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <p class="text-center">Procesos de producción de olivo en Tacna</p>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/eKsRXmZ8TYU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>