<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/gallery.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/sceditor/default.min.css" media="screen" />
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/jquery.sceditor.xhtml.min.js"></script>
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/jquery.sceditor.es.min.js"></script>
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/angular.min.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Producto
            <small>Nueva Producto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Producto</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>
    <div class="content">
    <div class="row" ng-app>
        <div class="span12 widget">
           <div class="col-xs-12">
            <!-- Accion -->
            <div class="widget-header">
            <span class="title">
                <i class="icon-tag icon-white"></i>
               Detalles de Producto
            </span>
        </div>
        <div class="widget-content form-container">
            <br>
            <div class="nav-tabs-custom">
          <br>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#producto" data-toggle="tab">Información</a></li>
                <li><a href="#foto" data-toggle="tab">Galería</a></li>
            </ul> <br>
            <!-- Contenido de las pestañas -->
            <?php
                $atributos = array('class' => 'form-horizontal', 'id' => 'form');
                if ($products !== FALSE):
                    $row_prod = $products->row_object();
                    $row_prec = $price->row_object();
                   /* $row_prec_a = $price_mayor_a->row_object();
                    $row_prec_b = $price_mayor_b->row_object();
                    $row_prec_c = $price_mayor_c->row_object();*/
            ?>
            <?php echo form_open_multipart('files/edit_products',$atributos); ?>
            <input type="hidden" name="id_prod" value="<?php echo $row_prod->PRO_ID; ?>" />
            <input type="hidden" name="id_prec" value="<?php echo $row_prec->PRE_ID; ?>" />
            <!--<input type="hidden" name="id_prec_a" value="<?php echo $row_prec_a->PRE_ID; ?>" />
            <input type="hidden" name="id_prec_b" value="<?php echo $row_prec_b->PRE_ID; ?>" />
            <input type="hidden" name="id_prec_c" value="<?php echo $row_prec_c->PRE_ID; ?>" />-->
            <input type="hidden" name="id_category_ant" value="<?php echo $row_prod->CAT_ID; ?>" />
            <input type="hidden" name="orden_prod" value="<?php echo $row_prod->PRO_orden; ?>" />
            <div class="tab-content">
              <div class="tab-pane fade in active" id="producto">
                <?php if ($categorys != FALSE) : ?>
                 <div class="row">
                  <div class="control-group">
                    <label for="categoria_pro" class="col-xs-2 control-label"  >Marca *</label>
                    <div class="controls col-xs-5">
                      <select name="categoria_pro" id="categoria_pro" class="form-control select2" required='required'>
                        <option value='' >--Seleccione--</option>
                        <?php foreach ($categorys->result() as $row_cat) : ?>
                          <option value="<?php echo $row_cat->CAT_ID;?>" <?php echo ($row_cat->CAT_ID == $row_prod->CAT_ID ? 'selected' : ''); ?>> <?php echo $row_cat->CAT_nombre;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  </div> <br>
                  <?php else :?>
                    <div class="control-group">
                      <label for="categoria_pro" class="col-xs-2 control-label">Marca *</label>
                      <div class="controls col-xs-5">
                        <select name="categoria_pro" id="categoria_pro" class="form-control select2" required='required'>
                          <option value=''>--Seleccione--</option>
                        </select>
                      </div>
                      <span class="span3"></span>
                      <div class="alert span4" style="margin-top:10px;">
                        <!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
                        <i class=" icon-info-sign icon-blue"></i>
                        No existen categorías registradas. Debe insertar una categoría previamente a un producto.
                        <a href="<?php echo site_url('categorys/frm_add_category');?>">Ir al formulario agregar categoría</a>
                      </div>
                    </div>
                  <?php endif; ?>
                    <div class="row">
                    <div class="control-group">
                        <label for="subcategoria_pro" class="col-xs-2 control-label">Categoría *</label>
                        <div class="controls col-xs-5">
                            <select name="subcategoria_pro" id="subcategoria_pro" class="form-control select2" required>
                                <option value="">--Seleccione--</option>
                                <?php if ($subcategorys != false): ?>
                                  <?php foreach ($subcategorys->result() as $row_scat) : ?>
                                    <option value="<?= $row_scat->SCAT_ID ?>" <?= ($row_scat->SCAT_ID == $row_prod->SCAT_ID ? 'selected' : ''); ?>> <?= $row_scat->SCAT_nombre ?></option>
                                  <?php endforeach ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    </div> <br>
                    <div class="row">
                    <div class="control-group">
                        <label for="nombre_pro" class="col-xs-2 control-label">Nombre *</label>
                        <div class="controls col-xs-5">
                            <input name="nombre_pro" required value="<?php echo $row_prod->PRO_nombre; ?>" class="form-control" type="text" id="nombre_pro" />
                        </div>
                    </div>
                    </div> <br>
                    <div class="row">
                    <div class="control-group">
                    <label for="cliente_soles" class="col-xs-2 control-label">Precio a Clientes</label>
                    <div class="input-prepend input-append col-xs-3">
                      <span class="add-on">S/.</span>
                      <input name="cliente_soles" required class="span7 text-right form-control" type="text" id="cliente_soles" value="<?php echo number_format($row_prec->PRE_soles, 2, '.', ''); ?>" />
                    </div>
                  </div> 
                  </div> <br>
                  <div class="row">
                  <div class="control-group ">
                    <label for="cliente_soles" class="control-label col-xs-2">Stock</label>
                    <div class="controls col-xs-3">
                      <input name="stock" class="form-control" type="text" id="stock" value="<?php echo $row_prod->PRO_stock; ?>" required />
                    </div>
                  </div>
                   </div><br>
                    <div class="row">
                      <div class="control-group">
                        <label for="descripcion_pro " class="col-xs-2 control-label">Descripción *</label>
                        <div class="controls col-xs-5">
                            <textarea name="descripcion_pro" required="required" class="form-control" type="text" id="descripcion_pro" /><?php echo $row_prod->PRO_descripcion; ?></textarea>
                        </div>
                      </div>
                      
                    </div> <br>
                    <div class="row">
                    <div class="control-group">
                      <label for="estado_pro " class="col-xs-2 control-label">Publicado</label>
                      <div class="controls col-xs-3">
                        <select name="estado_pro" id="estado_pro" class="form-control select2">
                          <option value="1" <?php echo ($row_prod->PRO_estado == '1' ? 'selected' : ''); ?>>Sí</option>
                          <option value="0" <?php echo ($row_prod->PRO_estado == '0' ? 'selected' : ''); ?>>No</option>
                        </select>
                      </div>
                    </div>
                    </div> <br>
                    <div class="row">
                    <div class="control-group">
                      <label for="destacado_pro" class="col-xs-2 control-label">Destacado</label>
                      <div class="controls col-xs-3">
                        <select name="destacado_pro" id="destacado_pro" class="form-control ">
                          <option value="1" <?php echo ($row_prod->PRO_destacado == '1' ? 'selected' : ''); ?>>Sí</option>
                          <option value="0" <?php echo ($row_prod->PRO_destacado == '0' ? 'selected' : ''); ?>>No</option>
                        </select>
                      </div>
                    </div>
                    </div>

                    <?php if ( ! empty($sellos)): ?>
                        <div class="control-group">
                            <label for="destacado_pro">Sello para las imágenes del producto</label>
                            <div class="controls">
                                <span>
                                    <input type="radio" name="sello" value="" class="radio" checked>&nbsp;Sin sello&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>

                                <?php foreach ($sellos as $sello): ?>
                                    <span>
                                        <input type="radio" name="sello" value="<?= $sello->id_sello ?>" class="radio" <?= ($sello->id_sello == $row_prod->id_sello) ? 'checked' : '' ?>>&nbsp;<?= $sello->nombre_sello ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                <?php endforeach ?>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="alert">
                      <p><i class="icon-ok-sign"></i> <strong>Los campos con (*) son requeridos</strong><span>.</span></p>
                    </div>

                </div>
                
                <div class="tab-pane fade" id="foto" ng-controller="GaleriaCtrl">
                  <div class="row gallery-container gallery-portrait col-xs-8 col-xs-offset-2">
                    <div class="span6 offset3">
                      <div class="gallery-item gallery-browser" ng-repeat="gal in saved">
                        <img ng-src="{{gal.url}}" />
                        <div class="gallery-close-img" title="Eliminar" ng-click="removeSaved($index)">&times;</div>
                        <input type="hidden" name="gallery[]" value="{{gal.id}}" />
                      </div>
                      <div class="gallery-item gallery-browser" ng-repeat="url in items">
                        <img ng-src="{{url}}" onload="window.URL.revokeObjectURL(this.src)" />
                        <div class="gallery-close-img" title="Eliminar" ng-click="removeItem($index)">&times;</div>
                      </div>
                      <input name="images[]" type="file" accept="image/*" onchange="changeFile(this, 500, 710)" />
                      <span class="gallery-item gallery-add" rel="tooltip" data-placement="top" data-original-title="Seleccionar un archivo">
                        <div>
                          <span>+</span>
                          <span>Agregar</span>
                        </div>
                      </span>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-xs-6 col-xs-offset-2">
                  <div class="alert alert-info ">
                      <i class=" icon-info-sign icon-blue"></i>
                      Sólo se aceptan imágenes en formato jpg/png/gif.<br />
                      <i class=" icon-info-sign icon-blue" style="opacity:0"></i>
                      Se recomiendan dimensiones de 500 de ancho x 710 de alto.
                  </div>
                  </div>


                </div>

            </div>
            <div class="form-actions" align="center">
              <div class="item">
                  <input type="submit" class="btn btn-primary" id="btnGuardar" value="Guardar" />
              </div>
               <div class="item">
                    <a href="<?php echo site_url("products");?>" class="btn btn-info">Cancelar</a>
              </div>
            </div>
            <?php  echo form_close(); ?>
            <?php endif; ?>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
</div>
<script type="text/javascript">

    $('#descripcion_pro').sceditor({
        plugins: "xhtml",
        toolbar: "bold,italic,underline|left,center,right,justify|quote,font,size,color|youtube,link",
        emoticons: false,
        height: 150,
        style: "<?php echo base_url(); ?>resources/resources_sgc/css2/jquery.sceditor.default.min.css"
    });
    function GaleriaCtrl($scope) {
        $scope.items = ['#'];
        $scope.saved = [
        <?php
            if ($gallery !== FALSE) {
                foreach ($gallery->result() as $row_gal) {
        ?>
            {'id': '<?php echo $row_gal->GAL_ID; ?>', 'url': '<?php echo base_url() . '../images/products/standard/' . $row_gal->GAL_archivo; ?>'},
        <?php
                }
            }
        ?>
        ];
        $scope.setItem = function (url) {
            $scope.items[$scope.items.length - 1] = url;
            $scope.items.push('#');
            jQuery('<input name="images[]" type="file" accept="image/*" onchange="changeFile(this, 500, 710)" />').insertBefore(jQuery('.gallery-add'));
        };
        $scope.removeItem = function (index) {
            $scope.items.splice(index, 1);
            jQuery('.gallery-container input[type="file"]:eq(' + index + ')').remove();
        };
        $scope.removeSaved = function (index) {
            $scope.saved.splice(index, 1);
        };
    }

    $('#categoria_pro').change(function () {
        var categoria = $(this).val();

        $.ajax({
            type: "POST",
            url: "<?= base_url('subcategorys/ajax_by_category') ?>/" + categoria,
            beforeSend: function () {
                $('#subcategoria_pro').empty().append("<option value='0'>--Ninguna--</option>").attr('disabled', true);
            },
            success: function (data) {
                var json = JSON.parse(data);

                if (json.length > 0) {
                    $('#subcategoria_pro').html("<option value='-1' selected>--Seleccione--</option>").attr('disabled', false);

                    for (var i = 0; i < json.length; i++) {
                        $('#subcategoria_pro').append("<option value='" + json[i].SCAT_ID + "'>" + json[i].SCAT_nombre + "</option>");
                    }
                }
            }
        });
    });
</script>
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/gallery.js"></script>