<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Productos
            <small>Productos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Productos</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>
<div class="content">
<div class="row">
    <div class="col-xs-12">
       <div class="box box-primary">
        <div class="box-header with-border">
           <i class="ion ion-clipboard"></i>
            <h3 class="box-title">Listado de <?= isset($paginaDatos->tipoArticulo) ? $paginaDatos->tipoArticulo : 'Artículos' ?></h3>

                <div class="box-tools pull-right">
                    <a href="<?= base_url('products/frm_add_products') ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                </div>
        </div>

        <div class="box-body">
            <!-- loader -->
            <div id="DivLoader">
                <table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">
                    <thead >
                        <th class="th_padding" width="5%">NRO</th>
                        <th class="th_padding flechita" width="30%">NOMBRE</th>
                        <th class="th_padding flechita" width="15%">CATEGORÍA</th>
                        <th class="th_padding" width="10%">ESTADO</th>
                        <th class="th_padding flechita" width="10%">PRECIO</th>
                        <th class="th_padding flechita" width="10%">STOCK</th>
                        <th class="th_padding" width="20%">ACCIONES</th>
                    </thead>

                    <?php if ($products != false): ?>
                        <tbody>
                            <?php $count = 0 ?>
                            <?php foreach ($products->result() as $row): ?>
                                <tr>
                                    <td class="td_padding"><?= ++$count ?></td>
                                    <td class="td_padding"><?= $row->PRO_nombre ?></td>
                                    <td class="td_padding"><?= $row->CAT_nombre ?></td>
                                    <td class="td_padding">
                                        <?php if ($row->PRO_estado): ?>
                                            <div id="estado<?= $row->PRO_ID ?>" style="float:left">
                                                <img width="32" src="<?= base_url('resources/resources_sgc/images/button-check_green.png') ?>" />
                                                <a href="#" title="<?= ($row->PRO_estado == 1) ? 'No publicar' : 'Publicar' ?>" >
                                                    <img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?= base_url('resources/resources_sgc/images/change_2.png') ?>" onclick="cargar_estado('<?= base_url('products/DivLoader?pro_id=' . $row->PRO_ID . '&estado=0') ?>','estado<?= $row->PRO_ID ?>'); return false" />
                                                </a>
                                            </div>
                                        <?php else: ?>
                                            <div id="estado<?= $row->PRO_ID ?>" style="float:left">
                                                <img width="32" src="<?= base_url('resources/resources_sgc/images/button-cross_red.png') ?>" />
                                                <a href="#" title="<?= ($row->PRO_estado == 1) ? 'No publicar' : 'Publicar' ?>" >
                                                    <img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?= base_url('resources/resources_sgc/images/change_2.png') ?>" onclick="cargar_estado('<?= base_url('products/DivLoader?pro_id=' . $row->PRO_ID . '&estado=1') ?>','estado<?= $row->PRO_ID ?>'); return false" />
                                                </a>
                                            </div>
                                        <?php endif ?>
                                    </td>
                                    <td class="td_padding"><?= number_format($row->PRE_soles, 2, '.', '') ?></td>
                                    <td class="td_padding"><?= $row->PRO_stock ?></td>
                                    <td>
                                        <div class="">
                                            <a href="<?= base_url('products/frm_edit_products/' . $row->PRO_ID . '/' . $row->CAT_ID) ?>" title="Editar"
                                               toggle="tooltip"
                                                class="btn btn-info btn-xs">
                                                <i class="glyphicon glyphicon-pencil"></i> Editar
                                            </a>
                                        </div>

                                        <div class="">
                                            <a title="Eliminar"
                                               toggle="tooltip" class="btn btn-danger btn-xs" id="<?= $row->PRO_ID ?>">
                                                <i class="glyphicon glyphicon-trash"></i> Eliminar
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    <?php endif ?>
                </table>
            </div>
            <!-- End loader -->
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#DivLoader').on('click', '.btn-danger', function () {
            var eliminarId = $(this).attr("id");

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está usted seguro de eliminar este producto. Eliminará la imagen, los Pedidos y Compras asociados al Producto. ¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar este producto!',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('products/delete_products_id') ?>",
                    data: 'eliminarId=' + eliminarId,
                    beforeSend: function () {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?= base_url('resources/resources_sgc/images/loader.gif') ?>" alt="Loader"></div>');
                    },
                    success: function (resp) {
                        swal({
                            title: 'Eliminado!',
                            text: 'El producto seleccionado ha sido eliminado.',
                            type: 'success',
                            confirmButtonText: 'Continuar'
                        });

                        $('#DivLoader').html(resp).show();
                    }
                });
            });
        });

        if ($('#categoria_pro').val() != '-1') {
            $('#categoria_pro').trigger('change');
        }
    });
</script>

<?php $this->load->view('template/messages_alert') ?>