<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead>

						<th class="th_padding">ID</th>

						<th class="th_padding">NOMBRE</th>						

						<th class="th_padding">PUBLICADO</th>

						<th class="th_padding">ORDEN</th>

						<th class="th_padding">CAMBIAR ORDEN</th>

						<th class="th_padding">PRECIO</th>						

						<th class="th_padding">ACCIONES</th>

					</thead> 

					<?php if ($products != FALSE) : ?>

					<tbody>

						<?php foreach ($products->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo $row->PRO_orden; ?></td>

							<td class="td_padding"><?php echo $row->PRO_nombre; ?></td>

							<td width="8%" class="td_padding">

									<?php if($row->PRO_estado): ?>

										<div id="estado<?php echo $row->PRO_ID;?>" style="float:left">

										<img width="32" src="<?php echo base_url().'resources/resources_sgc/images/button-check_green.png' ?>" />

										<a href="#" title="<?php if($row->PRO_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/images/change_2.png' ?>" onclick="cargar_estado('<?=site_url("products")?>/DivLoader?pro_id=<?php echo $row->PRO_ID; ?>&estado=0','estado<?php echo $row->PRO_ID; ?>')" /></a>

										</div>

								<?php else: ?> 

										<div id="estado<?php echo $row->PRO_ID;?>"  style="float:left">

										<img width="32" src="<?php echo base_url().'resources/resources_sgc/images/button-cross_red.png' ?>"  />

										<a href="#" title="<?php if($row->PRO_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/images/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("products")?>/DivLoader?pro_id=<?php echo $row->PRO_ID; ?>&estado=1','estado<?php echo $row->PRO_ID; ?>')" /></a>

										</div>

								<?php endif;?>		

	

							</td>

							<td class="td_padding"><?php echo $row->PRO_orden; ?> </td>

							<td class="td_padding"> 

								<?php if ($row->PRO_orden < $products->num_rows()) : ?>

								<a title="Bajar" href="javascript:cargar_producto('<?php echo site_url("products")?>/bajar/<?php echo $row->PRO_ID; ?>/<?php echo $row->PRO_orden; ?>/<?php echo $row->CAT_ID; ?>','DivLoader')"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/images/navigation-down-button_green.png" /></a>

								<?php endif;?>

								<?php if ($row->PRO_orden != 1) : ?>

								<a title="Subir" href="javascript:cargar_producto('<?php echo site_url("products")?>/subir/<?php echo $row->PRO_ID; ?>/<?php echo $row->PRO_orden; ?>/<?php echo $row->CAT_ID; ?>','DivLoader')"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/images/navigation-up-button_green.png" /></a>

								<?php endif; ?>

							</td>

							<td class="td_padding">0.00<?php #echo $row->PRO_precio; ?></td>

							<td width="30%">
								<div class="item">
								<a href="<?php echo site_url("products/frm_edit_products/".$row->PRO_ID."/".$row->CAT_ID."/".$row->PRO_destacado."/".$row->PRO_estado);?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>
								</div>
								<div class="item">
								<a  title="Eliminar" class="btn btn-danger" id="<?php echo $row->PRO_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>				
								</div>
							</td>
						</tr>

						<?php endforeach; ?>

					</tbody>

					<?php else : ?>

                    	<h4>No hay registros de Productos.</h4>

                    <?php endif; ?>

				</table>

				

		<script type="text/javascript">



$(document).ready(function(){

	

	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).attr("id");

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar este producto. <br />Eliminará la imagen, los Pedidos y Compras asociados al Producto.<br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',

			'buttons'	: {

				'Sí'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("products/delete_products_id")?>",

						  data: 'eliminarId=' + eliminarId,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/images/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            $('#DivLoader').html(resp).show();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});

	

});</script>