<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/gallery.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/sceditor/default.min.css" media="screen" />
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/jquery.sceditor.xhtml.min.js"></script>
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/jquery.sceditor.es.min.js"></script>
<script type="text/javascript" src="<?=base_url().'resources';?>/resources_sgc/js2/angular.min.js"></script>

<div class="content-wrapper">
<section class="content-header">
        <h1>
            Producto
            <small>Nueva Producto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Producto</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>

<div class="content">
<div class="row" ng-app>
    <div class="span12 widget">
        <!-- Accion -->
        <div class="col-xs-12">
        <div class="widget-header">
            <span class="title">
                <i class="icon-tag icon-white"></i> Detalles de Producto
            </span>
        </div>

        <div class="widget-content form-container">
            <br>
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs bg-gray">
                <li class="active">
                    <a href="#producto" data-toggle="tab">Información</a>
                </li>
                <!--<li>
                    <a href="#precio" data-toggle="tab">Venta</a>
                </li>-->
                <li>
                    <a href="#foto" data-toggle="tab">Galería</a>
                </li>
            </ul>

            <!-- Contenido de las pestañas -->
            <?php $atributos = array('class' => 'form-horizontal', 'id' => 'form') ?>
            <?= form_open_multipart('files/add_products', $atributos) ?>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="producto">
                        <?php if ($categorys != FALSE) : ?>
                            <div class="row ">
                                <label for="categoria_pro" class="col-xs-2 control-label">Marca *</label>
                                <div class="controls col-xs-5">
                                    <select name="categoria_pro" id="categoria_pro" class="span3 form-control select2" required>
                                        <option value="">--Seleccione--</option>
                                        <?php foreach ($categorys->result() as $row) : ?>
                                            <option value="<?= $row->CAT_ID ?>"><?= $row->CAT_nombre ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="row">
                                <label for="categoria_pro" class="col-xs-2 control-label">Categoría *</label>
                                <div class="controls col-xs-5">
                                    <select name="categoria_pro" id="categoria_pro" class="span3 form-control select2" required>
                                        <option value=''>--Seleccione--</option>
                                    </select>
                                </div>

                                <span class="span3"></span>
                                <div class="alert span4" style="margin-top:10px;">
                                    <i class=" icon-info-sign icon-blue"></i> No existen categorías registradas. Debe insertar una categoría previamente a un producto.
                                    <a href="<= base_url('categorys/frm_add_category') ?>">Ir al formulario agregar categoría</a>
                                </div>
                            </div>
                        <?php endif ?>

                        <div class="row row-padding-top">
                            <label for="subcategoria_pro" class="col-xs-2 control-label">Categoría *</label>
                            <div class="controls col-xs-5">
                                <select name="subcategoria_pro" id="subcategoria_pro" class="span3 form-control select2" required>
                                    <option value="">--Seleccione--</option>
                                </select>
                            </div>
                        </div>

                        <div class="row row-padding-top">
                            <label for="nombre_pro" class="col-xs-2 control-label">Nombre *</label>
                            <div class="controls col-xs-5">
                                <input name="nombre_pro" class="span4 form-control" type="text" id="nombre_pro" required />
                            </div>
                        </div>
                         <div class="row">
                            <label for="cliente_soles"  class="col-xs-2 control-label">Precio a Clientes</label>
                            <div class="input-prepend input-append span2">
                            <div class="col-xs-5">
                                <span class="add-on">S/.</span>
                            
                                <input name="cliente_soles" class="span7 text-right form-control" type="text" id="cliente_soles" required />
                            </div>
                            </div>
                        </div>
                        <div class="row row-padding-top">
                            <label for="cliente_soles" class="col-xs-2 control-label">Stock inicial</label>
                            <div class="controls col-xs-5">
                                <input name="stock" class="span3 form-control" type="text" id="stock" value="0" required />
                            </div>
                        </div>

                        <div class="row row-padding-top">
                            <label for="Descripción" class="col-xs-2 control-label">Descripción *</label>
                            <div class="controls col-xs-5">
                                <textarea name="descripcion_pro" class=" form-control" type="text" id="descripcion_pro" required /></textarea>
                            </div>
                        </div>

                        <div class="row row-padding-top">
                            <label for="estado_pro" class="col-xs-2 control-label">Publicado</label>
                            <div class="controls col-xs-5">
                                <select name="estado_pro" id="estado_pro" class="span2 form-control ">
                                    <option value="1" selected>Sí</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="row row-padding-top">
                                    <label for="destacado_pro" class="col-xs-2 control-label">Destacado</label>
                            <div class="controls col-xs-5">
                                <select name="destacado_pro" id="destacado_pro" class="span2 form-control">
                                    <option value="1">Sí</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>

                        <!--<?php if ( ! empty($sellos)): ?>
                            <div class="row row-padding-top">
                                <label for="destacado_pro">Sello para las imágenes del producto</label>
                                <div class="controls">
                                    <span>
                                        <input type="radio" name="sello" value="" class="radio" checked>&nbsp;Sin sello&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>

                                    <?php foreach ($sellos as $sello): ?>
                                        <span>
                                            <input type="radio" name="sello" value="<?= $sello->id_sello ?>" class="radio">&nbsp;<?= $sello->nombre_sello ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        <?php endif ?>-->

                        <div class="row row-padding-top">
                            <div class="controls">
                                <div class="alert span8">
                                    <p>
                                        <i class="icon-ok-sign"></i> <strong>Los campos con (*) son requeridos</strong><span>.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                   

                    <div class="tab-pane fade" id="foto" ng-controller="GaleriaCtrl">
                        <div class="row gallery-container gallery-portrait">
                            <div class=" col-lg-6 col-lg-offset-3">
                                <div class="gallery-item gallery-browser" ng-repeat="imag in items">
                                    <div style="background:url({{imag}});background-size: 100% 100%;background-repeat: no-repeat;background-position: center;height:255px;width:180px"></div>
                                    <div class="gallery-close-img" title="Eliminar" ng-click="removeItem($index)">&times;</div>
                                </div>
                                <input name="images[]" type="file" accept="image/*" onchange="changeFile(this, 500, 710)" />

                                <span class="gallery-item gallery-add" rel="tooltip" data-placement="top" data-original-title="Seleccionar un archivo">
                                    <div>
                                        <span>+</span>
                                        <span>Agregar</span>
                                    </div>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="controls">
                                <div class="alert alert-info col-lg-6 col-lg-offset-3">
                                    <i class="icon-info-sign icon-blue"></i> Sólo se aceptan imágenes en formato jpg/png/gif.<br />
                                    <i class="icon-info-sign icon-blue" style="opacity:0"></i> Se recomiendan dimensiones de 500 de ancho x 710 de alto.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions" align="center">
                    <div class="item">
                        <input type="submit" class="btn btn-primary" id="btnGuardar" value="Guardar" />
                    </div>

                    <div class="item">
                        <a href="<?= base_url('products') ?>" class="btn btn-info">Cancelar</a>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>


<!--
<script type="text/javascript" src="<?= base_url('resources/resources_sgc/js2/gallery.js')?>"></script>
-->
<script type="text/javascript">

    /*$('#descripcion_pro').sceditor({
        plugins: "xhtml",
        toolbar: "bold,italic,underline|left,center,right,justify|quote,font,size,color|youtube,link",
        emoticons: false,
        height: 150,
        style: "<?php echo base_url(); ?>resources/resources_sgc/css2/jquery.sceditor.default.min.css"
    });*/
    function GaleriaCtrl($scope) {
        $scope.items = ['#'];
        $scope.setItem = function (url) {
            $scope.items[$scope.items.length - 1] = url;
            $scope.items.push('#');
            jQuery('<input name="images[]" type="file" accept="image/*" onchange="changeFile(this, 500, 710)" />').insertBefore(jQuery('.gallery-add'));
        };
        $scope.removeItem = function (index) {
            $scope.items.splice(index, 1);
            jQuery('.gallery-container input[type="file"]:eq(' + index + ')').remove();
        };
    }

    $('#categoria_pro').change(function () {
        var categoria = $(this).val();

        $.ajax({
            type: "POST",
            url: "<?= base_url('subcategorys/ajax_by_category') ?>/" + categoria,
            beforeSend: function () {
                $('#subcategoria_pro').empty().append("<option value='0'>--Ninguna--</option>").attr('disabled', true);
            },
            success: function (data) {
                var json = JSON.parse(data);

                if (json.length > 0) {
                    $('#subcategoria_pro').html("<option value='-1' selected>--Seleccione--</option>").attr('disabled', false);

                    for (var i = 0; i < json.length; i++) {
                        $('#subcategoria_pro').append("<option value='" + json[i].SCAT_ID + "'>" + json[i].SCAT_nombre + "</option>");
                    }
                }
            }
        });
    });
</script>