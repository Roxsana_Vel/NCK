<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">
    <thead class="fondo">
        <th class="th_padding" width="5%">NRO</th>
        <th class="th_padding flechita" width="30%">NOMBRE</th>
        <th class="th_padding flechita" width="15%">CATEGORÍA</th>
        <th class="th_padding" width="10%">ESTADO</th>
        <th class="th_padding flechita" width="10%">PRECIO</th>
        <th class="th_padding flechita" width="10%">STOCK</th>
        <th class="th_padding" width="20%">ACCIONES</th>
    </thead>

    <?php if ($products != false): ?>
        <tbody>
            <?php $count = 0 ?>
            <?php foreach ($products->result() as $row): ?>
                <tr>
                    <td class="td_padding"><?= ++$count ?></td>
                    <td class="td_padding"><?= $row->PRO_nombre ?></td>
                    <td class="td_padding"><?= $row->CAT_nombre ?></td>
                    <td class="td_padding">
                        <?php if ($row->PRO_estado): ?>
                            <div id="estado<?= $row->PRO_ID ?>" style="float:left">
                                <img width="32" src="<?= base_url('resources/resources_sgc/images/button-check_green.png') ?>" />
                                <a href="#" title="<?= ($row->PRO_estado == 1) ? 'No publicar' : 'Publicar' ?>" >
                                    <img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?= base_url('resources/resources_sgc/img/change_2.png') ?>" onclick="cargar_estado('<?= base_url('products/DivLoader?pro_id=' . $row->PRO_ID . '&estado=0') ?>','estado<?= $row->PRO_ID ?>'); return false" />
                                </a>
                            </div>
                        <?php else: ?>
                            <div id="estado<?= $row->PRO_ID ?>" style="float:left">
                                <img width="32" src="<?= base_url('resources/resources_sgc/images/button-cross_red.png') ?>" />
                                <a href="#" title="<?= ($row->PRO_estado == 1) ? 'No publicar' : 'Publicar' ?>" >
                                    <img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?= base_url('resources/resources_sgc/images/change_2.png') ?>" onclick="cargar_estado('<?= base_url('products/DivLoader?pro_id=' . $row->PRO_ID . '&estado=1') ?>','estado<?= $row->PRO_ID ?>'); return false" />
                                </a>
                            </div>
                        <?php endif ?>
                    </td>
                    <td class="td_padding"><?= number_format($row->PRE_soles, 2, '.', '') ?></td>
                    <td class="td_padding"><?= $row->PRO_stock ?></td>
                    <td>
                        <div class="item">
                            <a href="<?= base_url('products/frm_edit_products/' . $row->PRO_ID . '/' . $row->CAT_ID) ?>" title="Editar" class="btn btn-info">
                                <i class="icon-edit icon-white"></i> Editar
                            </a>
                        </div>

                        <div class="item">
                            <a title="Eliminar" class="btn btn-danger" id="<?= $row->PRO_ID ?>">
                                <i class="icon-trash icon-white"></i> Eliminar
                            </a>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    <?php endif ?>
</table>

<script src="<?= base_url('resources/resources_sgc/js2/script.js') ?>"></script>

<!--script type="text/javascript">
    $(document).ready(function () {
        $('.btn-danger').click(function () {
            var eliminarId = $(this).attr("id");

            $.confirm({
                'title'     : 'Confirmar Eliminación',
                'message'   : 'Está usted seguro de eliminar este producto. <br />Eliminará la imagen, los Pedidos y Compras asociados al Producto.<br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                'buttons'   : {
                    'Sí'    : {
                        'class' : 'blue',
                        'action': function () {
                            $.ajax({
                                type: "POST",
                                url: "<?= base_url('products/delete_products_id') ?>",
                                data: 'eliminarId=' + eliminarId,
                                beforeSend: function () {
                                    $('#DivLoader').html('<div id="loader" class="loader"><img src="<?= base_url('resources/resources_sgc/img/loader.gif') ?>" alt="Loader"></div>');
                                },
                                success: function (resp) {
                                    $('#DivLoader').html(resp).show();
                                }
                            });
                        }
                    },
                    'No'    : {
                        'class' : 'gray',
                        'action': function () {
                        }   // Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
        });
    });
</script-->