<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />
<div class="content-wrapper">
<section class="content-header">
        <h1>
            Marca
            <small>Nueva MarcaProducto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">MarcaProducto</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>
<div class="content">
<div class="row">
  <div class="col-xs-12 widget">
    <div class="widget-header">
      <span class="title">
        <i class="icon-tag icon-white"></i>
      Información de la Marca
      </span>
    </div>
     
      <div class="widget-content form-container">
      
       <div class="nav-tabs-custom">
          
            <ul class="nav nav-tabs">
                <li class="active"><a href="#producto" data-toggle="tab">Información</a></li>
                
            </ul> <br>
        <div>
            <?php if ($categorys!= FALSE): ?>
            <?php $row = $categorys->row_object(); ?>
          <form method="POST" class="form-horizontal" action="<?php echo site_url("categorys/update_category_id"); ?>">
            <input type="hidden" name="id_category" value="<?php echo $row->CAT_ID; ?>" />
            <div class="row">
                <span class="help-block">&nbsp;</span>
                <label for="Nombre" class="col-xs-2 control-label">Nombre *</label>
                <div class="controls col-xs-5">
                   <input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $row->CAT_nombre; ?>" maxlength="100" required autofocus />
                </div>
            </div>

            <div class="row">
              <span class="help-block">&nbsp;</span>
              <label for="estado" class="control-label col-xs-2">Estado</label>
              <div class="controls col-xs-5">
                <select name="estado" id="estado" class="form-control">
                  <option value="1" <?php echo ($row->CAT_estado == '1' ? 'selected' : ''); ?>>Activo</option>
                  <option value="0" <?php echo ($row->CAT_estado == '0' ? 'selected' : ''); ?>>Inactivo</option>
                </select>
              </div>
            </div>
  <div class="control-group" style="display:none">
        <div class="controls">
          <?php if($this->session->flashdata('message_upload_error')) {?>
            <div class="alert <?php echo $this->session->flashdata('message_type_error');?>" style="float:left;width:300px">
                  <?php echo $this->session->flashdata('message_upload_error');?>
            </div>
                  <?php }?>
                  <?php if($this->session->flashdata('message_upload_success')) {?>
           <div class="alert <?php echo $this->session->flashdata('message_type_success');?>" style="float:left;width:300px">
              <?php echo $this->session->flashdata('message_upload_success');?>
           </div>
              <?php }?>
          <?php if($this->session->flashdata('file_data')) {?>
             <div class="alert <?php echo $this->session->flashdata('file_data_type_info');?>" style="float:left;width:300px">
        <?php
              echo "<ul>";
              $data_info =$this->session->flashdata('file_data');
                echo "<li>" .$data_info['file_name']. "</li>";
                    echo "</ul>";
        ?>
            </div>
         <?php }?>
        </div>
    </div> <br>
    <div class="row">
      <div class="form-actions" align="center">
        
          <input type="submit" class="btn btn-primary" id="btnAgregarImagen" value="Guardar" />
        
          <a href="<?php echo site_url("categorys"); ?>" class="btn btn-info">Cancelar</a>
        
      </div>
    </div>
    </form>
    <?php endif; ?>
      
    <?php if($this->session->flashdata('message_upload')):?>
    <?php endif;?>
    </div>
  </div>
 </div>
</div>
</div>
</div>
</div>


