<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/screen.css" media="screen, projection" />

<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css/print.css" media="print" />



<div>

    <ul class="breadcrumb">

        <li><a href="<?php echo site_url("welcome");?>">Panel de Control</a><span class="divider">/</span></li>

        <li><a href="<?php echo site_url("subcategorys");?>">Subcategorías</a><span class="divider">/</span></li>

        <li class="active">Nueva</li>

    </ul>

</div>



<div class="row-fluid">

  <div class="span12 widget">

    <div class="widget-header">

      <span class="title">

        <i class="icon-tag icon-white"></i>

      Información de la Subcategoría

      </span>

    </div>

      <div class="widget-content form-container">

        <div>



          	<form method="POST" class="form-horizontal" action="<?php echo site_url("subcategorys/add_subcategory/"); ?>">



			<span class="help-block">&nbsp;</span>

			

			<?php if ($categorys !== FALSE): ?>

			<div class="row">



				<label for="categoria_sub">Categoría *</label>                      

				<div class="controls">

					<select name="categoria_sub" id="categoria_sub" class="span3" required autofocus>

						<option value='' selected>--Seleccione--</option>

						<?php foreach ($categorys->result() as $row) : ?>

							<option value="<?php echo $row->CAT_ID;?>"> <?php echo $row->CAT_nombre;?></option> 

						<?php endforeach; ?>

					</select>

				</div>                                       

			</div>

			<?php endif; ?>

			

			<div class="row row-padding-top">

				<label for="nombre">Nombre *</label>                      

				<div class="controls">

				   <input class="span8" type="text" name="nombre" id="nombre" value="" maxlength="100" required />

				</div>                                       

			</div>

			

			<div class="row">

		        <span class="help-block">&nbsp;</span>



		        <label for="estado">Estado</label>

		        <div class="controls">

		          <select name="estado" id="estado" class="span3">

		            <option value="1" selected>Activo</option>

		            <option value="0">Inactivo</option>

		          </select> 

		        </div>

		    </div>



		    <span class="help-block">&nbsp;</span>

    

	<div class="row-fluid">

	  <div class="form-actions" align="center">

		<div class="item">

		  <input type="submit" class="btn btn-primary" id="btnAgregarImagen" value="Guardar">

		</div>

		 <div class="item">

		  <a href="<?php echo site_url("subcategorys"); ?>" class="btn btn-info">Cancelar</a>

		</div>

	  </div> 

    </div>







      		</form>

             

        </div>

      </div>

  </div>

</div>

