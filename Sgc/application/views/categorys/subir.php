<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead>

						<th class="th_padding">ID</th>

						<th class="th_padding">NOMBRE</th>

						<th class="th_padding">ESTADO</th>

						<th class="th_padding">ORDEN</th>

						<th class="th_padding">CAMBIAR ORDEN</th>

						<th class="th_padding">ACCIONES</th>

					</thead> 

					<?php if ($categorys != FALSE) : ?>

					<tbody>

						<?php foreach ($categorys->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo $row->CAT_ID; ?></td>

							<td class="td_padding"><?php echo $row->CAT_nombre; ?></td>

							<td width="10%" class="td_padding">

								<?php if($row->CAT_estado): ?>

										<div id="estado<?php echo $row->CAT_ID;?>" style="float:left">

										<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-check_green.png' ?>" />

										<a href="#" title="<?php if($row->CAT_estado==1) { echo "Deshabilitar";}else{ echo "Habilitar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>" onclick="cargar_estado('<?=site_url("categorys")?>/DivLoader?cat_id=<?php echo $row->CAT_ID; ?>&estado=0','estado<?php echo $row->CAT_ID; ?>')" /></a>

										</div>

								<?php else: ?> 

										<div id="estado<?php echo $row->CAT_ID;?>"  style="float:left">

										<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-cross_red.png' ?>"  />

										<a href="#" title="<?php if($row->CAT_estado==1) { echo "Deshabilitar";}else{ echo "Habilitar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("categorys")?>/DivLoader?cat_id=<?php echo $row->CAT_ID; ?>&estado=1','estado<?php echo $row->CAT_ID; ?>')" /></a>

										</div>

								<?php endif;?>	

	

							</td>

							<td class="td_padding"><?php echo $row->CAT_orden; ?></td>

							<td class="td_padding">

							<?php if ($row->CAT_orden < $categorys->num_rows()) : ?>

								<a title="Bajar" href="javascript:cargar('<?php echo site_url("categorys")?>/bajar/<?php echo $row->CAT_ID; ?>/<?php echo $row->CAT_orden; ?>','DivLoader')"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/img/navigation-down-button_green.png" /></a>

								<?php endif;?>

								<?php if($row->CAT_orden!=1) : ?>

								<a title="Subir" href="javascript:cargar('<?php echo site_url("categorys")?>/subir/<?php echo $row->CAT_ID; ?>/<?php echo $row->CAT_orden; ?>','DivLoader')"><img width="32" src="<?php echo base_url();?>resources/resources_sgc/img/navigation-up-button_green.png" /></a>

								<?php endif; ?>

							</td>

							<td width="30%">

								<div class="item">

								<a href="<?php echo site_url("categorys/frm_edit_category/".$row->CAT_ID);?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a  title="Eliminar" class="btn btn-danger" id="<?php $cadena = $row->CAT_ID.'/'.$row->CAT_nombre;  echo $cadena; ?>"><i class="icon-trash icon-white"></i> Eliminar</a>

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

					<?php else : ?>

                    	<h3>No hay registros de Categorías.</h3>

                    <?php endif; ?>

				</table>

				<script type="text/javascript">



$(document).ready(function(){

	

	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).attr("id");



		var arreglo = eliminarId.split("/"); // segmentamos la cadena en un arreglo donde cada posicion se divide tomando como punto de division el "/" (barra)





		nwElementoNombreCat = arreglo[1].trim(); //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo

		nwElementoIdCat = arreglo[0].trim();	 //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo



		/*		

		var newStr = arreglo[1].substring(0, arreglo[1].length-1); //Elimina el último caracter de la cadena

		*/

	



		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar la categoría "'+nwElementoNombreCat+'"<br /><b>Eliminará los productos y la imagen asociada.</b><br />¡No se podrá restaurar después de realizada la acción! ¿Continuar?',

			'buttons'	: {

				'Si'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("categorys/delete_category_id")?>",

						  data: 'eliminarId=' + nwElementoIdCat,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            $('#DivLoader').html(resp).show();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});





	

});</script>

