<div class="content-wrapper">
  <section class="content-header">
        <h1>
            Categoria
            <small>Listado de <?= isset($paginaDatos->tipoArticulo) ? $paginaDatos : 'categorys' ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Categoria</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>


<div class="content">
<div class="row">

	<div class="col-xs-12">
        <div class="box box-primary">
        <div class="box-header with-border">
			<span class="title">
				<i class="icon-tag icon-white"></i>
				Gestión de Subcategorías
			</span>
			<div class="box-tools pull-right">
			    <a class="btn btn-sm btn-success" href="<?php echo site_url('subcategorys/frm_add_subcategorys');?>"><i class="glyphicon glyphicon-plus"></i> Agregar</a>
			</div>
		</div>
        <div class="widget-content">
            <?php if ($this->session->flashdata('success_message')): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>

	                   <?php echo $this->session->flashdata('success_message'); ?>
	            </div>
	           <?php endif; ?>



				<?php if ($this->session->flashdata('error_message')): ?>

				<div class="alert alert-error">

					<button type="button" class="close" data-dismiss="alert">&times;</button>

	                <?php echo $this->session->flashdata('error_message'); ?>

	            </div>

	        	<?php endif; ?>



				 <?php if ($categorys != FALSE) : ?> 

				 <form class="form-horizontal">

                    <div class="control-group">

                        <label for="categoria_sub" style="width: 10%; text-align: left">Categoría</label>

                        <div class="controls" style="margin-left: 0">

                            <select name="categoria_sub" id="categoria_sub" class="span3">

                                <option value='-1' selected>--Todas--</option>

                                <?php foreach ($categorys->result() as $row) : ?>

                                    <option value="<?php echo $row->CAT_ID;?>"> <?php echo $row->CAT_nombre;?></option> 

                                <?php endforeach; ?>

                            </select>       

                        </div>

                    </div>

                 </form>

                 <?php endif; ?>

		<!-- loader -->

		<div id="DivLoader" class="box-body">

				<table id="datatable" class="table table-striped table-bordered table-condensed table-hover bootstrap-datatable dataTable">

					<thead class="fondo">

						<th class="th_padding flechita" width="10%">NRO</th>

						<th class="th_padding flechita" width="20%">CATEGORIA</th>						

						<th class="th_padding flechita" width="30%">NOMBRE</th>						

						<th class="th_padding" width="15%">ESTADO</th>						

						<th class="th_padding" width="25%">ACCIONES</th>

					</thead> 

					<?php if ($subcategorys != FALSE): ?>

					<tbody>

						<?php $count = 0; foreach ($subcategorys->result() as $row) : ?>

						<tr>

							<td class="td_padding"><?php echo ++$count; ?></td>

							<td class="td_padding"><?php echo $row->CAT_nombre; ?></td>

							<td class="td_padding"><?php echo $row->SCAT_nombre; ?></td>

							<td class="td_padding">

								<?php if($row->SCAT_estado): ?>

									<div id="estado<?php echo $row->SCAT_ID;?>" style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-check_green.png' ?>" />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>" onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=0','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php else: ?> 

									<div id="estado<?php echo $row->SCAT_ID;?>"  style="float:left">

									<img width="32" src="<?php echo base_url().'resources/resources_sgc/img/button-cross_red.png' ?>"  />

									<a href="#" title="<?php if($row->SCAT_estado==1) { echo "No publicar";}else{ echo "Publicar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'resources/resources_sgc/img/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("subcategorys")?>/DivLoader?subcat_id=<?php echo $row->SCAT_ID; ?>&estado=1','estado<?php echo $row->SCAT_ID; ?>')" /></a>

									</div>

								<?php endif;?>		

	

							</td>							

							<td>

								<div class="item">

								<a href="<?php echo site_url("subcategorys/frm_edit_subcategorys/".$row->SCAT_ID); ?>" title="Editar" class="btn btn-info"><i class="icon-edit icon-white"></i> Editar</a>

								</div>

								<div class="item">

								<a  title="Eliminar" class="btn btn-danger" data-id="<?php echo $row->SCAT_ID?>"><i class="icon-trash icon-white"></i> Eliminar</a>

								</div>

							</td>

						</tr>

						<?php endforeach; ?>

					</tbody>

                    <?php endif; ?>

				</table>

				</div>



			

				

			

		<!-- End loader -->

			</div>



		



	</div>

</div>
</div>
</div>
</div>


<script type="text/javascript">



$(document).ready(function(){



	$('#categoria_sub').change(function() {

  

  		var id_cat = $(this).val();



  		$('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');



        $.ajax({

			url: "<?=site_url('subcategorys')?>/filter_by_category/" + id_cat,

			success: function(resp) {

				$('#DivLoader').html(resp).show();

			}

		});

	});

	

	$('.btn-danger').click(function(){

		

		/*var elem = $(this).closest('.btn-danger');*/

		var eliminarId = $(this).data("id");

		$.confirm({

			'title'		: 'Confirmar Eliminación',

			'message'	: 'Está usted seguro de eliminar esta subcategoría. <br />Eliminará la imagen y los registros asociados a la subcategoría.<br />¡No se podrá deshacer la acción! ¿Continuar?',

			'buttons'	: {

				'Sí'	: {

					'class'	: 'blue',

					'action': function(){

						

						$.ajax({

						  type: "POST",

						  url: "<?=site_url("subcategorys/delete_subcategorys_id")?>",

						  data: 'eliminarId=' + eliminarId,

						   beforeSend: function() {

                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?=base_url()?>resources/resources_sgc/img/loader.gif" alt="Loader"></div>');

                },

			               success: function(resp) {

			            //$('#DivLoader').html(resp).show();

			            window.location.reload();

                  

                }



						});

											}

				},

				'No'	: {

					'class'	: 'gray',

					'action': function(){



						

					}	// Nothing to do in this case. You can as well omit the action property.

				}

			}

		});

		

	});

	

	if ($('#categoria_sub').val() !== '-1') {

		$('#categoria_sub').trigger('change');

	}

	

});</script>

