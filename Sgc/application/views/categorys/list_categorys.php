<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Marca
            <small>Listado de Marca</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Marca</a></li>
            <li class="active">Listado</li>
        </ol>
    </section>
<div class="content">
	<div class="row">
		<div class="col-xs-12">
			 <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Listado de Marca</h3>

                        <div class=" box-tools pull-right"><a class="btn btn-sm btn-success" href="<?php echo site_url("categorys");?>/frm_add_category/<?php if ($categorys != FALSE) : echo  $categorys->num_rows();  endif;?>"><i class="glyphicon glyphicon-plus"></i> Agregar</a>
                        </div>
                    </div>
                  
		
		
			<div class="widget-content">
				<?php if ($this->session->flashdata('success_message')): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
	                <?php echo $this->session->flashdata('success_message'); ?>
	            </div>
	        	<?php endif; ?>
				<?php if ($this->session->flashdata('error_message')): ?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
	                <?php echo $this->session->flashdata('error_message'); ?>
	            </div>
	        	<?php endif; ?>

		<!-- loader -->
		<div id="DivLoader " class="box-body">
				<table id="datatable" class="table table-striped table-bordered table-condensed  table-hover bootstrap-datatable dataTable">
					<thead class="fondo">
						<th class="th_padding flechita" width="8%">ORDEN</th>
						<th class="th_padding flechita" width="43%">NOMBRE</th>
						<th class="th_padding" width="10%">ESTADO</th>
						<th class="th_padding" width="10%">CAMBIAR ORDEN</th>
						<th class="th_padding" width="10%">ACCIONES</th>
					</thead>
					<?php if ($categorys != FALSE) : ?>
					<tbody>
						<?php $count = 0; foreach ($categorys->result() as $row) : ?>
						<tr>
							<td class="td_padding"><?php echo ++$count; ?></td>
							<td class="td_padding"><?php echo $row->CAT_nombre; ?></td>
							<td class="td_padding">
								<?php if($row->CAT_estado): ?>
  								<div id="estado<?php echo $row->CAT_ID;?>" style="float:left">
  								  <img width="32" src="<?php echo base_url().'dist2/img/button-check_green.png' ?>" />
  								  <a href="#" title="<?php if($row->CAT_estado==1) { echo "Deshabilitar";}else{ echo "Habilitar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" id="rotateMe" width="26" src="<?php echo base_url().'dist2/img/change_2.png' ?>" onclick="cargar_estado('<?=site_url("categorys")?>/DivLoader?cat_id=<?php echo $row->CAT_ID; ?>&estado=0','estado<?php echo $row->CAT_ID; ?>')" /></a>
  								</div>
								<?php else: ?>
									<div id="estado<?php echo $row->CAT_ID;?>"  style="float:left">
										<img width="32" src="<?php echo base_url().'dist2/img/button-cross_red.png' ?>"  />
										<a href="#" title="<?php if($row->CAT_estado==1) { echo "Deshabilitar";}else{ echo "Habilitar";} ?>" ><img style="margin-bottom:7px;" class="rotateMe" width="26" src="<?php echo base_url().'dist2/img/change_2.png' ?>"  id="rotateMe"  onclick="cargar_estado('<?=site_url("categorys")?>/DivLoader?cat_id=<?php echo $row->CAT_ID; ?>&estado=1','estado<?php echo $row->CAT_ID; ?>')" /></a>
									</div>
								<?php endif;?>
							</td>
							<td class="td_padding">
								<?php if ($count < $categorys->num_rows()) : ?>
								<a title="Bajar" href="javascript:cargar('<?php echo site_url("categorys")?>/bajar/<?php echo $row->CAT_ID; ?>/<?php echo $row->CAT_orden; ?>','DivLoader', true)"><img width="32" src="<?php echo base_url();?>dist2/img/navigation-down-button_green.png" /></a>
								<?php endif;?>
								<?php if($count!=1) : ?>
								<a title="Subir" href="javascript:cargar('<?php echo site_url("categorys")?>/subir/<?php echo $row->CAT_ID; ?>/<?php echo $row->CAT_orden; ?>','DivLoader', true)"><img width="32" src="<?php echo base_url();?>dist2/img/navigation-up-button_green.png" /></a>
								<?php endif; ?>
							</td>
							<td class="text-center">
								
								<a href="<?php echo site_url("categorys")."/frm_edit_category/".$row->CAT_ID;?>" title="Editar"
								toggle="tooltip" class="btn btn-info btn-xs">
								<span class="glyphicon glyphicon-pencil"></span></a>
								
								
								<a  title="Eliminar" class="btn btn-danger btn-xs" data-toggle="tooltip" data-id="<?php $cadena = $row->CAT_ID.'/'.$row->CAT_nombre;  echo $cadena; ?>"><span class="glyphicon glyphicon-trash"></span></a>
								
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
					<?php else : ?>
          	<h3>No hay registros de Categorías.</h3>
          <?php endif; ?>
				</table>
			</div>
		<!-- End loader -->
			</div>
        </div>
        </div>
	</div>
</div></div>
<script type="text/javascript">
$(document).ready(function(){
	$('.btn-danger').click(function(){
		/*var elem = $(this).closest('.btn-danger');*/
		var eliminarId = $(this).data("id");
		var arreglo = eliminarId.split("/"); // segmentamos la cadena en un arreglo donde cada posicion se divide tomando como punto de division el "/" (barra)
		nwElementoNombreCat = arreglo[1].trim(); //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo
		nwElementoIdCat = arreglo[0].trim();	 //eliminamos espacios en blanco al final y al comienzo del elemento del arreglo
		/*
		var newStr = arreglo[1].substring(0, arreglo[1].length-1); //Elimina el último caracter de la cadena
		*/
		
    

            swal({
                title: 'Confirmar Eliminación',
                text: 'Está usted seguro de eliminar la Marca "'+nwElementoNombreCat +'"  Eliminará los productos y la imagen asociada.¡No se podrá restaurar después de realizada la acción! ¿Continuar?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esta Marca!',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url("categorys/delete_categorys_id") ?>",
                    data: 'eliminarId=' + eliminarId,
                    beforeSend: function () {
                        $('#DivLoader').html('<div id="loader" class="loader"><img src="<?= base_url('resources/resources_sgc/images/loader.gif') ?>" alt="Loader"></div>');
                    },
                    success: function (resp) {
                        swal({
                            title: 'Eliminado!',
                            text: 'La categoria seleccionado ha sido eliminado.',
                            type: 'success',
                            confirmButtonText: 'Continuar'
                        });

                        $('#DivLoader').html(resp).show();
                    }
                });
            });
        });
		});
	</script>
