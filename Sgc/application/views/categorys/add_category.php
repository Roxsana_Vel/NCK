<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?=base_url().'resources';?>/resources_sgc/css2/print.css" media="print" />

<div class="content-wrapper">
<section class="content-header">
        <h1>
            Marca
            <small>Nueva Marca</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
            <li><a href="#">Marca</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>
<div class="content">
  <div class="row">
    <div class="col-xs-12">
       <div>
          <form method="POST" class="form-horizontal" action="<?php echo site_url("categorys/add_category/" . $this->uri->segment(3)); ?>">
          
          <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs bg-gray" id="tabsFormNuevoArticulo">
                            <li class="active"><a href="#tabInformacion" data-toggle="tab" aria-expanded="true">Información</a></li>
                            <li class="pull-right">
                                <!-- <a href=""></a> -->
                            </li>
                        </ul>
			<div class=" row">
				<span class="help-block">&nbsp;</span>
				<label for="nombre" class="col-sm-2 control-label">Nombre *</label>
				<div class="col-xs-5 controls">
				   <input class="span8  form-control" type="text" name="nombre" id="nombre" value="" maxlength="100" required autofocus />
			</div>
			</div>

			<div class="row">
        <span class="help-block">&nbsp;</span>
        <label for="estado" class="col-sm-2 control-label">Estado</label>
        <div class="controls col-xs-5">
          <select name="estado" id="estado" class="span3 form-control select2"  style="width: 100%;">
            <option value="1" selected>Activo</option>
            <option value="0">Inactivo</option>
          </select>
        </div>


      </div>
    <span class="help-block">&nbsp;</span>
	<div class="row-fluid">
      <div class="form-actions" align="center">
		
		      <input type="submit" class="btn btn-primary" id="btnAgregarImagen" value="Guardar">
		
		      <a href="<?php echo site_url('categorys'); ?>" class="btn btn-info">Cancelar</a>
         
     </div>
    </div>
</form>
</div>
</div>
</div>
</div>
</div> 
</div>


