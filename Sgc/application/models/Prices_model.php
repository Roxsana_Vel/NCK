<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Prices_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function insertPrices($data)
	{
		$this->db->insert_batch('precio', $data);
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function getPriceByProd($id_prod, $niv_usu) {
		try {
			$this->db->select('
				precio.PRE_ID,
				precio.PROD_ID,
				precio.NUSU_ID,
				precio.PRE_soles,
				precio.PRE_dolares
				');
				#producto.PRO_precio,
			$this->db->from('precio');
			$this->db->where('PROD_ID', $id_prod);
			$this->db->where('NUSU_ID', $niv_usu);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function updatePricesId($data)
	{
		$this->db->update_batch('precio', $data, 'PRE_ID');
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>