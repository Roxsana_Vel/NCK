<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Events_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getEvents($order = NULL)
	{
		try {
			$this->db->select('
				evento.EVE_ID,
				evento.EVE_titulo,
				evento.EVE_descripcion,
				evento.EVE_fecha,
				evento.EVE_estado,
				evento.EVE_url
			');
			$this->db->from('evento');
			if (!is_null($order)) {
				$this->db->order_by($order);
			}
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function insertEvent($data)
	{
		$this->db->insert('evento', $data);
		$affected_rows = $this->db->affected_rows();
		$last_id_inserted = $this->db->insert_id();
		if ($affected_rows > 0) {
			return $last_id_inserted;
		} else {
			return FALSE;
		}
	}
	function getEventId($id)
	{
		try {
			$this->db->where("EVE_ID", $id);
			$data = $this->db->get('evento');
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function updateEventId($id, $data)
	{
		$this->db->where("EVE_ID", $id);
		$this->db->update('evento', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function deleteEventsId($id)
	{
		$this->db->where('EVE_ID', $id);
		$this->db->delete('evento');

		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function UpdateState($id, $estado)
	{
		$this->db->where('EVE_ID', $id);
		$this->db->update('evento', array( 'EVE_estado' => $estado));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>