<?php 
class Files_model extends CI_Model {
	function __construct() 
	{
		parent::__construct();
	}
	public	function insertImagen($data)
	{
		$this->db->insert('galeria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
		public	function insertBanner($data)
	{
		$this->db->insert('banners', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public	function insertCategorys($data)
	{
		$this->db->insert('categoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public	function insertSubcategorys($data)
	{
		$this->db->insert('subcategoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function UpdateCategorys($id_category, $data)
	{  	
		$this->db->where('CAT_ID', $id_category);
		$this->db->update('categoria', $data);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}	
	public function UpdateSubcategorys($id_subcategory, $data)
	{  	
		$this->db->where('SCAT_ID', $id_subcategory);
		$this->db->update('subcategoria', $data);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}	
	
	public function UpdateImagen($id_archivo, $data)
	{  	
		$this->db->where('GAL_ID', $id_archivo);
		$this->db->update('galeria', $data);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}	
public function UpdateBanner($id_banner, $data)
	{  	
		$this->db->where('BAN_ID', $id_banner);
		$this->db->update('banners', $data);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
 ?>