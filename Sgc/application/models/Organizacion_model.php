<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Organizacion_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getOrganizacion()
    {
        $organizacion = $this->db->get_where('organizacion', array('ORG_ID' => 1))->row();

        return $organizacion;
    }

    public function editar($orgId, $data)
    {
        $this->db->where('ORG_ID', $orgId);
        $this->db->update('organizacion', $data);
    }


}