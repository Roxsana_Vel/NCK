<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vistas_publicas_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * obtener todas las vistas publicas
     * para listarlas
     * @return [type] [description]
     */
    public function getAll()
    {
        return $this->db->get('vistas_publicas')->result();
    }

    /**
     * actualizar campos de una vista publica
     * @param  [type] $idVip         [description]
     * @param  [type] $dataUpdateVip [description]
     * @return [type]                [description]
     */
    public function update($idVip, $data)
    {
        $this->db->where('VIP_ID', $idVip);
        $this->db->update('vistas_publicas', $data);
    }

    /**
     * obtener las vistas publicas con una imagen de banner especifico
     * @param  [type] $imgNombre [description]
     * @return [type]            [description]
     */
    public function getSeccionesConBanner($imgNombre)
    {
        $this->db->where('VIP_img_slogan', $imgNombre);
        return $this->db->get('vistas_publicas')->result();
    }

    public function VaciarImagenDeVistas($imagenBanner)
    {
        $vistas = $this->db->get_where('vistas_publicas', array('VIP_img_slogan' => $imagenBanner))->result();

        foreach ($vistas as $key => $vip) {
            $this->db->where('VIP_ID', $vip->VIP_ID);
            $this->db->update('vistas_publicas', array('VIP_img_slogan' => null));
        }
    }

}