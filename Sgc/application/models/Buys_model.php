<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Buys_model extends CI_Model {

		

	function __construct()

	{

		parent::__construct();

	}





	function getBuys()

	{

		try {

			$this->db->select('

				compra.COM_ID,

				compra.COM_fecha,

				compra.PED_ID,				

				pedido.PED_ID,

				pedido.PED_costoTotal,

				pedido.PED_estado,

				pedido.PED_moneda,

				persona.PER_ID,

				persona.PER_nombres,				

				persona.PER_documento

				');

			$this->db->from('compra');

			$this->db->join('pedido', 'pedido.PED_ID = compra.PED_ID', 'left');

			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');

			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');

			$this->db->where('pedido.PED_estado <> 1 AND pedido.PED_estado <> 3');

			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}	



	



	function getBuysDetailsId($id_ped)

	{

		try {







			$this->db->select('

				detalle_pedido.DPED_ID,

				detalle_pedido.DPED_cantidad,

				detalle_pedido.DPED_importe,

				detalle_pedido.PED_ID,

				detalle_pedido.PRO_ID,

				producto.PRO_ID,

				producto.PRO_nombre				

				');

				#producto.PRO_precio,

			$this->db->from('detalle_pedido');

			$this->db->join('producto', 'detalle_pedido.PRO_ID = producto.PRO_ID', 'left');			

			$this->db->where('detalle_pedido.PED_ID',$id_ped);

			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function getBuysId($id_com)

	{

		try {

			$this->db->select('

				compra.COM_ID,

				compra.COM_fecha,

				compra.PED_ID,				

				pedido.PED_ID,

				pedido.PED_fecha,

				pedido.PED_estado,

				pedido.PED_observacion,				

				pedido.USU_ID,

				pedido.PED_moneda,

				persona.PER_ID,

				persona.PER_nombres,

				persona.PER_direccion,

				persona.PER_telefono,

				persona.PER_documento

				');

			$this->db->from('compra');

			$this->db->join('pedido', 'pedido.PED_ID = compra.PED_ID', 'left');

			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');

			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');

			$this->db->where('compra.COM_ID', $id_com);

			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}	



	function getBuysExistsId($id_ped)

	{

		try {

			$this->db->select('

				compra.COM_ID,

				compra.COM_fecha,

				compra.PED_ID,				

				pedido.PED_ID,

				pedido.PED_fecha,

				pedido.PED_estado,

				pedido.PED_observacion,				

				pedido.USU_ID,

				pedido.PED_moneda,

				persona.PER_ID,

				persona.PER_nombres,

				persona.PER_direccion,

				persona.PER_telefono,

				persona.PER_documento

				');

			$this->db->from('compra');

			$this->db->join('pedido', 'pedido.PED_ID = compra.PED_ID', 'left');

			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');

			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');

			$this->db->where('compra.PED_ID', $id_ped);

			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}	





		



	function addBuys($data)

	{

		$this->db->insert('compra', $data);

		$affected_rows = $this->db->affected_rows();



		if ($affected_rows > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	



	function deleteBuysId($id)

	{

		$this->db->where('COM_ID', $id);

		$this->db->delete('compra');

		

		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function deleteBuysOrdersId($id)

	{

		$this->db->where('PED_ID', $id);

		$this->db->delete('compra');

		

		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}

}



?>