<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Text_model extends CI_Model {

		

	function __construct()

	{

		parent::__construct();

	}



	function getText($order = NULL)

	{

		try {

			$this->db->select('

				TEX_ID,

				TEX_alias,

				TEX_pagina,

				TEX_contenido

		');

		$this->db->from('texto');



		if (!is_null($order)) {

			$this->db->order_by($order);

		}



		$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function getTextId($id_text)

	{

		try {

			$this->db->select('

				TEX_ID,

				TEX_alias,

				TEX_pagina,

				TEX_contenido

			');

			$this->db->from('texto');

			$this->db->where('TEX_ID',$id_text);

		$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}

	



	function insertText($data)

	{

		$this->db->insert('texto', $data);

		$affected_rows = $this->db->affected_rows();



		if ($affected_rows > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function updateTextId($id, $values)

	{

		$this->db->where('TEX_ID', $id);

		$this->db->update('texto', $values);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function deleteTextId($id)

	{

		$this->db->where('TEX_ID', $id);

		$this->db->delete('texto');

		

		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	}