<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articulo_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getArticulos()
    {
        $this->db->select('*');
        $this->db->from('evento');
        return $this->db->get()->result();
    }

    public function getNovedades()
    {
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('EVE_tipo', 'novedad');
        return $this->db->get()->result();
    }

    public function getProyectos()
    {
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('EVE_tipo', 'proyecto');
        return $this->db->get()->result();
    }

    public function getArticulo($artId)
    {
        $datosArticulo = $this->db->get_where('evento', array(
            'EVE_ID' => $artId,
        ))->row();

        return $datosArticulo;
    }

    public function guardarNuevo($data)
    {
        $this->db->insert('evento', $data);

        return $this->db->insert_id();
    }

    public function guardarEdicion($artId, $data)
    {
        $this->db->where('EVE_ID', $artId);

        return $this->db->update('evento', $data);
    }

    public function activar_desactivar($artId)
    {
        $estadoAnterior = $this->db->get_where('evento', array('EVE_ID' => $artId))->row('EVE_estado');

        $nuevoEstado = ($estadoAnterior == 1) ? 0 : 1;
        /** cambiar estado en empresa_asociada */
        $this->db->where('EVE_ID', $artId);
        $this->db->update('evento', array(
            'EVE_estado' => $nuevoEstado,
        ));
    }

    public function eliminar($artId)
    {
        $this->db->where('EVE_ID', $artId);
        $this->db->delete('evento');
    }

    public function countArticulos($estado = 1)
    {
        $this->db->where('EVE_estado', $estado);
        return $this->db->get('evento')->num_rows();
    }
}