<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders_model extends CI_Model {
		
	function __construct()
	{
		parent::__construct();
	}
	function getOrders()
	{
		try {
			$this->db->select('
				pedido.PED_ID,
				pedido.PED_fecha,
				pedido.PED_metodoPago,
				pedido.PED_numOperacion,
				pedido.PED_estado,
				pedido.USU_ID,
				pedido.PED_moneda,
				pedido.PED_costoTotal,
				persona.PER_ID,
				persona.PER_nombres,
				persona.PER_apellidos,
				persona.PER_direccion,
				persona.PER_telefono,
				persona.PER_departamento,
				persona.PER_provincia,
				persona.PER_distrito,
				persona.PER_documento,
				persona.PER_email,
				persona.PER_sexo,
				persona.PER_fechaNacimiento
				');
			$this->db->from('pedido');
			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');
			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');
			$this->db->order_by('pedido.PED_fecha', 'desc');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
function getEmpresaId($id_ped)
	{
		try {
			$this->db->select('
				pedido.PED_ID,				
				persona.PER_ID,
				empresa.*,
				giro.*
				');
			$this->db->from('pedido');
			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');
			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');
			$this->db->join('empresa', 'empresa.EMP_ID = persona.EMP_ID', 'left');
			$this->db->join('giro', 'giro.GIR_ID = empresa.GIR_ID', 'left');
			$this->db->where('pedido.PED_ID',$id_ped);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
public function getGiro(){
	try {
			$this->db->select('
				giro.*
				');
			$this->db->from('giro');			
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
}
function getOrdersId($id_ped)
	{
		try {
			$this->db->select('
				pedido.*,
				persona.*,
				usuario.USU_ID,
				usuario.USU_login,
				direccion_pedido.*,
				departamento.*,
				provincia.*,
				distrito.*
				');
			$this->db->from('pedido');
			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');
			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');
			$this->db->join('direccion_pedido', 'direccion_pedido.PED_ID = pedido.PED_ID', 'left');
			$this->db->join('departamento', 'departamento.DPTO_ID = direccion_pedido.DPTO_ID', 'left');
			$this->db->join('provincia', 'provincia.PROV_ID = direccion_pedido.PROV_ID', 'left');
			$this->db->join('distrito', 'distrito.DIST_ID = direccion_pedido.DIST_ID', 'left');
			$this->db->where('pedido.PED_ID',$id_ped);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getOrdersId2($id_ped)
	{
		try {
			$this->db->select('
				pedido.PED_ID,
				pedido.PED_fecha,
				pedido.PED_estado,
				pedido.PED_numOperacion,
				pedido.PED_fechaDeposito,
				pedido.USU_ID,
				pedido.PED_costoTotal,
				pedido.PED_delivery,
				pedido.PED_metodoPago,
				pedido.PED_observacion,
				pedido.PED_moneda,
				persona.*,
				usuario.*,
				direccion_pedido.*
				');
			$this->db->from('pedido');
			$this->db->join('usuario', 'usuario.USU_ID = pedido.USU_ID', 'left');
			$this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'left');
			$this->db->join('direccion_pedido', 'direccion_pedido.PER_ID = persona.PER_ID', 'left');
			$this->db->where('pedido.PED_ID',$id_ped);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	
	function getOrdersDetailsId($id_ped)
	{
		try {
			$this->db->select('
				detalle_pedido.DPED_ID,
				detalle_pedido.DPED_cantidad,
				detalle_pedido.DPED_importe,
				detalle_pedido.PED_ID,
				detalle_pedido.PRO_ID,
				producto.PRO_ID,
				producto.PRO_nombre,
				precio.*
				');
				#producto.PRO_precio,
			$this->db->from('detalle_pedido');
			$this->db->join('producto', 'detalle_pedido.PRO_ID = producto.PRO_ID', 'left');			
			$this->db->join('precio', 'producto.PRO_ID = precio.PROD_ID');
			$this->db->where('detalle_pedido.PED_ID',$id_ped);
			$this->db->where('precio.NUSU_ID','4');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function UpdateState($id_ped, $data){
		$this->db->where("PED_ID", $id_ped);
		$this->db->update('pedido', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
		
	function insertOrders($data)
	{
		$this->db->insert('pedido', $data);
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function deleteOrdersId($id)
	{
		$this->db->where('PED_ID', $id);
		$this->db->delete('pedido');
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function deleteDvenOrdersId($id_ped){
		
		$this->db->where('PED_ID', $id_ped);
		$this->db->delete('detalle_pedido');
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
function deleteDetalleVentaId($id)
{	
try {		
			$sql = "DELETE t1 FROM detalle_pedido t1 JOIN producto t2 ON t1.PRO_ID = t2.PRO_ID WHERE t2.CAT_ID = ?";
			$params = array($id);
		$result = $this->db->query($sql, $params);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
		return TRUE;
		}
			
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
}
function deleteDetalleVentaIdPro($id)
{	
try {		
			$sql = "DELETE t1 FROM detalle_pedido t1 JOIN producto t2 ON t1.PRO_ID = t2.PRO_ID JOIN pedido t3 ON t1.PED_ID=t3.PED_ID WHERE t1.PRO_ID = ?";
			$params = array($id);
		$result = $this->db->query($sql, $params);
		
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
		return TRUE;
		}
			
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
}
function GetPedIdsToDelete($id)
{	
try {		
		
		$data = $this->db->query("SELECT * FROM detalle_pedido t1
			JOIN producto t2 ON t1.PRO_ID = t2.PRO_ID
			JOIN pedido t3 ON t1.PED_ID=t3.PED_ID
			WHERE t1.PRO_ID = ".$id);			
		if ($data->num_rows() > 0) {
		return $data;
		}else{
			return FALSE;
		}
			
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
}
}
?>