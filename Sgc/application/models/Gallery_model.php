<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gallery_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getGallery()
    {
        try {
            $this->db->select('
                galeria.GAL_ID,
                galeria.GAL_archivo,
                producto.PRO_ID
            ');
            $this->db->from('galeria');
            $this->db->join('producto', 'producto.PRO_ID = galeria.PRO_ID', 'left');
            $this->db->where('producto.PRO_ID = galeria.PRO_ID');
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getGalleryId($id)
    {
        try {
            $this->db->where("GAL_ID", $id);
            $data = $this->db->get('galeria');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getGalleryProdId($id)
    {
        try {
            $this->db->where("PRO_ID", $id);
            $data = $this->db->get('galeria');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getProductGalleryId($id_pro)
    {
        try {
            $this->db->where("PRO_ID", $id_pro);
            $data = $this->db->get('galeria');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getEventGalleryId($id_eve)
    {
        try {
            $this->db->where("EVE_ID", $id_eve);
            $data = $this->db->get('galeria');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function insertGallery($data)
    {
        $this->db->insert('galeria', $data);
        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteGalleryId($id)
    {
        $query = $this->db->get_where('galeria', array('GAL_ID' => $id));

        if ($query->num_rows() > 0) {
            $row   = $query->row();
            $photo = $row->GAL_archivo;

            $full_path  = "./resources/resources_web/images/" . $photo;
            $thumb_path = "./resources/resources_web/images/thumbs/" . $photo;

            unlink($full_path);
            unlink($thumb_path);

            $this->db->delete('galeria', array('GAL_ID' => $id));

            return true;
        }

        return false;
    }

    public function deleteGalleryProId($id)
    {
        $query = $this->db->get_where('galeria', array('PRO_ID' => $id));

        if ($query->num_rows() > 0) {
            $row   = $query->row();
            $photo = $row->GAL_archivo;

            $full_path  = "./resources/resources_web/images/" . $photo;
            $thumb_path = "./resources/resources_web/images/thumbs/" . $photo;

            unlink($full_path);
            unlink($thumb_path);

            $this->db->delete('galeria', array('PRO_ID' => $id));
            return true;
        }

        return false;
    }

    public function deleteGalleryCatId($id)
    {
        try {
            $this->db->select('
                galeria.GAL_ID,
                galeria.PRO_ID,
                galeria.GAL_archivo,
                categoria.CAT_ID,
                producto.PRO_ID
            ');
            $this->db->from('galeria');
            $this->db->join('producto', 'producto.PRO_ID = galeria.PRO_ID', 'left');
            $this->db->join('categoria', 'categoria.CAT_ID = producto.CAT_ID', 'left');
            $this->db->where('categoria.CAT_ID', $id);
            $data = $this->db->get();

            $row = $data->row();

            if ($data->num_rows() > 0) {
                foreach ($data->result() as $row) {
                    $photo      = $row->GAL_archivo;
                    $full_path  = "./resources/resources_web/images/" . $photo;
                    $thumb_path = "./resources/resources_web/images/thumbs/" . $photo;

                    unlink($full_path);
                    unlink($thumb_path);
                }
                $data->free_result(); // El objeto resultante $data dejará de estar disponible
                $delete_galeria = "DELETE t1 FROM galeria t1
                    JOIN producto t2 ON t1.PRO_ID = t2.PRO_ID
                    WHERE t2.CAT_ID = " . $id;
                $this->db->query($delete_galeria);

                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function getExcludedBy($field, $id, $ids)
    {
        try {
            $this->db->select('
                galeria.GAL_ID,
                galeria.GAL_archivo
            ');
            $this->db->where($field, $id);

            foreach ($ids as $id_gal) {
                $this->db->where('GAL_ID <>', $id_gal);
            }

            $data = $this->db->get('galeria');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function deleteGalleryBy($controller, $field, $id)
    {
        try {
            $this->db->select('
                galeria.GAL_ID,
                galeria.GAL_archivo
            ');
            $query = $this->db->get_where('galeria', array($field => $id));

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row_gal) {
                    $image = $row_gal->GAL_archivo;
                    $path  = '../images/' . $controller . '/';

                    if ($controller === 'products') {
                        $path_thumbs   = $path . 'thumbs/';
                        $path_standard = $path . 'standard/';

                        @unlink($path . $image);
                        @unlink($path_thumbs . $image);
                        @unlink($path_standard . $image);
                    } elseif ($controller === 'events') {
                        $path_standard = $path . 'standard/';

                        @unlink($path . $image);
                        @unlink($path_standard . $image);
                    }
                }

                $this->db->delete('galeria', array($field => $id));
            }

            return true;
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function deleteManyGalleryId($controller, $id, $files)
    {
        try {
            foreach ($files as $image) {
                $path = '../images/' . $controller . '/';

                if ($controller === 'products') {
                    $path_thumbs   = $path . 'thumbs/';
                    $path_standard = $path . 'standard/';

                    @unlink($path . $image);
                    @unlink($path_thumbs . $image);
                    @unlink($path_standard . $image);
                } elseif ($controller === 'events') {
                    $path_standard = $path . 'standard/';

                    @unlink($path . $image);
                    @unlink($path_standard . $image);
                }
            }

            foreach ($id as $id_gal) {
                $this->db->or_where('GAL_ID', $id_gal);
            }

            $this->db->delete('galeria');
            return true;
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }
}
