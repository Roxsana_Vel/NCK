<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * obtener todos los banners (habilitados y deshabilitados)
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    public function getBanner($order = NULL)
    {
        try {
            $this->db->select('
                *
            ');
            $this->db->from('banners');

            if ( ! is_null($order)) {
                $this->db->order_by($order);
            }

            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data->result();
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * habilitar o deshabilitar un banner (segun estado actual del banner)
     * @param  [type] $banId [description]
     * @return [type]        [description]
     */
    public function habilitar_deshabilitar($banId)
    {
        $banEstado = $this->db->get_where('banners', array(
            'BAN_ID' => $banId
        ))->row('BAN_estado');

        if ($banEstado == 1) {
            $this->db->where('BAN_ID', $banId);
            $this->db->update('banners', array(
                'BAN_estado' => 0
            ));
        } else {
            $this->db->where('BAN_ID', $banId);
            $this->db->update('banners', array(
                'BAN_estado' => 1
            ));
        }
    }

    /**
     * obtener un banner por su id
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getBannerId($id)
    {
        try {
            $this->db->select('
                *
            ');
            $this->db->where('BAN_ID', $id);

            $data = $this->db->get('banners');

            if ($data->num_rows() > 0) {
                return $data->row();
            } else {
                return FALSE;
            }
        } catch(Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function insertBanner($data)
    {
        $this->db->insert('banners', $data);
        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {
            $id_banner = $this->db->insert_id();
            return $id_banner;
        } else {
            return FALSE;
        }
    }

    public function updateBanner($ban_id, $data)
    {
        $this->db->where('BAN_ID', $ban_id);
        $this->db->update('banners', $data);

        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function deleteBannerId($id)
    {
        $query = $this->db->get_where('banners', array('BAN_ID' => $id));

        if ($query->num_rows() > 0) {
            $row = $query->row();
            $photo = $row->BAN_archivo;

            $full_path = '../images/banners/' . $photo;
            $thumb_path = '../images/banners/thumbs/' . $photo;

            @unlink($full_path);
            @unlink($thumb_path);

            $this->db->delete('banners', array('BAN_ID' => $id));
            return true;
        }

        return false;
    }

    public function UpdateState($ban_id, $estado)
    {
        $this->db->where('BAN_ID', $ban_id);
        $this->db->update('banners', array( 'BAN_estado' => $estado));

        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function eliminar($banId)
    {
        $this->db->where('BAN_ID', $banId);
        $this->db->delete('banners');
    }

    public function countBanners($estado = 1)
    {
        $this->db->where('BAN_estado', $estado);
        return $this->db->get('banners')->num_rows();
    }
}