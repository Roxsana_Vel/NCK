<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Makes_Model extends CI_Model {

		

	function __construct()

	{

		parent::__construct();

	}



	function getMakes($order = NULL)

	{

		try {

			$this->db->select('

				marca.MAR_ID,

				marca.MAR_nombre,

				marca.MAR_logo,

				marca.MAR_orden,

				marca.MAR_banner

			');
			
			$this->db->from('marca');

			if (is_null($order)) {

				$this->db->order_by('marca.MAR_orden','ASC');	

			} else {

				$this->db->order_by($order);

			}

			
			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}


	function insertMake($data)

	{

		$this->db->insert('marca', $data);

		$affected_rows = $this->db->affected_rows();



		if ($affected_rows > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function getMakeId($id)

	{

		try {

			$this->db->where("MAR_ID", $id);

			$data = $this->db->get('marca');



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}


 
	function updateMakeId($id, $data)

	{

		$this->db->where('MAR_ID', $id);

		$this->db->update('marca', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}

	

	function deleteMakeId($id)

	{

		$this->db->delete('marca', array('MAR_ID' => $id));

		$affected = $this->db->affected_rows();



		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}


	//*****************************


		function getMakesByCat($id_cat ,$id_subcat = -1, $order = -1) {

		try {

			$this->db->select('

				marca.MAR_ID,

				marca.MAR_nombre,

				marca.MAR_logo,

				marca.MAR_orden,

				marca.MAR_banner

			');

			$this->db->from('marca');

			$this->db->join('producto', 'producto.MAR_ID = marca.MAR_ID', 'left');

			$this->db->where('producto.CAT_ID', $id_cat);

			if ($id_subcat!= -1) {

				$this->db->where('producto.SCAT_ID', $id_subcat);

			}

			$this->db->group_by("producto.MAR_ID");

			if ($order!= -1) {

				$this->db->order_by($order, "asc");

			}else{
			$this->db->order_by("marca.MAR_nombre", "asc");
			}
			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}




		function getUpId($mar_orden){ //  devuelve el id de la fila de arriba

		try {			

		

		$orden_fila_superior = $mar_orden - 1;    		



   		$this->db->select('

				MAR_ID,

				MAR_nombre,

				MAR_descripcion,

				MAR_orden,

				MAR_logo');

			$this->db->from('marca');			

			$this->db->where('MAR_orden', $orden_fila_superior);		

			$fila_superior = $this->db->get();

			$row = $fila_superior->row();

			$id_fila_superior = $row->MAR_ID;	

			



		if ($fila_superior->num_rows() > 0) {

			return $id_fila_superior;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}


	function getDowndId($make_orden){ // Almacena el id de la fila inferior		

		try {			

		

		$orden_fila_superior = $make_orden + 1;    		



   		$this->db->select('

				MAR_ID,

				MAR_nombre,

				MAR_descripcion,

				MAR_logo,

				MAR_orden');

			$this->db->from('marca');			

			$this->db->where('MAR_orden', $orden_fila_superior);		

			$fila_superior = $this->db->get();

			$row = $fila_superior->row();

			$id_fila_superior = $row->MAR_ID;	

			



		if ($fila_superior->num_rows() > 0) {

			return $id_fila_superior;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

		

	}



		function UpdateOrderDown($make_id, $make_orden){ // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7") 

		try {			

		

		$new_order_row = $make_orden + 1; 

   		$this->db->select('

				MAR_ID,

				MAR_nombre,

				MAR_descripcion,

				MAR_logo,

				MAR_orden');

		$this->db->from('marca');			

		$this->db->where('MAR_ID', $make_id);

		$data = array("MAR_orden" => $new_order_row);

		$this->db->update('marca', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

		

	}






		function UpdateOrderUp($make_id, $make_orden){ // Disminuye el orden actual en 1

		try {			

		$new_order_row = $make_orden - 1; 

   		$this->db->select('

				MAR_ID,

				MAR_nombre,

				MAR_descripcion,

				MAR_logo,

				MAR_orden');

		$this->db->from('marca');			

		$this->db->where('MAR_ID', $make_id);

		$data = array("MAR_orden" => $new_order_row);

		$this->db->update('marca', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

		

	}


		function UpdateOrderRowDown($make_orden, $id_fila_inferior){ // Actualiza el orden de la fila inferior

		try {	

		

		$new_order_row_down = $make_orden;

    	$data = array("MAR_orden" => $new_order_row_down);

		$this->db->where('MAR_ID', $id_fila_inferior);

		$this->db->update('marca', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

		

	}



 


	function UpdateOrderRowUp($make_orden, $id_fila_superior){ //actualiza el orden de la fila superior

		try {	

		

		$new_order_row_up = $make_orden;

    	$data = array("MAR_orden" => $new_order_row_up);

		$this->db->where('MAR_ID', $id_fila_superior);

		$this->db->update('marca', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

		

	}



		function updateOrderBeforeDelete($orden) {

		try {

			$this->db->query("UPDATE marca SET MAR_orden = MAR_orden - 1 WHERE MAR_orden > $orden");



			$affected = $this->db->affected_rows();

			if ($affected > 0) {

				return TRUE;

			} else {

				return FALSE;

			}



		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}




}
?>