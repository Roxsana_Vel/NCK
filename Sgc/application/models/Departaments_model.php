<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Departaments_Model extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}
	function getCounter($estado = '1')
	{
		try {
			$this->db->select('COUNT(*) as result');
			$this->db->from('departamento');
			$this->db->where('departamento.DPTO_estado', $estado);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				$object = $data->row_object();
				return $object->result;
			} else {
				return 0;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getDepartaments($estado = NULL)
	{
		try {
			$this->db->select('
				departamento.DPTO_ID,
				departamento.DPTO_nombre,
				departamento.DPTO_estado
			');
			$this->db->from('departamento');
			if (!is_null($estado)) {
				$this->db->where('DPTO_estado', $estado);
			}
			$this->db->order_by('DPTO_nombre ASC');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function insertDepartament($data)
	{
		$this->db->insert('departamento', $data);
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function getDepartamentId($id)
	{
		try {
			$this->db->where("DPTO_ID", $id);
			$data = $this->db->get('departamento');
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function updateDepartamentId($id, $data)
	{
		$this->db->where('DPTO_ID', $id);
		$this->db->update('departamento', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function deleteDepartamentId($id)
	{
		$this->db->delete('departamento', array('DPTO_ID' => $id));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function UpdateState($id, $estado)
	{
		$this->db->where('DPTO_ID', $id);
		$this->db->update('departamento', array( 'DPTO_estado' => $estado));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>