<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Districts_Model extends CI_Model {



	function __construct()

	{

		parent::__construct();

	}



	function getCounter($estado = '1')

	{

		try {

			$this->db->select('COUNT(*) as result');			

			$this->db->from('distrito');



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				$object = $data->row_object();



				return $object->result;

			} else {

				return 0;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function getDistricts($province = NULL)

	{

		try {

			$this->db->select('

				distrito.DIST_ID,

				distrito.DIST_nombre,

				distrito.PROV_ID,

				provincia.PROV_nombre,

				departamento.DPTO_nombre,

				departamento.DPTO_estado

			');			

			$this->db->from('distrito');

			$this->db->join('provincia', "provincia.PROV_ID = distrito.PROV_ID");

			$this->db->join('departamento', "provincia.DPTO_ID = departamento.DPTO_ID");

			

			if (!is_null($province)) {

				$this->db->where('distrito.PROV_ID', $province);

			}

			

			$this->db->order_by('DPTO_nombre ASC, PROV_nombre ASC, DIST_nombre ASC');



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function insertDistrict($data)

	{

		$this->db->insert('distrito', $data);

		$affected_rows = $this->db->affected_rows();



		if ($affected_rows > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function getDistrictId($id)

	{

		try {

			$this->db->select('

				distrito.DIST_ID,

				distrito.DIST_nombre,

				distrito.PROV_ID,

				provincia.PROV_ID,

				provincia.PROV_nombre,

				departamento.DPTO_ID,

				departamento.DPTO_nombre,

				departamento.DPTO_estado

			');			

			$this->db->from('distrito');

			$this->db->join('provincia', "provincia.PROV_ID = distrito.PROV_ID");

			$this->db->join('departamento', "provincia.DPTO_ID = departamento.DPTO_ID");

			$this->db->where("DIST_ID", $id);



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function updateDistrictId($id, $data)

	{

		$this->db->where('DIST_ID', $id);

		$this->db->update('distrito', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}

	

	function deleteDistrictId($id)

	{

		$this->db->delete('distrito', array('DIST_ID' => $id));

		$affected = $this->db->affected_rows();



		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



}



?>