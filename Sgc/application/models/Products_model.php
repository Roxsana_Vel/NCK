<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getProducts()
	{
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID,
				categoria.CAT_ID,
				categoria.CAT_nombre
				');
				#producto.PRO_precio,
			$this->db->from('producto');
			$this->db->join('categoria', 'producto.CAT_ID = categoria.CAT_ID');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getFilterProdByCat($id_cat){
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID
				');
				#producto.PRO_precio,
			$this->db->from('producto');
			$this->db->where('CAT_ID', $id_cat);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}

	function getProductsOrder($order = NULL){
	try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID,
				precio.PRE_soles,
				categoria.CAT_ID,
				categoria.CAT_nombre
			');
			$this->db->from('producto');
			$this->db->join('precio', 'producto.PRO_ID = precio.PROD_ID');
			$this->db->join('categoria', 'producto.CAT_ID = categoria.CAT_ID');
			$this->db->where('precio.NUSU_ID', 4); # 4: Cliente Final
			if (is_null($order)) {
				$this->db->order_by('PRO_orden','asc');
			} else {
				$this->db->order_by($order);
			}
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	public function getOrdenNumId($cat_id, $subcat_id = NULL){
		try {
			$this->db->select('producto.PRO_ID');
			$this->db->from('producto');
			$this->db->where('CAT_ID', $cat_id);
			$filas = $this->db->get();
			$data = $filas->num_rows();
			return $data;
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getUpId($pro_orden, $cat_id){ //  devuelve el id de la fila de arriba
		try {

		$orden_fila_superior = $pro_orden - 1;
   		$this->db->select('PRO_ID');
		$this->db->from('producto');
		$this->db->where('CAT_ID', $cat_id);
		$this->db->where('PRO_orden', $orden_fila_superior);
		$fila_superior = $this->db->get();
		$row = $fila_superior->row();
		$id_fila_superior = $row->PRO_ID;

		if ($fila_superior->num_rows() > 0) {
			return $id_fila_superior;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
		function getDowndId($pro_orden, $cat_id){ // Almacena el id de la fila inferior
		try {

		$orden_fila_superior = $pro_orden + 1;
   		$this->db->select('
				PRO_ID,
				PRO_nombre,
				PRO_estado,
				PRO_orden');
			$this->db->from('producto');
			$this->db->where('CAT_ID', $cat_id);
			$this->db->where('PRO_orden', $orden_fila_superior);
			$fila_superior = $this->db->get();
			$row = $fila_superior->row();
			$id_fila_superior = $row->PRO_ID;

		if ($fila_superior->num_rows() > 0) {
			return $id_fila_superior;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderUp($pro_id, $pro_orden){ // Disminuye el orden actual en 1
		try {

		$new_order_row = $pro_orden - 1;
   		$this->db->select('
				PRO_ID,
				PRO_nombre,
				PRO_estado,
				PRO_orden');
		$this->db->from('producto');
		$this->db->where('PRO_ID', $pro_id);
		$data = array("PRO_orden" => $new_order_row);
		$this->db->update('producto', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderDown($pro_id, $pro_orden){ // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")
		try {

		$new_order_row = $pro_orden + 1;
   		$this->db->select('
				PRO_ID,
				PRO_nombre,
				PRO_estado,
				PRO_orden');
		$this->db->from('producto');
		$this->db->where('PRO_ID', $pro_id);
		$data = array("PRO_orden" => $new_order_row);
		$this->db->update('producto', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderRowUp($pro_orden, $id_fila_superior){ //actualiza el orden de la fila superior
		try {

		$new_order_row_up = $pro_orden;
    	$data = array("PRO_orden" => $new_order_row_up);
		$this->db->where('PRO_ID', $id_fila_superior);
		$this->db->update('producto', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderRowDown($pro_orden, $id_fila_inferior){ // Actualiza el orden de la fila inferior
		try {

		$new_order_row_down = $pro_orden;
    	$data = array("PRO_orden" => $new_order_row_down);
		$this->db->where('PRO_ID', $id_fila_inferior);
		$this->db->update('producto', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function getProductsPhoto()
	{
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				galeria.GAL_ID,
				galeria.GAL_nombre
				');
				#producto.PRO_precio,
			$this->db->from('producto');
			$this->db->join('galeria', 'galeria.PRO_ID = producto.PRO_ID', 'left');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getProductsId($id)
	{
		try {
			$this->db->where("PRO_ID", $id);
			$data = $this->db->get('producto');
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getProductsElement()
	{
		try {
			$this->db->where("PRO_estado = 1");
			$data = $this->db->get('producto');
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}

	function insertProducts($data)
	{
		$this->db->insert('producto', $data);
		$affected_rows = $this->db->affected_rows();
		$last_id_inserted = $this->db->insert_id();
		if ($affected_rows > 0) {
			return $last_id_inserted;
		} else {
			return FALSE;
		}
	}
	function updateProductsId($id, $data)
	{
		$this->db->where("PRO_ID", $id);
		$this->db->update('producto', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function deleteProductsId($id)
	{
		$this->db->where('PRO_ID', $id);
		$this->db->delete('producto');

		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function deleteProductCatId($id)
	{
		$this->db->where('CAT_ID', $id);
		$this->db->delete('producto');

		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function UpdateState($pro_id, $estado)
	{
		$this->db->where('PRO_ID', $pro_id);
		$this->db->update('producto', array( 'PRO_estado' => $estado));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function updateOrderBeforeDelete($orden, $cat_id) {
		try {
			$this->db->query("UPDATE producto SET PRO_orden = PRO_orden - 1 WHERE PRO_orden > $orden AND CAT_ID = $cat_id");
			$affected = $this->db->affected_rows();
			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
		function getProducts_ByMake($id_make, $categ = -1, $subcat = -1)
		{
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID,
				precio.PRE_soles,
				marca.MAR_nombre
				');
			$this->db->from('producto');
			$this->db->join('precio', 'precio.PROD_ID = producto.PRO_ID', 'left');
			$this->db->join('marca', 'marca.MAR_ID = producto.MAR_ID', 'left');
			$this->db->where('producto.MAR_ID', $id_make);
			$this->db->where('precio.NUSU_ID','4');
			if ($categ != -1) {
				$this->db->where('producto.CAT_ID', $categ);
			}
			if ($subcat != -1) {

				$this->db->where('producto.SCAT_ID', $subcat);
			}
			$this->db->order_by('producto.PRO_nombre','ASC');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
}
?>