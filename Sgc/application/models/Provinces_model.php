<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Provinces_Model extends CI_Model {



	function __construct()

	{

		parent::__construct();

	}



	function getCounter($estado = '1')

	{

		try {

			$this->db->select('COUNT(*) as result');			

			$this->db->from('provincia');



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				$object = $data->row_object();



				return $object->result;

			} else {

				return 0;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function getProvinces($departament = NULL)

	{

		try {

			$this->db->select('

				provincia.PROV_ID,

				provincia.PROV_nombre,

				departamento.DPTO_ID,

				departamento.DPTO_nombre,

				departamento.DPTO_estado

			');			

			$this->db->from('provincia');

			$this->db->join('departamento', "provincia.DPTO_ID = departamento.DPTO_ID");

			

			if (!is_null($departament)) {

				$this->db->where('provincia.DPTO_ID', $departament);

			}

			

			$this->db->order_by('DPTO_nombre ASC, PROV_nombre ASC');



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function insertProvince($data)

	{

		$this->db->insert('provincia', $data);

		$affected_rows = $this->db->affected_rows();



		if ($affected_rows > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



	function getProvinceId($id)

	{

		try {

			$this->db->select('

				provincia.PROV_ID,

				provincia.PROV_nombre,

				provincia.DPTO_ID,

				departamento.DPTO_nombre,

				departamento.DPTO_estado

			');			

			$this->db->from('provincia');

			$this->db->join('departamento', "provincia.DPTO_ID = departamento.DPTO_ID");

			$this->db->where('provincia.PROV_ID', $id);



			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch(Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}



	function updateProvinceId($id, $data)

	{

		$this->db->where('PROV_ID', $id);

		$this->db->update('provincia', $data);



		$affected = $this->db->affected_rows();

		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}

	

	function deleteProvinceId($id)

	{

		$this->db->delete('provincia', array('PROV_ID' => $id));

		$affected = $this->db->affected_rows();



		if ($affected > 0) {

			return TRUE;

		} else {

			return FALSE;

		}

	}



}



?>