<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration_model extends CI_Model {

    function getConfiguration()
    {
        try {
            $this->db->select('
                ORG_ID,
                ORG_empresa,
                ORG_contacto,
                ORG_licencia,
                ORG_soporte,
                ORG_email,
                ORG_direccion1,
                ORG_direccion2,
                ORG_telefono,
                ORG_celular,
                ORG_sitio,
                ORG_horario,
                ORG_ubicacion,
                ORG_bancoUno,
                ORG_bancoDos,
                ORG_ecommerce,
                ORG_id_analytics,
                ORG_paypal_account,
                ORG_paypal_mode,
                ORG_mailchimp_apikey,
                ORG_mailchimp_list_id,
                ORG_facebook_app_id
            ');
            $this->db->from('organizacion');
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function update($data, $id)
    {
        $this->db->where('ORG_ID', $id);
        $this->db->update('organizacion', $data);

        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}