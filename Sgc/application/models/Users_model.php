<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUsers($order = null)
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.USU_login,
                usuario.USU_password,
                usuario.USU_estado,
                usuario.PER_ID,
                usuario.NUSU_ID,
                persona.PER_ID,
                persona.PER_nombres,
                persona.PER_apellidos,
                persona.PER_email,
                persona.PER_direccion,
                persona.PER_telefono,
                persona.PER_celular,
                persona.PER_departamento,
                persona.PER_provincia,
                persona.PER_distrito,
                persona.PER_documento,
                nivel_usuario.NUSU_ID,
                nivel_usuario.NUSU_nivel
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->join('nivel_usuario', 'nivel_usuario.NUSU_ID = usuario.NUSU_ID', 'LEFT');
            if (!is_null($order)) {
                $this->db->order_by($order);
            }
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getLevels()
    {
        try {
            $this->db->select('
                NUSU_ID,
                NUSU_nivel
            ');
            $this->db->from('nivel_usuario');
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUserSession()
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.USU_login,
                usuario.USU_password,
                usuario.USU_estado,
                usuario.PER_ID,
                usuario.NUSU_ID,
                persona.PER_ID,
                persona.PER_nombres,
                persona.PER_apellidos,
                persona.PER_email,
                persona.PER_telefono,
                persona.PER_celular,
                persona.PER_departamento,
                persona.PER_provincia,
                persona.PER_distrito,
                persona.PER_documento,
                persona.PER_sexo,
                persona.PER_fechaNacimiento,
                persona.PER_direccion
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->where('usuario.USU_ID', $this->session->userdata('sgc_id'));
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getAdmin()
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.USU_login,
                usuario.USU_password,
                usuario.USU_estado,
                usuario.PER_ID,
                usuario.NUSU_ID,
                persona.PER_ID,
                persona.PER_nombres,
                persona.PER_email,
                persona.PER_direccion,
                persona.PER_telefono,
                persona.PER_departamento,
                persona.PER_provincia,
                persona.PER_distrito,
                persona.PER_documento
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->join('nivel_usuario', 'nivel_usuario.NUSU_ID = usuario.NUSU_ID', 'LEFT');
            $this->db->where('usuario.NUSU_ID', 1);
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUserId($id)
    {
        try {
            $this->db->where('USU_ID', $id);
            $data = $this->db->get('usuario');

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUserPeopleId($id)
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.USU_login,
                usuario.USU_password,
                usuario.USU_estado,
                usuario.PER_ID,
                usuario.NUSU_ID,
                persona.PER_ID,
                persona.PER_nombres,
                persona.PER_apellidos,
                persona.PER_email,
                persona.PER_telefono,
                persona.PER_celular,
                persona.PER_departamento,
                persona.PER_provincia,
                persona.PER_distrito,
                persona.PER_documento,
                persona.PER_sexo,
                persona.PER_fechaNacimiento,
                persona.PER_direccion
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->join('empresa', 'empresa.EMP_ID = persona.EMP_ID', 'LEFT');
            $this->db->where('usuario.USU_ID', $id);
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUserPeopleGiroId($id)
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.PER_ID,
                persona.PER_ID,
                persona.EMP_ID,
                persona.PER_tipFac,
                empresa.*,
                giro.*
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->join('empresa', 'empresa.EMP_ID = persona.EMP_ID', 'LEFT');
            $this->db->join('giro', 'giro.GIR_ID = empresa.GIR_ID', 'LEFT');
            $this->db->where('usuario.USU_ID', $id);
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUserPeopleGiro()
    {
        try {
            $this->db->select('
                giro.*
            ');
            $this->db->from('giro');
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getUsersPeopleId($id)
    {
        try {
            $this->db->select('
                usuario.USU_ID,
                usuario.PER_ID,
                persona.PER_ID
            ');
            $this->db->from('usuario');
            $this->db->join('persona', 'persona.PER_ID = usuario.PER_ID', 'LEFT');
            $this->db->where('usuario.USU_ID', $id);
            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function updateUserId($id, $values)
    {
        $this->db->where('USU_ID', $id);
        $this->db->update('usuario', $values);
        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserValidation($user)
    {
        $this->db->where('usuario.USU_login', $user);
        $data = $this->db->get('usuario');

        if ($data->num_rows() > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function addAccountData($datos_cuenta)
    {
        $this->db->insert('usuario', $datos_cuenta);
        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function deleteUserId($id)
    {
        $this->db->where('USU_ID', $id);
        $this->db->delete('usuario');
        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function existUser($user)
    {
        $this->db->where('USU_login', $user);
        $data = $this->db->get('usuario');

        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validPass($array)
    {
        $this->db->where($array);
        $data = $this->db->get('usuario');

        if ($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function UpdateState($usu_id, $estado)
    {
        $this->db->where('USU_ID', $usu_id);
        $this->db->update('usuario', array('USU_estado' => $estado));
        $affected = $this->db->affected_rows();

        if ($affected > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * insertar nuevo usuario
     * vinculado a un asociado
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function insertUser($data)
    {
        $this->db->insert('usuario', $data);
        return $this->db->insert_id();
    }

    public function getUsuarioAsociadoId($asocId)
    {
        $usuId = $this->db->get_where('usuario', array('USU_asoc_id' => $asocId))->row('USU_ID');

        $datosUsuario = $this->db->get_where('usuario', array('USU_asoc_id' => $asocId))->row();

        return $datosUsuario;
    }

    public function userEditar($usuId, $dataUsuarioEditar)
    {
        $this->db->where('USU_ID', $usuId);
        $this->db->update('usuario', $dataUsuarioEditar);
    }

    /**
     * obtener el detalle de un usuario por su id
     * @param  [type] $usuId [description]
     * @return [type]        [description]
     */
    public function getUsuario($usuId)
    {
        return $this->db->get_where('usuario', array('USU_ID' => $usuId))->row();
    }
}
