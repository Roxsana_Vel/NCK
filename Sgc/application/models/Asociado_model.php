<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asociado_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * obtener todas las empresas asociadas activas
     * @return [type] [description]
     */
    public function getAsociados()
    {
        $this->db->select('*');
        $this->db->from('empresa_asociada');
        $empresasAsociadas = $this->db->get()->result();

        return $empresasAsociadas;
    }

    /**
     * obtener datos del asociado por su id
     * @param  [type] $asocId [description]
     * @return [type]         [description]
     */
    public function getAsociado($asocId)
    {
        $datosAsociado = $this->db->get_where('empresa_asociada', array(
            'ASOC_id' => $asocId,
        ))->row();

        return $datosAsociado;
    }

    /**
     * guardar los datos generales de un nuevo asociado
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function asociadoNuevoGuardar($data)
    {
        $this->db->insert('empresa_asociada', $data);

        return $this->db->insert_id();
    }

    /**
     * editar datos de la empresa asociada pro su id
     * @param  [type] $asocId             [description]
     * @param  [type] $dataAsociadoEditar [description]
     * @return [type]                     [description]
     */
    public function asociadoEditar($asocId, $dataAsociadoEditar)
    {
        $this->db->where('ASOC_id', $asocId);
        $this->db->update('empresa_asociada', $dataAsociadoEditar);
    }

    /**
     * habilitar un socio
     * o
     * deshabilitar un socio
     * segun su estado actual
     * @param  [type] $asocId [description]
     * @return [type]         [description]
     */
    public function habilitar_deshabilitar($asocId)
    {
        $estadoActual = $this->db->get_where('empresa_asociada', array('ASOC_id' => $asocId))->row('ASOC_estado');

        if ($estadoActual == 'habilitado') {
            $asocEstado = 'deshabilitado';
            $usuEstado  = 0;
        }
        else {
            $asocEstado = 'habilitado';
            $usuEstado  = 1;
        }

        /** cambiar estado en empresa_asociada */
        $this->db->where('ASOC_id', $asocId);
        $this->db->update('empresa_asociada', array(
            'ASOC_estado' => $asocEstado,
        ));

        /** cambiar estado en usuario */
        $usuarioId = $this->db->get_where('usuario', array('USU_asoc_id' => $asocId))->row('USU_ID');

        $this->db->where('USU_ID', $usuarioId);
        $this->db->update('usuario', array(
            'USU_estado' => $usuEstado,
        ));
    }

    /**
     * eliminar definitivamente, sin opcion a revertir, a un asociado seleccionado
     * @param  [type] $asocId [description]
     * @return [type]         [description]
     */
    public function eliminar($asocId)
    {
        /** eliminar en empresa_asociada */
        $this->db->where('ASOC_id', $asocId);
        $this->db->delete('empresa_asociada');

        /** eliminar en usuario */
        $usuarioId = $this->db->get_where('usuario', array('USU_asoc_id' => $asocId))->row('USU_ID');

        $this->db->where('USU_ID', $usuarioId);
        $this->db->delete('usuario');
    }

    /**
     * contar el total de asociados
     * habilitados o deshabilitados
     *
     * @param  string $estado [description]
     * @return [type]         [description]
     */
    public function countAsociados($estado='habilitado')
    {
        $asociados = $this->db->get_where('empresa_asociada', array(
            'ASOC_estado' => $estado
        ))->result();

        return sizeof($asociados);
    }

    public function getEmailAsociados()
    {
        $this->db->select('ASOC_email');
        $this->db->from('empresa_asociada');
        $this->db->where('ASOC_estado', 'habilitado');

        $asociados = $this->db->get()->result();

        $resultado = array();
        foreach ($asociados as $asociado) {
            $resultado[] = $asociado->ASOC_email;
        }

        return $resultado;
    }
}