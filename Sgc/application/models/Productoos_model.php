<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getArticulos()
    {
        $this->db->select('*');
        $this->db->from('producto');
        return $this->db->get()->result();
    }

    public function getNovedades()
    {
        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('EVE_tipo', 'novedad');
        return $this->db->get()->result();
    }

    
    /*---------------------------*/
    public function getProductos()
    {
        $this->db->select('*');
        $this->db->from('producto');
        
        return $this->db->get()->result();
    }

    public function getArticulo($artId)
    {
        $datosArticulo = $this->db->get_where('producto', array(
            'PRO_ID' => $artId,
        ))->row();

        return $datosArticulo;
    }

    public function guardarNuevo($data)
    {
        $this->db->insert('producto', $data);

        return $this->db->insert_id();
    }

    public function guardarEdicion($artId, $data)
    {
        $this->db->where('PRO_ID', $artId);

        return $this->db->update('producto', $data);
    }

    public function activar_desactivar($artId)
    {
        $estadoAnterior = $this->db->get_where('producto', array('PRO_ID' => $artId))->row('PRO_estado');

        $nuevoEstado = ($estadoAnterior == 1) ? 0 : 1;
        /** cambiar estado en empresa_asociada */
        $this->db->where('PRO_ID', $artId);
        $this->db->update('evento', array(
            'PRO_estado' => $nuevoEstado,
        ));
    }

    public function eliminar($artId)
    {
        $this->db->where('PRO_ID', $artId);
        $this->db->delete('producto');
    }

    public function countArticulos($estado = 1)
    {
        $this->db->where('PRO_estado', $estado);
        return $this->db->get('producto')->num_rows();
    }
}