<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acuerdo_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAcuerdos()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        return $this->db->get()->result();
    }

    public function getInformesConsejo()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        $this->db->where('ACU_tipo_informe', 'consejo directivo');
        return $this->db->get()->result();
    }

    public function getInformesAsamblea()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        $this->db->where('ACU_tipo_informe', 'asamblea de socios');
        return $this->db->get()->result();
    }

    public function getInformesReuniones()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        $this->db->where('ACU_tipo_informe', 'reuniones de trabajo');
        return $this->db->get()->result();
    }

    public function getInformesProyectos()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        $this->db->where('ACU_tipo_informe', 'proyectos');
        return $this->db->get()->result();
    }

    public function getInformesBoletin()
    {
        $this->db->select('*');
        $this->db->from('acuerdos');
        $this->db->where('ACU_tipo_informe', 'boletin');
        return $this->db->get()->result();
    }

    /**
     * obtener los dis ultimos acuerdos
     * para la vista de inicio
     * @return [type] [description]
     */
    public function get2ultimos()
    {
        $this->db->select('*');
        $this->db->where('ACU_estado', 1);
        $this->db->order_by('ACU_fecha', 'desc');
        return $this->db->get('acuerdos', 2)->result();
    }

    public function getAcuerdo($acuId)
    {
        $datosAcuerdo = $this->db->get_where('acuerdos', array(
            'ACU_ID' => $acuId,
        ))->row();

        return $datosAcuerdo;
    }

    public function guardarNuevo($data)
    {
        $this->db->insert('acuerdos', $data);

        return $this->db->insert_id();
    }

    public function guardarEdicion($acuId, $data)
    {
        $this->db->where('ACU_ID', $acuId);

        return $this->db->update('acuerdos', $data);
    }

    public function eliminar($acuId)
    {
        $this->db->where('ACU_ID', $acuId);
        $this->db->delete('acuerdos');
    }

    public function activar_desactivar($acuId)
    {
        $estadoActual = $this->db->get_where('acuerdos', array('ACU_ID' => $acuId))->row('ACU_estado');

        if ($estadoActual == 1) {
            $estadoCambiado = 0;
        } else {
            $estadoCambiado = 1;
        }

        $this->db->where('ACU_ID', $acuId);
        $this->db->update('acuerdos', array(
            'ACU_estado' => $estadoCambiado,
        ));
    }

    public function countAcuerdos($estado = 1)
    {
        $this->db->where('ACU_estado', $estado);
        return $this->db->get('acuerdos')->num_rows();
    }
}