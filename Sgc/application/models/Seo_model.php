<?php

class Seo_model extends CI_Model {

    public function get($pagina)
    {
        $this->db->where('page', $pagina);
        return $this->db->get('seo_configuration')->row();
    }

    public function update($pagina, $data)
    {
        $this->db->where('page', $pagina);
        return $this->db->update('seo_configuration', $data);
    }
}