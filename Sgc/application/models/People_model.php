<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class People_model extends CI_Model
{

    public function __construct()
    {

        parent::__construct();

    }

    public function addPersonalData($datos_personales)
    {

        $this->db->insert('persona', $datos_personales);

        $data = $this->db->insert_id();

        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {

            return $data;

        } else {

            return false;

        }

    }

    public function deletePeopleId($id)
    {

        $this->db->where('PER_ID', $id);

        $this->db->delete('persona');

        $affected = $this->db->affected_rows();

        if ($affected > 0) {

            return true;

        } else {

            return false;

        }

    }

    public function updatePeopleId($id, $data)
    {

        $this->db->where("PER_ID", $id);

        $this->db->update('persona', $data);

        $affected = $this->db->affected_rows();

        if ($affected > 0) {

            return true;

        } else {

            return false;

        }

    }

    public function updatePersonaEmpresaId($id_per, $id_emp, $TipoFac, $data1, $data2)
    {

        if ($TipoFac == "empresa") {

            if (!empty($id_emp)) {

                $this->db->update('empresa', $data2);

            } else {

                $this->db->insert('empresa', $data2);

                $data1['EMP_ID'] = $this->db->insert_id();

            }

        }

        $this->db->where("PER_ID", $id_per);

        $this->db->update('persona', $data1);

        if ($TipoFac != "empresa") {

            if (!empty($id_emp)) {

                $this->db->where('EMP_ID', $id_emp);

                $this->db->delete('empresa');

            }

        }

        $affected = $this->db->affected_rows();

        if ($affected > 0) {

            return true;

        } else {

            return false;

        }

    }

    //empenzando owo

    public function updateCuentId($id, $data)
    {

        $this->db->where("USU_ID", $id);

        $this->db->update('usuario', $data);

        $affected = $this->db->affected_rows();

        if ($affected > 0) {

            return true;

        } else {

            return false;

        }

    }

    //finzalizando owo

}
