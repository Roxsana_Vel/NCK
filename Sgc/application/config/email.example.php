<?php

$config['protocol']  = 'mail';
$config['mailpath']  = '/usr/sbin/sendmail';
$config['useragent'] = 'nck.com';
$config['charset']   = 'utf-8';
$config['mailtype']  = 'html';
$config['wordwrap']  = true;