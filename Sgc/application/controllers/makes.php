<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Makes extends CI_Controller {

 

	function __construct()

	{

		parent::__construct();

		$this->load->model('makes_model');

		$this->load->model('categorys_model');

		/*$this->load->view('templates/session_validation');*/

	}

	

	public function index()

	{
        $pagina             = new stdClass();
        $pagina->vista      = 'makes/list_makes';
        $pagina->menu       = 'makes';
        $pagina->subMenu    = 'makes-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;

		$data['makes'] = $this->makes_model->getMakes('marca.MAR_orden');

		/*$data['categorys'] = $this->categorys_model->getCategoryOrder("CAT_nombre");*/

		$this->load->view("template/template", $data);

			

	}



	public function frm_add_make() 
    {
        
        $pagina             = new stdClass();
        $pagina->vista      = 'makes/add_make';
        $pagina->menu       = 'makes';
        $pagina->subMenu    = 'makes-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		/*$data['orden']=$orden;*/
		$this->load->view("template/template",$data);

	}

	

	public function add_make()

	{

		$descripcion = $this->input->post('descripcion');

		$marca_nombre = $this->input->post('nombre');

		$orden = $this->input->post('orden');

		$orden+=1;

		if (!empty($marca_nombre)) {
			
			//*******Subida de Logotipo
				$i=0;

				foreach($_FILES as $file=>$file_info) {

				    $nombre = $file_info['name'];

				    $ruta_temporal = $file_info['tmp_name'];


				    if ($i==0) {		
						
						if ($nombre != '') {

							$ext = strtolower(end(explode('.', $nombre)));
							
							$id_logo="".uniqid();
							
							$upload_path = './uploads/logos/';

							$this->load->library('image_lib');

							
							$config = array(

								'width' => 400,

								'height' => 400,

								'source_image' => $ruta_temporal,

								'master_dim' => 'width',

								'new_image' => $upload_path . "img_$id_logo.$ext"

							);

							$this->image_lib->initialize($config);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

							

							$configThumb = array();



							$configThumb['width'] = 100;

							$configThumb['height'] = 100;

					        $configThumb['source_image'] = $ruta_temporal;

					        $configThumb['new_image'] = $upload_path."/thumbs/img_$id_logo.$ext";

					        $this->image_lib->initialize($configThumb);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

							
					        $archivo_logo="img_$id_logo.$ext";

				       		$i=$i+1;
						


						}else{
							$archivo_logo="";
						}

							


					}else{


							if ($nombre != '') {

							$ext = strtolower(end(explode('.', $nombre)));
							
							$id_banner="".uniqid();
							
							$upload_path = './uploads/banners/';

							$this->load->library('image_lib');

							
							$config = array(

								'width' => 1180,

								'height' => 275,

								'source_image' => $ruta_temporal,

								'master_dim' => 'width',

								'new_image' => $upload_path . "img_$id_banner.$ext"

							);

							$this->image_lib->initialize($config);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

							

							$configThumb = array();



							$configThumb['width'] = 100;

							$configThumb['height'] = 100;

					        $configThumb['source_image'] = $ruta_temporal;

					        $configThumb['new_image'] = $upload_path."/thumbs/img_$id_banner.$ext";

					        $this->image_lib->initialize($configThumb);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

							
					        $archivo_banner="img_$id_banner.$ext";

				       		$i=$i+1;

							}else{
							
							$archivo_banner="";
							
							}

							

						}





				}

			
						$data = array(

							"MAR_descripcion" => $descripcion,

							'MAR_logo' => $archivo_logo,

							"MAR_nombre" => $marca_nombre,

							"MAR_orden" => $orden,

							"MAR_banner" => $archivo_banner

						);



			$add = $this->makes_model->insertMake($data);



			if ($add == TRUE) {

				$this->session->set_flashdata('success_message', 'Se agregó la marca correctamente');

				redirect(base_url('makes'));

			} else {

				echo "<h3>ERROR: No se pudo agregar correctamente la marca.</h3>";

				echo anchor(base_url()."makes/", 'Ir atr&aacute;s');

			}

		} else {

			echo "<h3>ERROR: Ingrese los campos requeridos.</h3>";

			echo anchor(base_url()."makes/", 'Ir atr&aacute;s');

		}

	}



	public function frm_edit_make($id) {
         $pagina             = new stdClass();
        $pagina->vista      = 'makes/edit_make';
        $pagina->menu       = 'makes';
        $pagina->subMenu    = 'makes-edit';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		$data['makes'] = $this->makes_model->getMakeId($id);
		
		$this->load->view("template/template", $data);
		
	}



	public function update_make($id) 

	{	

				$i=0;

				foreach($_FILES as $file=>$file_info){

				    $nombre = $file_info['name'];

				    $ruta_temporal = $file_info['tmp_name'];

					if ($i==0) {
					
						if ($nombre != '') {

							$image_path = './uploads/logos/';

							$image_name = $this->input->post('image_name');
							
							$cod_logo=uniqid()."";

							@unlink($image_path . $image_name);

							$ext = strtolower(end(explode('.', $nombre)));

							$data = array(

								"MAR_nombre" => $this->input->post('nombre'),

								"MAR_descripcion" => $this->input->post('descripcion'),

								'MAR_logo' => "img_$cod_logo.$ext"

							);


							$update = $this->makes_model->updateMakeId($id, $data);


							$this->load->library('image_lib');

							$config = array(

								'width' => 400,

								'height' => 400,

								'source_image' => $ruta_temporal,

								'master_dim' => 'width',

								'new_image' => $image_path . "img_$cod_logo.$ext"

							);

							
							$this->image_lib->initialize($config);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

					        $configThumb = array();

							$configThumb['width'] = 100;

							$configThumb['height'] = 30;

					        $configThumb['source_image'] = $ruta_temporal;

					        $configThumb['new_image'] = $image_path."/thumbs/img_$cod_logo.$ext";

					        $this->image_lib->initialize($configThumb);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

						} else{

							$data = array(
								"MAR_nombre" => $this->input->post('nombre'),

								"MAR_descripcion" => $this->input->post('descripcion'),
							);

							$update = $this->makes_model->updateMakeId($id, $data);

					   }	

				   $i=$i+1;

				   }else{

				   	if ($nombre != '') {

							$image_path = './uploads/banners/';

							$image_name = $this->input->post('image_name');
							
							$cod_banner=uniqid()."";

							@unlink($image_path . $image_name);

							$ext = strtolower(end(explode('.', $nombre)));

							$data = array(

								'MAR_banner' => "img_$cod_banner.$ext"

							);

							$update = $this->makes_model->updateMakeId($id, $data);

							$this->load->library('image_lib');

							$config = array(

								'width' => 1180,

								'height' => 275,

								'source_image' => $ruta_temporal,

								'master_dim' => 'width',

								'new_image' => $image_path . "img_$cod_banner.$ext"

							);

							
							$this->image_lib->initialize($config);

					        $this->image_lib->resize();

					        $this->image_lib->clear();

					        $configThumb = array();

							$configThumb['width'] = 100;

							$configThumb['height'] = 100;

					        $configThumb['source_image'] = $ruta_temporal;

					        $configThumb['new_image'] = $image_path."/thumbs/img_$cod_banner.$ext";

					        $this->image_lib->initialize($configThumb);

					        $this->image_lib->resize();

					        $this->image_lib->clear();
						}
				   }
		
				}


		if ($update == TRUE) {

			$this->session->set_flashdata('success_message', 'Se actualizó la marca correctamente');

			redirect(base_url('makes'));

		} 

		else {

			echo "Error al actualizar la marca.<br>";

			echo "<a href='".base_url()."makes/'>Ir atr&aacute;s</a>";

		}

	}



	public function delete_make_id() 

	{	


		$id = $this->input->post('eliminarId');
		$orden = $this->input->post('orden');

		$delete = $this->makes_model->deleteMakeId($id);

		$update=$this->makes_model->updateOrderBeforeDelete($orden);

		if ($delete == TRUE) {

			$this->session->set_flashdata('success_message', 'Se eliminó la marca correctamente');

		}else {

			$this->session->set_flashdata('error_message', 'Ocurrió un error al intentar eliminar el registro');

		}


	}


	//**********************************************


		public function filterByCategoria(){
            

			$id_categ = $this->input->post('categoria');

			$id_subcat = $this->input->post('subcat');

			if ($id_subcat=="-1") {
				
				$data['data'] = $this->makes_model->getMakesByCat($id_categ);

				$this->load->view("ajax_view", $data);

			}else{

			$data['data'] = $this->makes_model->getMakesByCat($id_categ, $id_subcat);

			$this->load->view("ajax_view", $data);

			}
		}


	/*	public function filterByCategoria_Subcat(){

			

			
			$id_categ = $this->input->post('categoria');

			$id_subcat = $this->input->post('subcat');



			if ($id_categ=="-1") {

				$data['makes'] = $this->makes_model->getMakes('marca.MAR_orden');

				$data['categorys'] = $this->categorys_model->getCategoryOrder("CAT_nombre");
				
				$this->load->view("makes/tbl_makes", $data);

			}else{

				if ($id_subcat=="-1") {
					
					$data['makes'] = $this->makes_model->getMakesByCat($id_categ,-1,"marca.MAR_orden");

					$this->load->view("makes/tbl_makes", $data);

				}else{

					$data['makes'] = $this->makes_model->getMakesByCat($id_categ, $id_subcat,"marca.MAR_orden");

					$this->load->view("makes/tbl_makesResume", $data);

				}

			}



		}*/



		public function subir(){

		$make_id = $this->uri->segment(3);

		$make_orden = $this->uri->segment(4);

		$id_fila_superior = $this->makes_model->getUpId($make_orden); // Almacena el id de la fila superior

		$this->makes_model->UpdateOrderUp($make_id, $make_orden); 	// Disminuye el orden actual en 1 de la fila la cual será subida ("Ejemplo de la posicion 7 a la 6")

		$update = $this->makes_model->UpdateOrderRowUp($make_orden, $id_fila_superior); // Actualiza el orden de la fila superior


			if ($update != FALSE) {

			$data['makes'] = $this->makes_model->getMakes();	

			$this->load->view("makes/subir", $data); // Muestra la tabla refrescada posterior a la accion de subir

			}

			else{

				echo "Error al actualizar orden de las categorias";

			}			

		}





	public function bajar(){

		

		$make_id = $this->uri->segment(3);

		$make_orden = $this->uri->segment(4);

		$id_fila_inferior = $this->makes_model->getDowndId($make_orden); // Almacena el id de la fila inferior		

		$this->makes_model->UpdateOrderDown($make_id, $make_orden); // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7") 

		$update = $this->makes_model->UpdateOrderRowDown($make_orden, $id_fila_inferior); // Actualiza el orden de la fila inferior



		if ($update != FALSE) {

		$data['makes'] = $this->makes_model->getMakes();	

		$this->load->view("makes/bajar", $data); // Muestra la tabla refrescada posterior a la accion de bajar

		}

		else{

			echo "Error al actualizar orden de las categorias";

		}			

	}


}

?>