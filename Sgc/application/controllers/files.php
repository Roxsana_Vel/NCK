<?php
class Files extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('gallery_model');
		$this->load->model('files_model');
		$this->load->model('products_model');
		$this->load->model('prices_model');
		$this->load->model('banner_model');
		$this->load->model('events_model');
		/*$this->load->view('template/session_validation');*/
	}

    public function add_products()
    {
        $this->load->helper('inflector');
        $this->load->helper('text');

        $cat_id = $this->input->post('categoria_pro');
        $orden_num = $this->products_model->getOrdenNumId($cat_id) + 1;

        $data = array(
            'PRO_nombre'      => $this->input->post('nombre_pro'),
            'PRO_descripcion' => $this->input->post('descripcion_pro'),
            'PRO_destacado'   => $this->input->post('destacado_pro'),
            'CAT_ID'          => $cat_id,
            'SCAT_ID'         => $this->input->post('subcategoria_pro'),
            'PRO_estado'      => $this->input->post('estado_pro'),
            'PRO_orden'       => $orden_num,
            'PRO_stock'       => $this->input->post('stock'),
            'PRO_url'         => url_title(strtolower(convert_accented_characters($this->input->post('nombre_pro')))),
            /*'id_sello'        => $this->input->post('sello'),*/
        );
        $id_pro = $this->products_model->insertProducts($data);

        if ($id_pro !== FALSE) {
            $data_precios = array(
                array(
                    'PROD_ID'     => $id_pro,
                    'NUSU_ID'     => 2, # MAYORISTA TIPO A
                    'PRE_soles'   => $this->input->post('mayor_a_soles')
                ),
                array(
                    'PROD_ID'     => $id_pro,
                    'NUSU_ID'     => 3, # MAYORISTA TIPO B
                    'PRE_soles'   => $this->input->post('mayor_b_soles')
                ),
                array(
                    'PROD_ID'     => $id_pro,
                    'NUSU_ID'     => 4, # CLIENTE
                    'PRE_soles'   => $this->input->post('cliente_soles')
                ),
                array(
                    'PROD_ID'     => $id_pro,
                    'NUSU_ID'     => 5, # MAYORISTA TIPO C
                    'PRE_soles'   => $this->input->post('mayor_c_soles')
                )
            );
            $id_pre = $this->prices_model->insertPrices($data_precios);

            if ($id_pre !== FALSE) {
                if ($this->doUpload('products', 'PRO_ID', $id_pro, 'images')) {
                    $this->session->set_flashdata('success_message', 'Se agregó el producto correctamente');
                } else {
                    $this->session->set_flashdata('error_message', 'Ocurrió un error al subir los archivos');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Ocurrió un error al registrar los precios');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Ocurrió un error al agregar el producto');
        }

        redirect('products');
    }

    public function edit_products()
    {
        $this->load->helper('inflector');
        $this->load->helper('text');

        $id_prod = $this->input->post('id_prod');
        $old_cat_id = $this->input->post('id_category_ant');
        $orden = $this->input->post('orden_prod');
        $cat_id = $this->input->post('categoria_pro');

        $data = array(
            'PRO_nombre'      => $this->input->post('nombre_pro'),
            'PRO_descripcion' => $this->input->post('descripcion_pro'),
            'PRO_destacado'   => $this->input->post('destacado_pro'),
            'CAT_ID'          => $cat_id,
            'SCAT_ID'         => $this->input->post('subcategoria_pro'),
            'PRO_estado'      => $this->input->post('estado_pro'),
            'PRO_stock'       => $this->input->post('stock'),
            'PRO_url'         => url_title(strtolower(convert_accented_characters($this->input->post('nombre_pro')))),
            'id_sello'        => $this->input->post('sello'),
        );

        if ($cat_id != $old_cat_id) {
            $this->products_model->updateOrderBeforeDelete($orden, $old_cat_id);
            $data['PRO_orden'] = $this->products_model->getOrdenNumId($cat_id) + 1;
        }

        $this->products_model->updateProductsId($id_prod, $data);
        $id_prec = $this->input->post('id_prec');
        $id_prec_a = $this->input->post('id_prec_a');
        $id_prec_b = $this->input->post('id_prec_b');
        $id_prec_c = $this->input->post('id_prec_c');

        $data_precios = array(
            array(
                'PRE_ID'      => $id_prec_a,
                'PRE_soles'   => $this->input->post('mayor_a_soles')
            ),
            array(
                'PRE_ID'      => $id_prec_b,
                'PRE_soles'   => $this->input->post('mayor_b_soles')
            ),
            array(
                'PRE_ID'      => $id_prec,
                'PRE_soles'   => $this->input->post('cliente_soles')
            ),
            array(
                'PRE_ID'      => $id_prec_c,
                'PRE_soles'   => $this->input->post('mayor_c_soles')
            )
        );
        $this->prices_model->updatePricesId($data_precios);

        if ($this->doUploadEdit('products', 'PRO_ID', $id_prod, 'images', 'gallery')) {
            $this->session->set_flashdata('success_message', 'Se actualizó el producto correctamente');
        } else {
            $this->session->set_flashdata('error_message', 'Ocurrió un error al subir los archivos');
        }

        redirect('products');
    }

    public function add_events()
    {
        $this->load->helper('inflector');
        $this->load->helper('text');

        $fecha = $this->input->post('fecha_eve');
        $fecha = implode('-', array_reverse(explode('/', $fecha)));

        $data = array(
            'categoria'       => $this->input->post('categoria_eve'),
            'EVE_titulo'      => $this->input->post('titulo_eve'),
            'EVE_fecha'       => $fecha,
            'EVE_resumen'     => $this->input->post('resumen_eve'),
            'EVE_descripcion' => $this->input->post('descripcion_eve'),
            'EVE_estado'      => $this->input->post('estado_eve'),
            'EVE_url'         => url_title(convert_accented_characters(strtolower($this->input->post('titulo_eve')))),
            'EVE_video'       => str_replace('https://www.youtube.com/watch?v=', '', $this->input->post('video'))
        );
        $id_eve = $this->events_model->insertEvent($data);

        if ($this->input->post('categoria_eve') == 'cocina') {
            $subcategorias = $this->input->post('subcategorias_agregadas');

            foreach ($subcategorias as $subcategoria) {
                $evento_subcategoria = array(
                    'id_evento'       => $id_eve,
                    'id_subcategoria' => $subcategoria
                );
                $this->db->insert('evento_subcategorias', $evento_subcategoria);
            }
        }

        if ($id_eve !== FALSE) {
            if ($this->doUpload('events', 'EVE_ID', $id_eve, 'images')) {
                $this->session->set_flashdata('success_message', 'Se registró el evento correctamente');
            } else {
                $this->session->set_flashdata('error_message', 'Ocurrió un error al subir los archivos');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Ocurrió un error al agregar el evento');
        }

        redirect('events');
    }

    public function edit_events()
    {
        $this->load->helper('inflector');
        $this->load->helper('text');

        $id_eve = $this->input->post('id_eve');
        $fecha = $this->input->post('fecha_eve');
        $fecha = implode('-', array_reverse(explode('/', $fecha)));

        $data = array(
            'categoria'       => $this->input->post('categoria_eve'),
            'EVE_titulo'      => $this->input->post('titulo_eve'),
            'EVE_fecha'       => $fecha,
            'EVE_resumen'     => $this->input->post('resumen_eve'),
            'EVE_descripcion' => $this->input->post('descripcion_eve'),
            'EVE_estado'      => $this->input->post('estado_eve'),
            'EVE_url'         => url_title(convert_accented_characters(strtolower($this->input->post('titulo_eve')))),
            'EVE_video'       => str_replace('https://www.youtube.com/watch?v=', '', $this->input->post('video'))
        );

        if ( ! $this->input->post('fecha_eve')) {
            unset($data['EVE_fecha']);
            unset($data['EVE_url']);
        }

        $this->events_model->updateEventId($id_eve, $data);

        $this->db->where('id_evento', $id_eve);
        $this->db->delete('evento_subcategorias');

        if ($this->input->post('categoria_eve') == 'cocina') {
            $subcategorias = $this->input->post('subcategorias_agregadas');

            foreach ($subcategorias as $subcategoria) {
                $evento_subcategoria = array(
                    'id_evento'       => $id_eve,
                    'id_subcategoria' => $subcategoria
                );
                $this->db->insert('evento_subcategorias', $evento_subcategoria);
            }
        }

        if ($this->doUploadEdit('events', 'EVE_ID', $id_eve, 'images', 'gallery')) {
            $this->session->set_flashdata('success_message', 'Se actualizó el evento correctamente');
        } else {
            $this->session->set_flashdata('error_message', 'Ocurrió un error al subir los archivos');
        }

        redirect('events');
    }

    public function doUpload($controller, $field, $id, $files) {
        $return = false;
        $updated = false;
        $upload_path = '../images/' . $controller . '/';
        $this->explodeFiles($files);

        foreach ($_FILES as $file => $file_info) {
            $nombre = $file_info['name'];
            $ruta_temporal = $file_info['tmp_name'];

            if ( ! empty($nombre)) {
                $updated = true;
                $ext = strtolower(end(explode('.', $nombre)));
                $data_img = array(
                    $field        => $id,
                    'GAL_archivo' => ''
                );
                $id_gal = $this->files_model->insertImagen($data_img);

                if ($id_gal !== FALSE) {
                    $data_gal = array(
                        'GAL_archivo' => strtolower(substr($controller, 0, 3)) . "$id-$id_gal.$ext"
                    );

                    $updated = $this->files_model->UpdateImagen($id_gal, $data_gal);

                    if ($updated) {
                        $this->load->library('image_lib');
                        $config = array(
                            'source_image' => $ruta_temporal,
                            'master_dim'   => 'width',
                            'new_image'    => $upload_path . $data_gal['GAL_archivo']
                        );
                        $configThumb = array(
                            'master_dim' => 'width'
                        );

                        if ($controller === 'products') {
                            $config['width'] = 500;
                            $config['height'] = 710;
                            $config['master_dim'] = 'height';
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $this->image_lib->clear();

                            $configThumb['width'] = 70;
                            $configThumb['height'] = 100;
                            $configThumb['master_dim'] = 'height';
                            $configThumb['source_image'] = $ruta_temporal;
                            $configThumb['new_image'] = $upload_path . 'thumbs/' . $data_gal['GAL_archivo'];
                            $this->image_lib->initialize($configThumb);
                            $this->image_lib->resize();
                            $this->image_lib->clear();

                            $configThumb['width'] = 340;
                            $configThumb['height'] = 480;
                            $configThumb['master_dim'] = 'height';
                            $configThumb['source_image'] = $ruta_temporal;
                            $configThumb['new_image'] = $upload_path . 'standard/' . $data_gal['GAL_archivo'];
                            $this->image_lib->initialize($configThumb);
                            $this->image_lib->resize();
                            $this->image_lib->clear();
                        } elseif ($controller === 'events') {
                            $config['width'] = 680;
                            $config['height'] = 300;
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $this->image_lib->clear();

                            $configThumb['width'] = 480;
                            $configThumb['height'] = 340;
                            $configThumb['source_image'] = $ruta_temporal;
                            $configThumb['new_image'] = $upload_path . 'standard/' . $data_gal['GAL_archivo'];
                            $this->image_lib->initialize($configThumb);
                            $this->image_lib->resize();
                            $this->image_lib->clear();
                        }

                        $return = true;
                    }
                }
            }
        }

        if ($updated) {
            return $return;
        } else {
            return true;
        }
    }

    public function doUploadEdit($controller, $field, $id, $files, $images) {
        $return = FALSE;

        if ($this->input->post($images)) {
            $excluded = $this->gallery_model->getExcludedBy($field, $id, $this->input->post($images));

            if ($excluded !== FALSE) {
                $to_delete = array();
                $files_to_delete = array();

                foreach ($excluded->result() as $row_excluded) {
                    $to_delete[] = $row_excluded->GAL_ID;
                    $files_to_delete[] = $row_excluded->GAL_archivo;
                }

                $deleted = $this->gallery_model->deleteManyGalleryId($controller, $to_delete, $files_to_delete);
            } else {
                $deleted = TRUE;
            }
        } else {
            $deleted = $this->gallery_model->deleteGalleryBy($controller, $field, $id);
        }

        if ($deleted) {
            $return = $this->doUpload($controller, $field, $id, $files);
        }
        return $return;
    }

    public function doUploadBanner()
    {
        foreach($_FILES as $file => $file_info) {
            $nombre = $file_info['name'];
            $ruta_temporal = $file_info['tmp_name'];

            if ($nombre != '') {
                $url = $this->input->post('url_ima1');
                $data = array(
                    'BAN_titulo'       => $this->input->post('titulo_ima1'),
                    'BAN_descripcion'  => $this->input->post('descripcion'),
                    'BAN_estado'       => $this->input->post('estado'),
                    'BAN_archivo'      => $nombre,
                    'BAN_url'          => (empty($url) ? NULL : $url)
                );

                $ext = strtolower(end(explode('.', $nombre)));
                $id_banner = $this->banner_model->insertBanner($data, $ext);

                $upload_path = '../images/banners/';
                $this->load->library('image_lib');

                $config = array(
                    'width'        => 1180,
                    'height'       => 275,
                    'source_image' => $ruta_temporal,
                    'master_dim'   => 'width',
                    'new_image'    => $upload_path . 'ban' . $id_banner . '.' . $ext
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                $configThumb = array();
                $configThumb['width'] = 100;
                $configThumb['height'] = 100;
                $configThumb['source_image'] = $ruta_temporal;
                $configThumb['new_image'] = $upload_path . 'thumbs/ban' . $id_banner . '.' . $ext;

                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();
                $this->image_lib->clear();
                $this->session->set_flashdata('success_message', 'Se agregó el banner correctamente');
            }
        }

        redirect('banner');
    }

    public function doUploadEditBanner()
    {
        $id_banner = $this->input->post('id_banner');

        foreach($_FILES as $file => $file_info) {
            $nombre = $file_info['name'];
            $ruta_temporal = $file_info['tmp_name'];

            if ($nombre != '') {
                $image_path = '../images/banners/';
                $thumb_path = $image_path . 'thumbs/';
                $image_name = $this->input->post('image_name');

                @unlink($image_path . $image_name);
                @unlink($thumb_path . $image_name);

                $url = $this->input->post('url_ima1');
                $ext = strtolower(end(explode('.', $nombre)));

                $data = array(
                    'BAN_titulo'       => $this->input->post('titulo_ima1'),
                    'BAN_descripcion'  => $this->input->post('descripcion'),
                    'BAN_estado'       => $this->input->post('estado'),
                    'BAN_archivo'      => 'ban' . $id_banner . '.' . $ext,
                    'BAN_url'          => (empty($url) ? NULL : $url)
                );
                $this->banner_model->updateBanner($id_banner, $data);

                $this->load->library('image_lib');

                $config = array(
                    'width'        => 1180,
                    'height'       => 275,
                    'source_image' => $ruta_temporal,
                    'master_dim'   => 'width',
                    'new_image'    => $image_path . 'ban' . $id_banner . '.' . $ext
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                $configThumb = array();
                $configThumb['width'] = 100;
                $configThumb['height'] = 100;
                $configThumb['source_image'] = $ruta_temporal;
                $configThumb['new_image'] = $image_path . 'thumbs/ban' . $id_banner . '.' . $ext;
                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();
                $this->image_lib->clear();

                $this->session->set_flashdata('success_message', 'Se actualizó el banner correctamente');
            } else {
                $url = $this->input->post('url_ima1');
                $data = array(
                    'BAN_titulo'      => $this->input->post('titulo_ima1'),
                    'BAN_descripcion' => $this->input->post('descripcion'),
                    'BAN_estado'      => $this->input->post('estado'),
                    'BAN_url'         => (empty($url) ? NULL : $url)
                );
                $updated = $this->banner_model->updateBanner($id_banner, $data);

                if ($updated) {
                    $this->session->set_flashdata('success_message', 'Se actualizó el banner correctamente');
                }
            }
        }

        redirect('banner', 'refresh');
    }

	function explodeFiles($name) {
		if (isset($_FILES[$name])) {
			$image_files = $_FILES[$name];
			if (is_array($image_files['name']) and is_array($image_files['type']) and is_array($image_files['tmp_name']) and is_array($image_files['error']) and is_array($image_files['size'])) {
				for ($i = 0; $i < count($image_files['name']); $i++) {
					if (!empty($image_files['name'])) {
						$_FILES['image' . $i] = array(
							'name' => $image_files['name'][$i],
							'type' => $image_files['type'][$i],
							'tmp_name' => $image_files['tmp_name'][$i],
							'error' => $image_files['error'][$i],
							'size' => $image_files['size'][$i]
						);
					}
				}
				unset($_FILES[$name]);
			}
		}
	}
}
?>