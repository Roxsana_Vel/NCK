<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect();
        }

        /** librerias */
        $this->load->library('migration');

    }

    public function run($version = null)
    {
        if (is_null($version)) {
            if ($this->migration->latest()) {
                echo 'Success latest<br>';
            } else {
                echo $this->migration->error_string();
            }
        } else {
            if ($this->migration->version($version)) {
                echo 'Success version ' . $version . '<br>';
            } else {
                echo $this->migration->error_string();
            }
        }
    }
}