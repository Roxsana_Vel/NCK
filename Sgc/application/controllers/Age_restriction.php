<?php

class Age_restriction extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect();
        }
    }

    public function index()
    {
        if ($this->input->post('age_restriction')) {
            if ($this->input->post('age_restriction') == 'Sí') {
                $this->session->set_userdata('is_adult', true);
            } else {
                $this->session->set_userdata('is_adult', false);
            }
        }

        echo json_encode(array('mayor_edad' => $this->session->userdata('is_adult')));
    }
}