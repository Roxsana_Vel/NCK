<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('cart_model');
        $this->load->helper('text');
    }

    public function index()
    {
        $this->data['cart'] = $this->session->userdata('cart');

        $this->load->view('templates/header');
        $this->load->view('cart/index', $this->data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $product = $this->input->post('product');

        if ($product) {
            $info_product = $this->cart_model->get_product($product);

            if (empty($info_product)) {
                return FALSE;
            }

            // bandera para saber si se agrego o no el producto al carrito
            $flag = 0;

            $cart = $this->session->userdata('cart');

            if ( ! $cart) {
                $add_cart = array(
                    'product'       =>  $info_product->PRO_ID,
                    'name'          =>  $info_product->PRO_nombre,
                    'photo'         =>  $info_product->GAL_archivo,
                    'abstract'      =>  $info_product->PRO_descripcion,
                    'qty'           =>  1,
                    'price'         =>  $info_product->PRE_soles,
                    'price_dolares' =>  $info_product->PRE_dolares,
                );

                $cart[] = $add_cart;
                $flag = 1;
            } else {
                foreach ($cart as $key => $item) {
                    if ($item['product'] == $product) {
                        $cart[$key]['qty']++;
                        $flag = 1;
                        break;
                    }
                }

                if ($flag == 0) {
                    $add_cart = array(
                        'product'       =>  $info_product->PRO_ID,
                        'name'          =>  $info_product->PRO_nombre,
                        'photo'         =>  $info_product->GAL_archivo,
                        'abstract'      =>  $info_product->PRO_descripcion,
                        'qty'           =>  1,
                        'price'         =>  $info_product->PRE_soles,
                        'price_dolares' =>  $info_product->PRE_dolares,
                    );

                    $cart[] = $add_cart;
                    $flag = 1;
                }
            }

            $this->session->set_userdata('cart', $cart);

            echo json_encode($flag);
        }
    }

    public function del()
    {
        $product = $this->input->post('product');

        if ($product) {
            $key = $this->_search_product($product);
            if ($key !== FALSE) {
                $cart = $this->session->userdata('cart');
                array_splice($cart, $key, 1);
                $this->session->set_userdata('cart', $cart);

                if (empty($cart)) {
                    echo json_encode('empty');
                } else {
                    echo json_encode(1);
                }
            }
        }
    }

    public function update_item()
    {
        $product = $this->input->post('product');
        $qty = $this->input->post('qty');

        $key = $this->_search_product($product);
        if ($key !== FALSE) {
            $cart = $this->session->userdata('cart');
            $cart[$key]['qty'] = $qty;
            $this->session->set_userdata('cart', $cart);

            echo json_encode("");
        }
    }

    private function _search_product($product)
    {
        $cart = $this->session->userdata('cart');

        if ($cart) {
            $flag = 0;
            foreach ($cart as $key => $item) {
                if ($item['product'] == $product) {
                    $flag = 1;
                    break;
                }
            }

            return ($flag == 1) ? $key : FALSE;
        }
    }
}