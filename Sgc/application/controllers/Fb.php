<?php

class Fb extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('user_model');

    }

    public function login()
    {
        $data = $this->input->post('data');
        $next_redirect = $this->input->post('next');

        $email = $data['email'];

        $id = $this->user_model->login_fb($email);

        if ($id == null) {
            $this->user_model->create_using_fb($data);
            $this->user_model->login_fb($email);
        }

        echo $next_redirect;
    }

    public function close()
    {
        echo '<script>window.close();</script>';
    }
}