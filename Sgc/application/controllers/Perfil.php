<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perfil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect();
        }

        /** modelos */
        $this->load->model('users_model');
        $this->load->model('asociado_model');
        $this->load->model('makes_model');
        $this->load->model('organizacion_model');
    }

    /**
     * vista del listado de asociados
     * @return [type] [description]
     */
    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->menu       = 'perfil';
        $pagina->subMenu    = '';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Editar mi perfil';

        $data['paginaDatos'] = $pagina;

        $usuId        = $this->session->userdata('sgc_id');
        $datosUsuario = $this->users_model->getUsuario($usuId);

        /**
         * datos de usuario para editar
         */
        $data['datosUsuario'] = $datosUsuario;

        /** si es asociado */
        if ($datosUsuario->NUSU_ID == 3) {
            $pagina->vista         = 'perfil/editar';
            $datosAsociado         = $this->asociado_model->getAsociado($datosUsuario->USU_asoc_id);
            $data['datosAsociado'] = $datosAsociado;
        }
        /** si es adminisitrador */
        elseif (in_array($datosUsuario->NUSU_ID, [2, 1])) {
            $pagina->vista             = 'perfil/editar_organizacion';
            $datosOrganizacion         = $this->organizacion_model->getOrganizacion();
            $data['datosOrganizacion'] = $datosOrganizacion;
        }

        /** array de marcas */
        $marcas              = $this->makes_model->getMakes();
        $data['arrayMarcas'] = ($marcas === false) ? array() : $marcas;

        $this->load->view('template/template', $data);
    }

    /**
     * guardar formulario de editar perfil
     * @return [type] [description]
     */
    public function guardar_edicion_perfil()
    {
        $usuId  = $this->session->userdata('sgc_id');
        $asocId = $this->input->post('inputAsociadoId');

        $arrayMarcas  = $this->input->post('selectMarca');
        if (empty($arrayMarcas)) {
            $stringMarcas = '';
        } else {
            $stringMarcas = implode(',', $arrayMarcas);
        }

        $config['upload_path']   = './uploads/asociados_logo/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputAsociadoLogo')) {
            if (isset($_FILES['inputAsociadoLogo']) && $_FILES['inputAsociadoLogo']['name'] != '') {
                $error = '<small>No se logró subir el logo, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            }

            $dataAsociadoEditar = array(
                'ASOC_empresa'          => $this->input->post('inputEmpresa'),
                'ASOC_productos'        => $this->input->post('inputProductos'),
                'ASOC_direccion'        => $this->input->post('inputDireccion'),
                'ASOC_telefono'         => $this->input->post('inputTelefono'),
                'ASOC_pagina_web'       => $this->input->post('inputPaginaWeb'),
                'ASOC_persona_contacto' => $this->input->post('inputPersonaContacto'),
                'ASOC_email'            => $this->input->post('inputEmail'),
                'ASOC_estado'           => 'habilitado',
                'ASOC_marca'            => $stringMarcas
            );
        } else {
            $dataImagen = array('upload_data' => $this->upload->data());
            $imagen     = $dataImagen['upload_data'];

            $dataAsociadoEditar = array(
                'ASOC_empresa'          => $this->input->post('inputEmpresa'),
                'ASOC_productos'        => $this->input->post('inputProductos'),
                'ASOC_direccion'        => $this->input->post('inputDireccion'),
                'ASOC_telefono'         => $this->input->post('inputTelefono'),
                'ASOC_pagina_web'       => $this->input->post('inputPaginaWeb'),
                'ASOC_persona_contacto' => $this->input->post('inputPersonaContacto'),
                'ASOC_email'            => $this->input->post('inputEmail'),
                'ASOC_estado'           => 'habilitado',
                'ASOC_marca'            => $stringMarcas,
                'ASOC_logo'             => $imagen['file_name']
            );

            $this->session->set_userdata('usu_logo', $imagen['file_name']);
        }

        $this->asociado_model->asociadoEditar($asocId, $dataAsociadoEditar);

        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del perfil correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del perfil correctamente.');
        }

        redirect();
    }

    /**
     * guardar formulario de edicion de organizacion
     * @return [type] [description]
     */
    public function guardar_edicion_organizacion()
    {
        $usuId = $this->session->userdata('sgc_id');
        $orgId = $this->input->post('inputOrganizacionId');

        $config['upload_path']   = './uploads/asociados_logo/';
        $config['allowed_types'] = 'gif|jpg|png|svg';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputOrganizacionLogo')) {
            if (isset($_FILES['inputOrganizacionLogo']) && $_FILES['inputOrganizacionLogo']['name'] != '') {
                $error = '<small>No se logró subir el logo, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            }

            $dataOrganizacionEditar = array(
                'ORG_empresa'    => $this->input->post('inputEmpresa'),
                'ORG_direccion1' => $this->input->post('inputDireccion'),
                'ORG_direccion2' => $this->input->post('inputDireccion'),
                'ORG_telefono'   => $this->input->post('inputTelefono'),
                'ORG_celular'    => $this->input->post('inputTelefono'),
                'ORG_contacto'   => $this->input->post('inputAdministrador'),
                'ORG_email'      => $this->input->post('inputEmail'),
                'ORG_licencia'   => $this->input->post('inputLicencia'),
            );

        } else {
            $dataImagen = array('upload_data' => $this->upload->data());
            $imagen     = $dataImagen['upload_data'];

            $dataOrganizacionEditar = array(
                'ORG_empresa'    => $this->input->post('inputEmpresa'),
                'ORG_direccion1' => $this->input->post('inputDireccion'),
                'ORG_direccion2' => $this->input->post('inputDireccion'),
                'ORG_telefono'   => $this->input->post('inputTelefono'),
                'ORG_celular'    => $this->input->post('inputTelefono'),
                'ORG_contacto'   => $this->input->post('inputAdministrador'),
                'ORG_email'      => $this->input->post('inputEmail'),
                'ORG_licencia'   => $this->input->post('inputLicencia'),
                'ORG_logo'      => $imagen['file_name'],
            );

            $this->session->set_userdata('usu_logo', $imagen['file_name']);
        }

        $this->organizacion_model->editar($orgId, $dataOrganizacionEditar);

        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del perfil correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del perfil correctamente.');
        }

        redirect();
    }
}