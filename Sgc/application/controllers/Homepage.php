<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('homepage_model');

    }

    public function index()
    {
     //articulos
        $this->data['banners'] = $this->homepage_model->get_banners();
        $this->data['products'] = $this->homepage_model->get_products();
        $this->data['articles'] = $this->homepage_model->get_articles();
        $this->data['meses'] = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

        $this->load->view('templates/header');
        $this->load->view('homepage/index', $this->data);
        $this->load->view('templates/footer');
    }
}