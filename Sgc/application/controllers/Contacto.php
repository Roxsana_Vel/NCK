<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('contacto_model');

    }

    public function index()
    {
        $this->load->config('recaptcha');

        $this->data['contacto'] = $this->contacto_model->get_contacto();

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('asunto', 'Asunto', 'required');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header');
            $this->load->view('contacto/index', $this->data);
            $this->load->view('templates/footer');
        }
        else
        {
            $this->load->library('curl');

            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
            $userIp = $this->input->ip_address();

            $secret = config_item('secret_key');
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptchaResponse.'&remoteip='.$userIp;

            $response = $this->curl->simple_get($url);
            $status = json_decode($response, true);

            if ($status['success']) {
                // envio de mensaje al correo de contacto
                $this->data['contacto'] = $this->contacto_model->get_contacto();

                $to = $this->data['contacto']->ORG_email;

                $subject = "[contacto] ".$this->input->post('asunto');

                $header  = "From: ".$this->input->post('email')." <".$this->input->post('email')."> \r\n";
                $header .= "Mime-Version: 1.0 \r\n";
                $header .= "Content-type: text/html; charset=utf-8\r\n";

                $message = $this->load->view('contacto/message', array_merge($this->input->post(), $this->data), TRUE);

                @mail($to, $subject, $message, $header);

                $this->session->set_flashdata('alert', 'info');
                $this->session->set_flashdata('message', 'Su mensaje ha sido enviado con éxito.');

                redirect('contacto');
            } else {
                $this->data['captcha'] = 'El captcha es necesario.';

                $this->load->view('templates/header');
                $this->load->view('contacto/index', $this->data);
                $this->load->view('templates/footer');
            }
        }
    }
}