<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Subcategorys extends CI_Controller {



	function __construct() {

		parent::__construct();

		$this->load->model('gallery_model');

		$this->load->model('categorys_model');

		$this->load->model('subcategorys_model');

		$this->load->model('gallery_model');

		$this->load->model('files_model');

		$this->load->model('orders_model');

		/*$this->load->view('templates/session_validation');*/



	}



	public function index(){

        $pagina             = new stdClass();
        $pagina->vista      = 'subcategorys/list_subcategorys';
        $pagina->menu       = 'subcategoria';
        $pagina->subMenu    = 'subcategoria-listados';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';
        
        $data['paginaDatos'] = $pagina;
		$data['categorys'] = $this->categorys_model->getCategorys();

		$data['subcategorys'] = $this->subcategorys_model->getSubcategorysOrder('categoria.CAT_nombre asc, subcategoria.SCAT_nombre asc');
		$this->load->view("template/template", $data);
	}



	public function frm_add_subcategorys() {
        
        $pagina             = new stdClass();
        $pagina->vista      = 'subcategorys/add_subcategory';
        $pagina->menu       = 'subcategoria';
        $pagina->subMenu    = 'subcategoria-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		$data['categorys'] = $this->categorys_model->getCategorys();
		$this->load->view("template/template",$data);
	}



	public function frm_edit_subcategorys($id_subcat) {
        $pagina             = new stdClass();
        $pagina->vista      = 'subcategorys/edit_subcategory';
        $pagina->menu       = 'subcategoria';
        $pagina->subMenu    = 'subcategoria-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
        

		$data['categorys'] = $this->categorys_model->getCategorys();

		$data['subcategorys'] = $this->subcategorys_model->getSubcategorysId($id_subcat);

		$this->load->view("template/template", $data);
	

	}



	public function ajax_by_category($id_cat) {

		$data['data'] = $this->subcategorys_model->getSubcategorysByCat($id_cat);

		$this->load->view("ajax_view", $data);

	}



	public function filter_by_category($id_cat = -1) {

		if ($id_cat == -1) {

			$view = 'list_FBC_unordered';

			$data['subcategorys'] = $this->subcategorys_model->getSubcategorysOrder('categoria.CAT_nombre asc, subcategoria.SCAT_nombre asc');

		} else {

			$view = 'list_FBC';

			$data['subcategorys'] = $this->subcategorys_model->filterByCat($id_cat, 'SCAT_orden asc');

		}



		$this->load->view("subcategorys/$view", $data);

	}



	public function DivLoader(){

		$subcat_id = $_GET['subcat_id'];

		$estado = $_GET['estado'];

		$query = $this->subcategorys_model->UpdateState($subcat_id, $estado);



		if($query == TRUE){

			$data['subcategorys'] = $this->subcategorys_model->getSubcategorysId($subcat_id);


			$this->load->view("subcategorys/DivLoader", $data);


	    }

	    else{echo 'Hubo un error al actualizar el estado';}

	}



	public function subir($scat_id, $scat_orden, $cat_id){

		$id_fila_superior = $this->subcategorys_model->getUpId($scat_orden, $cat_id); // Almacena el id de la fila superior

		$this->subcategorys_model->UpdateOrderUp($scat_id, $scat_orden); 	// Disminuye el orden actual en 1 de la fila la cual será subida ("Ejemplo de la posicion 7 a la 6")



		if ($id_fila_superior !== FALSE) {

			$update = $this->subcategorys_model->UpdateOrderRowUp($scat_orden, $id_fila_superior); // Actualiza el orden de la fila superior

		} else {

			$update = TRUE; // No hace falta actualizar el orden de uno superior porque no hay :S

		}



		if ($update != FALSE) {

			echo 'Done';

		} else{

			echo "Error al actualizar orden de los productos.";

		}

	}



	public function bajar($scat_id, $scat_orden, $cat_id){

		$id_fila_inferior = $this->subcategorys_model->getDowndId($scat_orden, $cat_id); // Almacena el id de la fila inferior

		$this->subcategorys_model->UpdateOrderDown($scat_id, $scat_orden); // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")



		if ($id_fila_inferior !== FALSE) {

			$update = $this->subcategorys_model->UpdateOrderRowDown($scat_orden, $id_fila_inferior); // Actualiza el orden de la fila inferior

		} else {

			$update = TRUE; // No hace falta actualizar el orden de uno superior porque no hay :S

		}



		if ($update != FALSE) {

			echo 'Done';

		}

		else{

			echo "Error al actualizar orden de los productos.";

		}

	}



	public function add_subcategory()

	{

		if ($this->input->post('nombre') != '') {

			$cat_id = $this->input->post('categoria_sub');

			$orden_num = $this->subcategorys_model->getOrdenNumId($cat_id) + 1;



			$data = array(

				'SCAT_nombre' => $this->input->post('nombre'),

				'SCAT_estado' => $this->input->post('estado'),

				'SCAT_orden' => $orden_num,

				'CAT_ID' => $cat_id

			);



			$add = $this->subcategorys_model->insertSubcategory($data);



			if ($add == TRUE) {

				$this->session->set_flashdata('success_message', 'Se agregó la subcategoría correctamente');



			} else {

				$this->session->set_flashdata('error_message', 'Ocurrió un error al intentar agregar el registro');

			}

		} else {

			$this->session->set_flashdata('error_message', 'Ingrese todos los campos requeridos');

		}



		redirect(base_url()."subcategorys");

	}



	public function update_subcategory()

	{

		$id = $this->input->post('id_subcategory');

		$id_category = $this->input->post('categoria_sub');

		$id_category_ant = $this->input->post('id_category_ant');

		$orden_subcategory = $this->input->post('orden_subcategory');



		$data = array(

			"SCAT_nombre" => $this->input->post('nombre'),

			"SCAT_estado" => $this->input->post('estado'),

			'CAT_ID' => $id_category

		);



		if ($id_category != $id_category_ant) {

			$this->subcategorys_model->updateOrderBeforeDelete($orden_subcategory, $id_category_ant);



			$data['SCAT_orden'] = $this->subcategorys_model->getOrdenNumId($id_category) + 1;

		}



		$update = $this->subcategorys_model->updateSubcategoryId($id, $data);



		if ($update == TRUE) {

			$this->session->set_flashdata('success_message', 'Se actualizó la subcategoría correctamente');

			redirect(base_url()."subcategorys");

		}

		else {
			redirect('subcategorys');
		}

	}



	public function delete_subcategorys_id()

	{

		$id = $this->input->post('eliminarId');

		$subcategorys = $this->subcategorys_model->getSubcategorysId($id);



		if ($subcategorys !== FALSE) {

			$row_scat = $subcategorys->row_object();



			$orden_scat = $row_scat->SCAT_orden;

			$cat_id = $row_scat->CAT_ID;



			$this->subcategorys_model->updateOrderBeforeDelete($orden_scat, $cat_id);

			$delete = $this->subcategorys_model->deleteSubcategoryId($id);



			if ($delete == TRUE) {

				$this->session->set_flashdata('success_message', 'Se eliminó la subcategoría correctamente');

			} else {

				$this->session->set_flashdata('error_message', 'Ocurrió un error al intentar eliminar');

			}

		}

	}



}