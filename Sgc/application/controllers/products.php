<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('products_model');
		$this->load->model('gallery_model');
		$this->load->model('categorys_model');
		$this->load->model('subcategorys_model');
		$this->load->model('prices_model');
		$this->load->model('files_model');
		$this->load->model('orders_model');
		/*$this->load->view('template/session_validation');*/
	}
	public function index(){
        $pagina             = new stdClass();
        $pagina->vista      = 'products/list_products';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		$data['products'] = $this->products_model->getProductsOrder('PRO_nombre asc');
		$data['categorys'] = $this->categorys_model->getCategoryOrder('CAT_nombre asc');
		$this->load->view("template/template", $data);
		
	}

	public function productsAll(){
	$data['products'] = $this->products_model->getProducts();
	$this->load->view('products/tbl_products', $data);
	}

	public function filter_productsByCategorys(){
		$id_cat= $this->input->post('id_cat');
		$data['products'] = $this->products_model->getFilterProdByCat($id_cat);
		$this->load->view("products/list_productsFBC", $data);

	}
	public function subir($pro_id, $pro_orden, $cat_id){

		$id_fila_superior = $this->products_model->getUpId($pro_orden, $cat_id); // Almacena el id de la fila superior
		$this->products_model->UpdateOrderUp($pro_id, $pro_orden); 	// Disminuye el orden actual en 1 de la fila la cual será subida ("Ejemplo de la posicion 7 a la 6")
		$update = $this->products_model->UpdateOrderRowUp($pro_orden, $id_fila_superior); // Actualiza el orden de la fila superior
		if ($update != FALSE) {
			#$data['products'] = $this->products_model->getFilterProdByCat($cat_id);
			$data['products'] = $this->products_model->filterBySubcat($cat_id, 'PRO_orden asc');
			$this->load->view("products/list_productsFBC", $data); // Muestra la tabla refrescada posterior a la accion de subir
		}
		else{
			echo "Error al actualizar orden de los productos.";
		}
	}
	public function bajar($pro_id, $pro_orden, $cat_id){
		$id_fila_inferior = $this->products_model->getDowndId($pro_orden, $cat_id); // Almacena el id de la fila inferior
		$this->products_model->UpdateOrderDown($pro_id, $pro_orden); // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")
		$update = $this->products_model->UpdateOrderRowDown($pro_orden, $id_fila_inferior); // Actualiza el orden de la fila inferior
		if ($update != FALSE) {
			#$data['products'] = $this->products_model->getFilterProdByCat($cat_id);
			#$this->load->view("products/bajar", $data); // Muestra la tabla refrescada posterior a la accion de bajar
			$data['products'] = $this->products_model->filterBySubcat($cat_id, 'PRO_orden asc');
			$this->load->view("products/list_productsFBC", $data); // Muestra la tabla refrescada posterior a la accion de subir
		}
		else{
			echo "Error al actualizar orden de los productos.";
		}
	}

    public function frm_add_products()
    {
        $pagina             = new stdClass();
        $pagina->vista      = 'products/add_products';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
    
        $data['categorys'] = $this->categorys_model->getCategoryOrder('CAT_nombre asc');

        /*$data['sellos'] = $this->db->get('sellos')->result();*/

       
        $this->load->view('template/template', $data);
       
    }

    public function frm_edit_products($id_pro, $id_cat) {
        $pagina             = new stdClass();
        $pagina->vista      = 'products/edit_products';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
        $data['products'] = $this->products_model->getProductsId($id_pro);
        $data['categorys'] = $this->categorys_model->getCategorys();
        $data['subcategorys'] = $this->subcategorys_model->getSubcategorysByCat($data['products']->row('CAT_ID'));
        $data['gallery'] = $this->gallery_model->getProductGalleryId($id_pro);

        $data['price_mayor_a'] = $this->prices_model->getPriceByProd($id_pro, 3);
        $data['price_mayor_b'] = $this->prices_model->getPriceByProd($id_pro, 4);
        $data['price'] = $this->prices_model->getPriceByProd($id_pro, 4);
        $data['price_mayor_c'] = $this->prices_model->getPriceByProd($id_pro, 6);

        $data['sellos'] = $this->db->get('sellos')->result();

        
        $this->load->view('template/template', $data);
      
    }

	public function DivLoader(){
		$pro_id = $_GET['pro_id'];
		$estado = $_GET['estado'];
		$query = $this->products_model->UpdateState($pro_id, $estado);
		if($query == TRUE){
		$data['products'] = $this->products_model->getProductsId($pro_id);

		$this->load->view("products/DivLoader", $data);

	    }
	    else{echo 'Hubo un error al actualizar el estado';}
	}
	public function delete_products_id()
	{

		$id = $this->input->post('eliminarId');
		$id_cat = $this->input->post('catId');
		$product = $this->products_model->getProductsId($id);
		if ($product !== FALSE) {
			$row_pro = $product->row_object();
			$orden = $row_pro->PRO_orden;
			$cat_id = $row_pro->CAT_ID;
			if ($this->gallery_model->deleteGalleryBy('products', 'PRO_ID', $id)) {
				$this->products_model->updateOrderBeforeDelete($orden, $cat_id);
				if ($this->products_model->deleteProductsId($id)) {
					$data['products'] = $this->products_model->getProductsOrder('PRO_nombre asc');
					$this->load->view('products/tbl_products', $data);
				} else {
					echo "Error al eliminar el producto.<br>";
					echo "<a href='".base_url()."products/'>Ir atr&aacute;s</a>";
				}
			} else {
				echo "Error al eliminar los archivos de la galería del producto.<br>";
				echo "<a href='".base_url()."products/'>Ir atr&aacute;s</a>";
			}
		}
	}
}