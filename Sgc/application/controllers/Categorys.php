<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorys extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('categorys_model');
		$this->load->model('gallery_model');
		$this->load->model('products_model');
		$this->load->model('orders_model');
		$this->load->view('template/session_validation');
	}

	public function index()
	{
        $pagina             = new stdClass();
        $pagina->vista      = 'categorys/list_categorys';
        $pagina->menu       = 'categorys';
        $pagina->subMenu    = 'categorys-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		//$this->load->view('templates/session_validation');
		$data['categorys'] = $this->categorys_model->getCategoryOrder();
		$this->load->view("template/template", $data);
		
	}
	public function subir(){
		$cat_id = $this->uri->segment(3);
		$cat_orden = $this->uri->segment(4);
		$id_fila_superior = $this->categorys_model->getUpId($cat_orden); // Almacena el id de la fila superior
		$this->categorys_model->UpdateOrderUp($cat_id, $cat_orden); 	// Disminuye el orden actual en 1 de la fila la cual será subida ("Ejemplo de la posicion 7 a la 6")
		$update = $this->categorys_model->UpdateOrderRowUp($cat_orden, $id_fila_superior); // Actualiza el orden de la fila superior
		if ($update != FALSE) {
		$data['categorys'] = $this->categorys_model->getCategoryOrder();
		$this->load->view("categorys/subir", $data); // Muestra la tabla refrescada posterior a la accion de subir
		}
		else{
			echo "Error al actualizar orden de las categorias";
		}
	}
	public function bajar(){

		$cat_id = $this->uri->segment(3);
		$cat_orden = $this->uri->segment(4);
		$id_fila_inferior = $this->categorys_model->getDowndId($cat_orden); // Almacena el id de la fila inferior
		$this->categorys_model->UpdateOrderDown($cat_id, $cat_orden); // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")
		$update = $this->categorys_model->UpdateOrderRowDown($cat_orden, $id_fila_inferior); // Actualiza el orden de la fila inferior
		if ($update != FALSE) {
		$data['categorys'] = $this->categorys_model->getCategoryOrder();
		$this->load->view("categorys/bajar", $data); // Muestra la tabla refrescada posterior a la accion de bajar
		}
		else{
			echo "Error al actualizar orden de las categorias";
		}
	}
	public function DivLoader(){
		$cat_id = $_GET['cat_id'];
		$estado = $_GET['estado'];
		$query = $this->categorys_model->UpdateState($cat_id, $estado);
		if($query == TRUE){
		$data['categorys'] = $this->categorys_model->getCategorysId($cat_id);

		$this->load->view("categorys/DivLoader", $data);

	    }
	    else{echo 'Hubo un error al actualizar el estado';}
	}
	public function frm_add_category() {
        
        $pagina             = new stdClass();
        $pagina->vista      = 'categorys/add_category';
        $pagina->menu       = 'categorys';
        $pagina->subMenu    = 'categorys-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		$this->load->view("template/template", $data);
		
	}
	public function add_category($orden_num)
	{
		if ($this->input->post('nombre') != '') {
			#$orden_num = $this->input->post('orden_num');
			$orden_num = $orden_num + 1;
			$data = array(
				"CAT_nombre" => $this->input->post('nombre'),
				"CAT_estado" => $this->input->post('estado'),
				"CAT_orden" => $orden_num
				);
			$add = $this->categorys_model->insertCategory($data);
			if ($add == TRUE) {
				$this->session->set_flashdata('success_message', 'Se agregó la categoría correctamente');
				redirect(base_url()."categorys");
			} else {
				echo "<h3>ERROR: No se pudo agregar correctamente la categoría.</h3>";
				echo anchor(base_url()."categorys/", 'Ir atr&aacute;s');
			}
		} else {
			echo "<h3>ERROR: Ingrese los campos requeridos.</h3>";
			echo anchor(base_url()."categorys/", 'Ir atr&aacute;s');
		}
	}
	public function frm_edit_category() {
        $pagina             = new stdClass();
        $pagina->vista      = 'categorys/edit_category';
        $pagina->menu       = 'categorys';
        $pagina->subMenu    = 'categorys-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		
        
		$id = $this->uri->segment(3);
		$data['categorys'] = $this->categorys_model->getCategorysId($id);
		$this->load->view("template/template", $data);
	}
	public function update_category_id()
	{
		$id = $this->input->post('id_category');
		$data = array(
			"CAT_nombre" => $this->input->post('nombre'),
			"CAT_estado" => $this->input->post('estado')
		);
		$update = $this->categorys_model->updateCategoryId($id, $data);
		if ($update == TRUE) {
			$this->session->set_flashdata('success_message', 'Se actualizó la categoría correctamente');
			redirect(base_url()."categorys");
		}
		else {
			redirect('categorys');
		}
	}
	public function delete_category_id()
	{

		$id = $this->input->post('eliminarId');
		$categorys = $this->categorys_model->getCategorysId($id);
		if ($categorys !== FALSE) {
			$row_cat = $categorys->row_object();

			$orden_cat = $row_cat->CAT_orden;
			$this->categorys_model->updateOrderBeforeDelete($orden_cat);
			$delete = $this->categorys_model->deleteCategoryId($id);
			if ($delete == TRUE) {
				$this->session->set_flashdata('success_message', 'Se eliminó la categoría correctamente');
			} else {
				$this->session->set_flashdata('error_message', 'Ocurrió un error al intentar eliminar');
			}
		}
	}
	function filter_by_category(){
		$id = $this->input->post('categoria');
		$data['categorys'] = $this->categorys_model->getCategorysId($id);
		$this->load->view("categorys/tbl_category", $data);
	}
}
?>










