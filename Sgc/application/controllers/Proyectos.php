<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	public $data = array();

	public function __construct()
	{
		parent::__construct();

		/** impedir el acceso si no es un usuario del sistema */
		if(is_null($this->session->userdata('sgc_user'))) {
		    redirect(base_url());
		}
	}

	public function index()
	{
		$this->data['title'] = 'Proyectos';

		$this->load->view('templates/header');
		$this->load->view('templates/page_header', $this->data);
		$this->load->view('proyectos/index');
		$this->load->view('templates/footer');
	}
}




