<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Acuerdos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect('?next=' . $this->uri->uri_string());
        }

        /* bloquear acciones para asociado */
        if ($this->session->userdata('sgc_user_nivel') == 'ASOCIADO') {
            $metodosDeshabilitados = array(
                'nuevo',
                'guardar_nuevo',
                'editar',
                'guardar_edicion',
                'dar_de_baja'
            );

            if (in_array($this->router->method, $metodosDeshabilitados)) {
                redirect('acuerdos');
            }
        }

        /** modelos */
        $this->load->model('acuerdo_model');
    }

    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getAcuerdos();

        $this->load->view('template/template', $data);
    }

    public function consejo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado-consejo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $pagina->tipoInforme = 'Consejo Directivo';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getInformesConsejo();

        $this->load->view('template/template', $data);
    }

    public function asamblea()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado-asamblea';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $pagina->tipoInforme = 'Asamblea de Socios';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getInformesAsamblea();

        $this->load->view('template/template', $data);
    }

    public function reuniones()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado-reuniones';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $pagina->tipoInforme = 'Reuniones de Trabajo';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getInformesReuniones();

        $this->load->view('template/template', $data);
    }

    public function proyectos()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado-proyectos';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $pagina->tipoInforme = 'Proyectos';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getInformesProyectos();

        $this->load->view('template/template', $data);
    }

    public function boletin()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/listado';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-listado-boletin';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Informes';

        $pagina->tipoInforme = 'Proyectos';

        $data['paginaDatos'] = $pagina;

        $data['arrayAcuerdos'] = $this->acuerdo_model->getInformesBoletin();

        $this->load->view('template/template', $data);
    }

    public function nuevo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/nuevo';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Nuevo Informe';

        $data['paginaDatos'] = $pagina;

        $this->load->view('template/template', $data);
    }

    public function guardar_nuevo()
    {
        $config['upload_path']   = './uploads/acuerdos/';
        $config['allowed_types'] = 'pdf|rar|zip';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputAdjunto')) {
            $error = '<small>No se logró subir el archivo adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
        } else {
            $adjunto = $this->upload->data('file_name');
        }

        $acuerdoNuevo = array(
            'ACU_tipo_informe'=> $this->input->post('inputTipoInforme'),
            'ACU_fecha'       => DateTime::createFromFormat('d/m/Y', $this->input->post('inputFecha'))->format('Y-m-d'),
            'ACU_nombre'      => $this->input->post('inputNombre'),
            'ACU_descripcion' => $this->input->post('inputDescripcion'),
            'ACU_adjunto'     => isset($adjunto) ? $adjunto : null,
            'ACU_estado'      => $this->input->post('inputEstado') ? 1 : 0
        );
        $acuerdoId = $this->acuerdo_model->guardarNuevo($acuerdoNuevo);

        $url = base_url('acuerdos/detalle/' . $acuerdoId);
        $this->_enviar_mail_asociados($acuerdoNuevo['ACU_nombre'], $url);

        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo informe correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo informe correctamente.');
        }

        $redireccion = explode(' ', $this->input->post('inputTipoInforme'))[0];

        if (method_exists($this, $redireccion)) {
            redirect('acuerdos/' . $redireccion);
        }

        redirect('acuerdos');
    }

    public function editar($acuId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/editar';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Editar Informe';

        $data['paginaDatos'] = $pagina;

        $data['datosAcuerdo'] = $this->acuerdo_model->getAcuerdo($acuId);

        $this->load->view('template/template', $data);
    }

    public function guardar_edicion($acuId)
    {
        $config['upload_path']   = './uploads/acuerdos/';
        $config['allowed_types'] = 'pdf|rar|zip';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES['inputAdjunto']) && $_FILES['inputAdjunto']['name'] != '') {
            if ( ! $this->upload->do_upload('inputAdjunto')) {
                $error = '<small>No se logró subir el archivo adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $adjunto = $this->upload->data('file_name');
            }
        }

        /** editar datos del asociado */
        $acuerdo = array(
            'ACU_tipo_informe'=> $this->input->post('inputTipoInforme'),
            'ACU_fecha'       => DateTime::createFromFormat('d/m/Y', $this->input->post('inputFecha'))->format('Y-m-d'),
            'ACU_nombre'      => $this->input->post('inputNombre'),
            'ACU_descripcion' => $this->input->post('inputDescripcion'),
            'ACU_estado'      => $this->input->post('inputEstado') ? 1 : 0
        );
        if (isset($adjunto)) {
            $acuerdo['ACU_adjunto'] = $adjunto;
        }
        $this->acuerdo_model->guardarEdicion($acuId, $acuerdo);

        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del informe correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del informe correctamente.');
        }

        $redireccion = explode(' ', $this->input->post('inputTipoInforme'))[0];

        if (method_exists($this, $redireccion)) {
            redirect('acuerdos/' . $redireccion);
        }

        redirect('acuerdos');
    }

    public function eliminar($acuId)
    {
        $acuerdo = $this->acuerdo_model->getAcuerdo($acuId);

        $redireccion = explode(' ', $acuerdo->ACU_tipo_informe)[0];

        $this->acuerdo_model->eliminar($acuId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha eliminado el informe correctamente.');

        if (method_exists($this, $redireccion)) {
            redirect('acuerdos/' . $redireccion);
        }

        redirect('acuerdos');
    }

    public function activar_desactivar($acuId)
    {
        $acuerdo = $this->acuerdo_model->getAcuerdo($acuId);

        $redireccion = explode(' ', $acuerdo->ACU_tipo_informe)[0];

        $this->acuerdo_model->activar_desactivar($acuId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha actualizado el estado del informe correctamente.');

        if (method_exists($this, $redireccion)) {
            redirect('acuerdos/' . $redireccion);
        }

        redirect('acuerdos');
    }

    public function detalle($acuId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'acuerdos/detalle';
        $pagina->menu       = 'acuerdos';
        $pagina->subMenu    = 'acuerdos-detalle';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Detalle Informe';

        $data['paginaDatos'] = $pagina;

        $data['datosAcuerdo'] = $this->acuerdo_model->getAcuerdo($acuId);

        $this->load->view('template/template', $data);
    }

    private function _enviar_mail_asociados($nombreAcuerdo, $url)
    {
        $this->load->model('asociado_model');
        $asociados = $this->asociado_model->getEmailAsociados();

        $this->load->library('MyPHPMailer');

        $email['titulo'] = 'Nuevo informe';
        $email['parrafo_inicial'] = 'Se ha registrado un nuevo informe ("' . $nombreAcuerdo . '"), para ver los detalles siga el enlace';
        $email['enlace'] = $url;
        $email['enlace_nombre'] = 'Ver acuerdo';

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom('contacto@web.proolivo.com', 'PROOLIVO');

        $mail->Subject    = '[Pro Olivo - Nuevo informe] ' . $nombreAcuerdo;
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view('template/modelo_email', $email, true);

        foreach ($asociados as $asociado) {
            $mail->addAddress($asociado);
        }

        $this->load->model('organizacion_model');
        $email_organizacion = $this->organizacion_model->getOrganizacion()->ORG_email;
        $mail->addBcc($email_organizacion);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

         return true;
    }
}