<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('products_model');
    }

    public function index()
    {
        $this->data['products'] = $this->products_model->get_products();
        $this->data['categories'] = $this->products_model->get_categories();

        $this->load->view('templates/header');
        $this->load->view('productos/index', $this->data);
        $this->load->view('templates/footer');
    }

    public function detalles($url)
    {
        $this->load->helper('inflector');
        $this->load->helper('string');

        $this->data['product'] = $this->products_model->get_product($url);
        $this->data['gallery'] = $this->products_model->get_gallery($url);

        if (empty($this->data['product'])) {
            redirect('productos');
        }

        $this->seo_title = str_replace('Productos', $this->data['product']->PRO_nombre, $this->seo_title);
        $this->seo_description = strip_tags($this->data['product']->PRO_descripcion);
        $this->seo_description = str_replace("\n", '', $this->seo_description);
        $this->seo_description = str_replace("\t", '', $this->seo_description);
        $this->seo_description = reduce_multiples($this->seo_description, ' ');
        $this->seo_description = trim($this->seo_description);

        $this->load->view('templates/header');
        $this->load->view('productos/detalles', $this->data);
        $this->load->view('templates/footer');
    }
}