<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password_recovery extends CI_Controller {

    public function index()
    {
        $email = $this->input->post('inputEmailRecovery');

        // admin
        $this->db->where('ORG_email', $email);
        $admin = $this->db->get('organizacion')->row();

        if (is_null($admin)) {
            $this->db->select('usuario.*');
            $this->db->where('ASOC_email', $email);
            $this->db->join('usuario', 'usuario.USU_asoc_id = empresa_asociada.ASOC_id');
            $usuario = $this->db->get('empresa_asociada')->row();
        } else {
            $this->db->where('USU_asoc_id', null);
            $usuario = $this->db->get('usuario')->row();
        }

        if (is_null($usuario)) {
            echo json_encode(['status' => 'error', 'message' => 'No existe una cuenta asociada a ese correo electrónico']);
            return;
        }

        $this->load->helper('string');

        $passwordRecoveryToken = random_string('alnum', 50);

        $this->db->where('USU_ID', $usuario->USU_ID);
        $this->db->update('usuario', array('USU_recovery' => $passwordRecoveryToken));

        $url = base_url('password_recovery/reset_password?usuario=' . $usuario->USU_login . '&token=' . $passwordRecoveryToken);
        $this->_send_email_recovery($usuario->USU_login, $email, $url);

        echo json_encode(['status' => 'ok', 'message' => 'Se le ha enviado un correo con el enlace para reestablecer su contraseña. Revise en su bandeja de entrada o en la carpeta de spam.']);
    }

    public function reset_password()
    {
        $usuario = $this->input->get('usuario');
        $token = $this->input->get('token');

        $this->db->where('USU_login', $usuario);
        $this->db->where('USU_recovery', $token);
        $cuenta = $this->db->get('usuario')->row();

        if (is_null($cuenta)) {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_message', '<b>El enlace para reestablecer su contraseña no es válido</b>. Solicite un nuevo enlace para restablecer su contraseña');

            redirect();
        }

        $data['cuenta'] = $cuenta;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Contraseña', 'required|matches[re-password]');
        $this->form_validation->set_rules('re-password', 'Repita su Contraseña', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('reset_password', $data);
        } else {
            $this->db->where('USU_ID', $cuenta->USU_ID);
            $this->db->update('usuario', array(
                    'USU_password' => crypt($this->input->post('password'), $this->config->item('encryption_key')),
                    'USU_recovery' => null
                )
            );

            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_message', '<b>Su contraseña ha sido cambiada con exito</b>. Ingrese a su cuenta con su nueva contraseña');

            redirect();
        }
    }

    private function _send_email_recovery($usuario, $emailRecovery, $url)
    {
        $this->load->library('MyPHPMailer');

        $email['titulo'] = 'Reestablecer Contraseña';
        $email['parrafo_inicial'] = 'Estimad@ ' . $usuario . ':<br> Se le ha enviado este mensaje para reestablecer la contraseña de su cuenta en <a href="http://www.proolivo.com">proolivo.com</a>.<br><br>Su usuario: ' . $usuario . '<br>' . 'Para ingresar una nueva contraseña haga click en el enlace.';
        $email['enlace'] = $url;
        $email['enlace_nombre'] = 'Reestablecer contraseña';

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom('contacto@web.proolivo.com', 'PROOLIVO');

        $mail->Subject    = '[Pro Olivo] Reestablecer contraseña';
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view('template/modelo_email', $email, true);

        $mail->addAddress($emailRecovery);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

        return true;
    }
}