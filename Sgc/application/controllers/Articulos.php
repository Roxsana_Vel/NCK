<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Articulos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect('?next=' . $this->uri->uri_string());
        }

        /** modelos */
        $this->load->model('articulo_model');
    }

    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/listado';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->articulo_model->getArticulos();

        $this->load->view('template/template', $data);
    }

    public function novedades()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/listado';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-listado-novedades';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Artículos';

        $pagina->tipoArticulo = 'Noticias';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->articulo_model->getNovedades();

        $this->load->view('template/template', $data);
    }

    public function proyectos()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/listado';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-listado-proyectos';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Artículos';

        $pagina->tipoArticulo = 'Proyectos';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->articulo_model->getProyectos();

        $this->load->view('template/template', $data);
    }

    public function nuevo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/nuevo';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Nuevo Artículo';

        $data['paginaDatos'] = $pagina;

        $this->load->view('template/template', $data);
    }

    public function guardar_nuevo()
    {
        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputImagen')) {
            $error = '<small>No se logró subir la imagen, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
        } else {
            $imagen = $this->upload->data('file_name');
        }

        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'pdf';
        $this->upload->initialize($config);

        if (isset($_FILES['inputAdjunto']) && $_FILES['inputAdjunto']['name'] != '') {
            if ( ! $this->upload->do_upload('inputAdjunto')) {
                $errorAdjunto = '<small>No se logró subir el pdf adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $adjunto = $this->upload->data('file_name');
            }
        }

        $articuloNuevo = array(
            'EVE_titulo'      => $this->input->post('inputTitulo'),
            'EVE_fecha'       => DateTime::createFromFormat('d/m/Y', $this->input->post('inputFecha'))->format('Y-m-d'),
            'EVE_resumen'     => $this->input->post('inputResumen'),
            'EVE_descripcion' => $this->input->post('inputDescripcion'),
            'EVE_visibilidad' => $this->input->post('inputVisibilidad') ? 'socios' : 'publico',
            'EVE_imagen'      => isset($imagen) ? $imagen : null,
            'EVE_estado'      => $this->input->post('inputEstado') ? 1 : 0,
            'EVE_tipo'        => $this->input->post('inputTipo'),
            'EVE_pdf_adjunto' => isset($adjunto) ? $adjunto : null
        );
        $articuloId = $this->articulo_model->guardarNuevo($articuloNuevo);

        if ($this->session->userdata('sgc_user_nivel') == 'ASOCIADO') {
            // enviar correo al administrador
            $url = base_url('articulos/editar/' . $articuloId);
            $this->_enviar_mail_admin($articuloNuevo['EVE_titulo'], $url);
        }

        $this->session->set_flashdata('alert_type', 'success');

        $errorFlashdata = '';
        if (isset($error)) {
            $errorFlashdata .= $error . '<br>';
        }
        if (isset($errorAdjunto)) {
            $errorFlashdata .= $errorAdjunto . '<br>';
        }

        if ($errorFlashdata != '') {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo artículo correctamente.<br><br>' . $errorFlashdata);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo artículo correctamente.');
        }

        redirect('articulos');
    }

    public function editar($artId)
    {
        /**-- datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/editar';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Editar Artículo';

        $data['paginaDatos'] = $pagina;

        $data['datosArticulo'] = $this->articulo_model->getArticulo($artId);

        $this->load->view('template/template', $data);
    }

    public function guardar_edicion($artId)
    {
        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES['inputImagen']) && $_FILES['inputImagen']['name'] != '') {
            if ( ! $this->upload->do_upload('inputImagen')) {
                $error = '<small>No se logró subir la imagen, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $imagen = $this->upload->data('file_name');
            }
        }

        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'pdf';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES['inputAdjunto']) && $_FILES['inputAdjunto']['name'] != '') {
            if ( ! $this->upload->do_upload('inputAdjunto')) {
                $errorAdjunto = '<small>No se logró subir el pdf adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $adjunto = $this->upload->data('file_name');
            }
        }

        
        
        
        /** editar datos del asociado */
        $articulo = array(
            'EVE_titulo'      => $this->input->post('inputTitulo'),
            'EVE_fecha'       => DateTime::createFromFormat('d/m/Y', $this->input->post('inputFecha'))->format('Y-m-d'),
            'EVE_resumen'     => $this->input->post('inputResumen'),
            'EVE_descripcion' => $this->input->post('inputDescripcion'),
            'EVE_visibilidad' => $this->input->post('inputVisibilidad') ? 'socios' : 'publico',
            'EVE_tipo'        => $this->input->post('inputTipo'),
        );
        if ($this->session->userdata('sgc_user_nivel') != 'ASOCIADO') {
            $articulo['EVE_estado'] = $this->input->post('inputEstado') ? 1 : 0;
        }
        if (isset($imagen)) {
            $articulo['EVE_imagen'] = $imagen;
        }
        if (isset($adjunto)) {
            $articulo['EVE_pdf_adjunto'] = $adjunto;
        }
        $this->articulo_model->guardarEdicion($artId, $articulo);

        $this->session->set_flashdata('alert_type', 'success');

        $errorFlashdata = '';
        if (isset($error)) {
            $errorFlashdata .= $error . '<br>';
        }
        if (isset($errorAdjunto)) {
            $errorFlashdata .= $errorAdjunto . '<br>';
        }

        if ($errorFlashdata != '') {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del artículo correctamente.<br><br>' . $errorFlashdata);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del artículo correctamente.');
        }

        redirect('articulos');
    }

    public function activar_desactivar($artId)
    {
        $this->articulo_model->activar_desactivar($artId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha actualizado el estado del artículo correctamente.');

        redirect('articulos');
    }

    public function eliminar($artId)
    {
        $this->articulo_model->eliminar($artId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha eliminado el artículo correctamente.');

        redirect('articulos');
    }

    public function detalle($artId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'articulos/detalle';
        $pagina->menu       = 'articulos';
        $pagina->subMenu    = 'articulos-detalle';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Detalle Artículo';

        $data['paginaDatos'] = $pagina;

        $data['datosArticulo'] = $this->articulo_model->getArticulo($artId);

        $this->load->view('template/template', $data);
    }

    private function _enviar_mail_admin($nombreArticulo, $url)
    {
        $this->load->model('organizacion_model');
        $email_organizacion = $this->organizacion_model->getOrganizacion()->ORG_email;

        $this->load->library('MyPHPMailer');

        $email['titulo'] = 'Artículo por aprobar su publicación';
        $email['parrafo_inicial'] = 'Se ha registrado un nuevo artículo ("' . $nombreArticulo . '"), para editar y/o habilitar el artículo siga el enlace';
        $email['enlace'] = $url;
        $email['enlace_nombre'] = 'Ir a Artículo';

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom('roxsana@idw.com.pe', 'PROOLIVO');

        $mail->Subject    = '[Pro Olivo - Artículo por aprobar] ' . $nombreArticulo;
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view('template/modelo_email', $email, true);

        $mail->addAddress($email_organizacion);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

        return true;
    }
}