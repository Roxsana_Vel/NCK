<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Marca extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect();
        }

        /** modelos */
        $this->load->model('makes_model');
    }

    public function guardar_y_actualizar()
    {
        /** post */
        $dataMarcaNueva = array(
            'MAR_nombre' => $this->input->post('nombreMarca'),
        );

        $this->makes_model->insertMake($dataMarcaNueva);

        $arrayMarcas = $this->makes_model->getMakes();

        $cadena_option = "<option value=''>Seleccione</option>";
        foreach ($arrayMarcas as $marca) {
            $cadena_option .= "<option value='" . $marca->MAR_nombre . "'>" . $marca->MAR_nombre . "</option>";
        }
        echo json_encode($cadena_option);

    }



}
