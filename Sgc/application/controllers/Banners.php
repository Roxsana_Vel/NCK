<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banners extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect('?next=' . $this->uri->uri_string());
        }

        /** modelos */
        $this->load->model('banner_model');
        $this->load->model('vistas_publicas_model');
    }

    /**
     * vista del listado de asociados
     * @return [type] [description]
     */
    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'banners/listado';
        $pagina->menu       = 'banners';
        $pagina->subMenu    = 'banners-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Banners';

        $data['paginaDatos'] = $pagina;

        /** array de banners */
        $banners = $this->banner_model->getBanner();

        $data['arrayBanners'] = ($banners == false) ? array() : $banners;

        $this->load->view('template/template', $data);
    }

    /**
     * habilitar o deshabilitar el banner seleccionado
     * @param  [type] $banId [description]
     * @return [type]        [description]
     */
    public function habilitar_deshabilitar($banId)
    {
        $this->banner_model->habilitar_deshabilitar($banId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha actualizado el estado del banner correctamente.');

        redirect('banners');
    }

    /**
     * vista para agregar un nuevo asociado
     * @return [type] [description]
     */
    public function nuevo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'banners/nuevo';
        $pagina->menu       = 'banners';
        $pagina->subMenu    = 'banners-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Nuevo Banner';

        $data['paginaDatos'] = $pagina;

        /** vistas publicas */
        $data['arrayVistasPublicas'] = $this->vistas_publicas_model->getAll();

        $this->load->view('template/template', $data);
    }

    /**
     * insertar un nuevo usuario
     * con datos de la empresa asociada
     * y datos del usuario de sistema para el nuevo asociado
     * @return [type] [description]
     */
    public function guardar_nuevo()
    {
        /**
         * guardar el banner y sus datos
         */
        $config['upload_path']   = './uploads/banners/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_size']      = 5000;
        // $config['max_width']     = 1024;
        // $config['max_height']    = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('inputBannerArchivo')) {
            $error = '<small>No se logró subir el banner, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
        } else {
            $dataImagen = array('upload_data' => $this->upload->data());
            $imagen     = $dataImagen['upload_data'];
        }

        $dataBannerNuevo = array(
            'BAN_titulo'      => $this->input->post('inputBannerTitulo'),
            'BAN_archivo'     => isset($imagen) ? $imagen['file_name'] : '',
            'BAN_estado'      => $this->input->post('inputBannerEstado'),
            'BAN_url'         => $this->input->post('inputBannerUrl'),
            'BAN_descripcion' => $this->input->post('inputBannerDescripcion'),
            'BAN_nombre_url'  => $this->input->post('inputBannerNombreUrl'),
        );
        $bannerId = $this->banner_model->insertBanner($dataBannerNuevo);

        if ($this->session->userdata('sgc_user_nivel') == 'ASOCIADO') {
            // enviar correo al administrador
            $url = base_url('banners/editar/' . $bannerId);
            $this->_enviar_mail_admin($dataBannerNuevo['BAN_titulo'], $url);
        }

        /**
         * guardar el banner como fondo de portada para las secciones seleccionadas
         */
        $arrayIdVistasPublicas = $this->input->post('selectVistaPublica');
        if ( ! is_null($arrayIdVistasPublicas)) {
            foreach ($arrayIdVistasPublicas as $key => $idVip) {
                if ($idVip == 'portada') {
                    $this->db->where('BAN_ID', $bannerId);
                    $this->db->update('banners', array('BAN_mostrar_portada' => 1));
                } else {
                    $dataUpdateVip = array(
                        'VIP_img_slogan' => isset($imagen) ? $imagen['file_name'] : null,
                    );
                    $this->vistas_publicas_model->update($idVip, $dataUpdateVip);
                }
            }
        }

        /**
         * alertas
         */
        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo banner correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo banner correctamente.');
        }

        redirect('banners');
    }

    /**
     * vista para editar los datos del banner
     * @param  [type] $banId [description]
     * @return [type]        [description]
     */
    public function editar($banId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'banners/editar';
        $pagina->menu       = 'banners';
        $pagina->subMenu    = 'banners-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Editar Banner';

        $data['paginaDatos'] = $pagina;

        /** datos del banner */
        $data['datosBanner'] = $this->banner_model->getBannerId($banId);

        /** vistas publicas para el select */
        $data['arrayVistasPublicas'] = $this->vistas_publicas_model->getAll();

        /** secciones publicas que usan el banner en su portada */
        $vistasPublicasConBanner = $this->vistas_publicas_model->getSeccionesConBanner($data['datosBanner']->BAN_archivo);

        $arrayIdVistasConBanner = array();
        foreach ($vistasPublicasConBanner as $key => $vip) {
            array_push($arrayIdVistasConBanner, $vip->VIP_ID);
        }
        $data['vistasPublicasId'] = $arrayIdVistasConBanner;

        $this->load->view('template/template', $data);
    }

    /**
     * guardar edicion de un banner
     * @return [type] [description]
     */
    public function guardar_edicion($banId)
    {
        /**
         * guardar img del banner y sus datos
         */
        $config['upload_path']   = './uploads/banners/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_size']      = 5000;
        // $config['max_width']     = 1024;
        // $config['max_height']    = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputBannerArchivo')) {
            if (isset($_FILES['inputBannerArchivo']) && $_FILES['inputBannerArchivo']['name'] != '') {
                $error = '<small>No se logró subir el banner, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            }

            $dataBannerNuevo = array(
                'BAN_titulo'      => $this->input->post('inputBannerTitulo'),
                'BAN_estado'      => $this->input->post('inputBannerEstado'),
                'BAN_url'         => $this->input->post('inputBannerUrl'),
                'BAN_descripcion' => $this->input->post('inputBannerDescripcion'),
                'BAN_nombre_url'  => $this->input->post('inputBannerNombreUrl'),
            );
        } else {
            $dataImagen = array('upload_data' => $this->upload->data());
            $imagen     = $dataImagen['upload_data'];

            $dataBannerNuevo = array(
                'BAN_titulo'      => $this->input->post('inputBannerTitulo'),
                'BAN_archivo'     => $imagen['file_name'],
                'BAN_estado'      => $this->input->post('inputBannerEstado'),
                'BAN_url'         => $this->input->post('inputBannerUrl'),
                'BAN_descripcion' => $this->input->post('inputBannerDescripcion'),
                'BAN_nombre_url'  => $this->input->post('inputBannerNombreUrl'),
            );
        }

        $this->banner_model->updateBanner($banId, $dataBannerNuevo);

        /**
         * guardar el banner como fondo de portada para las secciones seleccionadas
         */
        $imagenBanner = $this->db->get_where('banners', array('BAN_ID' => $banId))->row('BAN_archivo');
        $this->vistas_publicas_model->VaciarImagenDeVistas($imagenBanner);

        $arrayIdVistasPublicas = $this->input->post('selectVistaPublica');
        if ( ! is_null($arrayIdVistasPublicas)) {
            $this->db->where('BAN_ID', $banId);
            $this->db->update('banners', array('BAN_mostrar_portada' => null));

            foreach ($arrayIdVistasPublicas as $key => $idVip) {
                if ($idVip == 'portada') {
                    $this->db->where('BAN_ID', $banId);
                    $this->db->update('banners', array('BAN_mostrar_portada' => 1));
                } else {
                    $dataUpdateVip = array(
                        'VIP_img_slogan' => $imagenBanner,
                    );
                    $this->vistas_publicas_model->update($idVip, $dataUpdateVip);
                }
            }
        }

        /**
         * alertas
         */
        $this->session->set_flashdata('alert_type', 'success');
        if (isset($error)) {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del banner correctamente.<br><br>' . $error);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del banner correctamente.');
        }

        redirect('banners');
    }

    public function eliminar($banId)
    {
        /**
         * eliminar la imagen del banner de las vistas publicas
         */
        $imagen = $this->banner_model->getBannerId($banId)->BAN_archivo;

        $this->db->where('VIP_img_slogan', $imagen);
        $this->db->update('vistas_publicas', array('VIP_img_slogan' => null));


        /**
         * eliminar el banner
         */
        $this->banner_model->eliminar($banId);

        /**
         * alertas
         */
        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha eliminado el banner correctamente.');

        redirect('banners');
    }

    private function _enviar_mail_admin($nombreBanner, $url)
    {
        $this->load->model('organizacion_model');
        $email_organizacion = $this->organizacion_model->getOrganizacion()->ORG_email;

        $this->load->library('MyPHPMailer');

        $email['titulo'] = 'Banner por aprobar su publicación';
        $email['parrafo_inicial'] = 'Se ha registrado un nuevo banner ("' . $nombreBanner . '"), para editar y/o habilitar el banner siga el enlace';
        $email['enlace'] = $url;
        $email['enlace_nombre'] = 'Ir al Banner';

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom('contacto@web.proolivo.com', 'PROOLIVO');

        $mail->Subject    = '[Pro Olivo - Banner por aprobar] ' . $nombreBanner;
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view('template/modelo_email', $email, true);

        $mail->addAddress($email_organizacion);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

        return true;
    }
}