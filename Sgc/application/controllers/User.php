<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url('?next=' . $this->uri->uri_string()));
        }

        /** modelos */
        $this->load->model('users_model');
    }

    public function login()
    {
        if ($this->session->userdata('user_id')) {
            redirect();
        }

        $this->form_validation->set_rules('user', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');
        $this->form_validation->set_rules('remember', 'Remember', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('user/login');
            $this->load->view('templates/footer');
        } else {
            $user = $this->input->post();

            if ($this->users_model->login($user)) {
                redirect('cart');
            } else {
                $this->load->view('templates/header');
                $this->load->view('user/login', array('auth_error' => 1));
                $this->load->view('templates/footer');
            }
        }
    }

    public function logout()
    {
        $session = array(
            'user_id'        => '',
            'user'           => '',
            'user_nivel'     => '',
            'user_persona'   => '',
            'user_nombres'   => '',
            'user_apellidos' => '',
            'is_adult'       => false,
        );
        $this->session->unset_userdata($session);

        redirect('user/login');
    }

    /**
     * verificar existencia de un nombre de usuario
     *
     * @return boolean retorna true si el usuario ya existe
     *                         false si no existe
     */
    public function verificar_usuario_existente()
    {
        $usuarioNombre = $this->input->post('usuarioNombre');

        echo json_encode($this->users_model->existUser($usuarioNombre));
    }

    public function register()
    {
        if ($this->session->userdata('user_id')) {
            redirect();
        }

        $this->load->config('recaptcha');

        $this->form_validation->set_rules('user', 'Usuario', 'trim|required|is_unique[usuario.USU_login]');
        $this->form_validation->set_rules('password', 'Contraseña', 'required|matches[re-password]');
        $this->form_validation->set_rules('re-password', 'Repita su Contraseña', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[persona.PER_email]');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required');
        $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required');
        $this->form_validation->set_rules('dni', 'DNI', 'trim|required');
        $this->form_validation->set_rules('sexo', 'Sexo', 'trim|required');
        $this->form_validation->set_rules('nacimiento', 'Fecha de Nacimiento', 'trim');
        $this->form_validation->set_rules('celular', 'Celular', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('user/register');
            $this->load->view('templates/footer');
        } else {
            $this->load->library('curl');

            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
            $userIp = $this->input->ip_address();

            $secret = config_item('secret_key');
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptchaResponse.'&remoteip='.$userIp;

            $response = $this->curl->simple_get($url);
            $status = json_decode($response, true);

            if ($status['success']) {
                $user = $this->input->post();
                $this->users_model->register($user);

                $this->session->set_flashdata('alert', 'info');
                $this->session->set_flashdata('message', '<b>Su cuenta se ha creado con éxito</b>. Se ha enviado un mensaje al correo proporcionado en el registro para la activación de la cuenta.');

                redirect('user/register');
            } else {
                $this->load->view('templates/header');
                $this->load->view('user/register', array('captcha' => 'El captcha es necesario.'));
                $this->load->view('templates/footer');
            }
        }
    }

    public function activate()
    {
        if ($this->session->userdata('user_id')) {
            redirect();
        }

        $code = $this->input->get('code');

        if ($code == '') {
            $this->session->set_flashdata('alert', 'danger');
            $this->session->set_flashdata('message', '<b>El código de activación no es válido</b>.');
        } elseif ($this->users_model->activate_account($code)) {
            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Su cuenta ha sido activada con éxito</b>. Haga uso de su cuenta ingresando su usuario y contraseña');
        } else {
            $this->session->set_flashdata('alert', 'danger');
            $this->session->set_flashdata('message', '<b>El código de activación no es válido</b>.');
        }

        redirect('user/login');
    }

    public function send_email()
    {
        if ($this->session->userdata('user_id')) {
            redirect();
        }

        $user_email = $this->input->post('user-email');

        if ($user_email) {
            if ($this->users_model->send_mail_restore_password($user_email)) {
                $this->session->set_flashdata('alert', 'info');
                $this->session->set_flashdata('message', '<b>Se ha enviado un enlace a su email para restablecer su contraseña</b>. Revise la bandeja de entrada del email ingresado.');
            } else {
                $this->session->set_flashdata('alert', 'danger');
                $this->session->set_flashdata('message', '<b>El email o usuario ingresado no existen</b>. Vuelva a intentar.');
            }

            redirect('user/login');
        }
    }

    public function account_recovery()
    {
        if ($this->session->userdata('user_id')) {
            redirect();
        }

        $code = $this->input->get('code');

        $account = $this->users_model->recovery_account($code);

        if (empty($account) || $code == '') {
            $this->session->set_flashdata('alert', 'danger');
            $this->session->set_flashdata('message', '<b>El enlace para restablecer su contraseña no es válido</b>. Solicite un nuevo enlace para restablecer su contraseña');

            redirect('user/login');
        }

        $this->form_validation->set_rules('password', 'Contraseña', 'required|matches[re-password]');
        $this->form_validation->set_rules('re-password', 'Repita su Contraseña', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header');
            $this->load->view('user/form_recovery', array('account' => $account));
            $this->load->view('templates/footer');
        } else {
            $user = $this->input->post();
            $user['code'] = $code;
            $this->users_model->restore_password($user);

            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Su contraseña ha sido cambiada con exito</b>. Ingrese a su cuenta con su nueva contraseña');

            redirect('user/login');
        }
    }
    public function guardar_usuario_editado($usuId)
    {
        /** editar datos del usuario del asociado */

        /** si la contraseña esta vacia, no actualziamos ese campo */
        if ($this->input->post('inputLoginPassword') == '') {
            $dataUsuarioEditar = array(
                'USU_login'    => $this->input->post('inputLoginUsuario'),
                'USU_estado'   => 1
            );
        } else {
            $dataUsuarioEditar = array(
                'USU_login'    => $this->input->post('inputLoginUsuario'),
                'USU_password' => crypt($this->input->post('inputLoginPassword'), $this->config->item('encryption_key')),
                'USU_estado'   => 1
            );
        }
        $this->users_model->userEditar($usuId, $dataUsuarioEditar);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha guardado la información de usuario correctamente.');

        redirect('asociados');
    }
}