<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comprar extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->session->userdata('user_id')) {
            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Para realizar la compra se requiere autenticación</b>. Ingrese su usuario y contraseña para continuar.');
            redirect('user/login');
        }

        if ( ! $this->session->userdata('cart')) {
            redirect('cart');
        }

        $this->load->model('comprar_model');
        $this->load->model('pedidos_model');
        $this->load->model('client_model');

        $this->data['client'] = $this->comprar_model->get_client();
    }

    public function index()
    {
        if ($this->input->post()) {
            $order = $this->input->post();
            $client = $this->input->post('client');

            if ($this->input->post('actualizar_datos') == 1) {
                $this->client_model->update_data_client($client);
            }

            $pedido = $this->comprar_model->add_order($order);

            // informacion de contacto
            $this->data['contacto'] = $this->db->get('organizacion')->row();
            // informacion de cliente
            $this->db->join('persona', 'persona.PER_ID = pedido.PER_ID');
            $this->db->where('pedido.PED_ID', $pedido);
            $this->data['cliente'] = $this->db->get('pedido')->row();

            // mensaje al cliente
            $data_message_cliente = array(
                'subject'     => 'NUEVO PEDIDO',
                'to_email'    => $this->data['cliente']->PER_email,
                'to_nombre'   => $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'from_email'  => $this->data['contacto']->ORG_email,
                'from_nombre' => $this->data['contacto']->ORG_empresa,
                'message'     => $this->_report_details_order($pedido),
            );
            $this->_send_message($data_message_cliente);

            // mensaje a ventas@bodegaviejomolino.com
            $data_message_admin = array(
                'subject'     => 'NUEVO PEDIDO',
                'to_email'    => $this->data['contacto']->ORG_empresa,
                'to_nombre'   => 'Administrador de '.$this->data['contacto']->ORG_empresa,
                'from_email'  => $this->data['cliente']->PER_email,
                'from_nombre' => $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'message'     => $this->_report_details_order($pedido),
            );
            $this->_send_message($data_message_admin);

            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Su pedido se ha guardado con éxito</b>.');
            redirect('pedidos');
        }

        $this->data['organizacion'] = $this->comprar_model->get_organizacion();
        $this->data['departaments'] = $this->comprar_model->get_departaments();

        $this->load->view('templates/header');
        $this->load->view('comprar/index', $this->data);
        $this->load->view('templates/footer');
    }

    public function load_provinces()
    {
        $departament = $this->input->post('departament');

        if ($departament) {
            $provinces = $this->comprar_model->get_provinces($departament);
            echo json_encode($provinces);
        }

        if ($departament == '') {
            echo json_encode(array());
        }
    }

    public function load_districts()
    {
        $province = $this->input->post('province');

        if ($province) {
            $districts = $this->comprar_model->get_districts($province);
            echo json_encode($districts);
        }

        if ($province == '') {
            echo json_encode(array());
        }
    }

    private function _report_details_order($pedido)
    {
        $this->data['order'] = $this->pedidos_model->get_order($pedido);
        $this->data['details'] = $this->pedidos_model->get_details($pedido);

        return $this->load->view('pedidos/report_details_order', $this->data, TRUE);
    }

    private function _send_message($data_message)
    {
        $to = $data_message['to_email'];

        $subject = $data_message['subject'];

        $header  = "From: ".$data_message['from_nombre']." <".$data_message['from_email']."> \r\n";

        $header .= "Mime-Version: 1.0 \r\n";
        $header .= "Content-type: text/html; charset=utf-8\r\n";

        $message = $this->load->view('pedidos/message', $data_message, TRUE);

        @mail($to, $subject, $message, $header);
    }
}