<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novedades extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('articulos_model');

    }

    public function index()
    {
        $this->data['articles'] = $this->articulos_model->get_articles();
        $this->data['meses'] = array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

        $this->load->view('templates/header');
        $this->load->view('articulos/index', $this->data);
        $this->load->view('templates/footer');
    }

    public function detalle($url)
    {
        if ($url == 'galeria') {
            redirect('novedades');
        }

        $this->data['article'] = $this->articulos_model->get_article($url);
        $this->data['gallery'] = $this->articulos_model->get_gallery($url);

        if (empty($this->data['article'])) {
            redirect('novedades');
        }

        $this->seo_title = str_replace('Novedades', $this->data['article']->EVE_titulo, $this->seo_title);
        $this->seo_description = $this->data['article']->EVE_resumen;

        $this->load->view('templates/header');
        $this->load->view('articulos/detalle', $this->data);
        $this->load->view('templates/footer');
    }
}