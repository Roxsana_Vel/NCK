<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pay extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }

        /** modelos */
        $this->load->model('pay_model');

    }

    public function index($id_order)
    {
        $this->db->select('ORG_paypal_account, ORG_paypal_mode');
        $paypal_options = $this->db->get('organizacion')->row();

        if ($paypal_options->ORG_paypal_account == '') {
            $this->session->set_flashdata('alert', 'danger');
            $this->session->set_flashdata('message', 'El pago por PAYPAL no está displonible por el momento');
            redirect('pedidos');
        }

        $this->load->model('pay_model');

        $config['business']             = $paypal_options->ORG_paypal_account;
        $config['cpp_header_image']     = ''; //Image header url [750 pixels wide by 90 pixels high]
        $config['return']               = base_url('pedidos');
        $config['cancel_return']        = base_url('pedidos');
        //$config['notify_url']           = 'process_payment.php'; //IPN Post
        $config['production']           = $paypal_options->ORG_paypal_mode ? true : false;
        $config['invoice']              = $id_order;

        $this->load->library('paypal', $config);

        $order = $this->pay_model->get_order($id_order);
        $details = $this->pay_model->get_details($id_order);

        if (empty($order) || empty($details)) {
            $this->session->set_flashdata('alert', 'danger');
            $this->session->set_flashdata('message', 'El pedido no existe, seleccione otro pedido.');
            redirect('pedidos');
            redirect('pedidos');
        }

        foreach ($details as $key => $detail) {
            $this->paypal->add($detail->PRO_nombre, $detail->DPED_importe_dolares, $detail->DPED_cantidad);
        }

        $this->paypal->add('Envio a '.$order->DPTO_nombre, $order->DIRP_costoDolares);
        $this->paypal->pay();
    }

    public function success()
    {
        // javier.coaila-buyer-2@bodegaviejomolino.com
        // 123456789
        // 3YA4219005051533W
        //var_dump($this->input->post());

        if ($this->input->post('payment_status') == 'Completed')
        {
            $id_order = $this->input->post('invoice');

            // pagar con paypal
            $this->pay_model->pay_to_paypal($id_order);

            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Su cuenta se ha creado con éxito</b>. Se ha enviado un mensaje al correo proporcionado en el registro para la activación de la cuenta.');
            redirect('pedidos');
        }
        else
        {
            redirect('pedidos');
        }
    }
}