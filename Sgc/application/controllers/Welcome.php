<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('users_model');
        $this->load->model('asociado_model');
        $this->load->model('articulo_model');
        $this->load->model('acuerdo_model');
        $this->load->model('banner_model');
    }

    public function index()
    {
        $nextUrl = $this->input->get('next');

        if ($this->is_logged_in()) {
            if ($nextUrl) {
                redirect($nextUrl);
            } else {
                /** datos de pagina */
                $pagina             = new stdClass();
                $pagina->vista      = 'inicio/inicio';
                $pagina->menu       = 'inicio';
                $pagina->subMenu    = '';
                $pagina->subSubMenu = '';
                $pagina->titulo     = 'NCK Ingenieros';

                $data['paginaDatos'] = $pagina;

                $data['numAsociados'] = $this->asociado_model->countAsociados();
                $data['numArticulos'] = $this->articulo_model->countArticulos();
                $data['numAcuerdos']  = $this->acuerdo_model->countAcuerdos();
                $data['numBanners']   = $this->banner_model->countBanners();

                $data['ultimosAcuerdos'] = $this->acuerdo_model->get2ultimos();

                $this->load->view('template/template', $data);
            }
        } else {
            $data = array();

            if ($nextUrl) {
                $data['nextUrl'] = $nextUrl;
            }

            $this->load->view('login', $data);
        }
    }

    public function user_validation()
    {
        $nextUrl = $this->input->get('next');

        $user     = $this->input->post('usuario');
        $password = $this->input->post('contrasena');

        $record = $this->users_model->getUserValidation($user);

        if ($record != false) {
            $row = $record->row();

            if ($row->USU_login == $user && crypt($password, $row->USU_password) == $row->USU_password) {
                /** si es super administrador */
                if (in_array($row->NUSU_ID, [1, 2, 3])) {
                    if ($row->USU_estado == 1) {
                        /** logo del usuario */
                        if ($row->NUSU_ID == 1) {
                            $logo = $this->db->get_where('organizacion', array('ORG_ID' => 1))->row('ORG_logo');
                        } elseif ($row->NUSU_ID == 2) {
                            $logo = $this->db->get_where('organizacion', array('ORG_ID' => 1))->row('ORG_logo');
                        } elseif ($row->NUSU_ID == 3) {
                            $asocId = $this->db->get_where('usuario', array('USU_ID' => $row->USU_ID))->row('USU_asoc_id');
                            $logo   = $this->db->get_where('empresa_asociada', array('ASOC_id' => $asocId))->row('ASOC_logo');
                        }

                        /** datos de sesion */
                        $user_session = array(
                            'sgc_id'            => $row->USU_ID,
                            'sgc_user'          => $row->USU_login,
                            'sgc_user_nivel'    => $this->db->get_where('nivel_usuario', array('NUSU_ID' => $row->NUSU_ID))->row('NUSU_nivel'),
                            'sgc_user_nivel_id' => $row->NUSU_ID,
                            'sgc_state'         => $row->USU_estado,
                            'usu_logo'          => $logo,
                        );
                        $this->session->set_userdata($user_session);

                        if ($nextUrl) {
                            redirect($nextUrl);
                        } else {
                            redirect();
                        }
                    } else {
                        $data1 = array(
                            'error_1' => 'El usuario se encuentra deshabilitado',
                            'nextUrl' => $nextUrl
                        );

                        $this->load->view('login', $data1);
                    }
                } else {
                    $data4 = array(
                        'error_4' => 'Usted no tiene permiso para accesar al sistema.',
                        'nextUrl' => $nextUrl
                    );

                    $this->load->view('login', $data4);
                }
            } else {
                $data2 = array(
                    'error_2' => 'La contrase&ntilde;a no coincide.',
                    'nextUrl' => $nextUrl
                );

                $this->load->view('login', $data2);
            }
        } else {
            $data3 = array(
                'error_3' => 'Verifique el nombre de usuario',
                'nextUrl' => $nextUrl
            );

            $this->load->view('login', $data3);
        }
    }

    public function session_validation()
    {
        $this->load->view('session_validation');
    }

    public function is_logged_in()
    {
        $user = $this->session->userdata('sgc_user');

        if ($user != "") {
            return true;
        } else {
            return false;
        }
    }

    public function remove_session()
    {
        $this->session->sess_destroy();

        redirect();
    }
}
