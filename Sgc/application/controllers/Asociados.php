<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asociados extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect('?next=' . $this->uri->uri_string());
        }

        /** modelos */
        $this->load->model('asociado_model');
        $this->load->model('users_model');
        $this->load->model('makes_model');
    }

    /**
     * vista del listado de asociados
     * @return [type] [description]
     */
    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'asociados/listado';
        $pagina->menu       = 'asociados';
        $pagina->subMenu    = 'asociados-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Lista de Asociados';

        $data['paginaDatos'] = $pagina;

        $data['arrayAsociados'] = $this->asociado_model->getAsociados();

        $this->load->view('template/template', $data);
    }

    /**
     * vista para agregar un nuevo asociado
     * @return [type] [description]
     */
    public function nuevo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'asociados/nuevo';
        $pagina->menu       = 'asociados';
        $pagina->subMenu    = 'asociados-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Nuevo Asociado';

        $data['paginaDatos'] = $pagina;

        /** array de marcas */
        $marcas              = $this->makes_model->getMakes();
        $data['arrayMarcas'] = ($marcas === false) ? array() : $marcas;

        $this->load->view('template/template', $data);
    }

    /**
     * insertar un nuevo usuario
     * con datos de la empresa asociada
     * y datos del usuario de sistema para el nuevo asociado
     * @return [type] [description]
     */
    public function guardar_nuevo()
    {
        $arrayMarcas  = $this->input->post('selectMarca');

        if (empty($arrayMarcas)) {
            $stringMarcas = '';
        } else {
            $stringMarcas = implode(',', $arrayMarcas);
        }

        $dataAsociadoNuevo = array(
            'ASOC_empresa'          => $this->input->post('inputEmpresa'),
            'ASOC_productos'        => $this->input->post('inputProductos'),
            'ASOC_direccion'        => $this->input->post('inputDireccion'),
            'ASOC_telefono'         => $this->input->post('inputTelefono'),
            'ASOC_pagina_web'       => $this->input->post('inputPaginaWeb'),
            'ASOC_persona_contacto' => $this->input->post('inputPersonaContacto'),
            'ASOC_email'            => $this->input->post('inputEmail'),
            'ASOC_estado'           => 'habilitado',
            'ASOC_marca'            => $stringMarcas,
        );
        $asociadoId = $this->asociado_model->asociadoNuevoGuardar($dataAsociadoNuevo);

        $dataUsuarioNuevo = array(
            'USU_login'    => $this->input->post('inputLoginUsuario'),
            'USU_password' => crypt($this->input->post('inputLoginPassword'), $this->config->item('encryption_key')),
            'USU_estado'   => 1,
            'USU_asoc_id'  => $asociadoId,
            'NUSU_ID'      => 3,
        );
        $usuarioId = $this->users_model->insertUser($dataUsuarioNuevo);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo asociado correctamente.');

        redirect('asociados');
    }

    public function editar($asocId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'asociados/editar';
        $pagina->menu       = 'asociados';
        $pagina->subMenu    = 'asociados-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'Pro Olivo - Sistema Gestor de contenidos - Editar Asociado';

        $data['paginaDatos'] = $pagina;

        $data['datosAsociado'] = $this->asociado_model->getAsociado($asocId);
        $data['datosUsuario']  = $this->users_model->getUsuarioAsociadoId($asocId);

        /** array de marcas */
        $marcas              = $this->makes_model->getMakes();
        $data['arrayMarcas'] = ($marcas === false) ? array() : $marcas;

        $this->load->view('template/template', $data);
    }

    public function guardar_edicion_asociado($asocId)
    {
        $arrayMarcas  = $this->input->post('selectMarca');
        $stringMarcas = implode(',', $arrayMarcas);

        /** editar datos del asociado */
        $dataAsociadoEditar = array(
            'ASOC_empresa'          => $this->input->post('inputEmpresa'),
            'ASOC_productos'        => $this->input->post('inputProductos'),
            'ASOC_direccion'        => $this->input->post('inputDireccion'),
            'ASOC_telefono'         => $this->input->post('inputTelefono'),
            'ASOC_pagina_web'       => $this->input->post('inputPaginaWeb'),
            'ASOC_persona_contacto' => $this->input->post('inputPersonaContacto'),
            'ASOC_email'            => $this->input->post('inputEmail'),
            'ASOC_estado'           => 'habilitado',
            'ASOC_marca'            => $stringMarcas,
        );
        $this->asociado_model->asociadoEditar($asocId, $dataAsociadoEditar);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha guardado la información de asociado correctamente.');

        redirect('asociados');
    }

    /**
     * habilitar o deshabilitar un asociado
     * segun su estado actual
     * @param  [type] $asocId [description]
     * @return [type]         [description]
     */
    public function habilitar_deshabilitar($asocId)
    {
        $this->asociado_model->habilitar_deshabilitar($asocId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha actualizado el estado del asociado correctamente.');

        redirect('asociados');
    }

    public function eliminar($asocId)
    {
        $this->asociado_model->eliminar($asocId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha eliminado el asociado correctamente.');

        redirect('asociados');
    }
}