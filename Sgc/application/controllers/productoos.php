<?php
defined('BASEPATH') or exit('No direct script access allowed');

class products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if (is_null($this->session->userdata('sgc_user'))) {
            redirect('?next=' . $this->uri->uri_string());
        }

        /** modelos */
        $this->load->model('Products_model');
        $this->load->model('categorys_model');
        $this->load->model('Subcategorys_model');
        $this->load->model('makes_model');
    }

    public function index()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/listado';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Lista de Productos';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->articulo_model->getArticulos();
        $data['categorys'] = $this->categorys_model->getCategoryOrder('CAT_nombre asc');

        $this->load->view('template/template', $data);
    }

    public function novedades()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/listado';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'productss-listado-novedades';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Lista de Productos';

        $pagina->tipoArticulo = 'Noticias';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->Products_model->getNovedades();

        $this->load->view('template/template', $data);
    }

    
    public function proyectos()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/listado';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-listado-proyectos';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Lista de Productos';

        $pagina->tipoArticulo = 'Productos';

        $data['paginaDatos'] = $pagina;

        $data['arrayArticulos'] = $this->Products_model->getProductos();

        $this->load->view('template/template', $data);
    }

    public function nuevo()
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/nuevo';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-nuevo';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Nuevo Producto';

        $data['paginaDatos'] = $pagina;
        $marcas              = $this->makes_model->getMakes();
        $data['arrayMarcas'] = ($marcas === false) ? array() : $marcas;
        $data['categorys'] = $this->categorys_model->getCategoryOrder('CAT_nombre asc');
        $this->load->view('template/template', $data);
    }

    public function guardar_nuevo()
    {
        $arrayMarcas  = $this->input->post('selectMarca');

        if (empty($arrayMarcas)) {
            $stringMarcas = '';
        } else {
            $stringMarcas = implode(',', $arrayMarcas);
        }
        
        $config['upload_path']   = './uploads/productos/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('inputImagen')) {
            $error = '<small>No se logró subir la imagen, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
        } else {
            $imagen = $this->upload->data('file_name');
        }

        $config['upload_path']   = './uploads/productos/';
        $config['allowed_types'] = 'pdf';
        $this->upload->initialize($config);

        if (isset($_FILES['inputAdjunto']) && $_FILES['inputAdjunto']['name'] != '') {
            if ( ! $this->upload->do_upload('inputAdjunto')) {
                $errorAdjunto = '<small>No se logró subir el pdf adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $adjunto = $this->upload->data('file_name');
            }
        }

        $articuloNuevo = array(
            'PRO_nombre'      => $this->input->post('inputNombre'),
            'PRO_descripcion' => $this->input->post('inputDescripcion'),
            'PRO_imagen'      => isset($imagen) ? $imagen : null,
            'PRO_stock'      => $this->input->post('inputStock') ? 1 : 0,
            'EVE_tipo'        => $this->input->post('inputTipo'),
            
        );
        $articuloId = $this->Products_model->guardarNuevo($articuloNuevo);

        if ($this->session->userdata('sgc_user_nivel') == 'ASOCIADO') {
            // enviar correo al administrador
            $url = base_url('products/editar/' . $articuloId);
            $this->_enviar_mail_admin($articuloNuevo['EVE_titulo'], $url);
        }

        $this->session->set_flashdata('alert_type', 'success');

        $errorFlashdata = '';
        if (isset($error)) {
            $errorFlashdata .= $error . '<br>';
        }
        if (isset($errorAdjunto)) {
            $errorFlashdata .= $errorAdjunto . '<br>';
        }

        if ($errorFlashdata != '') {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo artículo correctamente.<br><br>' . $errorFlashdata);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado un nuevo artículo correctamente.');
        }

        redirect('products');
    }

    public function editar($artId)
        {
        $arrayMarcas  = $this->input->post('selectMarca');
        $stringMarcas = implode(',', $arrayMarcas);
        $data['categorys'] = $this->categorys_model->getCategorys();
        $data['subcategorys'] = $this->subcategorys_model->getSubcategorysByCat($data['products']->row('CAT_ID'));
    
        /**-- datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/editar';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-editar';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Editar Productos';

        $data['paginaDatos'] = $pagina;

        $data['datosArticulo'] = $this->articulo_model->getArticulo($artId);

        $this->load->view('template/template', $data);
    }

    public function guardar_edicion($artId)
    {
        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES['inputImagen']) && $_FILES['inputImagen']['name'] != '') {
            if ( ! $this->upload->do_upload('inputImagen')) {
                $error = '<small>No se logró subir la imagen, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $imagen = $this->upload->data('file_name');
            }
        }

        $config['upload_path']   = './uploads/articulos/';
        $config['allowed_types'] = 'pdf';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES['inputAdjunto']) && $_FILES['inputAdjunto']['name'] != '') {
            if ( ! $this->upload->do_upload('inputAdjunto')) {
                $errorAdjunto = '<small>No se logró subir el pdf adjunto, ocurrió el siguiente error: ' . $this->upload->display_errors(' ', ' ') . '</small>';
            } else {
                $adjunto = $this->upload->data('file_name');
            }
        }

        
        
        
        /** editar datos del asociado */
        $articulo = array(
             'PRO_nombre'      => $this->input->post('inputNombre'),
            'PRO_descripcion' => $this->input->post('inputDescripcion'),
            'PRO_imagen'      => isset($imagen) ? $imagen : null,
            'PRO_stock'      => $this->input->post('inputStock') ? 1 : 0,
           
        );
       
        if (isset($imagen)) {
            $articulo['PRO_imagen'] = $imagen;
        }
        
        $this->articulo_model->guardarEdicion($artId, $articulo);

        $this->session->set_flashdata('alert_type', 'success');

        $errorFlashdata = '';
        if (isset($error)) {
            $errorFlashdata .= $error . '<br>';
        }
        if (isset($errorAdjunto)) {
            $errorFlashdata .= $errorAdjunto . '<br>';
        }

        if ($errorFlashdata != '') {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del artículo correctamente.<br><br>' . $errorFlashdata);
        } else {
            $this->session->set_flashdata('alert_message', 'Se ha guardado la información del artículo correctamente.');
        }

        redirect('products');
    }

    public function activar_desactivar($artId)
    {
        $this->articulo_model->activar_desactivar($artId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha actualizado el estado del artículo correctamente.');

        redirect('products');
    }

    public function eliminar($artId)
    {
        $this->articulo_model->eliminar($artId);

        $this->session->set_flashdata('alert_type', 'success');
        $this->session->set_flashdata('alert_message', 'Se ha eliminado el artículo correctamente.');

        redirect('products');
    }

    public function detalle($artId)
    {
        /** datos de pagina */
        $pagina             = new stdClass();
        $pagina->vista      = 'products/detalle';
        $pagina->menu       = 'products';
        $pagina->subMenu    = 'products-detalle';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'NCK Ingenieros - Detalle del Producto';

        $data['paginaDatos'] = $pagina;

        $data['datosArticulo'] = $this->articulo_model->getArticulo($artId);

        $this->load->view('template/template', $data);
    }

    private function _enviar_mail_admin($nombreArticulo, $url)
    {
        $this->load->model('organizacion_model');
        $email_organizacion = $this->organizacion_model->getOrganizacion()->ORG_email;

        $this->load->library('MyPHPMailer');

        $email['titulo'] = 'Artículo por aprobar su publicación';
        $email['parrafo_inicial'] = 'Se ha registrado un nuevo artículo ("' . $nombreArticulo . '"), para editar y/o habilitar el artículo siga el enlace';
        $email['enlace'] = $url;
        $email['enlace_nombre'] = 'Ir a Artículo';

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom('roxsana@idw.com.pe', 'PROOLIVO');

        $mail->Subject    = '[Pro Olivo - Artículo por aprobar] ' . $nombreArticulo;
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view('template/modelo_email', $email, true);

        $mail->addAddress($email_organizacion);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

        return true;
    }
}