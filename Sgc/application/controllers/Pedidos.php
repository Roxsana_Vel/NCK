<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedidos extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->session->userdata('user_id')) {
            $this->session->set_flashdata('alert', 'info');
            $this->session->set_flashdata('message', '<b>Para realizar la compra se requiere autenticación</b>. Ingrese su usuario y contraseña para continuar.');
            redirect('user/login');
        }

        $this->load->model('pedidos_model');
    }

    public function index()
    {
        if ($this->input->post('payment_status') == 'Completed') {
            $id_order = $this->input->post('invoice');

            // pagar con paypal
            $this->pedidos_model->pay_to_paypal($id_order);

            $this->data['alert'] = 'info';
            $this->data['message'] = '<b>Su pago se ha realizado con éxito.</b>';
        }

        $this->data['orders'] = $this->pedidos_model->get_orders();

        $this->load->view('templates/header');
        $this->load->view('pedidos/index', $this->data);
        $this->load->view('templates/footer');
    }

    public function update()
    {
        if ($this->input->post('pedido')) {
            $order = $this->input->post();
            $this->pedidos_model->update_order($order);

            // informacion de contacto
            $this->data['contacto'] = $this->db->get('organizacion')->row();
            // informacion de cliente
            $this->db->join('persona', 'persona.PER_ID = pedido.PER_ID');
            $this->db->where('pedido.PED_ID', $order['pedido']);
            $this->data['cliente'] = $this->db->get('pedido')->row();

            // mensaje al cliente
            $data_message_cliente = array(
                'subject'       =>  'ACTUALIZACIÓN DE DATOS DE PEDIDO',
                'to_email'      =>  $this->data['cliente']->PER_email,
                'to_nombre'     =>  $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'from_email'    =>  $this->data['contacto']->ORG_email,
                'from_nombre'   =>  $this->data['contacto']->ORG_empresa,
                'message'       =>  $this->_report_details_order($order['pedido']),
            );
            $this->_send_message($data_message_cliente);

            // mensaje a ventas@bodegaviejomolino.com
            $data_message_admin = array(
                'subject'       =>  'CAMBIO DE DATOS EN PEDIDO',
                //'to_email'        =>  $this->data['contacto']->ORG_email,
                'to_email'      =>  $this->data['contacto']->ORG_empresa,
                'to_nombre'     =>  'Administrador de '.$this->data['contacto']->ORG_empresa,
                'from_email'    =>  $this->data['cliente']->PER_email,
                'from_nombre'   =>  $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'message'       =>  $this->_report_details_order($order['pedido']),
            );
            $this->_send_message($data_message_admin);

            echo json_encode(true);
        }
    }

    public function detalle()
    {
        $pedido = $this->input->post('pedido');

        if ($pedido) {
            $this->data['order'] = $this->pedidos_model->get_order($pedido);
            $this->data['details'] = $this->pedidos_model->get_details($pedido);

            $this->load->view('pedidos/detalle', $this->data);
        }
    }

    public function form_upload()
    {
        $pedido = $this->input->post('pedido');

        $this->load->view('pedidos/voucher', array('pedido' => $pedido));
    }

    public function upload_voucher($pedido)
    {
        $config['upload_path'] = './vouchers/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('voucher')) {
            $datos_imagen = $this->upload->data();
            $this->pedidos_model->save_voucher($datos_imagen, $pedido);

            // informacion de contacto
            $this->data['contacto'] = $this->db->get('organizacion')->row();
            // informacion de cliente
            $this->db->join('persona', 'persona.PER_ID = pedido.PER_ID');
            $this->db->where('pedido.PED_ID', $pedido);
            $this->data['cliente'] = $this->db->get('pedido')->row();

            // mensaje al cliente
            $data_message_cliente = array(
                'subject'       =>  'DETALLE DE PEDIDO',
                'to_email'      =>  $this->data['cliente']->PER_email,
                'to_nombre'     =>  $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'from_email'    =>  $this->data['contacto']->ORG_email,
                'from_nombre'   =>  $this->data['contacto']->ORG_empresa,
                'message'       =>  $this->_report_details_order($pedido),
            );
            $this->_send_message($data_message_cliente);

            // mensaje a ventas@bodegaviejomolino.com
            $data_message_admin = array(
                'subject'       =>  'CAMBIO DE VOUCHER EN PEDIDO',
                'to_email'      =>  $this->data['contacto']->ORG_empresa,
                'to_nombre'     =>  'Administrador de '.$this->data['contacto']->ORG_empresa,
                'from_email'    =>  $this->data['cliente']->PER_email,
                'from_nombre'   =>  $this->data['cliente']->PER_nombres.' '.$this->data['cliente']->PER_apellidos,
                'message'       =>  $this->_report_details_order($pedido),
            );
            $this->_send_message($data_message_admin);

            echo json_encode($datos_imagen['file_name']);
        } else {
            echo json_encode(0);
        }
    }

    private function _report_details_order($pedido)
    {
        $this->data['order'] = $this->pedidos_model->get_order($pedido);
        $this->data['details'] = $this->pedidos_model->get_details($pedido);

        return $this->load->view('pedidos/report_details_order', $this->data, TRUE);
    }

    private function _send_message($data_message)
    {
        $to = $data_message['to_email'];

        $subject = $data_message['subject'];

        $header  = "From: ".$data_message['from_nombre']." <".$data_message['from_email']."> \r\n";

        $header .= "Mime-Version: 1.0 \r\n";
        $header .= "Content-type: text/html; charset=utf-8\r\n";

        $message = $this->load->view('pedidos/message', $data_message, TRUE);

        @mail($to, $subject, $message, $header);
    }
}