<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeria extends CI_Controller {

    public $data = array();

    public function __construct()
    {
        parent::__construct();

        /** impedir el acceso si no es un usuario del sistema */
        if(is_null($this->session->userdata('sgc_user'))) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $this->load->model('articulos_model');
        $galeria_info = $this->articulos_model->get_article('galeria');
        $galeria_images = $this->articulos_model->get_gallery('galeria');

        if (empty($galeria_info) || empty($galeria_images)) {
            redirect();
        }

        $data['galeria_info']   = $galeria_info;
        $data['galeria_images'] = $galeria_images;

        $this->load->view('templates/header');
        $this->load->view('galeria/index', $data);
        $this->load->view('templates/footer');
    }
}