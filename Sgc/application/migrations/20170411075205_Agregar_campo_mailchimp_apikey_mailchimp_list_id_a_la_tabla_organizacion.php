<?php

class Migration_agregar_campo_mailchimp_apikey_mailchimp_list_id_a_la_tabla_organizacion extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_mailchimp_apikey' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                ),
                'ORG_mailchimp_list_id' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_mailchimp_apikey');
        $this->dbforge->drop_column('organizacion', 'ORG_mailchimp_list_id');
    }
}