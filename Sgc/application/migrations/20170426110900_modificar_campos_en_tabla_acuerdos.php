<?php

class Migration_modificar_campos_en_tabla_acuerdos extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'ACU_tipo' => array(
                'type' => 'ENUM("informe", "documento")'
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);

        $fields = array(
            'ACU_tipo_informe' => array(
                'type' => 'ENUM("consejo directivo", "asamblea de socios", "reuniones de trabajo")',
                'null' => true
            )
        );
        $this->dbforge->add_column('acuerdos', $fields);

    }

    public function down()
    {
        $fields = array(
            'ACU_tipo' => array(
                'type' => 'ENUM("acuerdo", "documento")'
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);

        $this->dbforge->drop_column('acuerdos', 'ACU_tipo_informe');
    }
}