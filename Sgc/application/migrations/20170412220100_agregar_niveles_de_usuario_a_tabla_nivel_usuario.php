<?php

class Migration_Agregar_niveles_de_usuario_a_tabla_nivel_usuario extends CI_Migration
{

    public function up()
    {
        /** vaciando la tabla nivel_usuario' */
        $nivelesAnteriores = $this->db->get('nivel_usuario')->result();
        foreach ($nivelesAnteriores as $key => $nivel) {
            $this->db->where('NUSU_ID', $nivel->NUSU_ID)->delete('nivel_usuario');
        }

        /** poblando la tabla nivel_usuario */
        $niveles = array(
            array(
                'NUSU_ID'    => 1,
                'NUSU_nivel' => 'SUPER ADMINISTRADOR',
            ),
            array(
                'NUSU_ID'    => 2,
                'NUSU_nivel' => 'ADMINISTRADOR',
            ),
            array(
                'NUSU_ID'    => 3,
                'NUSU_nivel' => 'ASOCIADO',
            ),
        );
        foreach ($niveles as $key => $nivel) {
            $this->db->insert('nivel_usuario', $nivel);
        }

        /** insertar usuario super administrador */
        $dataUsuarioSuperAdmin = array(
            'USU_ID'         => 1,
            'USU_login'      => 'admin',
            'USU_password'   => '2Tn68XyUMBCXk',
            'USU_activacion' => '2468b383707cd894717c',
            'USU_estado'     => 1,
            'PER_ID'         => 1,
            'NUSU_ID'        => 1,
        );
        $this->db->insert('usuario', $dataUsuarioSuperAdmin);
    }

    public function down()
    {
    }
}
