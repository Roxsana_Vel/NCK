<?php

class Migration_crear_tabla_seo_configuration extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id_seo' => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'description' => array(
                    'type' => 'TEXT'
                ),
                'keywords' => array(
                    'type' => 'TEXt'
                ),
                'prefix_title' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                ),
                'page' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                )
            )
        );
        $this->dbforge->add_key('id_seo', true);
        $this->dbforge->create_table('seo_configuration');

        // INICIALIZANDO TABLA SEO CONFIGURATION
        $paginas = array('homepage', 'productos', 'nosotros', 'bodega', 'novedades', 'galeria', 'contacto');

        foreach ($paginas as $pagina) {
            $seo = array(
                'description' => 'Finca Maravilla es una empresa innovadora, pionera y referente de un sector
                                  vitivinícola en continua transformación. Aquí aplicamos el lema “De la tierra a
                                  la botella” para la producción de nuestros productos, donde tratamos con esmero
                                  y cuidado el viñedo, con tratamientos nobles, evitando el uso excesivo de
                                  productos químicos para tener uvas sanas y ecológicas, con los cuales elaborar
                                  vinos de una calidad óptima para el exigente consumidor de productos vínicos.',
                'keywords'    => 'finca maravilla, vinos y piscos de tacna, vinos y piscos en tacna, bodegas de
                                  vinos y pisco en tacna, bodega de piscos de tacna, bodega de vinos en tacna,
                                  piscos y vinos de tacna, empresas de pisco de tacna, pisco tacneño, pisco de
                                  peru, pisco peruano, piscos peruanos.',
                'page'        => $pagina
            );
            $this->db->insert('seo_configuration', $seo);
        }

        // INSERTANDO TITLE
        $title = array(
            'description' => 'Finca Maravilla',
            'keywords'    => '',
            'page'        => 'title'
        );
        $this->db->insert('seo_configuration', $title);
    }

    public function down()
    {
        $this->dbforge->drop_table('seo_configuration');
    }
}