<?php

class Migration_Agregar_campo_eve_tipo_a_tabla_evento extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'evento',
            array(
                'EVE_tipo' => array(
                    'type' => 'ENUM("articulo", "evento")',
                    'null' => true
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('evento', 'EVE_tipo');
    }
}
