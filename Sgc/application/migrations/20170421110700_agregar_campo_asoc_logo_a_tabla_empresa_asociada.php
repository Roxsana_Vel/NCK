<?php

class Migration_Agregar_campo_asoc_logo_a_tabla_empresa_asociada extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'empresa_asociada',
            array(
                'ASOC_logo' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('empresa_asociada', 'ASOC_logo');
    }
}
