<?php

class Migration_Agregar_campos_de_empresa_a_tabla_persona extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'ASOC_id'               => array(
                    'type'           => 'INT',
                    'auto_increment' => true,
                ),
                'ASOC_empresa'          => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_productos'        => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_direccion'        => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_telefono'         => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                ),
                'ASOC_pagina_web'       => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_persona_contacto' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_email'            => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ),
                'ASOC_estado'           => array(
                    'type' => 'ENUM("habilitado", "deshabilitado")',
                ),
            )
        );
        $this->dbforge->add_key('ASOC_id', true);
        $this->dbforge->create_table('empresa_asociada');

        /** poblando tabla 'empresa_asociada' */
        $empresasAsociadas = array(
            array(
                'ASOC_empresa'          => 'Acaville SAC',
                'ASOC_productos'        => 'Aceite de oliva',
                'ASOC_direccion'        => 'Calle Comandante Moore 170 Miraflores',
                'ASOC_telefono'         => '+51-1-445-1516',
                'ASOC_pagina_web'       => 'www.acaville.com.pe',
                'ASOC_persona_contacto' => 'Renzo Gonzalez García',
                'ASOC_email'            => 'rgonzalez@acaville.com.pe',
                'ASOC_estado'           => 'activo',
            ),
            array(
                'ASOC_empresa'          => 'Agroindustrias del Sur S.A.',
                'ASOC_productos'        => 'Aceite de oliva',
                'ASOC_direccion'        => 'Jirón Enrique Quijano N° 455 Tacna',
                'ASOC_telefono'         => '+51-52-411938',
                'ASOC_pagina_web'       => 'www.aceitemontefiori.com',
                'ASOC_persona_contacto' => 'Mauricio Gnecco Canepa',
                'ASOC_email'            => 'mauricio@gneccoycia.com',
                'ASOC_estado'           => 'activo',
            ),
            array(
                'ASOC_empresa'          => 'Agroindustrias Gonzales S.A.C.',
                'ASOC_productos'        => 'Aceituna de mesa y Aceite de oliva',
                'ASOC_direccion'        => 'Parque Industrial MZ I Lt 15 Tacna',
                'ASOC_telefono'         => '+51-52-426305',
                'ASOC_pagina_web'       => 'www.vallesurperu.com',
                'ASOC_persona_contacto' => 'Lourdes Gonzales Koc',
                'ASOC_email'            => 'info@vallesurperu.com',
                'ASOC_estado'           => 'activo',
            ),
        );

        foreach ($empresasAsociadas as $key => $asociado) {
            $this->db->insert('empresa_asociada', $asociado);
        }

    }

    public function down()
    {
        $this->dbforge->drop_table('empresa_asociada');
    }
}
