<?php

class Migration_agregar_campo_video_a_la_tabla_evento extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'evento',
            array(
                'EVE_video' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 50
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('evento', 'EVE_video');
    }
}