<?php

class Migration_crear_tabla_redes_sociales extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id_redes' => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'facebook' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                ),
                'twitter' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                ),
                'youtube' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                ),
                'google' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                )
            )
        );
        $this->dbforge->add_key('id_redes', true);
        $this->dbforge->create_table('redes_sociales');

        // INICIALIZANDO LAS REDES SOCIALES
        $redes_sociales = array(
            'facebook' => '',
            'twitter'  => '',
            'youtube'  => '',
            'google'   => ''
        );
        $this->db->insert('redes_sociales', $redes_sociales);
    }

    public function down()
    {
        $this->dbforge->drop_table('redes_sociales');
    }
}