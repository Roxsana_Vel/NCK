<?php

class Migration_agregar_campo_mostrar_portada_a_tabla_banners extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_column(
            'banners',
            array(
                'BAN_mostrar_portada' => array(
                    'type' => 'BOOLEAN'
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('banners', 'BAN_mostrar_portada');
    }
}
