<?php

class Migration_agregar_boletin_a_tipo_de_informe_en_tabla_acuerdos extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'ACU_tipo_informe' => array(
                'type' => 'ENUM("consejo directivo", "asamblea de socios", "reuniones de trabajo", "proyectos", "boletin")',
                'null' => true
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);
    }

    public function down()
    {
        $fields = array(
            'ACU_tipo_informe' => array(
                'type' => 'ENUM("consejo directivo", "asamblea de socios", "reuniones de trabajo", "proyectos")',
                'null' => true
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);
    }
}