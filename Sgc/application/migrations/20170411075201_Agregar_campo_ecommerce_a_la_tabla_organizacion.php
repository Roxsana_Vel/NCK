<?php

class Migration_agregar_campo_ecommerce_a_la_tabla_organizacion extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_ecommerce' => array(
                    'type'  => 'BOOLEAN'
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_ecommerce');
    }
}