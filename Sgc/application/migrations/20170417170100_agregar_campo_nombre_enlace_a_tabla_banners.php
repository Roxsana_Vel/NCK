<?php

class Migration_Agregar_campo_nombre_enlace_a_tabla_banners extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'banners',
            array(
                'BAN_nombre_url' => array(
                    'type' => 'varchar',
                    'constraint' => 100,
                    'null' => true,
                ),
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('banners', 'BAN_nombre_url');
    }
}
