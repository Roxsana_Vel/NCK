<?php

class Migration_Modificar_campo_marca_de_tabla_emoresa_asociada extends CI_Migration
{
    public function up()
    {
        /** remover la columna marca_id */
        $this->dbforge->drop_column('empresa_asociada', 'marca_id');

        /** agregar la nueva columna ASOC_marca */
        $this->dbforge->add_column(
            'empresa_asociada',
            array(
                'ASOC_marca' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
            )
        );
    }

    public function down()
    {
        /** restaurar la columna marca_id */
        $this->dbforge->add_column(
            'empresa_asociada',
            array(
                'marca_id' => array(
                    'type' => 'INT',
                    'null' => true,
                ),
            )
        );

        /** remover la nueva columna ASOC_marca */
        $this->dbforge->drop_column('empresa_asociada', 'ASOC_marca');
    }
}
