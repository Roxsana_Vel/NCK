<?php

class Migration_crear_tabla_acuerdos extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'ACU_ID' => array(
                    'type'           => 'INT',
                    'auto_increment' => true
                ),
                'ACU_tipo' => array(
                    'type' => 'ENUM("acuerdo", "documento")'
                ),
                'ACU_fecha' => array(
                    'type' => 'DATE'
                ),
                'ACU_nombre' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                ),
                'ACU_descripcion' => array(
                    'type' => 'TEXT'
                ),
                'ACU_adjunto' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                ),
                'ACU_visibilidad' => array(
                    'type' => 'ENUM("publico", "socios")'
                ),
                'ACU_estado' => array(
                    'type' => 'INT'
                )
            )
        );
        $this->dbforge->add_key('ACU_ID', true);
        $this->dbforge->create_table('acuerdos');
    }

    public function down()
    {
        $this->dbforge->drop_table('acuerdos');
    }
}