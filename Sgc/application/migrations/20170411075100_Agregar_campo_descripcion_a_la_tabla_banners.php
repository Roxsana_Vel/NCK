<?php

class Migration_agregar_campo_descripcion_a_la_tabla_banners extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'banners',
            array(
                'BAN_descripcion' => array(
                    'type'  => 'TEXT'
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('banners', 'BAN_descripcion');
    }
}