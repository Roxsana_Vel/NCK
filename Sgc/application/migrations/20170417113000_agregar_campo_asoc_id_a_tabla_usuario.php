<?php

class Migration_Agregar_campo_asoc_id_a_tabla_usuario extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'usuario',
            array(
                'USU_asoc_id' => array(
                    'type' => 'INT',
                    'null' => true,
                ),
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('usuario', 'USU_asoc_id');
    }
}
