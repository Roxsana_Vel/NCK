<?php

class Migration_agregar_campo_id_analytics_a_la_tabla_organizacion extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_id_analytics' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 100
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_id_analytics');
    }
}