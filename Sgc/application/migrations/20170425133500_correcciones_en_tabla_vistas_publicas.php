<?php

class Migration_correcciones_en_tabla_vistas_publicas extends CI_Migration
{

    public function up()
    {
        $this->db->where('VIP_nombre', 'Estacionalidad de la cocecha');
        $this->db->update('vistas_publicas', array('VIP_nombre' => 'Estacionalidad de la cosecha'));
    }

    public function down()
    {
    }
}
