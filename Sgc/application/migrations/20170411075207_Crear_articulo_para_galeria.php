<?php

class Migration_crear_articulo_para_galeria extends CI_Migration {

    public function up()
    {
        $galeria = array(
            'EVE_titulo'      => 'Galeria',
            'EVE_resumen'     => '',
            'EVE_descripcion' => '',
            'EVE_autor'       => 'Finca Maravilla',
            'EVE_fecha'       => date('Y-m-d'),
            'EVE_estado'      => 1,
            'EVE_url'         => 'galeria'
        );
        $this->db->insert('evento', $galeria);
    }

    public function down()
    {
        $this->db->where('EVE_autor', 'Finca Maravilla');
        $this->db->where('EVE_url', 'galeria');
        $this->db->delete('evento');
    }
}