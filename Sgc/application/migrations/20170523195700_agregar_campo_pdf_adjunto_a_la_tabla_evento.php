<?php

class Migration_agregar_campo_pdf_adjunto_a_la_tabla_evento extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'evento',
            array(
                'EVE_pdf_adjunto' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('evento', 'EVE_pdf_adjunto');
    }
}