<?php

class Migration_agregar_campo_visibilidad_a_tabla_evento extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'evento',
            array(
                'EVE_visibilidad' => array(
                    'type' => 'enum("publico", "socios")',
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('evento', 'EVE_visibilidad');
    }
}
