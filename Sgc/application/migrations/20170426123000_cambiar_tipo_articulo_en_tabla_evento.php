<?php

class Migration_cambiar_tipo_articulo_en_tabla_evento extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'EVE_tipo' => array(
                'type' => 'ENUM("novedad", "proyecto")'
            )
        );
        $this->dbforge->modify_column('evento', $fields);
    }

    public function down()
    {
        $fields = array(
            'EVE_tipo' => array(
                'type' => 'ENUM("articulo", "evento")'
            )
        );
        $this->dbforge->modify_column('evento', $fields);
    }
}