<?php

class Migration_Agregar_campo_marca_a_empresa_asociada extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_column(
            'empresa_asociada',
            array(
                'marca_id' => array(
                    'type' => 'int',
                    'null' => true
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('empresa_asociada', 'marca_id');
    }
}
