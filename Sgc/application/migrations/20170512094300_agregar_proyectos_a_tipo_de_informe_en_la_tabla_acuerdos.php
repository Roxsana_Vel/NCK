<?php

class Migration_agregar_proyectos_a_tipo_de_informe_en_la_tabla_acuerdos extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'ACU_tipo_informe' => array(
                'type' => 'ENUM("consejo directivo", "asamblea de socios", "reuniones de trabajo", "proyectos")',
                'null' => true
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);
    }

    public function down()
    {
        $fields = array(
            'ACU_tipo_informe' => array(
                'type' => 'ENUM("consejo directivo", "asamblea de socios", "reuniones de trabajo")',
                'null' => true
            )
        );
        $this->dbforge->modify_column('acuerdos', $fields);
    }
}