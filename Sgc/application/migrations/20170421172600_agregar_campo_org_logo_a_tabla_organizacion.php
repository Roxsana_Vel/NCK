<?php

class Migration_Agregar_campo_org_logo_a_tabla_organizacion extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_logo' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_logo');
    }
}
