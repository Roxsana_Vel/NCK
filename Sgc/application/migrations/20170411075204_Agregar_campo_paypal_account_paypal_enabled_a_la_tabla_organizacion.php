<?php

class Migration_agregar_campo_paypal_account_paypal_enabled_a_la_tabla_organizacion extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_paypal_account' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                ),
                'ORG_paypal_mode' => array(
                    'type' => 'BOOLEAN'
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_paypal_account');
        $this->dbforge->drop_column('organizacion', 'ORG_paypal_mode');
    }
}