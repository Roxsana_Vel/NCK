<?php

class Migration_agregar_campo_facebook_app_id_a_la_tabla_organizacion extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'organizacion',
            array(
                'ORG_facebook_app_id' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 250
                )
            )
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('organizacion', 'ORG_facebook_app_id');
    }
}