<?php

class Migration_Crear_tabla_vistas_publicas extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(
            array(
                'VIP_ID'          => array(
                    'type'           => 'INT',
                    'auto_increment' => true,
                ),
                'VIP_nombre'      => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
                'VIP_nombre_ruta' => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
                'VIP_img_slogan'  => array(
                    'type'       => 'VARCHAR',
                    'constraint' => 500,
                    'null'       => true,
                ),
            )
        );
        $this->dbforge->add_key('VIP_ID', true);
        $this->dbforge->create_table('vistas_publicas');

        /** poblar la tabla de vistas publicas */
        $arrayVistas = array(
            $vistaPublica = array(
                'VIP_nombre'      => '¿Quiénes somos?',
                'VIP_nombre_ruta' => 'empresa',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Misión y visión',
                'VIP_nombre_ruta' => 'mision-y-vision',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Valores',
                'VIP_nombre_ruta' => 'valores',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Consejo directivo',
                'VIP_nombre_ruta' => 'consejo-directivo',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Asociados',
                'VIP_nombre_ruta' => 'asociados',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Localización',
                'VIP_nombre_ruta' => 'localizacion',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Estacionalidad de la cocecha',
                'VIP_nombre_ruta' => 'estacionalidad-de-la-cosecha',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Producción',
                'VIP_nombre_ruta' => 'produccion',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Noticias',
                'VIP_nombre_ruta' => 'noticias',
                'VIP_img_slogan'  => null,
            ),
            $vistaPublica = array(
                'VIP_nombre'      => 'Contacto',
                'VIP_nombre_ruta' => 'contacto',
                'VIP_img_slogan'  => null,
            ),
        );

        foreach ($arrayVistas as $key => $vista) {
            $this->db->insert('vistas_publicas', $vista);
        }
    }

    public function down()
    {
        $this->dbforge->drop_table('vistas_publicas');
    }
}
