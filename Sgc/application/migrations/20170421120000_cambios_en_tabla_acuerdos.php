<?php

class Migration_cambios_en_tabla_acuerdos extends CI_Migration
{
    public function up()
    {
        $adjunto = array(
            'ACU_adjunto' => array(
                'type'       => 'VARCHAR',
                'constraint' => 250,
                'null'       => true
            ),
        );
        $this->dbforge->modify_column('acuerdos', $adjunto);

        $this->dbforge->drop_column('acuerdos', 'ACU_visibilidad');
    }

    public function down()
    {
        $adjunto = array(
            'ACU_adjunto' => array(
                'type'       => 'VARCHAR',
                'constraint' => 250,
                'null'       => false
            ),
        );
        $this->dbforge->modify_column('acuerdos', $adjunto);

        $visibilidad = array(
            'ACU_visibilidad' => array(
                'type' => 'ENUM("publico", "socios")'
            )
        );
        $this->dbforge->add_column('acuerdos', $visibilidad);
    }
}
