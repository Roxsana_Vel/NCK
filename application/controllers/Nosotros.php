<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nosotros extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }

    public function index()
    {
        /**  */
    }

    /**
     * vista de quienes somos
     * @return [type] [description]
     */
    public function empresa()
    {
        $data['pagina'] = 'nosotros/empresa';
        $data['url']    = 'nosotros';
        $data['titulo'] = 'NCK Ingenieros - Empresa dedicada a la Fabricación, Reparación, Mantenimiento y Montaje de Estructuras Metálicas y Servicios Generales para los sectores productivos en General..';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('empresa');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/NCK Ingenieros/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Somos una organización privada sin fines de lucro representativa del sector olivícola peruano, conformada por empresas peruanas procesadoras y/o exportadoras de aceituna de mesa, aceite de oliva y derivados, comprometidos con el desarrollo del sector, la agroindustria y el país ...';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }
    

    public function sucursales()
    {
        $data['pagina'] = 'nosotros/sucursal';
        $data['url']    = 'nosotros';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Misión y visión';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('sucursal');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Al 2021, ser una institución representativa e innovadora del sector olivícola que conduzca al desarrollo sostenible a nivel nacional e internacional de sus asociados con responsabilidad social y ambiental ...';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function valores()
    {
        $data['pagina'] = 'nosotros/valores';
        $data['url']    = 'nosotros';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Valores';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('valores');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Responsabilidad: Comprometidos con el desarrollo de nuestros asociados, de la institución y del país. Credibilidad y Confianza: Garantizamos la transparencia de la gestión y planificación participativa en nuestra organización. Sostenibilidad: Fomentando la sostenibilidad económica, ambiental y social de nuestra asociación ...';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function consejo_directivo()
    {
        $data['pagina'] = 'nosotros/consejo_directivo';
        $data['url']    = 'nosotros';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Conseoj directivo';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('consejo-directivo');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'PRESIDENTE: Luciana Biondi Acosta, SECRETARIO: Carlos Guillen Velásquez, TESORERO: Lourdes Gonzalez Koc ...';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function localizacion()
    {
        $data['pagina'] = 'nosotros/localizacion';
        $data['url']    = 'informacion';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Localización';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('localizacion');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Las zonas con mejores condiciones para el cultivo del olivo se encuentran al sur del Perú, en los valles costeros desde Pisco hasta Tacna. En esta zona el suelo y el clima se conjugan para favorecer el cultivo del olivo y una de las características más marcadas es que la aceituna logra madurar en árbol y luego de un proceso de fermentación natural en salmuera, sin ningún tipo de aditivos químicos obtenemos nuestra Aceituna Negra Natural.';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function estacionalidad_cosecha()
    {
        $data['pagina'] = 'nosotros/estacionalidad_cosecha';
        $data['url']    = 'informacion';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Estacionalidad de la cosecha';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('estacionalidad-de-la-cosecha');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'En la actualidad se cultivan aprox. 20,000 hectáreas de olivos a nivel nacional, con un rendimiento promedio de 8,000 kg. de aceituna por hectárea.  El 20% de la producción peruana es destinado a la elaboración de aceite de oliva y el 80% al proceso de la aceituna de mesa.  Hoy en día a raíz del crecimiento y desarrollo productivo del sector, se está logrando la incorporación nuevas variedades en el cultivo.';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function produccion()
    {
        $data['pagina'] = 'nosotros/produccion';
        $data['url']    = 'informacion';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Producción';


        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('produccion');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'La producción de olivo y sus derivados en Perú presenta un dinámico y significativo crecimiento en los últimos años. Sus características naturales lo convierten en un país potencial para la producción de aceituna de mesa y aceite de oliva.';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }
}
