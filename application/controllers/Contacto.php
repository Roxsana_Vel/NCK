<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contacto extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');
        $this->load->model('email_model');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }

    public function index()
    {
        $data['pagina'] = 'contacto/contacto';
        $data['url']    = 'contacto';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Contacto';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('contacto');

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Póngase en contacto con nosotros, le responderemos en breve - Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->config('recaptcha');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('inputNombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('inputEmail', 'Correo electrónico', 'trim|required|valid_email');
        $this->form_validation->set_rules('inputAsunto', 'Asunto', 'trim|required');
        $this->form_validation->set_rules('inputMensaje', 'Mensaje', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/template', $data);
        } else {
            $this->load->library('curl');

            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
            $userIp = $this->input->ip_address();

            $secret = config_item('secret_key');
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $recaptchaResponse . '&remoteip=' . $userIp;

            $response = $this->curl->simple_get($url);
            $status = json_decode($response, true);

            if ($status['success']) {
                $this->_enviar_mensaje();
            } else {
                $data['captcha'] = 'El captcha es necesario.';

                $this->load->view('template/template', $data);
            }
        }
    }

    private function _enviar_mensaje()
    {
        /** datos del formulario de contacto */
        $nombre  = $this->input->post('inputNombre');
        $email   = $this->input->post('inputEmail');
        $asunto  = $this->input->post('inputAsunto');
        $mensaje = $this->input->post('inputMensaje');

        $dataMensaje = array(
            /** obligatorios */
            'emailEmisor' => $email,
            'asunto'      => "Pro Olivo - Formulario de contacto - $nombre",
            /** otros datos para la vista del mensaje */
            'fecha'       => date('d-m-Y'),
            'nombre'      => $nombre,
            'asunto_form' => $asunto,
            'mensaje'     => $mensaje,
        );

        $emailDestino    = $this->organizacion_model->getOrganizacion()['ORG_email'];
        $templateMensaje = 'email/contacto';

        $respuesta = $this->email_model->enviar_correo($emailDestino, $dataMensaje, $templateMensaje);

        redirect('contacto');
    }

}
