<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asociados extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('asociado_model');
        $this->load->model('marca_model');
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }

    public function index()
    {
        $data['pagina'] = 'asociados/asociados';
        $data['url']    = 'asociados';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Asociados';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('asociados');

        /** asociados */
        $data['arrayAsociados'] = $this->asociado_model->getAll();
        $data['arrayMarcas']    = $this->marca_model->getMarcasEmpresa();

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/'.$data['sloganImg']);
        $data['metaDescripcion'] = 'Listado de nuestros asociados vigentes - Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú';
        $data['metaKeywords'] = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    public function cargar_asociados_empresa()
    {
        $asocId = $this->input->post('asocId');

        /** si no se ha elegido una empresa, se muestran todas */
        if ($asocId == 'all') {
            $arrayAsociados = $this->asociado_model->getAll();
        } else {
            $arrayAsociados = $this->asociado_model->filtro_por_empresa($asocId);
        }

        if (sizeof($arrayAsociados) == 0) {
            die("<div class=\"alert alert-danger text-center\" role=\"alert\"> <strong>No se encontraron resultados ...</strong> Intente haciendo otra búsqueda</div>");
        }

        $data['arrayAsociados'] = $arrayAsociados;

        $this->load->view('asociados/filtro_busqueda', $data);
    }

    public function cargar_asociados_marca()
    {
        $marcaNombre = $this->input->post('marcaNombre');

        /** si no se ha elegido ninguna marca de empresa asociada, se muestran todas */
        if ($marcaNombre == 'all') {
            $arrayAsociados = $this->asociado_model->getAll();
        } else {
            $arrayAsociados = $this->asociado_model->filtro_por_marca($marcaNombre);
        }


        if (sizeof($arrayAsociados) == 0) {
            die("<div class=\"alert alert-danger text-center\" role=\"alert\"> <strong>No se encontraron resultados ...</strong> Intente haciendo otra búsqueda</div>");
        }

        $data['arrayAsociados'] = $arrayAsociados;

        $this->load->view('asociados/filtro_busqueda', $data);
    }

}
