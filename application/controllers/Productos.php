<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {
	function __construct() {
		parent::__construct();
		/** modelos */
        $this->load->model('articulo_model');
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');

        /** helpers */
        $this->load->helper('fecha');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
	}
	public function index(){
        $pagina             = new stdClass();
        $pagina->vista      = 'productos/listado';
        $pagina->menu       = 'producto';
        $pagina->subMenu    = 'producto-listado';
        $pagina->subSubMenu = '';
        $pagina->titulo     = 'nck-ingenieros';

        $data['paginaDatos'] = $pagina;
		/*$data['products'] = $this->products_model->getProductsOrder('PRO_nombre asc');
		$data['categorys'] = $this->categorys_model->getCategoryOrder('CAT_nombre asc');*/
		$this->load->view("template/template", $data);
		
	}
    public function detalle(){
        $data['pagina'] = 'productos/detalle';
        $data['url']    = 'proyectos';
        $data['titulo'] = 'NCK Ingenieros';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('proyectos');

        $arrayProyectos         = $this->articulo_model->getAllProyectos();
        $data['arrayProyectos'] = $arrayProyectos;

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor']     = 'https://www.facebook.com/nck/';
        $data['metaImagen']      = (is_null($data['sloganImg'])) ? base_url('dist/img/banner.jpg') : base_url('Sgc/uploads/banners/' . $data['sloganImg']);
        $data['metaDescripcion'] = 'NCK Ingenieros';
        $data['metaKeywords']    = 'NCK Ingenieros';

        $this->load->view('template/template', $data);  
		
	}
    
}
?>