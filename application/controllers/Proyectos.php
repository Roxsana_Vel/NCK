<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proyectos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('articulo_model');
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');

        /** helpers */
        $this->load->helper('fecha');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }
    /**
     * vista de listado de noticias
     * @return [type] [description]
     */
    public function index()
    {
        /** datos para la pagina */
        $data['pagina'] = 'proyectos/proyectos_listado';
        $data['url']    = 'proyectos';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Proyectos';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('proyectos');

        $arrayProyectos         = $this->articulo_model->getAllProyectos();
        $data['arrayProyectos'] = $arrayProyectos;

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor']     = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen']      = (is_null($data['sloganImg'])) ? base_url('dist/img/banner.jpg') : base_url('Sgc/uploads/banners/' . $data['sloganImg']);
        $data['metaDescripcion'] = 'Listado de las últimas proyectos - Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú';
        $data['metaKeywords']    = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    /**
     * vista de detalle de una noticia
     * @param  [type] $proyectoId [description]
     * @return [type]            [description]
     */
    public function detalle($proyectoId)
    {
        /** detalle de la noticia */
        $proyectoDetalle = $this->articulo_model->getArticuloId($proyectoId);

        if (is_null($proyectoDetalle)) {
            redirect(base_url('noticias'), 'refresh');
        } else {
            /** datos para la pagina */
            $data['pagina'] = 'proyectos/proyecto_detalle';
            $data['url']    = 'proyecto';
            $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - ' . $proyectoDetalle->EVE_titulo;

            /** detalle de la noticia */
            $data['proyectoDetalle'] = $proyectoDetalle;

            /** meta tags, tambien usados para opengraph */
            $data['metaOgAutor']     = 'https://www.facebook.com/pro.olivo/';
            $data['metaImagen']      = (is_null($proyectoDetalle->EVE_imagen)) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/' . $proyectoDetalle->EVE_imagen);
            $data['metaDescripcion'] = $proyectoDetalle->EVE_resumen;
            $data['metaKeywords']    = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

            $this->load->view('template/template', $data);
        }
    }
}