<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Noticias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('articulo_model');
        $this->load->model('organizacion_model');
        $this->load->model('vista_publica_model');

        /** helpers */
        $this->load->helper('fecha');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }
    /**
     * vista de listado de noticias
     * @return [type] [description]
     */
    public function index()
    {
        /** datos para la pagina */
        $data['pagina'] = 'noticias/noticias_listado';
        $data['url']    = 'noticias';
        $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - Noticias';

        $data['sloganImg'] = $this->vista_publica_model->getVistaPublicaImg('noticias');

        $arrayNoticias         = $this->articulo_model->getAllNovedades();
        $data['arrayNoticias'] = $arrayNoticias;

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor']     = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen']      = (is_null($data['sloganImg'])) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/banners/' . $data['sloganImg']);
        $data['metaDescripcion'] = 'Listado de las últimas noticias y artículos - Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú';
        $data['metaKeywords']    = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

        $this->load->view('template/template', $data);
    }

    /**
     * vista de detalle de una noticia
     * @param  [type] $noticiaId [description]
     * @return [type]            [description]
     */
    public function detalle($noticiaId)
    {
        /** detalle de la noticia */
        $noticiaDetalle = $this->articulo_model->getArticuloId($noticiaId);

        if (is_null($noticiaDetalle)) {
            redirect(base_url('noticias'), 'refresh');
        } else {
            /** datos para la pagina */
            $data['pagina'] = 'noticias/noticia_detalle';
            $data['url']    = 'noticias';
            $data['titulo'] = 'Pro Olivo - Procesadores Exportadores de Aceituna, Aceite de Oliva y Derivados del Perú - ' . $noticiaDetalle->EVE_titulo;

            /** detalle de la noticia */
            $data['noticiaDetalle'] = $noticiaDetalle;

            /** meta tags, tambien usados para opengraph */
            $data['metaOgAutor']     = 'https://www.facebook.com/pro.olivo/';
            $data['metaImagen']      = (is_null($noticiaDetalle->EVE_imagen)) ? base_url('dist/img/produccion.jpg') : base_url('Sgc/uploads/articulos/' . $noticiaDetalle->EVE_imagen);
            $data['metaDescripcion'] = $noticiaDetalle->EVE_resumen;
            $data['metaKeywords']    = 'pro olivo, procesadores de aceituna, exportacion de aceituna, aceite de oliva, aceituna, exportacion de aceituna peru, procesadores de aceituna peru';

            $this->load->view('template/template', $data);
        }

    }

}
