<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inicio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /** modelos */
        $this->load->model('banner_model');
        $this->load->model('articulo_model');
        $this->load->model('organizacion_model');

        /** datos de sesion */
        $datosOrganizacion = $this->organizacion_model->getOrganizacion();
        $this->session->set_userdata($datosOrganizacion);
    }

    public function index()
    {
        /** datos de la pagina */
        $data['pagina'] = 'inicio/inicio';
        $data['url']    = 'inicio';
        $data['titulo'] = 'NCK Ingenieros - Ingeniería Industrial';

        /** banners */
        $banners = $this->banner_model->getBannersPortada();
        $data['arrayBanners'] = ($banners == false) ? array() : $banners;

        /** tres ultimos articulos para la parte de "novedades" */
        $data['arrayNovedades'] = $this->articulo_model->get_novedades();

        /** tres ultimos eventos para la parte de "eventos más recientes" */
        $data['arrayEventos'] = $this->articulo_model->get_3_eventos();

        /** meta tags, tambien usados para opengraph */
        $data['metaOgAutor'] = 'https://www.facebook.com/pro.olivo/';
        $data['metaImagen'] = base_url('dist/img/produccion.jpg');
        $data['metaDescripcion'] = 'NCK Ingenieros - Ingeniería Industrial';
        $data['metaKeywords'] = 'NCK Ingenieros - Ingeniería Industrial';

        /** vista */
        $this->load->view('template/template', $data);
    }
}