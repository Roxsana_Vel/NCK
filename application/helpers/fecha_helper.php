<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * funcion para convertir la fecha en el formato d-m-Y
 * (util para usarlos en la vista)
 *
 * @param  date $fecha puede ser:
 *                      - Y-m-d
 *                      - d-m-Y
 *
 * @return date        fecha d-m-Y
 */
function fecha_dmy($fecha)
{
    $fecha_array = explode('-', $fecha);

    if (strlen($fecha_array[2]) == 4) {
        $fecha_ymd = $fecha;
    } else {
        $fecha_ymd = DateTime::createFromFormat('Y-m-d', $fecha)->format('d-m-Y');
    }
    return $fecha_ymd;
}

/**
 * funcion para convertir la fecha en el formato Y-m-d
 * (util para usarlos en alguna insercion o actualizacion de la db)
 *
 * @param  date $fecha puede ser:
 *                      - d-m-Y
 *                      - Y-m-d
 *
 * @return date        fecha Y-m-d
 */
function fecha_ymd($fecha)
{
    $fecha_array = explode('-', $fecha);

    if (strlen($fecha_array[0]) == 4) {
        $fecha_ymd = $fecha;
    } else {
        $fecha_ymd = DateTime::createFromFormat('d-m-Y', $fecha)->format('Y-m-d');
    }
    return $fecha_ymd;
}

function hora_hi($hora_his)
{
    $hora_hi = DateTime::createFromFormat('H:i:s', $hora_his)->format('H:i');
    return $hora_hi;
}

function get_nombre_dia($fecha_ymd)
{
    $fecha = strtotime($fecha_ymd);
    /**
     * el parametro w en la funcion date indica que queremos el dia de la semana
     * lo devuelve en numero 0 domingo, 1 lunes,....
     */
    switch (date('w', $fecha)) {
        case 0:return "domingo";
            break;
        case 1:return "lunes";
            break;
        case 2:return "martes";
            break;
        case 3:return "miercoles";
            break;
        case 4:return "jueves";
            break;
        case 5:return "viernes";
            break;
        case 6:return "sabado";
            break;
    }
}

/**
 * obtener el nombre del mes
 * por ejm: si recibe 4, devuelve 'abril'
 * @param  [type] $numMes ['1-12']
 * @return [type]         [enero-diciembre]
 */
function get_nombre_mes($numMes)
{
    /** definir el lenguaje a español */
    $lenguage = 'es_ES.UTF-8';
    putenv("LANG=$lenguage");
    setlocale(LC_ALL, $lenguage);

    /** obtengo el nombre del mes */
    $nombre = strftime("%B", mktime(0, 0, 0, $numMes, 1, 2000));
    return $nombre;
}

/**
 * obtener una fecha siguiente a la de hoy, en base a una cantidad de días que
 * agregamos (ejm.: '+7 day').
 * @param  string $dias_agregados cantidad de dias agregados
 *                                '+7 day'
 *                                '+1 day'
 *                                etc
 * @return [type]                 [description]
 */
function get_fecha_siguiente($fecha_inicio_ymd, $dias_agregados)
{
    $cde_fecha = strtotime($dias_agregados, strtotime($fecha_inicio_ymd));
    return date('Y-m-d', $cde_fecha);
}

/**
 * obtener la fecha del mes siguiente o el mes anterior
 *
 * @param  number $cant_meses_anteriores positivo o negativo
 *                                       (-1, -2, ...) calcula la fecha del(los) mes(es) anterior(es)
 *                                       (1, 2, ...) calcula la fecha del(los) mes(es) siguiente(es)
 *
 * @return [type]                        [description]
 */
function get_fecha_mes_sig_ant($cant_meses_anteriores)
{
    $fecha_mes_anterior = date('Y-m-d', strtotime("$cant_meses_anteriores month"));
    return $fecha_mes_anterior;
}

/**
 * obtener el total del días de un mes (de su correspondiente año)
 *
 * @param  string/int $Month ejm: emero -> $month = '01'
 *                                febrero -> $month = '02'
 *                                ...
 * @param  string/int $Year  año ('2017', '2018', ...)
 *
 * @return [type]        [description]
 */
function getTotalDiasDelMes($Month, $Year)
{
    if (is_callable("cal_days_in_month")) {
        return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
    } else {
        return date("d", mktime(0, 0, 0, $Month + 1, 0, $Year));
    }
}

function getDiferenciaDiasFechas($fecha_mayor, $fecha_menor)
{
    $date_menor      = new DateTime($fecha_menor);
    $date_mayor      = new DateTime($fecha_mayor);
    $diferencia_dias = $date_menor->diff($date_mayor)->days;

    return $diferencia_dias;
}
