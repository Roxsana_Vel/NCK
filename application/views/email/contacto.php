<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title><?= "$nombre quiere ponerse en contacto" ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i" rel="stylesheet">
</head>
<body style="margin: 20px 10px;
            font-family: 'Roboto', sans-serif;">
    <div style="background: #eeeeee;
            padding: 20px 10px;
            border-radius: 4px;
            max-width: 400px;
            margin: 0 auto;">
        <h2 style="margin: 0;
            padding: 0;
            padding-bottom: 15px;
            border-bottom: 1px dashed black;
            display: block;
            text-align: center;
            font-weight: 300;"><?= "$nombre quiere ponerse en contacto" ?></h2>

        <p style="font-style: italic;"><strong style="font-style: normal;">Fecha: </strong><?= $fecha ?></p>
        <p style="font-style: italic;"><strong style="font-style: normal;">Asunto: </strong><?= $asunto_form ?></p>

        <p style="font-style: italic;"><strong style="font-style: normal;">Mesaje: </strong><?= $mensaje ?></p>

        <div style="
            border-bottom: 1px dashed black;
            display: block;
            width: 100%;
            height: 1px;
        "></div>

        <p style="font-style: italic;">Mensaje enviado desde <a href="<?= base_url('contacto') ?>"><?= base_url('contacto') ?></a></p>
        <p>Por <?= $nombre ?> (<?= $emailEmisor ?>)</p>

    </div>
</body>
</html>
