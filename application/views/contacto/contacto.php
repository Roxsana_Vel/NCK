<!-- slogan -->
<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Contacto</h3>
    </div>
</div>

<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Contacto</h2>
            </div>
        </div>

        <div class="row seccion--contacto">
            <!-- formulario de contacto -->
            <div class="col-xs-12 col-sm-8">
                <div class="formulario_contacto wow fadeInLeft" data-wow-delay="0.5s">

                    <p>Escribanos un mensaje, le responderemos en breve</p>

                    <form id="formNuevoArticulo" class="form-horizontal" method="post">

                        <div class="form-group">
                            <label for="inputNombre" class="col-sm-4 control-label">Nombre</label>

                            <div class="col-sm-8">
                                <input type="text" id="inputNombre" name="inputNombre" class="form-control" placeholder="sus nombres y apellidos" value="<?= set_value('inputNombre') ?>" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-4 control-label">Correo electrónico</label>

                            <div class="col-sm-8">
                                <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="correo@correo.com" value="<?= set_value('inputEmail') ?>" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputAsunto" class="col-sm-4 control-label">Asunto</label>

                            <div class="col-sm-8">
                                <input type="text" id="inputAsunto" name="inputAsunto" class="form-control" placeholder="Asunto" value="<?= set_value('inputAsunto') ?>" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputMensaje" class="col-sm-4 control-label">Mensaje</label>

                            <div class="col-sm-8">
                                <textarea id="inputMensaje" name="inputMensaje" class="form-control" rows="4" placeholder="Escriba su mensaje" required><?= set_value('inputMensaje') ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <?php if (isset($captcha)) : ?>
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <?= $captcha ?>
                                    </div>
                                <?php endif ?>

                                <div class="g-recaptcha" data-sitekey="<?= config_item('site_key') ?>"></div>
                            </div>
                        </div>



                        <div class="text-center">
                            <button type="submit" class="btn btn-lg btn-info">Enviar</button>
                        </div>

                        <div class="text-center">
                            <br>Dirección: Calle Arias y Araguez #451 - Tacna - Perú
                            <br>Teléfono: +51 52 631448
                            <br>Correo electrónico: <a href="mailto:proolivo@proolivo.com">proolivo@proolivo.com</a>
                        </div>
                    </form>

                </div>

            </div>

            <!-- mensaje por facebook -->
            <div class="col-xs-12 col-sm-4">
                <div class="mensaje_facebook wow fadeInRight" data-wow-delay="1s">

                    <p>También puede escribirnos al facebook</p>
                    <div class="fb-page" data-href="https://www.facebook.com/pro.olivo/" data-tabs="messages,timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pro.olivo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div>

                </div>
            </div>

        </div>
    </div>
</div>

