<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp"><?= $proyectoDetalle->EVE_titulo ?></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-9">
                <div class="tarjeta tarjeta--noticia_detalle wow fadeInUp text-center hoverable">
                    <img src="<?= base_url('Sgc/uploads/articulos/' . $proyectoDetalle->EVE_imagen) ?>" alt="">
                    <div class="titulo_fecha">
                        <h4><?= $proyectoDetalle->EVE_titulo ?></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i> <?= fecha_dmy($proyectoDetalle->EVE_fecha) ?></span>
                    </div>
                    <div class="contenido">
                        <?= $proyectoDetalle->EVE_descripcion ?>

                        <?php if ($proyectoDetalle->EVE_pdf_adjunto): ?>
                            <a href="<?= base_url('Sgc/uploads/articulos/' . $proyectoDetalle->EVE_pdf_adjunto) ?>" download="<?= $proyectoDetalle->EVE_titulo . '.pdf' ?>">Descargar PDF Adjunto</a>
                        <?php endif ?>
                    </div>
                    <div class="titulo_fecha">
                        <h4>Comentarios</h4>
                    </div>

                    <?php
                        $host = $_SERVER["HTTP_HOST"];
                        $url = $_SERVER["REQUEST_URI"];
                        $urlActual = "http://" . $host . $url;
                     ?>

                    <div class="fb-comments" data-href="<?= $urlActual ?>" data-numposts="5"></div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 text-center">
                <div class="fb-page z-depth-1" data-href="https://www.facebook.com/pro.olivo/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pro.olivo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div>
            </div>
        </div>
    </div>
</div>