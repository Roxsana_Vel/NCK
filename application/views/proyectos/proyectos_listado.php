<!-- slogan -->
<div class="banner">
   <div class="container">
        <h1 class="text-uppercase">Proyectos</h1>
   </div>
</div>

<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Proyecto - SPCC Cuajone</h2>
            </div>
        </div>

        <div class="row">
          
          <section class="pasos">
			<div class="container">
				<div class="row">
					<div class="col-md-6">

					</div>
					<div class="col-md-6 col-pasos-info">
						<div class="pasos-info">

							<h3>Lorem ipsum dolor sit amet</h3>

							<div class="step1">
								<span class="icon-blue"><i class="icon-documents"></i></span>
								<h3>METAL MECÁNICA</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate nesciunt, accusantium exercitationem illo minima velit dignissimos ipsam ea, labore asperiores qui nostrum! Doloremque in quae delectus velit molestiae facere, tempora!</p>
							</div>
							<div class="step2">
								<span class="icon-blue"><i class="icon-clock"></i></span>
								<h3>METAL MECÁNICA</h3>

								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque corrupti, eveniet tenetur ea perspiciatis adipisci cum quod at architecto quam, soluta, delectus aspernatur. Mollitia vitae neque delectus, inventore corrupti obcaecati.</p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
           <!-- <?php foreach ($arrayProyectos as $proyecto): ?>
                <div class="tarjeta--noticia wow fadeInUp">
                    <div class="col-xs-3 noticia_img">
                        <img src="<?= base_url('Sgc/uploads/articulos/' . $proyecto->EVE_imagen) ?>" alt="<?= $proyecto->EVE_titulo ?>">
                    </div>
                    <div class="col-xs-9 noticia_resumen">
                        <h4><?= $proyecto->EVE_titulo ?></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i><?= fecha_dmy($proyecto->EVE_fecha) ?></span>

                        <div class="noticia_img__movil">
                            <img src="<?= base_url('Sgc/uploads/articulos/' . $proyecto->EVE_imagen) ?>" alt="<?= $proyecto->EVE_titulo ?>">
                        </div>

                        <p><?= $proyecto->EVE_resumen ?></p>
                        <div class="text-right">
                            <a class="btn btn-success" href="<?= base_url('proyectos/detalle/' . $proyecto->EVE_ID) ?>">LEER MÁS</a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>-->
        </div>
    </div>
</div> <hr>
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Proyecto - Angloamerica Quellaveco</h2>
            </div>
        </div>

        <div class="row">
          
          <section class="pasos">
			<div class="container">
				<div class="row">
					<div class="col-md-6">

					</div>
					<div class="col-md-6 col-pasos-info">
						<div class="pasos-info">

							<h3>Lorem ipsum dolor sit amet</h3>

							<div class="step1">
								<span class="icon-blue"><i class="icon-documents"></i></span>
								<h3>METAL MECÁNICA</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate nesciunt, accusantium exercitationem illo minima velit dignissimos ipsam ea, labore asperiores qui nostrum! Doloremque in quae delectus velit molestiae facere, tempora!</p>
							</div>
							<div class="step2">
								<span class="icon-blue"><i class="icon-clock"></i></span>
								<h3>METAL MECÁNICA</h3>

								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque corrupti, eveniet tenetur ea perspiciatis adipisci cum quod at architecto quam, soluta, delectus aspernatur. Mollitia vitae neque delectus, inventore corrupti obcaecati.</p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
           <!-- <?php foreach ($arrayProyectos as $proyecto): ?>
                <div class="tarjeta--noticia wow fadeInUp">
                    <div class="col-xs-3 noticia_img">
                        <img src="<?= base_url('Sgc/uploads/articulos/' . $proyecto->EVE_imagen) ?>" alt="<?= $proyecto->EVE_titulo ?>">
                    </div>
                    <div class="col-xs-9 noticia_resumen">
                        <h4><?= $proyecto->EVE_titulo ?></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i><?= fecha_dmy($proyecto->EVE_fecha) ?></span>

                        <div class="noticia_img__movil">
                            <img src="<?= base_url('Sgc/uploads/articulos/' . $proyecto->EVE_imagen) ?>" alt="<?= $proyecto->EVE_titulo ?>">
                        </div>

                        <p><?= $proyecto->EVE_resumen ?></p>
                        <div class="text-right">
                            <a class="btn btn-success" href="<?= base_url('proyectos/detalle/' . $proyecto->EVE_ID) ?>">LEER MÁS</a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>-->
        </div>
    </div>
</div