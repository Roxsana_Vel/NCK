<!-- slogan -->
<div class="banner">
   <div class="container">
       <div class="row">
           <div class="col-md-4 col-md-offset-8">
              <div class="box-empresas">
                  <div class="titulo-box">
                      <img src="<?= base_url('dist/img/img/chicago.fw.png') ?>" width="250px;" alt="">
                  </div>
                  <div class="cont-box">
                      <p>Facilitamos a nuestro personal equipos especializados de trabajo por área. Asi mismo exigimos responsabilidad en las medidas de seguridad en el desempeño diario de sus labores.</p>
                  </div>
              </div>
           </div>
       </div>
   </div>
</div>
<div class="container detalle-productos">
   <h4>Productos</h4>
   <hr class="hr-stile">
    <div class="row">
     
        <div class="col-md-3 col-xs-6 col-sm-6 ">
            <figure class="box">
                <a href=""><img src="<?= base_url('dist/img/h1.jpg') ?>" alt="">
                </a>
                <div class="text-info"> 
                   <span class=""></span>
                    <p class="text-center">nombre anonimo</p>
                </div>
                
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure  class="box">
                <a href=""><img src="<?= base_url('dist/img/h2.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure  class="box">
                <a href=""><img src="<?= base_url('dist/img/h3.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure class="box">
                <a href=""><img src="<?= base_url('dist/img/h4.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
    </div>
 <div class="row">
    
        <div class="col-md-3 col-xs-6 col-sm-6 ">
            <figure class="box">
                <a href=""><img src="<?= base_url('dist/img/h1.jpg') ?>" alt="">
                </a>
                <div class="text-info"> 
                   <span class=""></span>
                    <p class="text-center">nombre anonimo</p>
                </div>
                
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure  class="box">
                <a href=""><img src="<?= base_url('dist/img/h2.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure  class="box">
                <a href=""><img src="<?= base_url('dist/img/h3.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
        <div class="col-md-3 col-xs-6 col-sm-6">
            <figure class="box">
                <a href=""><img src="<?= base_url('dist/img/h4.jpg') ?>" alt=""></a>
                <div class="text-info">
                    <p class="text-center">nombre anonimo</p>
                </div>
            </figure>
        </div>
    </div>
</div>