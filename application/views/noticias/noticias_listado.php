<!-- slogan -->
<!--<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Noticias</h3>
    </div>
</div>-->

<!-- lineas de bienvenida -->
<div class="seccion"  id="slider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Últimas Noticias</h2>
            </div>
        </div>

        <div class="row" >

         <?php foreach ($arrayNoticias as $key => $noti): ?>
<div class="tarjeta--noticia wow fadeInUp">
    <div class="quote-container ">
	    <div class="right-banner"><img  class="img-responsive"src="<?= base_url('Sgc/uploads/articulos/'.$noti->EVE_imagen) ?>" alt="<?= $noti->EVE_titulo ?>"></div>
		    <div class="container">
			    <div class="row">
				    <div class="col-xs-12">
					    <div class="quote-box animate-effect">
						    <h4><?= $noti->EVE_titulo ?></h4>
						    <span><i class="glyphicon glyphicon-calendar"></i><?= fecha_dmy($noti->EVE_fecha) ?></span>
						    <p>
				               <?= $noti->EVE_resumen ?>
						    </p>
						    <a class="btn btn-success" href="<?= base_url('noticias/detalle/'.$noti->EVE_ID) ?>">LEER MÁS</a>
					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
    </div> 

            <?php endforeach ?>
           


            <!-- <div class="text-center">
                <nav aria-label="Page navigation" class="wow fadeInUp">
                    <ul class="pagination">
                        <li>
                        <a href="#!" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="active"><a href="#!">1</a></li>
                        <li><a href="#!">2</a></li>
                        <li><a href="#!">3</a></li>
                        <li><a href="#!">4</a></li>
                        <li><a href="#!">5</a></li>
                        <li>
                        <a href="#!" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div> -->

        </div>

    </div>
</div>
