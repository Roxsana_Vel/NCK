<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp"><?= $noticiaDetalle->EVE_titulo ?></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-9">

                <div class="tarjeta tarjeta--noticia_detalle wow fadeInUp text-center hoverable">
                    <img src="<?= base_url('Sgc/uploads/articulos/'.$noticiaDetalle->EVE_imagen) ?>" alt="">
                    <div class="titulo_fecha">
                        <h4><?= $noticiaDetalle->EVE_titulo ?></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i> <?= fecha_dmy($noticiaDetalle->EVE_fecha) ?></span>
                    </div>
                    <div class="contenido">
                        <?= $noticiaDetalle->EVE_descripcion ?>
                        <?php if (!is_null($noticiaDetalle->EVE_pdf_adjunto)): ?>
                            <a href="<?= base_url('Sgc/uploads/articulos/' . $noticiaDetalle->EVE_pdf_adjunto) ?>" download="<?= $noticiaDetalle->EVE_titulo . '.pdf' ?>">Descargar PDF Adjunto</a>
                        <?php endif ?>
                    </div>
                    <div class="titulo_fecha">
                        <h4>Comentarios</h4>
                    </div>

                    <?php
                        $host= $_SERVER["HTTP_HOST"];
                        $url= $_SERVER["REQUEST_URI"];
                        $urlActual = "http://" . $host . $url;
                     ?>

                    <div class="fb-comments" data-href="<?= $urlActual ?>" data-numposts="5"></div>
                </div>

            </div>

            <div class="col-xs-12 col-sm-3 text-center">
                <div class="fb-page z-depth-1" data-href="https://www.facebook.com/pro.olivo/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pro.olivo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div>
            </div>



        </div>

    </div>
</div>
