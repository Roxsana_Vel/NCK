<div class="quote-container" id="slider">
	<div class="right-banner"><img src="../assets/img/mineria.jpg"></div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="quote-box animate-effect">
						<h4>Pese a crisis, mineras aún tienen márgenes de ganancias.<i></i></h4>
						<p>
				            Empresas todavía registran ganancias en operaciones. Sin embargo caída de precios de metales e inversiones en el país les restan optimismo. Canon caería en 24%.
						</p>
						<a href="<?= base_url('noticias') ?>">Más información</a>
					</div>
				</div>
			</div>
		</div>
	</div>