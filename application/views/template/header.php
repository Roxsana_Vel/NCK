<!DOCTYPE html>
<html lang="es">
<head>
    <!--<meta name="robots" content="noindex,nofollow"/>-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="googlebot" content="all, index, follow">
    <meta name="google-site-verification" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- favicon -->
    <link rel="shortcut icon" href="<?= base_url('dist/img/img/favicon.ico') ?>">

    <!-- adaptación del viewport y más -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Meta Facebook -->
    <link rel='canonical' href='<?= base_url() ?>'>
    <meta property='og:title' content='<?= $titulo ?>'>
    <meta property='og:site_name' content='<?= $titulo ?>'>

    <?php $urlActual = current_url() ?>

    <meta property='og:url' content='<?= $urlActual ?>'>
    <meta property='og:description' content='<?= $metaDescripcion ?>'>
    <meta property='og:type' content='website'>
    <meta property='og:locale' content='es_ES'>
    <meta property='og:locale:alternate' content='fr_FR'>
    <meta property='og:locale:alternate' content='it_IT'>
    <meta property='article:author' content='<?= $metaOgAutor ?>'>
    <meta property='article:publisher' content='<?= $metaOgAutor ?>'>
    <meta property='og:image' content='<?= $metaImagen ?>'>

   
    <!-- Meta -->
    <title><?= $titulo ?></title>
    <meta name="author" content="IDW Sistemas Integrales">
    <meta name="description" content="<?= $metaDescripcion ?>">
    <meta name="keywords" content="<?= $metaKeywords ?>">
    <meta name="robots" content="all, index, follow">

    <!-- estilos -->
    
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300italic,300%7CLibre+Baskerville:400,400italic,700%7CLato:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   
    <link href="<?= base_url('dist/css/style.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/global.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/demo.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/icomoon.css') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/skin.less') ?>" rel="stylesheet">
    <link href="<?= base_url('dist/css/responsive.css') ?>" rel="stylesheet">
    
    <!-- Select2 
    <link rel="stylesheet" href="<?= base_url('Sgc/plugins/select2/select2.min.css') ?>"> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- fontastic -->
    <script src="https://use.fontawesome.com/fcfd0a87ec.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


       <!--script nck_ing-->
       
       <script src="<?=base_url('dist/js/assets/js/less.js') ?>"></script>
       <script src="<?=base_url('dist/js/assets/js/jquery.bxslider.js') ?>"></script>
       <script src="<?=base_url('dist/js/assets/js/site.js') ?>"></script>
       <script src="<?=base_url('dist/js/assets/js/jquery.flexslider.js') ?>"></script>
        <script src="<?=base_url('dist/js/assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?=base_url('dist/js/assets/js/jquery.easing.js') ?>"></script>

   
		
		
		
   
    <?php if ($this->router->class == 'contacto'): ?>
        <!-- GoogleReCaptcha -->
        <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <?php endif ?>
</head>

<body class="home-slider">
		<!--Wrapper Section Start Here -->
<div id="wrapper">
         <!-- <div id="pageloader" class="row m0">
		    <div class="loader-item"><img src="../assets/svg/grid.svg" alt="loading"></div>
          </div>-->
<header id="header">
  <div class="secondry-header">
    <div class="container">
     <div class="row">
         <div class="col-xs-12  col-sm-12  right-header headercontacto__datos wow fadeInRight ">
          <ul>
            <li>
              <a href="mailto:info@nckingenieros.com.pe"><i class="fa fa-envelope-o"></i><?= $this->session->userdata('ORG_email') ?></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </li>
          </ul>
        </div>
     </div>
      <div class="row">
        <div class=" col-xs-6 col-sm-4 col-lg-3 text-lef">
          <a  class="logo" href=""> <img src="<?= base_url('dist/img/img/logo-3.png') ?>" alt="logo"  class="responsive"/></a>
        </div>
        <div class="col-xs-6 visible-xs text-right">
        
            <button class="nav-button"></button>
        
        </div>
        <br> 
        <div class="col-xs-11 col-sm-8  col-lg-9 menu-wrapper">
          
          <nav class="navigation">
                <ul class="menu">
                  <li>
                    <a href="<?= base_url('proyectos') ?>">Proyectos</a>
                    <ul class="submenu ">
                      <!--<li>
                        <a href="#">SPCC Cuajone</a>
                      </li>
                      <li>
                        <a href="#">SPCC Cuajone</a>
                      </li>
                      <li>
                        <a href="#">Enersur</a>
                      </li>
                      <li>
                        <a href="#">Angloamerica Quellaveco</a>
                      </li>-->
                    </ul>
                  </li>
                  <li>
                    <a href="#">Clientes</a>
                  </li>
                  <li>
                  <a href="#">Nosotros</a>
                  <ul class="submenu">
                    <li>
                      <a href="<?= base_url('empresa') ?>">Empresa</a>
                    </li>
                    <li>
                      <a href="<?= base_url('sucursal') ?>">Sucursales</a>
                    </li>
                    <li>
                      <a href="#">Representaciones</a>
                    </li>
                    <li>
                      <a href="#">Políticas</a>
                    </li>
                    <li>
                      <a href="#">Recursos Humanos</a>
                    </li>
                  </ul>
                  </li>
                  <li>
                    <a href="#">Servicios</a>
                    <ul class="submenu">
                      <li>
                        <a href="<?= base_url('Servicios/soluciones') ?>">Soluciones a medida</a>
                      </li>
                      <li>
                        <a href="<?= base_url('Servicios/ofrecemos') ?>">¿Qué Ofrecemos?</a>
                      </li>
                      <li>
                        <a href="<?= base_url('Servicios/trabajos') ?>">Trabajos que realizamos</a>
                      </li>
                      <li>
                        <a href="<?= base_url('Servicios/garantia') ?>">Soporte Técnico</a>
                      </li>
                      <li>
                        <a href="<?= base_url('Servicios/garantia') ?>">Garantía de Servicio</a>
                      </li>
                    </ul>
                  </li>
                  
                  <li>
                    <a href="">Contacto</a>
                  </li>
                </ul>
          </nav>
        </div>

      </div>
    </div>
  </div>

</header>


    
   
   
   
    


    <!-- scripts -->
    <script>
        $(document).ready(function() {

            /** idioma español para todos los select2 */
            $.fn.select2.defaults.set('language', 'es');
        });
    </script>
