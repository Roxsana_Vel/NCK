   
    
    
    
    <footer id="footer">
  <!--contact-us container Start Here -->
  <div class="contact-us-container">
    <div class="container">
      <div class="row animate-effect">
        <div class="box col-xs-12 col-sm-4">
            <h3>BOLSA DE TRABAJO<i></i></h3>
        </div>
        <div class="box col-xs-12 col-sm-5">
            <strong><?= $this->session->userdata('ORG_telefono') ?></strong>
            <span><a href="mailto:postula@nckingenieros.com.pe"><?= $this->session->userdata('ORG_email') ?></a></span>
        </div>
        <div class="box col-xs-12 col-sm-2">
            <a class="contact-us" href="#">POSTULA</a>
        </div>
      </div>
    </div>
  </div>
<!--contact-us container End Here -->

<div class="primary-footer">
<div class="container">
  <div class="row animate-effect">
    <div class="col-xs-12 col-sm-8">
      <h3>NCK Ingenieros<i></i></h3>
      <p>
        NCK Ingenieros E.I.R.L. constituye hoy en día una empresa que ofrece servicios de alto nivel competitivo. Es una compañía global que vende productos de prestigiosas líneas mundiales y pone a disposición sus servicios, para llevar a cabo proyectos en general para solucionar sus requerimientos.
      </p>

    </div>

    <div class="col-xs-12 col-sm-4">
      <h3>Enlaces <i></i></h3>
      <ul class="list clearfix">
        <li>
          <a href="#">Inicio</a>
        </li>
        <li>
          <a href="#">Nosotros</a>
        </li>
        <li>
          <a href="#">Proyectos</a>
        </li>
        <li>
          <a href="#">Servicios</a>
        </li>
        <li>
          <a href="#">Representaciones</a>
        </li>
        <li>
          <a href="#">Contacto</a>
        </li>
      </ul>
    </div>

    </div>
    <div class="row animate-effect">
      <div class="col-xs-12 copyright">
        <span>NCK Ingenieros E.I.R.L. | © 2007 - <?php echo date("Y"); ?>, Todos los derechos reservados</span>
      </div>
    </div>

    </div>
  </div>
</footer>
</div>


</body>
</html>