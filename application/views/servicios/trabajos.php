<div class="banner-soluciones">
    <div class="container text-center">
      <div class="cont-soluciones">
            <img class="" src="<?= base_url('dist/img/img/service.png') ?>" alt="" width="70px;">
            <h1 class="text-uppercase">Trabajos que Realizamos</h1>
            <hr>
      </div>
   </div>
</div>
<div class="trabajos">
    <div class="container">
       <div class="row">
          <div class="solu-text">
              <h3>¿Qué Ofrecemos?</h3>
               <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis veniam impedit, numquam porro quasi corporis fuga dolore officia sit quos deserunt consequatur, facilis dignissimos sed officiis explicabo possimus nesciunt eligendi.</p>
          </div>
       </div>
<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content tab-trabajos">
    <div role="tabpanel" class="tab-pane active" id="home">
        <div class="row" style="min-height:300px;">
            <div  class="">
                <hr/>
                <div class="col-md-3">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-left">
                       <h4>Metal Mecánica</h4>
                       <hr>
                        <li class="active"><a href="#hom" data-toggle="tab">Reparación Parcial y Total de:</a></li>
                        <li><a href="#profil" data-toggle="tab">	Fabricación y Montaje de:</a></li>
                        <li><a href="#message" data-toggle="tab">Fabricación de:</a></li>
                        <li><a href="#seting">Maquinado y Rectificado de Piezas en:</a></li>
                        
                    </ul>
                </div>
                <div class="col-md-9 reparacion">
                    <!-- Tab panes -->
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="hom">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                              <!-- Indicators -->
                                  <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                  </ol>

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner" role="listbox">
                                    <div class="item active text-center">
                                      <img src="<?= base_url('dist/img/img/4.jpg') ?>" alt="...">
                                      <div class="carousel-caption">
                                      <p>Lampones Letorneau, CAT y Komatsu</p>
                                      </div>
                                    </div>
                                    <div class="item">
                                      <img src="<?= base_url('dist/img/img/4.jpg') ?>" alt="...">
                                      <div class="carousel-caption">
                                        nombre
                                      </div>
                                    </div>

                                  </div>

                                  <!-- Controls -->
                                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                        </div>
                    </div>
                        </div>
                        <div class="tab-pane" id="profil">
                            <p>Ofrecemos un Mantenimiento Industrial apoyados en el conocimiento de la Ingeniería cuya función es asegurar la adecuada disponibilidad de nuestras máquinas, edificios y equipos del Taller. De tal modo que nos conlleven a un funcionamiento y óptimo rendimiento de la inversión que nos representan.</p>
                            <p>En consecuencia creemos que el Trabajo de Taller debe considerarse como parte integral e importante de nuestra organización basándonos en:</p>
                            <ul>
                                <li>Control y Mantenimiento de nuestros Equipos</li>
                                <li>Realizando periódicamente un Mantenimiento Preventivo</li>
                                <li>Desarrollando nuevas tecnologías</li>
                                <li>Contar con Equipos de Trabajo especializado</li>
                                <li>Almacenes distribuidos y ordenados</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="message"> 
                            <p>Los servicios que brindamos a nuestros clientes son integrales, por lo cual ponemos a su disposición nuestro personal altamente capacitado, para cubrir sus necesidades en las áreas de ingeniería en mantenimiento, reparaciones, ventas, fabricación, metál mecánica, electricidad, soldadura y nuestra asesoría técnica comercial que nos permitirá ofrecer a su empresa un servicio basado en la calidad, confianza y seguridad.</p>
                            <p>El contar con nuestros profesionales hará direccionar los esfuerzos de su empresa hacia la esencia de su negocio. Todo nuestro personal está capacitado para trabajar bajo presión, en equipo y en un 100% de su capacidad. No sólo tendrá un buen trabajador, sino también una persona con criterio y capacidad de análisis de cada una de las situaciones que se les presente en el día a día.</p>
                        </div>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div>
        
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">profile</div>
    <div role="tabpanel" class="tab-pane" id="messages">message</div>
    <div role="tabpanel" class="tab-pane" id="settings">setting</div>
  </div>
</div>

</div>
</div>