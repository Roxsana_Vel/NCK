<div class="banner-soluciones">
    <div class="container text-center">
      <div class="cont-soluciones">
            <img class="" src="<?= base_url('dist/img/img/service.png') ?>" alt="" width="70px;">
            <h1 class="text-uppercase">Trabajos que Realizamos</h1>
            <hr>
      </div>
   </div>
</div>
<div class="ofrecemos">
    <div class="container">
       <div class="row">
          <div class="solu-text">
              <h3>¿Qué Ofrecemos?</h3>
               <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis veniam impedit, numquam porro quasi corporis fuga dolore officia sit quos deserunt consequatur, facilis dignissimos sed officiis explicabo possimus nesciunt eligendi.</p>
          </div>
       </div>
       <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">...</div>
    <div role="tabpanel" class="tab-pane" id="profile">...</div>
    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div>
  </div>

</div>
       <div class="row" style="min-height:300px;">
            <div  class="">
               
                <hr/>
                <div class="col-md-2">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#home" data-toggle="tab">Servicio de Campo</a></li>
                        <li><a href="#profile" data-toggle="tab">Servicio de Taller</a></li>
                        <li><a href="#messages" data-toggle="tab">Horas - Hombre</a></li>
                        
                    </ul>
                </div>
                <div class="col-md-10">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <p> Una de las etapas más importantes del proceso de nuestro trabajo de campo es demostrar nuestro conocimiento y experiencia en el área de Mantenimiento Industrial apoyándonos en:</p>
                            <ul>
                                <li>La descentralización de las actividades.</li>
                                <li>Seguimiento y Supervisión Intensiva de labores realizadas por personal calificado.</li>
                                <li>Procesamiento paralelo.</li>
                                <li>Sistema de control de calidad intensivo y automatizado.</li>
                                <li>Sistema de gestión.</li>
                                <li>Contamos con Profesionales y Técnicos altamente calificados y con amplia experiencia en su área, Incentivándolos a altos y constantes Niveles de Superación.</li>
                                <li>Facilitamos a nuestro personal Equipos especializados de trabajo por área asimismo exigimos responsabilidad en las medidas de seguridad en el desempeño diario de sus labores.</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="profile">
                            <p>Ofrecemos un Mantenimiento Industrial apoyados en el conocimiento de la Ingeniería cuya función es asegurar la adecuada disponibilidad de nuestras máquinas, edificios y equipos del Taller. De tal modo que nos conlleven a un funcionamiento y óptimo rendimiento de la inversión que nos representan.</p>
                            <p>En consecuencia creemos que el Trabajo de Taller debe considerarse como parte integral e importante de nuestra organización basándonos en:</p>
                            <ul>
                                <li>Control y Mantenimiento de nuestros Equipos</li>
                                <li>Realizando periódicamente un Mantenimiento Preventivo</li>
                                <li>Desarrollando nuevas tecnologías</li>
                                <li>Contar con Equipos de Trabajo especializado</li>
                                <li>Almacenes distribuidos y ordenados</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="messages"> 
                            <p>Los servicios que brindamos a nuestros clientes son integrales, por lo cual ponemos a su disposición nuestro personal altamente capacitado, para cubrir sus necesidades en las áreas de ingeniería en mantenimiento, reparaciones, ventas, fabricación, metál mecánica, electricidad, soldadura y nuestra asesoría técnica comercial que nos permitirá ofrecer a su empresa un servicio basado en la calidad, confianza y seguridad.</p>
                            <p>El contar con nuestros profesionales hará direccionar los esfuerzos de su empresa hacia la esencia de su negocio. Todo nuestro personal está capacitado para trabajar bajo presión, en equipo y en un 100% de su capacidad. No sólo tendrá un buen trabajador, sino también una persona con criterio y capacidad de análisis de cada una de las situaciones que se les presente en el día a día.</p>
                        </div>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div>
</div>
</div>