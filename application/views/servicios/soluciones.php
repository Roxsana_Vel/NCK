<div class="banner-soluciones">
    <div class="container text-center">
      <div class="cont-soluciones">
            <img class="" src="<?= base_url('dist/img/img/solu.png') ?>" alt="" width="70px;">
            <h1 class="text-uppercase">Soluciones a Medida</h1>
            <hr>
      </div>
   </div>
</div>
<div class="soluciones">
    <div class="container">
       <div class="row">
          <div class="solu-text">
              <h3>Soluciones a Medida del Cliente</h3>
               <hr>
                <p>	En NCK Ingenieros estamos concientes de la necesidad de un desarrollo a medida para el cliente, para ello tomamos en cuenta su solicitud y planteamos una solución óptima y eficiente, evaluando antes el entorno en el cual se desenvuelve la empresa - cliente así como los requerimientos específicos del negocio en un contexto cambiante</p>
          </div>
       </div>
       <div class="row">
          <div class="col-md-8 col-md-offset-2 ">
             <img src="<?= base_url('dist/img/img/indu.jpg') ?>" alt=""> 
          </div>
       </div>
       <div class="row">
          <div class="solu-text">
              <p>Contamos con altos especialistas, que siempre están dispuestos a ayudarlo y apoyarlo en tomar la mejor decisión para su empresa, para ello usamos la última tecnología disponible en el mercado para adaptar las propuestas de mejora a las necesidades de los clientes, siguiendo normas estrictas de análisis y seguridad.</p>
          </div>
       </div>
       <div class="row">
           <div class="col-md-9 col-md-offset-1">
              <div class="box-grey-cont">
                  Contamos con altos especialistas, que siempre están dispuestos a ayudarlo y apoyarlo en tomar la mejor decisión para su empresa
                  <hr>
              </div>
               
           </div>
       </div>
        
        
    </div>
</div>