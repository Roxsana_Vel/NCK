<div class="service-container-2">
	<div class="container">
		<div class="row">

			<div class="left-detail">
				<h2>Servicios que ofrecemos<i></i></h2>
			</div>

			<div class="col-xs-12 animate-effect">
				<div id="carousel" class="flexslider">
					<ul class="our-service slides clearfix">
						<li class="zoom">
							<a href="<?= base_url('productos/detalle') ?>"> <em class="fa fa-caret-up"></em>
							<figure class="fade-effect">
								<img src="<?= base_url('dist/img/1.jpg') ?>" alt="" />
							</figure> <strong> <i></i>Cucharones de Palas</span> </strong> </a>
						</li>

						<li class="zoom">
							<a href="#"> <em class="fa fa-caret-up"></em>
							<figure class="fade-effect">
								<img src="<?= base_url('dist/img/5.jpg') ?>" alt="" />
							</figure> <strong> <i></i> Puertas Enrollables </span> </strong> </a>

						</li>

						<li class="zoom">
							<a href="#"> <em class="fa fa-caret-up"></em>
							<figure class="fade-effect">
								<img src="<?= base_url('dist/img/3.jpg') ?>" alt="" />
							</figure> <strong> <i></i>Zapatas de Pala BUCYRUS 495B2</strong> </a>

						</li>

						<li class="zoom">
							<a href="#"> <em class="fa fa-caret-up"></em>
							<figure class="fade-effect">
								<img src="<?= base_url('dist/img/6.jpg') ?>" alt="" />
							</figure> <strong> <i></i>Fabricación de Soportes</strong> </a>

						</li>

						<li class="zoom">
							<a href="#"> <em class="fa fa-caret-up"></em>
							<figure class="fade-effect">
								<img src="<?= base_url('dist/img/2.jpg') ?>" alt="" />
							</figure> <strong> <i></i>Fabricación de Plegadoras</strong> </a>

						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="service-container-3">
	<div class="container">
		<div class="row">
			<div class="left-detail">
				<h2>CATÁLOGO DE PRODUCTOS <i></i></h2>
				<strong>Explore nuestra lista de productos que tenemos a disposición.</strong>
				<ul class="office-list clearfix">
					<li class="zoom">
						<figure class="fade-effect">
							<a href="<?= base_url('productos/detalle') ?>"><img src="<?= base_url('dist/img/img/image-25.jpg') ?>" alt="" /></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">Chicago Pneumatic</a></h4>
						</div>
					</li>
					<li class="zoom">
						<figure class="fade-effect">
							<a href="#"><img src="<?= base_url('dist/img/img/emp2.jpg') ?>" alt="" /></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">Hytorc</a></h4>
						</div>
					</li>
					<li class="zoom">
						<figure class="fade-effect">
							<a href="<?= base_url('productos/detalle') ?>"><img src="<?= base_url('dist/img/img/emp3.jpg') ?>" alt="" /></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">Simplex</a></h4>
						</div>
					</li>
					<li class="zoom">
						<figure class="fade-effect">
							<a href="#"><img src="<?= base_url('dist/img/img/emp6.jpg') ?>" alt="" /></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">ARMSTRONG</a></h4>
						</div>
					</li>
					<li class="zoom">
						<figure class="fade-effect">
							<a href="#"><img src="<?= base_url('dist/img/img/emp4.jpg') ?>" alt="" /></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">Hydac</a></h4>
						</div>
					</li>
					<li class="zoom">
						<figure class="fade-effect">
							<a href="#"><img src="<?= base_url('dist/img/img/image-27.jpg') ?>" alt=""></a>
						</figure>
						<div class="detail animate-effect">
							<h4><a href="#">Portal Industrial Supplies</a></h4>
						</div>
					</li>
				</ul>

			</div>

		</div>
	</div>
</div>

<!--Service Section End Here -->
<!--clientes Section init Here -->
<div class="clientes service-container-3">
   <div class="container">
       <div class="row">
          <div class="left-detail">
				<h2>NUESTROS CLIENTES <i></i></h2>
          </div>
           <div class="col-md-4 logo-img">
               <img src="<?= base_url('dist/img/img/Southern.fw.png') ?>" alt="">
           </div>
           <div class="col-md-4 logo-img text-center">
               <img src="<?= base_url('dist/img/img/engieperu.png') ?>" alt="">
           </div>
           <div class="col-md-4 logo-img">
              <img src="<?= base_url('dist/img/img/logo-es.gif') ?>" alt="">

           </div>
           
       </div>
   </div>
   
    
</div>
<!--clientes Section End Here -->


