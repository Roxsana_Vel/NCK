            <!-- slider principal -->
            <section id="slider">
				<ul class="top-banner">
					<li><img src="<?= base_url('dist/img/img/slider-1.jpg') ?>" alt="#"/></li>
					<li><img src="<?= base_url('dist/img/img/slider-2.jpg') ?>"alt="#"/></li>
					<li><img src="<?= base_url('dist/img/img/slider-3.jpg') ?>" alt="#"/></li>
				</ul>
				<div class="detail-content">
					<div class="container">
						<div class="detail">
							<div class="top-row">
								<strong>PROYECTOS EN MINERIA</strong>

								<div class="direction-arrrow">
									<a href="javascript:;" class="prv"></a>
									<a href="javascript:;" class="next"></a>
								</div>

							</div>

							<div class="bottom-sec">
								<h2>Metal Mecánica</h2>
								<p>
									Facilitamos a nuestro personal equipos especializados de trabajo por área. Asi mismo exigimos responsabilidad en las medidas de seguridad en el desempeño diario de sus labores.
								</p>
								<a href="#">VER PROYECTOS</a>
							</div>

						</div>
					</div>
				</div>
			</section>
			<?php $this->load->view('/nosotros/somos') ?>
			<?php $this->load->view('/servicios/ofrecemos') ?>
			<?php $this->load->view('/noticias/noticia') ?>
<!-- lineas de bienvenida
<div class="seccion">
<div class="container">
<div class="row">
    <div class="col-xs-12 text-center">
        <h2 class="wow fadeInUp">BIENVENIDOS</h2>
        <p class="wow fadeIn">El olivo llega al Perú en 1559, traído desde Sevilla, España y se adaptó rápidamente a las condiciones especiales de nuestro clima y suelo, es especial en los valles costeros del sur, desde Pisco hasta Tacna. <strong>Pro Olivo</strong> está conformado por empresas procesadoras de aceituna, aceite de oliva y derivados y nuestra misión es fortalecer la cultura empresarial de nuestros asociados, garantizando la calidad de sus productos y la sostenibilidad de las exportaciones.</p>
    </div>
</div>
</div>
</div> -->


<!-- slogan 
<div class="slogan" style="background: url(<?= base_url('dist/img/fondo_slogan_1.jpg') ?>); background-size: cover; background-position: center 25%; background-attachment: fixed">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Nuestros productos de excelente calidad<br>se exportan al mundo entero.</h3>
    </div>
</div>-->

<!-- seccion de novedades
<?php if (sizeof($arrayNovedades) > 0): ?>

    <div class="seccion">
        <h2 class="text-center wow fadeInUp">Novedades</h2>
        <div class="container">
            <div class="row">

                <?php foreach ($arrayNovedades as $key => $art): ?>
                    <div class="col-xs-12 col-sm-4 wow fadeInUp">
                        <div class="hoverable">
                            <div class="tarjeta">
                                <a href="<?= base_url('noticias/detalle/'.$art->EVE_ID) ?>">
                                    <img src="<?= base_url('Sgc/uploads/articulos/'.$art->EVE_imagen) ?>" alt="">
                                </a>
                                <div class="tarjeta__titulo">
                                    <h4><?= $art->EVE_titulo ?></h4>
                                </div>
                            </div>
                            <div class="tarjeta__descripcion">
                                <p><?= $art->EVE_resumen ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>

            </div>
        </div>

        <br>

        <div class="text-center">
            <a href="<?= base_url('noticias') ?>" class="btn btn-info btn-lg wow fadeInUp">Ver todas</a>
        </div>
    </div>

<?php endif ?>
 -->
<!-- seccion de eventos y agenda de actividades 
<?php if (sizeof($arrayEventos) > 0): ?>

    <div class="seccion seccion--alterno">
        <h2 class="text-center wow fadeInUp">Eventos y actividades</h2>
        <div class="container">
            <div class="row">

                <?php foreach ($arrayEventos as $key => $eve): ?>
                    <div class="col-xs-12 col-sm-4 wow fadeInUp">
                        <div class="hoverable">
                            <div class="tarjeta">
                                <a href="<?= base_url('noticias/detalle/'.$eve->EVE_ID) ?>">
                                    <img src="<?= base_url('Sgc/uploads/articulos/'.$eve->EVE_imagen) ?>" alt="">
                                </a>
                                <div class="tarjeta__titulo">
                                    <h4><?= $eve->EVE_titulo ?></h4>
                                </div>
                            </div>
                            <div class="tarjeta__descripcion">
                                <p><?= $eve->EVE_resumen ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>

            </div>
        </div>
    </div>

<?php endif ?>
-->
<!-- seccion dividida, de video y facebook
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rX2Ln4GqfC0?controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ 
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>-->
<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>