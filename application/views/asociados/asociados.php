<!-- slogan -->
<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Asociados</h3>
    </div>
</div>

<!-- lineas de bienvenida -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="wow fadeInUp">Asociados</h2>
                <p class="wow fadeInUp" data-wow-delay="0.5s">Directorio de Empresas asociadas a Pro Olivo</p>
            </div>
        </div>

        <!-- filtros -->
        <div class="row">

            <br>

            <!-- filtro por empresa -->
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="selectFiltroEmpresas" class="col-sm-4 control-label text-right">Filtro por Empresas</label>

                    <div class="col-sm-8">
                        <select name="selectFiltroEmpresas" id="selectFiltroEmpresas" class="form-control">
                            <option value="">Búsqueda por empresas</option>
                            <option value="all">Ver todos los asociados</option>
                            <?php foreach ($arrayAsociados as $key => $asoc): ?>
                                <option value="<?= $asoc->ASOC_id ?>"><?= $asoc->ASOC_empresa ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>

            <!-- filtro por empresa -->
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="selectFiltroMarcas" class="col-sm-4 control-label text-right">Filtro por Marcas</label>

                    <div class="col-sm-8">
                        <select name="selectFiltroMarcas" id="selectFiltroMarcas" class="form-control">
                            <option value="">Búsqueda por marcas</option>
                            <option value="all">Ver todos los asociados</option>
                            <?php foreach ($arrayMarcas as $key => $marca): ?>
                                <option value="<?= $marca->MAR_nombre ?>"><?= $marca->MAR_nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="seccionAsociadosFiltro">
            <!-- cargado dinámicamente -->
        </div>
    </div>
</div>

<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rX2Ln4GqfC0?controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        /** plugins */
        $("#selectFiltroEmpresas").select2({
            placeholder: 'Búsqueda por empresas',
        });
        $("#selectFiltroMarcas").select2({
            placeholder: 'Búsqueda por marcas',
        });

        /** cargado de todos los asociados la primera vez */
        var data = {
            url              : '<?= base_url('asociados/cargar_asociados_empresa') ?>',
            selector_destino : '#seccionAsociadosFiltro',
            parametros       : {
                asocId : 'all',
            }
        };
        cargar_seccion(data);

        /** filtro por empresas */
        $('#selectFiltroEmpresas').on('change', function(event) {
            event.preventDefault();
            var data = {
                url              : '<?= base_url('asociados/cargar_asociados_empresa') ?>',
                selector_destino : '#seccionAsociadosFiltro',
                parametros       : {
                    asocId : $(this).val(),
                }
            };
            cargar_seccion(data);
        });


        /** filtro por marcas */
        $('#selectFiltroMarcas').on('change', function(event) {
            event.preventDefault();
            var data = {
                url              : '<?= base_url('asociados/cargar_asociados_marca') ?>',
                selector_destino : '#seccionAsociadosFiltro',
                parametros       : {
                    marcaNombre : $(this).val(),
                }
            };
            cargar_seccion(data);
        });


    });
</script>