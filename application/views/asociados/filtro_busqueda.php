<?php foreach ($arrayAsociados as $key => $asoc): ?>

    <div class="col-xs-12 col-sm-6 col-lg-4">
        <div class="tarjeta--asociado wow fadeInUp">
            <h4 class="text-center"><?= $asoc->ASOC_empresa ?></h4>

            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Productos: </strong></p>
                </div>
                <div class="col-xs-9">
                    <p><?= $asoc->ASOC_productos ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Dirección: </strong></p>
                </div>
                <div class="col-xs-9">
                    <p><?= $asoc->ASOC_direccion ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Teléfono: </strong></p>
                </div>
                <div class="col-xs-9">
                    <p><?= $asoc->ASOC_telefono ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Página web: </strong></p>
                </div>
                <div class="col-xs-9">
                    <a href="<?= 'http://'.$asoc->ASOC_pagina_web ?>" target="_blank"><?= $asoc->ASOC_pagina_web ?></a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Persona de contacto: </strong></p>
                </div>
                <div class="col-xs-9">
                    <p><?= $asoc->ASOC_persona_contacto ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Email: </strong></p>
                </div>
                <div class="col-xs-9">
                    <p><?= $asoc->ASOC_email ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 tarjeta--asociado__label">
                    <p><strong>Marca: </strong></p>
                </div>
                <div class="col-xs-9">
                    <?php
                        $arrayMarcas = explode(',', $asoc->ASOC_marca);
                    ?>
                    <p>
                        <?php foreach ($arrayMarcas as $key => $marca): ?>
                            <span class="label label-primary"><?= $marca ?></span>
                        <?php endforeach ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php endforeach ?>
