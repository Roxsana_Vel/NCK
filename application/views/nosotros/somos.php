<div id="content">
				<!--Info Section Start Here -->
				<div class="info-container-1">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 about-us">
								<header class="section-header">
									<h2>¿QUIENES SOMOS?<i></i></h2>
									<strong>NCK Ingenieros es una empresa dedicada a la Fabricación, Reparación, Mantenimiento y Montaje de Estructuras Metálicas, Soldadura en Acero y Servicios Generales para los sectores productivos en General..</strong>
								</header>

								<ul class="our-quality clearfix">
									<li class="zoom">
										<figure class="fade-effect"><img src="<?= base_url('dist/img/img/01.svg') ?>" class="svg" alt="" />
										</figure>
										<div class="animate-effect">
										<strong>Apasionados por la calidad</strong>
										<p>
											Trabajamos centrados en el cliente somos su “Socio Amigo”, sabemos que el cliente es nuestra razón de ser y las soluciones que proponemos no sólo son buenas, sino factibles.
										</p>
										</div>
									</li>
									<li class="zoom">
										<figure class="fade-effect"><img src="<?= base_url('dist/img/img/02.svg') ?>" class="svg" alt="" />
										</figure>
										<div class="animate-effect">
										<strong>Honestidad y Responsabilidad</strong>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nec erat erat. Integer blandit, nullahendrerit,
										</p>
										</div>
									</li>
									<li class="zoom">
										<figure class="fade-effect"><img src="<?= base_url('dist/img/img/03.svg') ?>" class="svg" alt="" />
										</figure>
										<div class="animate-effect">
										<strong> Constante mejora</strong>
										<p> <br>
											Trabajamos centrados en el cliente somos su “Socio Amigo”, sabemos que el cliente es nuestra razón de ser.
										</p>
										</div>

									</li>
									<li class="zoom">
										<figure class="fade-effect"><img src="<?= base_url('dist/img/img/04.svg') ?>" class="svg" alt=""  />
										</figure>
										<div class="animate-effect "> 
										<strong> Trabajo duro</strong>
										<p> <br>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nec erat erat. Integer blandit, nullahendrerit,
										</p>
										</div>
									</li>
								</ul>

							</div>

						</div>
					</div>
				</div>