<!-- slogan -->
<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Producción</h3>
    </div>
</div>

<!-- seccion de mision y vision -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <h2 class="text-center wow fadeInUp">Producción</h2>

            <div class="col-xs-12 col-sm-5">
                <img class="img-responsive wow fadeInLeft" data-wow-delay="0.5s" src="<?= base_url('dist/img/arboles-olivo.jpg') ?>" alt="Producción de olivo y sus derivados en Perú">
            </div>

            <div class="col-xs-12 col-sm-7 text-justify wow fadeInRight" data-wow-delay="1s">
                <p>La producción de olivo y sus derivados en Perú presenta un dinámico y significativo crecimiento en los últimos años. Sus características naturales lo convierten en un país potencial para la producción de aceituna de mesa y aceite de oliva.</p>
                <p>En la actualidad se cultivan aproximadamente 30,000 hectáreas de olivos a nivel nacional, de las cuales 20,000 has. se encuentran  en producción con un rendimiento promedio de 6,000  kg de aceituna por hectárea. El 80% de la producción peruana es destinado al proceso de aceituna de mesa y el 20% a la elaboración de aceite de oliva.</p>
                <p>Hoy en día a raíz del crecimiento y desarrollo productivo del sector, se está incorporando nuevas variedades con mejor mercado de exportación.</p>
            </div>
        </div>
    </div>
</div>




<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rX2Ln4GqfC0?controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>