	<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">¿Quiénes somos?</h3>
    </div>
</div>
                
                <div class="info-container-1  empresa">
					
					<div class="container">
						<div class="row about-us">
						<h2>HISTORIA.<i></i></h2>
							<div class="col-md-7">
								<div class="animate-effect ">  <!--quote-box-->
									
									<p>
								            NCK Ingenieros EIRL es fundada el 25 de junio de 1999 por la Familia Cornejo Carpio, bajo la dirección del Ingeniero César Cornejo, en un ideal de superación decidieron contribuir al desarrollo industrial de la Región Sur, implementando una Empresa dedicada a la Fabricación y Reparación de estructuras metálicas como también a la comercialización de herramientas, accesorios y equipos de reconocidas marcas a nivel mundial, para importantes sectores Industriales de nuestro País. <br>
 	
                                            El inicio de NCK fue producto del apoyo de muchas empresas que como nosotros en esos momentos recién iniciaban sus actividades, pero con mucho tesón y paciencia logramos ganar la confianza de otras Empresas de mayor experiencia.Ahora con mucho orgullo y satisfacción todos quienes conformamos la familia NCK nos sentimos satisfechos de los logros obtenidos, mas aún de ser un grupo con una base sólida en valores y con la firme decisión de brindar cada día lo mejor de nuestro trabajo siendo nuestro principal objetivo satisfacer los requerimientos de nuestros clientes, quienes para nosotros son el motivo principal de superarnos cada día más.</p>
                                        </div>
                                    </div>
								
							            <div class="col-md-5"><img  class="img-responsive"  src="<?= base_url('dist/img/img/historia.jpg') ?>" ></div>
                                       </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                            <p>  
                                             
                                            Tenemos como objetivo convertirnos en una Empresa , con proyección a brindar más puestos de trabajo, que colaboren a la desocupación de nuestro país, ayudando a muchos jóvenes emprendedores a superarse profesional y técnicamente. <br>

                                            Contamos con profesionales de diferentes áreas, que conjugan experiencia, conocimiento y práctica en; todos los servicios que brindamos, nos innovamos constantemente para estar a la vanguardia de la tecnología de punta, teniendo como desarrollo integral brindar Asesoramiento de Ingeniería, servicios y mantenimiento en general. <br>

                                            Para mejor desarrollo de nuestros servicios y atención de nuestros clientes, hemos implementado una moderna planta ubicada en el Parque Industrial de Ilo, respaldada con modernas instalaciones y equipos de trabajo que nos permite una rápida y eficaz atención a nuestros socios clientes en las reparaciones y fabricaciones que nos soliciten.  </p>
                                        </div>
                                    
							
						</div>
					</div>
				</div>
				<div class="service-container-2">
					<div class="container">
						<div class="row">

							<div class="left-detail">
								<h2>Misión y Visión<i></i></h2>
								<p>A lo largo de nuestra trayectoria, hemos tomando una posición considerable dentro del mercado e industria minera. Contamos en nuestra cartera de clientes con empresas como:</p>
							</div>

							<div class=" animate-effect">
							<div class="col-xs-12 col-md-8">
							    
							</div>
							<div class="col-md-4">
							    
							</div>
							
							</div>

						</div>
					</div>
				</div>
<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>