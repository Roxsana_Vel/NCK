<!-- slogan -->
<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">Localización</h3>
    </div>
</div>

<!-- seccion de mision y vision -->
<div class="seccion">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="text-center wow fadeInUp">Localización</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <p class="wow fadeInUp">Las zonas con mejores condiciones para el cultivo del olivo se encuentran al <strong>sur del Perú</strong>, en los valles costeros <strong>desde Pisco hasta Tacna</strong></p>
                        <p class="wow fadeInUp" data-wow-delay="0.5s">En esta zona el  suelo y el clima se conjugan para favorecer  el cultivo del olivo y una de las características más marcadas es que la  aceituna  logra madurar en árbol y luego de un proceso de fermentación natural en salmuera, sin ningún tipo de aditivos químicos  obtenemos  nuestra Aceituna Negra Natural.</p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <img class="img-responsive wow fadeInRight" data-wow-delay="0.5s" src="<?= base_url('dist/img/localizacion.jpg') ?>">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>




<!-- seccion dividida, de video y facebook -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 seccion">
            <h2 class="wow fadeInUp">Videos</h2>
            <div class="embed-responsive embed-responsive-4by3 wow fadeIn">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/rX2Ln4GqfC0?controls=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 seccion text-center">
            <h2 class="wow fadeInUp">Redes Sociales</h2>
            <!-- https://www.facebook.com/pro.olivo/ -->
            <div class="fb-page wow fadeIn" data-href="https://www.facebook.com/pro.olivo/" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pro.olivo/"><a href="https://www.facebook.com/pro.olivo/">PRO OLIVO</a></blockquote></div></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>