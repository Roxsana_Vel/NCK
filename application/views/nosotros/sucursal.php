<!-- slogan -->
<div class="slogan" style="
    background: url('Sgc/uploads/banners/<?= $sloganImg ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center;
">
    <div class="slogan__frase">
        <h3 class="wow bounceIn">SUCURSALES</h3>
    </div>
</div>

<!-- seccion de mision y vision -->
<div class="info-container-1  ">
					
	<div class="container">
		<div class="row about-us">
		<h2>SUCURSALES<i></i></h2>
			<div class="col-xs-12">

				<img  class="img-responsive" src="<?= base_url('dist/img/img/sucursales.jpg') ?>" alt="">			    
			</div>
			
							    
			</div>
							
			</div>

		</div>
	

<script>
    $(document).ready(function() {
        /**
         * sliders, con owl.carousel.js
         */

        /** slider principal */
        $('#sliderPrincipal').owlCarousel({
            loop            : true,
            autoplayTimeout : 4000,
            autoplay        : true,
            margin          : 0,
            nav             : false,
            items           : 1,
            autoHeight      : true,
            animateIn       : 'bounceInDown',
            animateOut      : 'fadeOutDown',
            smartSpeed      : 450,
        });
    });
</script>