<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Makes_Model extends CI_Model {

		

	function __construct()

	{

		parent::__construct();

	}



	function getMakes($order = NULL)

	{

		try {

			$this->db->select('

				marca.MAR_ID,

				marca.MAR_nombre,

				marca.MAR_logo,

				marca.MAR_orden,

				marca.MAR_banner

			');
			
			$this->db->from('marca');

			if (is_null($order)) {

				$this->db->order_by('marca.MAR_orden','ASC');	

			} else {

				$this->db->order_by($order);

			}

			
			$data = $this->db->get();



			if ($data->num_rows() > 0) {

				return $data;

			} else {

				return FALSE;

			}

		} catch (Exception $e) {

			echo "ERROR: ".$e->getMessage();

		}

	}
}
?>