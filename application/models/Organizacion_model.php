<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizacion_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getOrganizacion($orgId = 1)
    {
        $this->db->select('
            ORG_ID,
            ORG_empresa,
            ORG_contacto,
            ORG_licencia,
            ORG_email,
            ORG_direccion1,
            ORG_telefono,
            ORG_logo,
        ');
        $this->db->from('organizacion');
        $this->db->where('ORG_ID', $orgId);

        return $this->db->get()->row_array();
    }


}