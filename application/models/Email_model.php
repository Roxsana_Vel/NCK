<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Email_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        /** helpers */
        $this->load->helper('date');
        $this->load->helper('fecha');

        /** @var array configuracion global para el envío de emails */
        $this->email_config = array(
            'protocol'  => 'mail',
            'mailpath'  => '/usr/sbin/sendmail',
            'useragent' => 'proolivo.com',
            'charset'   => 'utf-8',
            'mailtype'  => 'html',
            'wordwrap'  => true,

            // 'protocol'    => 'smtp',
            // 'smtp_host'   => 'smtp.googlemail.com',
            // 'smtp_user'   => 'alvaro.sacari@gmail.com',
            // 'smtp_pass'   => '',
            // 'smtp_port'   => '465',
            // 'smtp_crypto' => 'ssl',
            // 'mailtype'    => 'html',
            // 'wordwrap'    => TRUE,
            // 'charset'     => 'utf-8',
        );

        /** librerias */
        $this->load->library('email', $this->email_config);
    }

    /**
     * enviar un correo electronico
     * @param  string $email_destino    email del receptor
     *
     * @param  string $dataMensaje      array(
     *                                      'emailEmisor'
     *                                      'asunto|titulo mensaje'
     *                                  )
     *                                  tambipen puede tener otroas datos que no son obligatorios, pero necesarios para la vista correspondiente
     *
     * @param  string $templateMensaje  ruta del template para el mensaje
     *                                  ejm: 'email/contacto'
     *
     * @return [type]                [description]
     */
    public function enviar_correo($emailDestino, $dataMensaje, $templateMensaje)
    {
        $this->load->library('MyPHPMailer');

        $mail = new PHPMailer();
        $mail->isSendmail();
        $mail->isHTML(true);
        $mail->setFrom($dataMensaje['emailEmisor']);

        $mail->Subject    = $dataMensaje['asunto'];
        $mail->CharSet    = 'UTF-8';
        $mail->Body       = $this->load->view($templateMensaje, $dataMensaje, true);

        $mail->addAddress($emailDestino);

        if ( ! $mail->send()) {
            log_message('error', '[Regap ' . __FILE__ . '] Error PHPMailer : ' . $mail->ErrorInfo);
            return false;
        }

        return true;
    }

}
