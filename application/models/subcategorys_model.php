<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subcategorys_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getSubcategorys()
	{
		try {
			$this->db->select('
				subcategoria.SCAT_ID,
				subcategoria.SCAT_nombre,
				subcategoria.SCAT_estado,
				subcategoria.SCAT_orden,
				subcategoria.CAT_ID
				');
				#producto.PRO_precio,
			$this->db->from('subcategoria');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getSubcategorysOrder($order = NULL){
		try {
			$this->db->select('
				subcategoria.SCAT_ID,
				subcategoria.SCAT_nombre,
				subcategoria.SCAT_estado,
				subcategoria.SCAT_orden,
				subcategoria.CAT_ID,
				categoria.CAT_nombre
			');
				#producto.PRO_precio,
			$this->db->from('subcategoria');
			$this->db->join('categoria', 'subcategoria.CAT_ID = categoria.CAT_ID');

			if (is_null($order)) {
				$this->db->order_by('SCAT_orden','asc');
			} else {
				$this->db->order_by($order);
			}
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	public function getOrdenNumId($cat_id){
		try {
			$this->db->select('
				subcategoria.SCAT_ID
			');
			$this->db->from('subcategoria');
			$this->db->where('CAT_ID', $cat_id);
			$filas = $this->db->get();
			$data = $filas->num_rows();
			return $data;
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getSubcategorysByCat($id_cat) {
		try {
			$this->db->select('
				subcategoria.SCAT_ID,
				subcategoria.SCAT_nombre,
				subcategoria.SCAT_estado,
				subcategoria.SCAT_orden
			');
				#producto.PRO_precio,
			$this->db->from('subcategoria');
			$this->db->where('subcategoria.CAT_ID', $id_cat);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getSubcategorysId($id_subcat) {
		try {
			$this->db->select('
				subcategoria.SCAT_ID,
				subcategoria.SCAT_nombre,
				subcategoria.SCAT_estado,
				subcategoria.SCAT_orden,
				subcategoria.CAT_ID
			');
				#producto.PRO_precio,
			$this->db->from('subcategoria');
			$this->db->where('subcategoria.SCAT_ID', $id_subcat);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function filterByCat($id_cat, $order){
		try {
			$this->db->select('
				subcategoria.SCAT_ID,
				subcategoria.SCAT_nombre,
				subcategoria.SCAT_estado,
				subcategoria.SCAT_orden,
				subcategoria.CAT_ID,
				categoria.CAT_nombre
			');
			$this->db->from('subcategoria');
			$this->db->join('categoria', 'subcategoria.CAT_ID = categoria.CAT_ID');
			$this->db->where('subcategoria.CAT_ID', $id_cat);
			$this->db->order_by($order);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function insertSubcategory($data)
	{
		$this->db->insert('subcategoria', $data);
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function updateSubcategoryId($id, $data)
	{
		$this->db->where('SCAT_ID', $id);
		$this->db->update('subcategoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function UpdateState($scat_id, $estado)
	{
		$this->db->where('SCAT_ID', $scat_id);
		$this->db->update('subcategoria', array( 'SCAT_estado' => $estado));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function getUpId($orden, $cat_id){ //  devuelve el id de la fila de arriba
		try {

	   		$this->db->select('SCAT_ID');
			$this->db->from('subcategoria');
			$this->db->where('CAT_ID', $cat_id);
			$this->db->where('SCAT_orden', $orden - 1);
			$fila = $this->db->get();
			if ($fila->num_rows() > 0) {
				$row = $fila->row_object();
				return $row->SCAT_ID;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getDowndId($orden, $cat_id){ // Almacena el id de la fila inferior
		try {
	   		$this->db->select('SCAT_ID');
			$this->db->from('subcategoria');
			$this->db->where('CAT_ID', $cat_id);
			$this->db->where('SCAT_orden', $orden + 1);
			$fila = $this->db->get();
			if ($fila->num_rows() > 0) {
				$row = $fila->row_object();
				return $row->SCAT_ID;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function UpdateOrderUp($id, $orden){ // Disminuye el orden actual en 1
		try {
			$data = array("SCAT_orden" => $orden - 1);

			$this->db->where('SCAT_ID', $id);
			$this->db->update('subcategoria', $data);
			$affected = $this->db->affected_rows();
			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderDown($id, $orden){ // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")
		try {
			$data = array("SCAT_orden" => $orden + 1);

			$this->db->where('SCAT_ID', $id);
			$this->db->update('subcategoria', $data);
			$affected = $this->db->affected_rows();

			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function UpdateOrderRowUp($orden, $id_fila_superior) { //actualiza el orden de la fila superior
		try {
	    	$data = array("SCAT_orden" => $orden);
			$this->db->where('SCAT_ID', $id_fila_superior);
			$this->db->update('subcategoria', $data);
			$affected = $this->db->affected_rows();
			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderRowDown($orden, $id_fila_inferior){ // Actualiza el orden de la fila inferior
		try {
	    	$data = array("SCAT_orden" => $orden);
			$this->db->where('SCAT_ID', $id_fila_inferior);
			$this->db->update('subcategoria', $data);
			$affected = $this->db->affected_rows();

			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function updateOrderBeforeDelete($scat_orden, $cat_id) {
		try {
			$this->db->query("UPDATE subcategoria SET SCAT_orden = SCAT_orden - 1 WHERE SCAT_orden > $scat_orden AND CAT_ID = $cat_id");
			$affected = $this->db->affected_rows();
			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function deleteSubcategoryId($id)
	{
		$this->db->where('SCAT_ID', $id);
		$this->db->delete('subcategoria');

		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>