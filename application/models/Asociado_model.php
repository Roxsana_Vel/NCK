<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asociado_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * obtener todos los asociados habilitados
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    public function getAll()
    {
        $arrayAsociados = $this->db->get_where('empresa_asociada', array(
            'ASOC_estado' => 'habilitado',
        ))->result();

        return $arrayAsociados;
    }

    public function filtro_por_empresa($asocId)
    {
        $arrayAsociados = $this->db->get_where('empresa_asociada', array(
            'ASOC_estado' => 'habilitado',
            'ASOC_id' => $asocId,
        ))->result();

        return $arrayAsociados;
    }

    public function filtro_por_marca($marcaNombre)
    {
        $this->db->select('*');
        $this->db->from('empresa_asociada');
        $this->db->where('ASOC_estado', 'habilitado');
        $this->db->like('ASOC_marca', $marcaNombre);

        $arrayAsociados = $this->db->get()->result();

        return $arrayAsociados;
    }


}