<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * obtener todos los banners (habilitados y deshabilitados)
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    public function getBanner($order = NULL)
    {
        try {
            $this->db->select('
                *
            ');
            $this->db->from('banners');
            $this->db->where('BAN_estado', 1);

            if ( ! is_null($order)) {
                $this->db->order_by($order);
            }

            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data->result();
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * obtener todos los banners de portada
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    public function getBannersPortada($order = NULL)
    {
        try {
            $this->db->from('banners');
            $this->db->where('BAN_estado', 1);
            $this->db->where('BAN_mostrar_portada', 1);

            if ( ! is_null($order)) {
                $this->db->order_by($order);
            }

            $data = $this->db->get();

            if ($data->num_rows() > 0) {
                return $data->result();
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getBannerId($id)
    {
        try {
            $this->db->select('
                *
            ');
            $this->db->where('BAN_ID', $id);

            $data = $this->db->get('banners');

            if ($data->num_rows() > 0) {
                return $data->row();
            } else {
                return FALSE;
            }
        } catch(Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

}