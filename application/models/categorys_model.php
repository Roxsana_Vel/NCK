<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorys_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getCategorys()
	{
		try {
			$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado');
			$this->db->from('categoria');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getCategoryOrder($order = NULL){
	try {
			$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado,
				CAT_orden');
			$this->db->from('categoria');
			if (is_null($order)) {
				$this->db->order_by('CAT_orden','asc');
			} else {
				$this->db->order_by($order);
			}
			$data = $this->db->get();

			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function updateOrderBeforeDelete($cat_orden) {
		try {
			$this->db->query("UPDATE categoria SET CAT_orden = CAT_orden - 1 WHERE CAT_orden > $cat_orden");
			$affected = $this->db->affected_rows();
			if ($affected > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getUpId($cat_orden){ //  devuelve el id de la fila de arriba
		try {

		$orden_fila_superior = $cat_orden - 1;
   		$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado,
				CAT_orden');
			$this->db->from('categoria');
			$this->db->where('CAT_orden', $orden_fila_superior);
			$fila_superior = $this->db->get();
			$row = $fila_superior->row();
			$id_fila_superior = $row->CAT_ID;

		if ($fila_superior->num_rows() > 0) {
			return $id_fila_superior;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
		function getDowndId($cat_orden){ // Almacena el id de la fila inferior
		try {

		$orden_fila_superior = $cat_orden + 1;
   		$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado,
				CAT_orden');
			$this->db->from('categoria');
			$this->db->where('CAT_orden', $orden_fila_superior);
			$fila_superior = $this->db->get();
			$row = $fila_superior->row();
			$id_fila_superior = $row->CAT_ID;

		if ($fila_superior->num_rows() > 0) {
			return $id_fila_superior;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderUp($cat_id, $cat_orden){ // Disminuye el orden actual en 1
		try {

		$new_order_row = $cat_orden - 1;
   		$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado,
				CAT_orden');
		$this->db->from('categoria');
		$this->db->where('CAT_ID', $cat_id);
		$data = array("CAT_orden" => $new_order_row);
		$this->db->update('categoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderDown($cat_id, $cat_orden){ // Incrementa el orden actual en 1 de la fila la cual será bajada ("Ejemplo de la posicion 6 a la 7")
		try {

		$new_order_row = $cat_orden + 1;
   		$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado,
				CAT_orden');
		$this->db->from('categoria');
		$this->db->where('CAT_ID', $cat_id);
		$data = array("CAT_orden" => $new_order_row);
		$this->db->update('categoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderRowUp($cat_orden, $id_fila_superior){ //actualiza el orden de la fila superior
		try {

		$new_order_row_up = $cat_orden;
    	$data = array("CAT_orden" => $new_order_row_up);
		$this->db->where('CAT_ID', $id_fila_superior);
		$this->db->update('categoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function UpdateOrderRowDown($cat_orden, $id_fila_inferior){ // Actualiza el orden de la fila inferior
		try {

		$new_order_row_down = $cat_orden;
    	$data = array("CAT_orden" => $new_order_row_down);
		$this->db->where('CAT_ID', $id_fila_inferior);
		$this->db->update('categoria', $data);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}

	}
	function getCategorysMenu($segmento)
	{
		try {
			$this->db->select('
				CAT_ID,
				CAT_nombre,
				CAT_estado');
			$this->db->from('categoria');
			$this->db->where('categoria.CAT_estado = 1 AND categoria.CAT_ID = '.$segmento.' OR categoria.CAT_ID = 9');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getCategorysId($id)
	{
		try {
			$this->db->where("CAT_ID", $id);
			$data = $this->db->get('categoria');
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch(Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function insertCategory($data)
	{
		$this->db->insert('categoria', $data);
		$affected_rows = $this->db->affected_rows();
		if ($affected_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function updateCategoryId($id, $values)
	{
		$this->db->where('CAT_ID', $id);
		$this->db->update('categoria', $values);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function deleteCategoryId($id)
	{
		$this->db->where('CAT_ID', $id);
		$this->db->delete('categoria');

		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	function UpdateState($cat_id, $estado)
	{
		$this->db->where('CAT_ID', $cat_id);
		$this->db->update('categoria', array( 'CAT_estado' => $estado));
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>