<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Articulo_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->select('*');
        $this->db->where('EVE_estado', 1);
        $this->db->where('EVE_visibilidad', 'publico');
        $this->db->order_by('EVE_fecha', 'desc');
        $novedades = $this->db->get('evento')->result();

        return (sizeof($novedades) == 0) ? array() : $novedades;
    }

    public function getAllNovedades()
    {
        $this->db->select('*');
        $this->db->where('EVE_tipo', 'novedad');
        $this->db->where('EVE_estado', 1);
        $this->db->where('EVE_visibilidad', 'publico');
        $this->db->order_by('EVE_fecha', 'desc');
        $novedades = $this->db->get('evento')->result();

        return (sizeof($novedades) == 0) ? array() : $novedades;
    }

    public function getAllProyectos()
    {
        $this->db->select('*');
        $this->db->where('EVE_tipo', 'proyecto');
        $this->db->where('EVE_estado', 1);
        $this->db->where('EVE_visibilidad', 'publico');
        $this->db->order_by('EVE_fecha', 'desc');
        $proyecto = $this->db->get('evento')->result();

        return (sizeof($proyecto) == 0) ? array() : $proyecto;
    }

    /**
     * obtener las novedades
     * son los 3 ultimos artiuclos regitrados
     *
     * @return [type] [description]
     */
    public function get_novedades()
    {
        $this->db->select('*');
        $this->db->where('EVE_estado', 1);
        $this->db->where('EVE_visibilidad', 'publico');
        $this->db->where('EVE_tipo', 'articulo');
        $this->db->order_by('EVE_fecha', 'desc');
        $novedades = $this->db->get('evento', 3)->result();

        return (sizeof($novedades) == 0) ? array() : $novedades;
    }

    public function get_3_eventos()
    {
        $this->db->select('*');
        $this->db->where('EVE_estado', 1);
        $this->db->where('EVE_visibilidad', 'publico');
        $this->db->where('EVE_tipo', 'evento');
        $this->db->order_by('EVE_fecha', 'desc');
        $eventos = $this->db->get('evento', 3)->result();

        return (sizeof($eventos) == 0) ? array() : $eventos;
    }

    /**
     * obtener el detalle de un articulo por su id
     * @param  [type] $noticiaId [description]
     * @return [type]            [description]
     */
    public function getArticuloId($noticiaId)
    {
        $noticiaDetalle = $this->db->get_where('evento', array('EVE_ID' => $noticiaId))->row();

        return $noticiaDetalle;
    }

}
