<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function getProducts()
	{
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID,
				categoria.CAT_ID,
				categoria.CAT_nombre
				');
				#producto.PRO_precio,
			$this->db->from('producto');
			$this->db->join('categoria', 'producto.CAT_ID = categoria.CAT_ID');
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
	function getFilterProdByCat($id_cat){
		try {
			$this->db->select('
				producto.PRO_ID,
				producto.PRO_nombre,
				producto.PRO_descripcion,
				producto.PRO_destacado,
				producto.PRO_estado,
				producto.PRO_orden,
				producto.PRO_stock,
				producto.CAT_ID
				');
				#producto.PRO_precio,
			$this->db->from('producto');
			$this->db->where('CAT_ID', $id_cat);
			$data = $this->db->get();
			if ($data->num_rows() > 0) {
				return $data;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			echo "ERROR: ".$e->getMessage();
		}
	}
}

?>