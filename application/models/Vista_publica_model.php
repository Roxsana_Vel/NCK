<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vista_publica_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getVistaPublicaImg($vipNombreRuta)
    {
        return $this->db->get_where('vistas_publicas', array('VIP_nombre_ruta' => $vipNombreRuta))->row('VIP_img_slogan');
    }


}